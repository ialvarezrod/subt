
	function placas(){

	var window,vista,ImgPerfil,btnGuardar,txtPlaca,btnCancelar;

	autoUpdateReq1 = Titanium.Network.createHTTPClient();
	 
	
	window = Ti.UI.createWindow({theme:'Theme.AppCompat.Translucent.NoTitleBar',backgroundColor :'transparent',opacity: 2});
	window .orientationModes = [Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT];

	vista=Ti.UI.createView({left:'5%',right:'5%',top:'30%',bottom:'30%',backgroundColor:'white'});
	texto= Ti.UI.createLabel({ center:0,width :'auto', height : 'auto', top : '3%' ,text : 'Escriba una placa',color:'black',font  : { fontSize : '25%'}});
	ImgPerfil= Ti.UI.createImageView({width:'21%',height:'26.5%',borderRadius:50,top:'21.5%',center:0,image:"http://subtmx.com/images/"+Ti.App.Properties.getString('fotografia'),zIndex:3});
	txtPlaca= Ti.UI.createTextField({hintText:'Escriba su placa aqui',top:'50%',center:0,width:'80%',heigth:'auto',color:'black',hintTextColor:'gray'});
    btnGuardar = Ti.UI.createButton({title:('Aceptar'),bottom:'5%',center:0,width:'auto',heigth:'auto',textAlign:'center',backgroundColor:'#0c83c7',color:'white'});
  	  
	vista.add(texto);
    vista.add(ImgPerfil);
    vista.add(txtPlaca);
    vista.add(btnGuardar);
  
    window.add(vista);
    
     window.addEventListener('android:back', function(){
     var confirmClear = Titanium.UI.createAlertDialog({
    			message:'Cerrar sesión', 
            buttonNames: ['aceptar','cancelar']
        });
        //confirmClear.show();
        confirmClear.addEventListener('click',function(e) {
            if (e.index === 0) {
            	Titanium.App.Properties.removeProperty('id_chofer');
				Titanium.App.Properties.removeProperty('usuario');
				Titanium.App.Properties.removeProperty('email');
				Titanium.App.Properties.removeProperty('telefono');
				Titanium.App.Properties.removeProperty('rfc');
				Titanium.App.Properties.removeProperty('razon');
				Titanium.App.Properties.removeProperty('disponible');
				Titanium.App.Properties.removeProperty('Cplaca');
				inicio = require('/interface/inicio');
				new inicio().open({modal:true});
				window.close();
			}
        });	
    
   
    });
    
    
	window.addEventListener('android:back', function(e){
		
	});	
   
    var alertaC = Ti.UI.createAlertDialog({
    cancel: 1,
    buttonNames: ['Aceptar'],
    message: 'Mantén tu celular con batería al 100%, no olvides traer siempre un cargador para auto, verifica que tengas toda la documentación en la guantera, RECUERDA:  Tu presentación e imagen es lo mas importante para prestar el servicio, los usuarios aprecian el buen estado de las unidades',
    title: 'UN CHOFER PRECAVIDO, VALE POR DOS!'
  	});
  	alertaC.addEventListener('click', function(e){
  		window.close();
  	});
  	
	btnGuardar.addEventListener('click', function(e){
			var pla;
		if(txtPlaca.value==''){
			pla =Ti.App.Properties.getString('placa');
		}else{
			pla=txtPlaca.value;
		}
		autoUpdateReq1.open("POST","http://subtmx.com/webservices/updateauto.php");
                             params = {
                            id_chofer:Ti.App.Properties.getInt('id_chofer'),
                			placa:pla,
                    		status_login:1,
                        	status_ocupado:0,
                        	status_solicitud:0,
                        	status_disponibilidad:0,
                             };
                            autoUpdateReq1.send(params);
                               
                               
                               
                               
          autoUpdateReq1.onload = function(){
            json = this.responseText;
            response = JSON.parse(json);
            if (response.logged == true){
            	Ti.App.Properties.setString('placa',response.placa);
            	Ti.App.Properties.setInt("tipo_auto", response.tipo_servicio);
            	alertDialog = Titanium.UI.createAlertDialog({title: 'Alert',message: response.message,buttonNames: ['OK']});
    			alertDialog.show();
                alertDialog.addEventListener('click',function(e){
                Ti.App.Properties.setInt("Cplaca",1);
                	alertaC.show();
                //	mapa= require('/interface/mapa');
            	//new mapa().open({modal:true});
                  	
                });
            }else{
                    alert(response.message);
             }
    	};

	});
	

	return window;
	};
	module.exports = placas;