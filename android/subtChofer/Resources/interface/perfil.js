function registro(id_chofer){
	
	var window,header,iconMenu,iconEdit,titulo,ImgPerfil,logo,fondoP,v1,v2,v3,v4,v5,v6,v7,
	txtUser,lblUSer,txtEmail,lblEmail,txtRfc,lblRfc,txtRS,lblRS,txtFace,txtClose,txtTel,lblTel;
	
	var id_chofer=id_chofer;
		window = Ti.UI.createWindow({theme:'Theme.AppCompat.Translucent.NoTitleBar',backgroundColor:'white',exitOnClose : true});
		window .orientationModes = [Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT];
		// vista central
		centerView =Ti.UI.createView({height: 'auto',right:0,top: 0,width:'100%',zIndex:1});
	    header = Ti.UI.createView({backgroundImage: '/images/barra.jpg',height: '10%',right:0,top: 0,width:'100%',zIndex: 10});
		iconMenu = Ti.UI.createButton({backgroundImage:'/images/menuicon.png', left:'3%',zIndex:2,height:'42%',width:'8.5%'});
		iconEdit = Ti.UI.createButton({	backgroundImage:'/images/editarP.png', left:'88%',zIndex:2,height:'53%',width:'8%',visible:true});
		iconEdit2 = Ti.UI.createButton({	backgroundImage:'/images/palomita.png', left:'88%',zIndex:2,height:'53%',width:'8%',visible:false,enabled:false});
	
	   	titulo=Ti.UI.createLabel({text:'Mi Perfil', color:'#232323',center:0,bottom:'25%',zIndex:2,font:{fontSize:'15.5%'}});
	   	ImgPerfil= Ti.UI.createImageView({width:'29%',height:'18.5%',borderRadius:50,top:'21.5%',center:0,image:"http://iego.com.mx/images/"+Ti.App.Properties.getString('fotografia'),zIndex:5});
		
	  
	   fondoP = Ti.UI.createView({backgroundImage: '/images/bgPerfil.jpg',height: '20%',left:0,top:'10%',width:'auto',zIndex: 4});
	    v1 = Ti.UI.createView({backgroundColor: '#ffffff',height: '25%',left:0,top:'30%',width:'auto',zIndex:3,borderColor:'#eeeeee'});
	    v2 = Ti.UI.createView({backgroundColor: '#ffffff',height: '9%',left:0,top:'55%',width:'auto',zIndex: 2});
	    v3 = Ti.UI.createView({backgroundColor: '#ffffff',height: '9%',left:0,top:'64%',width:'auto',zIndex: 3,borderColor:'#eeeeee'});
	    v4 = Ti.UI.createView({backgroundColor: '#ffffff',height: '9%',left:0,top:'73%',width:'auto',zIndex: 2});
	    v5 = Ti.UI.createView({backgroundColor: '#ffffff',height: '9%',left:0,top:'82%',width:'auto',zIndex: 3,borderColor:'#eeeeee'});
	    v6 = Ti.UI.createView({backgroundColor: '#ffffff',height: '9%',left:0,top:'91%',width:'auto',zIndex: 2});
	   lblUSer2  = Ti.UI.createTextField({value:Ti.App.Properties.getString('usuario'),color:'#151431',left:'30%',top:'10%',zIndex:4,font:{fontSize:'15.5%'},editable:false,focus:false,backgroundFocusedColor:'transparent'});
	   ImgPerfil2= Ti.UI.createImageView({width:'20%',height:'9%',borderRadius:50,top:'8%',left:'5%',image:"http://iego.com.mx/images/"+Ti.App.Properties.getString('fotografia'),zIndex:3});
   	   txtUser = Ti.UI.createLabel({text:'Usuario:', color:'#aeaec1',left:'5%',bottom:'12%',zIndex:4,font:{fontSize:'15.5%'}});
	   lblUSer  = Ti.UI.createTextField({value:Ti.App.Properties.getString('usuario'),color:'#151431',right:'3%',bottom:'11%',zIndex:4,font:{fontSize:'15.5%'},editable:false,focus:false});
	   txtEmail = Ti.UI.createLabel({text:'Email:', color:'#aeaec1',left:'5%',bottom:'25%',zIndex:4,font:{fontSize:'15.5%'}});
	   lblEmail = Ti.UI.createTextField({value:Ti.App.Properties.getString('email'),color:'#151431',right:'3%',bottom:'24%',zIndex:4,font:{fontSize:'15.5%'},editable:false,focus:false});
	   txtTel = Ti.UI.createLabel({text:'Telefono:', color:'#aeaec1',left:'5%',bottom:'25%',zIndex:4,font:{fontSize:'15.5%'}});
	   lblTel = Ti.UI.createTextField({value:Ti.App.Properties.getString('telefono'), color:'#151431',right:'3%',bottom:'24%',zIndex:4,font:{fontSize:'15.5%'},editable:false,focus:false});
	   txtClose = Ti.UI.createLabel({text:'Cerrar Sesión', color:'#aeaec1',left:'5%',bottom:'25%',zIndex:4,font:{fontSize:'15.5%'}});
	   imgClose = Ti.UI.createButton({	backgroundImage:'/images/closeS.png', right:'3%',bottom:'25%',zIndex:4,height:'50%',width:'10%'});
	   updateClienteReq = Titanium.Network.createHTTPClient();
	  	autoUpdateReq= Titanium.Network.createHTTPClient();
	    // vista lateral
	   menuView = Ti.UI.createView({backgroundImage:'/images/bgMenu.jpg',height: 'auto',left: '-75%',  top:0,width: '75%',zIndex: 9,   _toggle: false});
       logoLeft = Titanium.UI.createImageView({image:'/images/logo.png',width:'80%',height:'auto',zIndex:2,center:0,top:'13%',opacity:1});
       lblHome = Titanium.UI.createLabel({color:'black',text:'Home',top:'52%',left:'13%'});
       lblMisPedidos = Titanium.UI.createLabel({color:'black',text:'Historial',top:'60%',left:'13%'});
       lblMiPerfil = Titanium.UI.createLabel({color:'#0066cc',text:'Mi Perfil',top:'68%',left:'13%'});
    	
	  chkTerminos = Ti.UI.createSwitch({value:false,top:'76%',onTintColor:'#1ED760',tintColor:'#D90019',left:'45%'});
		disponible = Ti.UI.createLabel({color:'white',text:'Disponible',top:'76%',left:'13%'});
	  webview = Ti.UI.createWebView({url : '/firebase/index.html',width:0,height:0,top:0,left:0,visible:false,cacheMode:Titanium.UI.Android.WEBVIEW_LOAD_NO_CACHE});
      webview.addEventListener('load', function() {
		Ti.Geolocation.getCurrentPosition(function(e) {
			Ti.App.fireEvent('firebase:init', {
				latitude : e.coords.latitude,
				longitude : e.coords.longitude,
				id_chofer: id_chofer
			});
		});
	});
	window.add(webview);
	  	  var countDown =  function(m,s,fn_tick,fn_end){
    		return {
     			total_sec:m*60+s,timer:this.timer,
                	set: function(m,s) {
                		this.total_sec = parseInt(m)*60+parseInt(s);this.time = {m:m,s:s};
                		return this;
            		},
                	start: function() {
                		var self1 = this;
                	this.timer = setInterval( function() {
                		if (self1.total_sec) {
                    		self1.total_sec--;
                    		self1.time = { m : parseInt(self1.total_sec/60), s: (self1.total_sec%60) };
                    		fn_tick();
                		} else {
                    		self1.stop();
                    		fn_end();
                		}
                	}, 1000 );
            				return this;
            		},
            		stop: function() {
            				clearInterval(this.timer);
            				this.time = {m:0,s:0};
            				this.total_sec = 0;
            				return this;
            		}
       	 	};
    		}; 
	  	   
	  	var progressIndicator = Ti.UI.Android.createProgressIndicator({
  				message: 'Cerrando Sesion...',
  				location: Ti.UI.Android.PROGRESS_INDICATOR_DIALOG,
  				type: Ti.UI.Android.PROGRESS_INDICATOR_STATUS_BAR,
  				cancelable: true,
  				min: 0,
  				max: 8
			});
    		
    		var my_timer = new countDown(00,08,function() {
             	if (my_timer.time.s==0){
             	var webViewCacheDir = Ti.Filesystem.getFile(Ti.Filesystem.applicationCacheDirectory, "webviewCache");
					webViewCacheDir.deleteDirectory(true);
				  // inicio = require('/interface/inicio');
				  // new inicio().open({modal:true});
				  // 
				window.close();
   					var viewChildren = window.children;
						Ti.API.info(viewChildren);
						for (var i=0; i < viewChildren.length; i++) {
							var child = viewChildren[i];
						window.remove(child);	
					}	
                  	window.removeAllChildren();
                   	window.remove(webview);
				 	webview.release();
				  	webview = null;
				 	Ti.API.info('webviewC: ' + webview);
				 	window = null;
				 	Ti.API.info('windowC: ' + window);
             	
					var activity = Titanium.Android.currentActivity;
 					activity.finish();
             
               }else if(my_timer.time.s==04){
           	  }
        	},function() {});
	
	  	   
	   imgClose.addEventListener('click',function(){
	   	
				Ti.App.fireEvent('fbController:removeLogin',{
				id:id_chofer
				});
				progressIndicator.show();
	   			my_timer.start();
	   			
				autoUpdateReq.open("POST","http://subtmx.com/webservices/updateauto.php");
                		params = {
                			id_chofer:Ti.App.Properties.getInt('id_chofer'),
                			placa:Ti.App.Properties.getString('placa'),
                    		status_login:0,
                        	status_ocupado:0,
                        	status_solicitud:0,
                        	status_disponibilidad:0
                        	
                    };
            autoUpdateReq.send(params);	
			Titanium.App.Properties.removeProperty('id_chofer');
			Titanium.App.Properties.removeProperty('id_chofer');
			Titanium.App.Properties.removeProperty('usuario');
			Titanium.App.Properties.removeProperty('email');
			Titanium.App.Properties.removeProperty('telefono');
			Titanium.App.Properties.removeProperty('rfc');
			Titanium.App.Properties.removeProperty('razon');
			Titanium.App.Properties.removeProperty('disponible');
			Titanium.App.Properties.removeProperty('Cplaca');
			Titanium.App.Properties.removeProperty('solicitudAceptada');
			Titanium.App.Properties.removeProperty('yallego');
			Titanium.App.Properties.removeProperty('costominutos');
		    Titanium.App.Properties.removeProperty('iniciado');
	});	
	   
	   
	   	lblHome.addEventListener('click', function(){
	   				var webViewCacheDir = Ti.Filesystem.getFile(Ti.Filesystem.applicationCacheDirectory, "webviewCache");
					webViewCacheDir.deleteDirectory(true);
				 	home= require('/interface/mapa');
					new home().open({modal:true});
				  window.close();
   					var viewChildren = window.children;
						Ti.API.info(viewChildren);
						for (var i=0; i < viewChildren.length; i++) {
							var child = viewChildren[i];
						window.remove(child);	
					}	
                  	window.removeAllChildren();
                   	window.remove(webview);
				 	webview.release();
				  	webview = null;
				 	Ti.API.info('webviewC: ' + webview);
				 	window = null;
				 	Ti.API.info('windowC: ' + window);
					
				
				
		});	
		
	   	logoLeft.addEventListener('click', function(){
			clickMenu(null);
		});
		
	   	lblMisPedidos.addEventListener('click',function(){
		if (Ti.App.Properties.hasProperty('id_chofer')) {
			var webViewCacheDir = Ti.Filesystem.getFile(Ti.Filesystem.applicationCacheDirectory, "webviewCache");
					webViewCacheDir.deleteDirectory(true);
				 	pedidos = require('/interface/pedidos');
					new pedidos().open({modal:true});
				  	window.close();
   					var viewChildren = window.children;
						Ti.API.info(viewChildren);
						for (var i=0; i < viewChildren.length; i++) {
							var child = viewChildren[i];
						window.remove(child);	
					}	
                  	window.removeAllChildren();
                   	window.remove(webview);
				 	webview.release();
				  	webview = null;
				 	Ti.API.info('webviewC: ' + webview);
				 	window = null;
				 	Ti.API.info('windowC: ' + window);
					
				
			
		}else{
			alert('Solo usuarios registrados');
		}
	});
	
		
	centerView.addEventListener('swipe',function(event){
	if(event.direction=='left' || event.direction=='right' ){
		clickMenu(null);
	}	});
    iconMenu.addEventListener('click',function(event){
	clickMenu(null);
	});
	lblMiPerfil.addEventListener('click',function(event){
	clickMenu(null);
	});
	function clickMenu(direction){
	 if(menuView._toggle === false && (direction==null || direction=='right')){
		centerView.animate({
				right:'-75%' ,
				duration: 100,
				curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
			});
			menuView.animate({
				left: 0,
				duration: 100,
				curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
			});
		menuView._toggle=true;
		}else if(direction==null || direction=='left'){
			centerView.animate({
				right:0 ,
				duration: 100,
				curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
			});
			menuView.animate({
				left: '-75%',
				duration: 100,
				curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
			});		menuView._toggle=false;
		};}
                           
	    header.add(iconMenu);
	   //	header.add(iconEdit);
	   	//header.add(iconEdit2);
	  	 header.add(titulo);
	  	 centerView.add(header);
	  	 centerView.add(v1);  centerView.add(v2);
	  	 centerView.add(v3);  centerView.add(v4);
	  	 centerView.add(v5);  centerView.add(v6);
	  	  v1.add(txtUser);  v1.add(lblUSer);
	  	 v2.add(txtEmail);      v2.add(lblEmail);
	     v3.add(txtTel); v3.add(lblTel);
	   
	  	 v6.add(txtClose);   v6.add(imgClose);   
	  	 centerView.add(ImgPerfil); 
	     centerView.add(fondoP); 
	   
	   
		menuView.add(logoLeft);
		menuView.add(lblHome);
		menuView.add(lblMisPedidos);
		menuView.add(lblMiPerfil);
	 	menuView.add(lblUSer2);
		menuView.add(ImgPerfil2);
		//menuView.add(chkTerminos);
	    window.add(menuView);
	   	window.add(centerView);
	   
	   	 return window;
};
module.exports = registro;
