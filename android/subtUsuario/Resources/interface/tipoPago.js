function tipoPago() {

	var window,
	    vista,
	    ImgPerfil,
	    btnGuardar,
	    txtPlaca,
	    btnCancelar,
	    billing=0;

	idconeckta = Titanium.Network.createHTTPClient();
	idPpal= Titanium.Network.createHTTPClient();
	idPpal2= Titanium.Network.createHTTPClient();
	window = Ti.UI.createWindow({
		theme : 'Theme.AppCompat.Translucent.NoTitleBar',
		backgroundColor : '#80727374',
		opacity : 2
	});
	window.orientationModes = [Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT];
	
	dialogPadis = Ti.UI.createAlertDialog({
		cancel : 1,
		buttonNames : ['Aceptar'],
		message : 'Por el momento este metodo de pago no esta disponible'
	});
	
	vista = Ti.UI.createView({
		width : '80%',
		height : Ti.UI.SIZE,
		layout : 'vertical',
		backgroundColor : 'white'
	});
	texto = Ti.UI.createLabel({
		top : 30,
		text : 'Seleccione un Tipo de pago',
		color : 'black',
		font : {
			fontSize : '15%'
		}
	});
	//ImgPerfil= Ti.UI.createImageView({width:'21%',height:'26.5%',borderRadius:50,top:'21.5%',center:0,image:"http://subtmx.com/images/"+Ti.App.Properties.getString('fotografia'),zIndex:3});
	
	
	btnPaypal = Ti.UI.createButton({
		top:10,
		title : ('Paypal'),
		center:0,
		width : Ti.UI.SIZE,
		heigth : Ti.UI.SIZE,
		textAlign : 'center',
		backgroundColor : '#0c83c7',
		color : 'white',borderRadius:6
	});
	btnConekta = Ti.UI.createButton({
		title : ('Tarjeta de credito/debito'),
		center : 0,
		width : Ti.UI.SIZE,
		heigth : Ti.UI.SIZE,
		textAlign : 'center',
		backgroundColor : '#0c83c7',
		color : 'white',
		top:10,borderRadius:6
	});
	vista2 = Ti.UI.createView({
		width : '100%',
		height : 20,
	
	});
	optsEditPagos = {
		cancel : 2,
		options : ['Editar', 'Mantener', 'Cancelar'],
		selectedIndex : 2,
		destructive : 0,
		title : 'Seleccione una opción'
	};
	dialogEditPagos = Ti.UI.createOptionDialog(optsEditPagos);
	
	dialogEditPagos.addEventListener('click', function(ed){
            if (ed.index === 0){
                if (Ti.App.Properties.hasProperty('paypal')) {
                    paypal= require('/interface/paypal');
                   	new paypal().open();
                   	Ti.App.mapa.close();
                   	window.close();
                   	
                }else if (Ti.App.Properties.hasProperty('coneckta')){
                    paypal= require('/interface/pago');
                   new paypal().open();
                   Ti.App.mapa.close();
                   window.close();
                }
            }else if (ed.index === 1){
                window.close();
            }
        });
	
	vista.add(texto);
	vista.add(btnPaypal);
	vista.add(btnConekta);
	vista.add(vista2);

	window.add(vista);

	window.addEventListener('android:back', function() {

	});

	btnConekta.addEventListener('click', function() {
		Titanium.App.Properties.removeProperty('paypal');
		if (Ti.App.Properties.hasProperty('coneckta')) {
			
			dialogEditPagos.show();
		} else {
	
			idconeckta.open("POST", "http://subtmx.com/webservices/conektausuario.php");
			params = {
				id_usuario : Ti.App.Properties.getInt("id_usuario")
			};
			idconeckta.send(params);
		}

	});
	

	btnPaypal.addEventListener('click', function() {
		Titanium.App.Properties.removeProperty('coneckta');
		//dialogPadis.show();

		if (Ti.App.Properties.hasProperty('paypal')) {
		idPpal2.open("POST","http://subtmx.com/webservices/bilingusuario.php");
		params = {
		id_usuario: Ti.App.Properties.getInt("id_usuario")
		};
		idPpal2.send(params);
		dialogEditPagos.show();
		
		}else{
	
		idPpal.open("POST","http://subtmx.com/webservices/bilingusuario.php");
		params = {
		id_usuario: Ti.App.Properties.getInt("id_usuario")
		};
		idPpal.send(params);
		}
		
	});

	idconeckta.onload = function() {
		json = this.responseText;
		response = JSON.parse(json);
		if (response.logged == true) {
		
			Ti.App.Properties.setString("coneckta", 'coneckta');
			window.close();
		} else {
		
			paypal = require('/interface/pago');
			new paypal().open();
			window.close();
		}
	};

idPpal2.onload = function(){
            json = this.responseText;
            response = JSON.parse(json);
            if (response.logged == true){
            	Ti.App.Properties.setString("id_biling2",response.id_biling);
            	if(Ti.App.Properties.getString("id_biling2")==''){
            		Ti.API.info(Ti.App.Properties.getString("id_biling2"));
            		billing=0;
   					Ti.App.Properties.setInt("biling",billing);
    				paypal= require('/interface/paypal');
        			new paypal().open();
        			window.close();
    			}else{
    				billing=1;
    				Ti.App.Properties.setString("paypal",'paypal');
    				Ti.App.Properties.setInt("biling",billing);
    				Ti.API.info(Ti.App.Properties.getString("id_biling2"));
    				
    		 	}
           }else{
           			
           }
  };



	idPpal.onload = function(){
            json = this.responseText;
            response = JSON.parse(json);
            if (response.logged == true){
            	Ti.App.Properties.setString("id_biling2",response.id_biling);
            	if(Ti.App.Properties.getString("id_biling2")==''){
            		Ti.API.info(Ti.App.Properties.getString("id_biling2"));
            		billing=0;
   					Ti.App.Properties.setInt("biling",billing);
    				paypal= require('/interface/paypal');
        			new paypal().open();
        			window.close();
    			}else{
    				billing=1;
    				Ti.App.Properties.setString("paypal",'paypal');
    				Ti.App.Properties.setInt("biling",billing);
    				Ti.API.info(Ti.App.Properties.getString("id_biling2"));
    				window.close();
    		 	}
           }else{
           			paypal= require('/interface/paypal');
        			new paypal().open();
        			window.close();
           }
    	};
	return window;
};
module.exports = tipoPago; 