function ayuda(){
	var window = Titanium.UI.createWindow({theme:'Theme.AppCompat.Translucent.NoTitleBar',backgroundColor:'black'});
  	window .orientationModes = [Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT];
  	var name=Ti.App.Properties.getString('nombre');
  	var lastname=Ti.App.Properties.getString("apellido");
  	var usuario= Ti.App.Properties.getInt("id_usuario");
  	Ti.API.info(name);
  	Ti.API.info(lastname);
  	Ti.API.info(usuario);
	
	var txtRegistrate=Ti.UI.createLabel({text:'Ayuda',font:{fontSize:'15%'},top:'30%',center:0,color:'black'});
	var header = Ti.UI.createView({backgroundColor: 'white',height: '10%',top: 0,width:'100%',zIndex: 10});
    var cerrar = Ti.UI.createButton({backgroundImage:'/images/CloseWindow.png',width:'12%',height:'55%',top:'20%',left:'5%'});
	var webview = Titanium.UI.createWebView({top:'10%',url:'http://subtmx.com/landing/faqs.html'});
   
   	if(Ti.App.Properties.getInt('ingles')==1){
		txtRegistrate.text='Help';
	}
   
     window.addEventListener('android:back', function(){
     });
     cerrar.addEventListener('click', function(){
     	window.close();
     });
    
  
     window.add(webview);
     header.add(txtRegistrate);
     header.add(cerrar);
     window.add(header);
		return window;
}
module.exports = ayuda;