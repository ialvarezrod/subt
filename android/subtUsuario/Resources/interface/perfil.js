function perfil(){
	var window,header,iconMenu,iconEdit,titulo,ImgPerfil,logo,fondoP,v1,v2,v3,v4,v5,v6,v7,
	txtUser,lblUSer,txtEmail,lblEmail,txtRfc,lblRfc,txtRS,lblRS,txtFace,txtClose,txtTel,lblTel;
	
	
	window = Ti.UI.createWindow({theme:'Theme.AppCompat.Translucent.NoTitleBar',backgroundColor:'white'});
	window .orientationModes = [Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT];
	
	// vista central
	centerView =Ti.UI.createView({height: 'auto',right:0,top: 0,width:'100%',zIndex:1});
	header = Ti.UI.createView({backgroundImage: '/images/barra.jpg',height: '10%',right:0,top: 0,width:'100%',zIndex: 10});
	iconMenu = Ti.UI.createButton({backgroundImage:'/images/menuicon.png', left:'3%',zIndex:2,height:'42%',width:'8.5%'});
	iconEdit = Ti.UI.createButton({	backgroundImage:'/images/editarP.png', left:'88%',zIndex:2,height:'53%',width:'8%',visible:true});
	iconEdit2 = Ti.UI.createButton({	backgroundImage:'/images/palomita.png', left:'88%',zIndex:2,height:'53%',width:'8%',visible:false,enabled:false});
	titulo=Ti.UI.createLabel({text:'Mi Perfil', color:'#232323',center:0,bottom:'25%',zIndex:2,font:{fontSize:'15.5%'}});
	
		fondoP = Ti.UI.createView({backgroundImage: '/images/bgPerfil.jpg',height: '20%',left:0,top:'10%',width:'auto',zIndex: 2});
	v1 = Ti.UI.createView({backgroundColor: '#ffffff',height: '25%',left:0,top:'30%',width:'auto',zIndex:3,borderColor:'#eeeeee'});
	v2 = Ti.UI.createView({backgroundColor: '#ffffff',height: '9%',left:0,top:'55%',width:'auto',zIndex: 2});
	v3 = Ti.UI.createView({backgroundColor: '#ffffff',height: '9%',left:0,top:'64%',width:'auto',zIndex: 3,borderColor:'#eeeeee'});
	v4 = Ti.UI.createView({backgroundColor: '#ffffff',height: '9%',left:0,top:'73%',width:'auto',zIndex: 2});
	v5 = Ti.UI.createView({backgroundColor: '#ffffff',height: '9%',left:0,top:'82%',width:'auto',zIndex: 3,borderColor:'#eeeeee'});
	v6 = Ti.UI.createView({backgroundColor: '#ffffff',height: '9%',left:0,top:'91%',width:'auto',zIndex: 2});
	
	txtUser = Ti.UI.createLabel({text:'Usuario:', color:'#aeaec1',left:'5%',bottom:'12%',zIndex:4,font:{fontSize:'15.5%'}});
	lblUSer  = Ti.UI.createTextField({value:Ti.App.Properties.getString('usuario'),color:'#151431',right:'3%',bottom:'12%',zIndex:4,font:{fontSize:'15.5%'},editable:false,focus:false});
	txtEmail = Ti.UI.createLabel({text:'Email:', color:'#aeaec1',left:'5%',bottom:'25%',zIndex:4,font:{fontSize:'15.5%'}});
	lblEmail = Ti.UI.createTextField({value:Ti.App.Properties.getString('email'),color:'#151431',right:'3%',bottom:'25%',zIndex:4,font:{fontSize:'15.5%'},editable:false,focus:false});
	txtTel = Ti.UI.createLabel({text:'Telefono:', color:'#aeaec1',left:'5%',bottom:'25%',zIndex:4,font:{fontSize:'15.5%'}});
	lblTel = Ti.UI.createTextField({value:Ti.App.Properties.getString('telefono'), color:'#151431',right:'3%',bottom:'25%',zIndex:4,font:{fontSize:'15.5%'},editable:false,focus:false});
	txtRfc = Ti.UI.createLabel({text:'Rfc:', color:'#aeaec1',left:'5%',bottom:'25%',zIndex:4,font:{fontSize:'15.5%'}});
	lblRfc = Ti.UI.createTextField({value:Ti.App.Properties.getString('rfc'), color:'#151431',right:'3%',bottom:'25%',zIndex:4,font:{fontSize:'15.5%'},editable:false,focus:false});
	txtRS = Ti.UI.createLabel({text:'Razon social:', color:'#aeaec1',left:'5%',bottom:'25%',zIndex:4,font:{fontSize:'15.5%'}});
	lblRS = Ti.UI.createTextField({value:Ti.App.Properties.getString('razon'), color:'#151431',right:'3%',bottom:'25%',zIndex:4,font:{fontSize:'15.5%'},editable:false,focus:false});
	txtClose = Ti.UI.createLabel({text:'Cerrar Sesión', color:'#aeaec1',left:'5%',bottom:'25%',zIndex:4,font:{fontSize:'15.5%'}});
	imgClose = Ti.UI.createButton({	backgroundImage:'/images/closeS.png', right:'3%',bottom:'25%',zIndex:4,height:'50%',width:'10%'});
	updateClienteReq = Titanium.Network.createHTTPClient();
	closeSesion=Titanium.Network.createHTTPClient();

	// vista lateral
	menuView = Ti.UI.createView({backgroundImage:'/images/bgMenu.jpg',height: 'auto',left: '-75%',	top:0,width: '75%',zIndex: 9,	_toggle: false});
	logoLeft = Titanium.UI.createImageView({image:'/images/logo.png',width:'55%',height:'auto',zIndex:2,left:'15%',top:'20%',opacity:1});
	lblHome = Titanium.UI.createLabel({color:'black',text:'Home',top:'52%',left:'13%'});
	lblMisPedidos = Titanium.UI.createLabel({color:'black',text:'Mis Pedidos',top:'60%',left:'13%'});
	lblMiPerfil = Titanium.UI.createLabel({color:'blue',text:'Mi Perfil',top:'68%',left:'13%'});
	lblPromociones = Titanium.UI.createLabel({color:'black',text:'Promociones',top:'76%',left:'13%'});
	lblConfiguración = Titanium.UI.createLabel({color:'black',text:'Configuración',top:'92%',left:'13%'}); 
	lblPagos = Titanium.UI.createLabel({color:'black',text:'Pagos',top:'84%',left:'13%'});	
	lblUSer2  = Ti.UI.createTextField({value:Ti.App.Properties.getString('usuario'),color:'#151431',left:'13%',top:'10%',zIndex:4,font:{fontSize:'17%'},editable:false,focus:false});
  
	 
	  ImgPerfil= Ti.UI.createImageView({width:120,height:120,borderRadius:60,top:'21.5%',center:0,zIndex:8 ,backgroundColor:'gray'});
 	  ImgPerfil.image=Ti.App.Properties.getString("image");
  // ImgPerfil.add(ImgPerfil2);  
 
  
	var dialog = Titanium.UI.createOptionDialog({
      title: 'Selecciona una opción',
 	  options: ['Camara','Galeria', 'Cancelar'],
      cancel:2
	});
 
 	if(Ti.App.Properties.getInt('ingles')==1){
	  		dialog.title='Choose an image source...';
	  		dialog.options=['Camera','Photo Gallery', 'Cancel'];
   		}
	  
	
	
dialog.addEventListener('click', function(e) {
  
    if(e.index == 0){
           Titanium.Media.showCamera({
             success:function(event)
            {var image = event.media; 
                 if(event.mediaType == Ti.Media.MEDIA_TYPE_PHOTO){
                 	Ti.App.Properties.setString("image", image.nativePath);
                 	 ImgPerfil.image=image;
                }
           },
            cancel:function(){
            },
            error:function(error){
                 var a = Titanium.UI.createAlertDialog({title:'Camera'});
                     if (error.code == Titanium.Media.NO_CAMERA){
                    	a.setMessage('Device does not have camera');
               		}else{
                    a.setMessage('Unexpected error: ' + error.code);
               		 }
 	               a.show();
            },
            allowImageEditing:true,
            saveToPhotoGallery:true
        });
   
    }else if(e.index == 1){
           Titanium.Media.openPhotoGallery({
            success:function(event){
                var image = event.media; 
                   if(event.mediaType == Ti.Media.MEDIA_TYPE_PHOTO){
                           Ti.App.Properties.setString("image", image.nativePath); 
                            ImgPerfil.image=image;
                   }   
            },
            cancel:function(){
            }
        });
    }else
    {
    }
});
 
ImgPerfil.addEventListener('click',function(){
	if (Ti.Media.hasCameraPermissions()) {
 
			} else { 
		    Ti.Media.requestCameraPermissions(function(e) {
             if (e.success === true) {
         
             } else { }
    	});
			}
	dialog.show();
});  
	  
	  
	  
	  if(Ti.App.Properties.getInt('ingles')==1){
	  		titulo.text='Profile';
	  		txtUser.text='User';
	  		txtTel.text='Telephone';
	  		txtRS.text='Business name:';
	  		txtClose.text='Sign off';
   			lblMisPedidos.text='My books';
   			lblMiPerfil.text='Profile';
   			lblPromociones.text='Promotions';
   			lblPagos.text='Payment';
   			lblConfiguración.text='Configuration';
   		}
	  
	id_tipoServ = Ti.UI.createLabel({text:'2'});
  	id_usuario = Ti.UI.createLabel({text:'3'});
   	id_chofer = Ti.UI.createLabel({text:'4'});
  	latitud_origen  = Ti.UI.createLabel({text:'18.0019071'}); 
    	longitud_origen  = Ti.UI.createLabel({text:'-92.958017'}); 
    	lugar_origen = Ti.UI.createLabel({text:'espejo2'}); 
   	latitud_destino  = Ti.UI.createLabel({text:' 17.9931986'}); 
    	longitud_destino  = Ti.UI.createLabel({text:' ,-92.9555348'}); 
    	opcion_sevicio  = Ti.UI.createLabel({text:' ,-92.9555348'}); 
    tarifa_inicial  = Ti.UI.createLabel({text:' 80.00'}); 
    id_tipo_pago = Ti.UI.createLabel({text:'2'}); 
     
	iconEdit.addEventListener('click',function(){
		if (Ti.App.Properties.hasProperty('id_usuario')) {
	  		iconEdit2.visible=true;
	  		iconEdit2.enabled=true;
	  		iconEdit.visible=false;
	  		iconEdit.enabled=false;
	  	    lblRfc.editable = true;
         	lblRS.editable = true;
         	lblTel.editable = true;
     	}else{
     		if(Ti.App.Properties.getInt('ingles')==1){
        				alert ( 'Only registered');
        	}else{
        				alert('Solo usuarios registrados');
        	}
     	}
	});
	  	  
	iconEdit2.addEventListener('click',function(){
		iconEdit.visible=true;
	  	iconEdit.enabled=true;
	  	iconEdit2.visible=false;
	    iconEdit2.enabled=false;
	    lblRfc.editable = false;
        lblRS.editable = false;
        lblTel.editable = false;
		updateClienteReq.open("POST","http://subtmx.com/webservices/updateclientes.php");
    	    		params = {
                  id_cliente:Ti.App.Properties.getInt('id_cliente'),
                  rfc: lblRfc.value,
                  razon_social: lblRS.value,
                  telefono:lblTel.value
                  
            };
       	updateClienteReq.send(params);
	});
	    
	updateClienteReq.onload = function(){
    		json = this.responseText;
        response = JSON.parse(json);
        if (response.logged == true){
        	Ti.App.Properties.setString("rfc", lblRfc.value);
            Ti.App.Properties.setString("razon", lblRS.value);
            Ti.App.Properties.setString('telefono',lblTel.value);
            alert(response.message);
        }else{
        		alert(response.message);
       	}
	};
	   
	imgClose.addEventListener('click',function(){
		closeSesion.open("POST","http://subtmx.com/webservices/statususuario.php");
                		params = {
                			id_usuario:Ti.App.Properties.getInt('id_usuario'),
        };
        closeSesion.send(params);
		Titanium.App.Properties.removeProperty('coneckta');
		Titanium.App.Properties.removeProperty('paypal');
		Titanium.App.Properties.removeProperty('id_usuario');
		Titanium.App.Properties.removeProperty('usuario');
		Titanium.App.Properties.removeProperty('email');
		Titanium.App.Properties.removeProperty('telefono');
		Titanium.App.Properties.removeProperty('rfc');
		Titanium.App.Properties.removeProperty('razon');
		Titanium.App.Properties.removeProperty('BonoActual');
		inicio = require('/interface/inicio');
		new inicio().open({modal:true});
		window.close();
	});
	if(Ti.App.Properties.getInt('ingles')==1){
		var optsConfig = {options:  ['Help','cancellation airport'], selectedIndex: 2,destructive:0,title: 'Select an option'};
	}else{
	    var optsConfig = {options:  ['Ayuda','Cancelación aeropuerto'], selectedIndex: 2,destructive:0,title: 'Selecciona una opción'};	
	}
	
	var dialogConfiguracion = Ti.UI.createOptionDialog(optsConfig);
		
		dialogConfiguracion.addEventListener('click', function(ed){
	   	if (ed.index === 0){
	   		ayuda = require('/interface/ayuda');
			new ayuda().open({modal:true});
		
	   	}else if (ed.index === 1){
	   		aeropuerto = require('/interface/aeropuerto');
			new aeropuerto().open({modal:true});
	   	}
	});
	 lblConfiguración.addEventListener('click',function(){
	if (Ti.App.Properties.hasProperty('id_usuario')) {
			dialogConfiguracion.show();
			}else{
			if(Ti.App.Properties.getInt('ingles')==1){
				alert('Only registered users');
			}else{
				alert('Solo usuarios registrados');
			}
	}
	});  
	lblHome.addEventListener('click', function(){
		home= require('/interface/mapa');
		new home().open({modal:true});
		window.close();		
	});	
	   	
	lblMisPedidos.addEventListener('click',function(){
		if (Ti.App.Properties.hasProperty('id_usuario')) {
			pedidos = require('/interface/pedidos');
			new pedidos().open({modal:true});
			window.close();
		}else{
			if(Ti.App.Properties.getInt('ingles')==1){
        				alert ( 'Only registered');
        			}else{
        				alert('Solo usuarios registrados');
        			}
		}
	});
	
	lblConfiguración.addEventListener('click',function(){
	if (Ti.App.Properties.hasProperty('id_usuario')) {
			dialogConfiguracion.show();
			}else{
			if(Ti.App.Properties.getInt('ingles')==1){
				alert('Only registered users');
			}else{
				alert('Solo usuarios registrados');
			}
	}
	});
	lblPromociones.addEventListener('click', function(){
		if (Ti.App.Properties.hasProperty('id_usuario')) {
			promo= require('/interface/promociones');
			new promo().open({modal:true});
			window.close();
		}else{
			if(Ti.App.Properties.getInt('ingles')==1){
        				alert ( 'Only registered');
        			}else{
        				alert('Solo usuarios registrados');
        			}
		}
	});	
	
	lblPagos.addEventListener('click',function(){
		if (Ti.App.Properties.hasProperty('id_usuario')) {
					paypal = require('/interface/tipoPago');
					new paypal().open();
			}else{
			if(Ti.App.Properties.getInt('ingles')==1){
				alert('Only registered users');
			}else{
				alert('Solo usuarios registrados');
			}
		}
	});
	
	lblMiPerfil.addEventListener('click', function(){
    		clickMenu(null);
    }); 
	   	
	centerView.addEventListener('swipe',function(event){
		if(event.direction=='left' || event.direction=='right' ){
			clickMenu(null);
		}
	});
    	
    	iconMenu.addEventListener('click',function(event){
		clickMenu(null);
	});
		
	function clickMenu(direction){
		if(menuView._toggle === false && (direction==null || direction=='right')){
				centerView.animate({right:'-75%',duration: 100,curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT});
				menuView.animate({left: 0,duration: 100,curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT});
				menuView._toggle=true;
		}else if(direction==null || direction=='left'){
				centerView.animate({right:0,duration: 100,	curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT	});
				menuView.animate({left: '-75%',duration: 100,curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT});
				menuView._toggle=false;
		};
	}
                           
	header.add(iconMenu);
	header.add(iconEdit);
	header.add(iconEdit2);
	header.add(titulo);
	centerView.add(header);
	centerView.add(v1);centerView.add(v2);
	centerView.add(v3);centerView.add(v4);
	centerView.add(v5);centerView.add(v6);
	v1.add(txtUser);v1.add(lblUSer);
	v2.add(txtEmail);v2.add(lblEmail);
	v3.add(txtTel);v3.add(lblTel);
	v4.add(txtRfc);v4.add(lblRfc);
	v5.add(txtRS);v5.add(lblRS);
	v6.add(txtClose);v6.add(imgClose);   
	window.add(ImgPerfil); 
	centerView.add(fondoP); 
	menuView.add(logoLeft);
	menuView.add(lblHome);
	menuView.add(lblMisPedidos);
	menuView.add(lblMiPerfil);
	menuView.add(lblPromociones);
	menuView.add(lblConfiguración);
	menuView.add(lblPagos);
	menuView.add(lblUSer2);
	window.add(menuView);
	window.add(centerView);
	return window;
};
module.exports = perfil;
