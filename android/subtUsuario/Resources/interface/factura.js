function factura(){
	var window,lblUser,lblpass,btnlogin,scrollView,txtUser,txtPass,lblreset,btnFacebook,loginReq,json,response,resetReq,mapa;
	
	window = Ti.UI.createWindow({backgroundImage:'/images/bgLogin.png'});
	window .orientationModes = [Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT];
	scrollView = Titanium.UI.createScrollView({contentWidth:'auto',contentHeight:'auto',top:0,showVerticalScrollIndicator:true,showHorizontalScrollIndicator:false});
	lblUser = Titanium.UI.createLabel({text:'RFC',center:0,top:'5%',color:'#49a6ff'});
	txtUser = Titanium.UI.createTextField({color:'#0066cc',opacity:0.3,hintText:'CRTP891004BR4',hintTextColor:'gray',center:0,top:'9%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,value:"",textAlign:'center',backgroundFocusedColor:'transparent',font:{fontSize:12}});
	lblpass= Titanium.UI.createLabel({text:'Razón Social',center:0,top:'19%',color:'#49a6ff'});
	txtPass = Titanium.UI.createTextField({hintText:'Manzur y Asociados SA de CV',hintTextColor:'gray',center:0,top:'23%',color:'#0066cc',opacity:0.3,center:0,width:'80%',height:40,leftButtonPadding:10,borderRadius:6,value:"",textAlign:'center',backgroundFocusedColor:'transparent',font:{fontSize:12}});
	lblDFiscal= Titanium.UI.createLabel({text:'Dirección fiscal',center:0,top:'33%',color:'#49a6ff'});
	txtDFiscal = Titanium.UI.createTextField({hintText:'Dirección fiscal',hintTextColor:'gray',center:0,top:'37%',color:'#0066cc',opacity:0.3,center:0,width:'80%',height:40,leftButtonPadding:10,borderRadius:6,value:"",textAlign:'center',backgroundFocusedColor:'transparent',font:{fontSize:12}});
	lblCodigoP= Titanium.UI.createLabel({text:'Código postal',center:0,top:'47%',color:'#49a6ff'});
	txtCodigoP= Titanium.UI.createTextField({hintText:'Código postal',hintTextColor:'gray',center:0,top:'51%',color:'#0066cc',opacity:0.3,center:0,opacity:0.3,width:'80%',height:40,leftButtonPadding:10,borderRadius:6,value:"",textAlign:'center',backgroundFocusedColor:'transparent',font:{fontSize:12}});
	lblCorreo= Titanium.UI.createLabel({text:'Correo de facturación',center:0,top:'61%',color:'#49a6ff'});
	txtCorreo= Titanium.UI.createTextField({hintText:'Correo de facturación',center:0,top:'65%',color:'#0066cc',hintTextColor:'gray',center:0,opacity:0.3,width:'80%',height:40,leftButtonPadding:10,borderRadius:6,value:"",textAlign:'center',backgroundFocusedColor:'transparent',font:{fontSize:12}});
	btnlogin = Titanium.UI.createButton({title:'Registrarme',color:'white',borderRadius:30,borderColor:'#49a6ff',top:'85%',width:'85%',height:60,backgroundColor:'#49a6ff'});
	chkFactura = Ti.UI.createSwitch({value:false,top:'78%',onTintColor:'#62D337',tintColor:'#D90019',left:'10%'});
	lblFactura = Titanium.UI.createLabel({text:'Acepto términos y condiciones',right:'10%',top:'79%',color:'white'});
	registerReq = Titanium.Network.createHTTPClient();
	codigo = Titanium.Network.createHTTPClient(); 
	 var codigoAcceso= Ti.UI.createView({width:'100%', height:'100%', visible:false,zIndex: 20,backgroundColor:'#B3000000'});
     var vistacodigoAcceso= Ti.UI.createView({width:'80%', height:Ti.UI.SIZE,zIndex: 20,backgroundColor:'white',borderColor:'black',borderRadius:6,layout:'vertical'});
   	 var TitulocodigoAcceso=Titanium.UI.createLabel({text:'CÓDIGO DE ACCESO',top:20,color:'black',font:{fontSize:18,fontFamily: 'Menlo Bold Italic'}});
   	 var txtcodigoAcceso=Titanium.UI.createTextField({hintText:"0000",top:20,color:'#2980b9',hintTextColor:'gray',font:{fontSize: 15,fontFamily: 'Menlo Bold Italic'},textAlign:'center',width:'80%'});
   	 var vistacodigoAcceso1= Ti.UI.createView({width:Ti.UI.SIZE, height:Ti.UI.SIZE,layout:'horizontal',top:10,center:0});
	 var btncodigoAcceso = Titanium.UI.createButton({title:'Aceptar',center:0,backgroundColor:'#0c83c7',borderRadius:5});
   	 var btncodigoAcceso2 = Titanium.UI.createButton({title:'En otro momento',left:10,backgroundColor:'#0c83c7',borderRadius:5});
     var vistacodigoAcceso2= Ti.UI.createView({width:'100%', height:30,layout:'horizontal',top:0});
	
	var vistaCargando=Ti.UI.createView({width:'100%',height:'100%',visible:false,zIndex: 20,backgroundColor:'#B3000000'});
	var lblCargando=Titanium.UI.createLabel({text:'Registrando...',center:0,color:'white',font:{fontSize:18,fontFamily: 'Menlo Bold Italic'}});
   	
   	vistaCargando.add(lblCargando);
   	window.add(vistaCargando);
	codigoAcceso.add(vistacodigoAcceso);
	vistacodigoAcceso.add(TitulocodigoAcceso);
	vistacodigoAcceso.add(txtcodigoAcceso);
	vistacodigoAcceso.add(vistacodigoAcceso1);
	vistacodigoAcceso1.add(btncodigoAcceso);
	vistacodigoAcceso1.add(btncodigoAcceso2);
	vistacodigoAcceso.add(vistacodigoAcceso2);
    window.add(codigoAcceso);
    
    
    btncodigoAcceso.addEventListener('click', function() {
		txtcodigoAcceso.value=txtcodigoAcceso.value.replace(/^\s+/g, '').replace(/\s+$/g, '');
			if(txtcodigoAcceso.value!=''){
					codigo.open("POST", "http://subtmx.com/webservices/codigo.php");
						params = {
							usuario : Ti.App.Properties.getInt("id_usuario"),
							codigo : txtcodigoAcceso.value
						};
					codigo.send(params);		
			}else{
				Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'],message:"Debe introducir el código de seguridad enviado a su correo" ,title: 'Mensaje'}).show();
			}
	}); 

    btncodigoAcceso2.addEventListener('click', function() {
		txtcodigoAcceso.value='';
		codigoAcceso.visible=false;
		 Titanium.App.Properties.removeProperty("id_usuario");
          Titanium.App.Properties.removeProperty("id_cliente");
          Titanium.App.Properties.removeProperty("usuario2");
          Titanium.App.Properties.removeProperty("nombre");
          Titanium.App.Properties.removeProperty("apellido");
          Titanium.App.Properties.removeProperty("email");
          Titanium.App.Properties.removeProperty("telefono");
          Titanium.App.Properties.removeProperty("rfc");
          Titanium.App.Properties.removeProperty("razon");
          Titanium.App.Properties.removeProperty("genero");
          Titanium.App.Properties.removeProperty("nombrecompleto");
          inicio= require('/interface/inicio');
          new inicio().open({modal:true});
          window.close();
	});
	
	  codigo.onload = function() {
		 json = this.responseText;
         response = JSON.parse(json);
		  if (response.logged == true){
			Ti.App.Properties.setString("usuario",Ti.App.Properties.getString("usuario2"));
			paypal= require('/interface/mapa');
        	new paypal().open({modal:true});
        	window.close();
		  } else {
			Ti.UI.createAlertDialog({
				cancel : 1,
				buttonNames : ['Aceptar'],
				message : response.message,
				title : 'Mensaje'
			}).show();
		}
	};
	
	 
	 
	 
	 lblFactura.addEventListener('click',function(){
  	 Ti.Platform.openURL("http://subtmx.com/terminosycondiciones/index.html");
   	 });
	 window.addEventListener('android:back', function(){
	 	registro= require('/interface/registro');
        new registro().open({modal:true});
        window.close();
	 });
	
	if(Ti.App.Properties.getInt('ingles')==1){
		lblpass.text='Corporate name';
		txtPass.hintText='Manzur and associates SA of CV';
		lblDFiscal.text='Fiscal address';
		txtDFiscal.hintText='Fiscal address';
		lblCodigoP.text='Postal Code';
		txtCodigoP.hintText='Postal Code';
		lblCorreo.text='Email';
		txtCorreo.hintText='Email';
		lblFactura.text='I accept terms and conditions';
		btnlogin.title='Sign in';
	}

	function checkemail(emailAddress){
    		var str = emailAddress;
    		var filter = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    		if (filter.test(str)){
        		testresults = true;
    		}else{
        		testresults = false;
    		}
    		return (testresults);
	};
	
	txtUser.addEventListener('focus',function(){
		txtUser.value = '';
		txtUser.opacity=1;
	});
	txtPass.addEventListener('focus',function(){
		txtPass.value = '';
		txtPass.opacity=1;
	});
	txtDFiscal.addEventListener('focus',function(){
		txtDFiscal.value = '';
		txtDFiscal.opacity=1;
	});
	txtCodigoP.addEventListener('focus',function(){
		txtCodigoP.value = '';
		txtCodigoP.opacity=1;
	});
	txtCorreo.addEventListener('focus',function(){
		txtCorreo.value = '';
		txtCorreo.opacity=1;
	});
	
	window.addEventListener('foo', function(evtW){
		registerReq.onload = function(){
			json = this.responseText;
    			response = JSON.parse(json);
    		
    			if (response.message == "Insert failed" || response.message == 'El usuario o email ya existen' || response.message == 'Tu dispositivo fue bloqueado, consulte a soporte técnico: soporte@subtmx.com' || response.message == 'Su número de telefono ya fue registrado anteriormente'){
        			alert(response.message);
        			vistaCargando.visible=false;
        			btnlogin.editable=true;
    			}else{
    				if(Ti.App.Properties.getInt('ingles')==1){
    					alertDialog = Titanium.UI.createAlertDialog({title: 'Registry',message:"Debe introducir el código de seguridad enviado a su correo",buttonNames: ['OK']});
    				}else{
    					alertDialog = Titanium.UI.createAlertDialog({title: 'Registro',message:"Debe introducir el código de seguridad enviado a su correo",buttonNames: ['OK']});
    				}
    				alertDialog.show();
    				vistaCargando.visible=false;
    				alertDialog.addEventListener('click',function(e){
    					Ti.App.Properties.setInt("id_usuario", response.id_usuario);
                		Ti.App.Properties.setInt("id_cliente", response.id_cliente);
                		Ti.App.Properties.setString("usuario", response.usuario);
                		Ti.App.Properties.setString("nombre", response.nombre);
                		Ti.App.Properties.setString("apellido", response.apellido);
                		Ti.App.Properties.setString("email", response.email);
                		Ti.App.Properties.setString("telefono", response.telefono);
                		Ti.App.Properties.setString("rfc", response.rfc);
                		Ti.App.Properties.setString("razon", response.razon);
                		Ti.App.Properties.setString("genero", response.sexo);
                		codigoAcceso.visible=true;
                		// paypal= require('/interface/mapa');
        		 		// new paypal().open({modal:true});
        		 		// window.close();
    				});
    			}
		};
	
		btnlogin.addEventListener('click',function(){
			if (txtUser.value != '' && txtPass.value != '' && txtDFiscal.value != '' && txtCodigoP.value != '' && txtCorreo.value != ''){
				   if (!checkemail(txtCorreo.value)){
                        if(Ti.App.Properties.getInt('ingles')==1){
                        	alert("Enter a valid email");
                        }else{
                        	alert("Introduce un email valido");	
                        }
                       
                   }else{
                    	if (chkFactura.value==true){
                    vistaCargando.visible=true;		
                    btnlogin.editable=false;
					registerReq.open("POST","http://subtmx.com/webservices/registerResquest.php");
                		params = {
                    		username: evtW.username,
                    		password: Ti.Utils.md5HexDigest(evtW.password),
                    		correo: evtW.correo,
                    		rfc:txtUser.value,
                    		razon_social:txtPass.value,
                    		telefono:evtW.telefono,
                    		nombre:evtW.nombre,
                    		apellido_p:evtW.apellido_p,
                            apellido_m:evtW.apellido_m,
                    		direccion_fiscal:txtDFiscal.value,
                    		email_factura:txtCorreo.value,
                    		cp:txtCodigoP.value,
                    		device_token:Titanium.Platform.id,
                        	device_type:"android"
                    	};
                	registerReq.send(params);
                	}else{
                		 if(Ti.App.Properties.getInt('ingles')==1){
                        	alert ( 'You must accept the terms and conditions');
                        }else{
                        	alert('Debe aceptar los terminos y condiciones');
                        }
                	}
				}		
			}else{
				if(Ti.App.Properties.getInt('ingles')==1){
					alert ( 'All fields are mandatory');
				}else{
					alert('Todos los campos son obligatorios');
				}
				
			}
		});
	});
	
	window.addEventListener("open", function(evtop) { 
		var theActionBar = window.activity.actionBar; 
		if (theActionBar != undefined) {
			theActionBar.backgroundImage = '/images/barra.jpeg';
        		if(Ti.App.Properties.getInt('ingles')==1){
        		theActionBar.title = "Data of billing";	
        		}else{
        		theActionBar.title = "Datos de facturación";	
        		}
        		
        }
	});
	
	scrollView.add(lblUser);
	scrollView.add(txtUser);
	scrollView.add(lblpass);
	scrollView.add(txtPass);
	scrollView.add(lblDFiscal);
	scrollView.add(txtDFiscal);
	scrollView.add(lblCodigoP);
	scrollView.add(txtCodigoP);
	scrollView.add(lblCorreo);
	scrollView.add(txtCorreo);
	scrollView.add(chkFactura);
	scrollView.add(lblFactura);
	scrollView.add(btnlogin);
	window.add(scrollView);
	
	return window;
};
module.exports = factura;