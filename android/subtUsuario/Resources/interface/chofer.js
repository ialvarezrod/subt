function chofer(tipo_servicio,latInicio,lngInicio,latDestino,lngDestino,geocoderlbl,ti_ser,taxiRosaSelect,carroSelect,numPasajero){
	var Map = require('ti.map');
	Ti.API.info('tipo_servicio:  '+tipo_servicio);
	Ti.API.info('latInicio:  '+latInicio);
	Ti.API.info('lngInicio:  '+lngInicio);
	Ti.API.info('latDestino:  '+latDestino);
	Ti.API.info('lngDestino:  '+lngDestino);
	Ti.API.info('geocoderlbl:  '+geocoderlbl);
	Ti.API.info('ti_ser:  '+ti_ser);
	Ti.API.info('taxiRosaSelect:  '+taxiRosaSelect);
	Ti.API.info('carroSelect:  '+carroSelect);
	Ti.API.info('numPasajero:  '+numPasajero);
	var carroSelect=carroSelect;
	Ti.App.Properties.setString("carroSelect",carroSelect);
	Ti.App.Properties.setString("numPasajero",numPasajero);
	Ti.App.Properties.setInt("tipo_servicio",tipo_servicio);
	Ti.App.Properties.setInt("taxiRosaSelect",taxiRosaSelect);
	Ti.App.Properties.setString("lat1",latInicio);
	Ti.App.Properties.setString("lon1",lngInicio);
	Ti.App.Properties.setString("lat2",latDestino);
	Ti.App.Properties.setString("lon2",lngDestino);
	var windowC,header,iconMenu,iconEdit,titulo,ImgPerfil,logo,fondoP;
	var player1,player2,player3,player4,tarifa_inicial ,  horario_inicio,  modelo, conductor ,placaslbl ,dt,kmadicional,
	costoadicional ,costoTOTAL ,horario_inicio , telefono,ImgPerfil,lugar_origen,nombre_chofer,placas,
	estrellas ,lblAdicional,lblcosto,lblTotal,viewPro,track,progress,display_lbl,buscando_lbl,lblTotal,ti,tarifaFinal,tarifaA,fotoC;
	var my_timer,bbit;
	var re=0;
	if(Ti.App.Properties.getInt("Bono")==0){
		bbit=0;
	}else{
		bbit=1;
	}

	//var my_timer;
	windowC = Ti.UI.createWindow({theme:'Theme.AppCompat.Translucent.NoTitleBar',backgroundColor:'white'});
	windowC .orientationModes = [Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT];
	var viewP;
	var viewP2 = Titanium.UI.createView({top:'10%', width:'100%',height:'auto',backgroundColor:'white',zIndex:9});
	
	centerView =Ti.UI.createView({height: 'auto',right:0,top: 0,width:'100%',zIndex:2});
	var vistaMapa =Ti.UI.createView({height: '40%',right:0,bottom: 0,width:'100%',zIndex:10});
	var vistaMapa2 =Ti.UI.createView({height: '40%',right:0,bottom: 0,width:'100%',zIndex:10});
    header = Ti.UI.createView({backgroundColor: 'white',height: '10%',right:0,top: 0,width:'100%',zIndex: 10});
    titulo=Ti.UI.createLabel({text:'Chofer Disponible', color:'#232323',center:0,bottom:'25%',zIndex:2,font:{fontSize:'15.5%'}});
    btnCall = Titanium.UI.createButton({backgroundImage:'/images/tel.png',height:28,width:28});
    info = Titanium.UI.createButton({backgroundImage:'/images/info.png',height:28,width:28});
    telefono = Ti.UI.createLabel({});
    // var viewWeb= Ti.UI.createView({backgroundColor:'WHITE'});    
   ImgPerfil= Ti.UI.createImageView({width:'31%',height:'19.5%',borderRadius:100,top:'10%',center:0,image:'/images/foto.jpg',zIndex:10});
    fondoP = Ti.UI.createView({backgroundImage: '/images/logo4.png',height: '20%',left:0,top:'10%',width:'auto',zIndex: 2});
	var btnCanViaje = Titanium.UI.createButton({title:'Cancelar Viaje',color:'white',backgroundColor:'#007fff', left:'5%',height:'auto',width:'30%',top:'45%',zIndex:10,enabled:true,font:{fontSize:'11%'},borderRadius:7});
	var btnRecotizar = Titanium.UI.createButton({title:'Recotizar Viaje',color:'white',backgroundColor:'#007fff', right:'5%',height:'auto',width:'30%',top:'45%',zIndex:10,enabled:true,font:{fontSize:'11%'},borderRadius:7});
	var btnCanViaje2 = Titanium.UI.createButton({title:'Cancelar Viaje',color:'white',backgroundColor:'#007fff', left:'5%',height:'auto',width:'30%',top:'50%',zIndex:10,enabled:true,font:{fontSize:'11%'},borderRadius:7});
	var btnRecotizar2 = Titanium.UI.createButton({title:'Recotizar Viaje',color:'white',backgroundColor:'#007fff', right:'5%',height:'auto',width:'30%',top:'50%',zIndex:10,enabled:true,font:{fontSize:'11%'},borderRadius:7});
	
		player1 = Ti.Media.createAudioPlayer({url:"/sound/sonido.mp3"});
		player2 = Ti.Media.createAudioPlayer({url:"/sound/sonido.mp3"});
		player3 = Ti.Media.createAudioPlayer({url:"/sound/sonido.mp3"});
		player4 = Ti.Media.createAudioPlayer({url:"/sound/sonido.mp3"});
		
	

	viewPro= Ti.UI.createView({backgroundColor:'WHITE', widht:'100%',height:'100%',zIndex:100});
	track = Ti.UI.createView({ width: "80%", height: "2%", borderRadius:100, backgroundColor: '#c9ecfd',top:'50%'});
  	progress = Ti.UI.createView({ borderRadius:40, left: 0, width: '100%', height: "100%", backgroundColor : '#007fff'});
 	display_lbl =  Titanium.UI.createLabel({text:"03:00",font:{fontSize:'65%'},top:'55%',center:0,color:'#c2c2c2 ',borderRadius:10,textAlign:'center'});
	buscando_lbl =  Titanium.UI.createLabel({text:"Buscando chofer...",font:{fontSize:'26%'},top:'40%',center:0,color:'#c2c2c2 ',	borderRadius:10,textAlign:'center'});
	registerServ = Titanium.Network.createHTTPClient();
	registerUpdate= Titanium.Network.createHTTPClient();
	pagoReq = Titanium.Network.createHTTPClient();
	pagoReq3 = Titanium.Network.createHTTPClient();
	endServReq = Titanium.Network.createHTTPClient();
	pagoCan = Titanium.Network.createHTTPClient();
	tarifaFinal=0;
	tarifaA=0;var choferactivo;var viniciado=0,vyallegue=0,vaceptado=0,vfinalizado=0;
	var sa=0,idc='';
	var $eventualidad=0;
	var idEv=8;
	var costo_minutos=0; var nombreCh;var yallegue,viajeiniciado;
	var vistaActiva;var final,final2;
	Ti.App.Properties.setDouble("lat_in_recalcular",Ti.App.Properties.getDouble("latIN"));
	Ti.App.Properties.setDouble("long_in_recalcular",Ti.App.Properties.getDouble("longIN"));
	var mapview;	var annotations = [];var annotation;var locationAdded = false;var latitude;var longitude;var idch = Ti.UI.createLabel({});var sdial=false;var sdial2=false;var sdial3=false;var sdial4=false;
	var sonidoP1,sonidoP2,sonidoP3,sonidoP4;
	
	
	
	if(Ti.App.Properties.getInt('ingles')==1){
		titulo.text='Driver Available';
		btnCanViaje.title='Cancel the Travel';
		btnCanViaje2.title='Cancel the Travel';
		buscando_lbl.text="Searching a Driver...";
		btnRecotizar.title="Travel requote";
		btnRecotizar2.title="Travel requote";	
	}
	
	if(Ti.App.Properties.getInt('ingles')==1){
		var dialogCancelar = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Accept','Cancel'],message: 'Do you want to cancel the trip?',title: 'Cancel trip'});
	}else{
		var dialogCancelar = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar','Cancelar'],message: '¿Desea cancelar el viaje?',title: 'Cancelar viaje'});
	}
	
		dialogCancelar.addEventListener('click',function(ed){
		if(ed.index ==0){
			if(Ti.App.Properties.hasProperty('yallegue')){
		//	 Ti.App.Properties.setDouble("ctD",0.00);
			}else{
		//	 Ti.App.Properties.setDouble("ctD",0.00);			
			}
		Ti.App.fireEvent('fbController:cancelacion', {
		id_chofer:Ti.App.Properties.getInt("id_Chofer")
		});
			vistaFinalizado.visible=true;	
		
			// pagoCan.open("POST","http://subtmx.com/webservices/updateservicio.php");
  //          	params3 = {
//                        id_servicio: Ti.App.Properties.getInt("id_servicio"),
//                        costo_total: Ti.App.Properties.getDouble('ctD').toFixed(2)
//                    		};
//            pagoCan.send(params3);
		}else {
		}
	});
	
	var countDown =  function( m , s, fn_tick, fn_end  ) {
		return {
			total_sec:m*60+s,
			timer:this.timer,
			set: function(m,s) {
				this.total_sec = parseInt(m)*60+parseInt(s);
				this.time = {m:m,s:s};
				return this;
			},
			start: function() {
				var self1 = this;
				this.timer = setInterval( function() {
					if (self1.total_sec) {
						self1.total_sec--;
						self1.time = { m : parseInt(self1.total_sec/60), s: (self1.total_sec%60) };
						fn_tick();
					}else {
						self1.stop();
						fn_end();
					}
		   		}, 1000 );
				return this;
			},
			stop: function() {
				clearInterval(this.timer);
				this.time = {m:0,s:0};
				this.total_sec = 0;
				return this;
			}
		};
	};
	my_timer3 = new countDown(05,00,
		function() {
			 if (my_timer3.time.m<=5){
				Ti.API.info(my_timer3.time.m+':'+my_timer3.time.s);
				if (Ti.App.Properties.getInt('recotizado2') == 1) {
					
					
					my_timer3.stop();
					
					my_timer3.set(05,00);
					
		
  				var kmformateado=Ti.App.Properties.getString("distancias").split(',');
		 		var kmWeb = kmformateado[0] + '.' + kmformateado[1];
		 
    			var dus= parseFloat(kmWeb) + Ti.App.Properties.getDouble("Extra2");
    			
		 		// Ti.App.Properties.setDouble("Kmdus",dus);
				Ti.App.Properties.setString("kmt",dus);
				choferactivo =	Ti.App.Properties.getInt("id_Chofer");
				idch.text = Ti.App.Properties.getInt("id_Chofer");
				tarifa_inicial = Ti.UI.createLabel({text:'Tarifa inicial:',left:'2%',top:'10%',color:'black'});
				modelo = Ti.UI.createLabel({text:'Modelo:',left:'2%',top:'10%',color:'black'});
				conductor = Ti.UI.createLabel({text:'Conductor:',left:'2%',top:'16%',color:'black'});
				placaslbl = Ti.UI.createLabel({text:'Placas:',left:'2%',top:'22%',color:'black'});
				dt = Ti.UI.createLabel({text:'Distancia total:',left:'2%',top:'28%',color:'black'});
				kmadicional = Ti.UI.createLabel({text:'Km adicional:',left:'2%',top:'40%',visible:false,color:'black'});
				costoadicional = Ti.UI.createLabel({text:'Costo adicional:',left:'2%',top:'46%',visible:false,color:'black'});
				costoTOTAL = Ti.UI.createLabel({text:'Costo aproximado:',left:'2%',top:'34%',visible:true,color:'black'});
				lblTel = Ti.UI.createLabel({text:'Telefono:',left:'2%',top:'40%',visible:true,color:'black'});
				horario_inicio = Ti.UI.createLabel({text:'$' + Ti.App.Properties.getDouble("tarifa_inicial") + ' MXN',color:'#4ad34d',right:'2%',top:'10%'});
				ti=Ti.UI.createLabel({text:Ti.App.Properties.getDouble("tarifa_inicial")});
				telefono.text =	Ti.App.Properties.getString("num_telefono_chofer"); 
				ImgPerfil.image = "http://subtmx.com/images/"+Ti.App.Properties.getString("fotografiaC");
				lugar_origen = Ti.UI.createLabel({text:Ti.App.Properties.getString("modelo"),color:'#a2a1b8',right:'2%',top:'10%',font:{fontSize:14}});
				nombre_chofer = Ti.UI.createLabel({text:Ti.App.Properties.getString("nombre_chofer"),color:'#a2a1b8',right:'2%',top:'16%',font:{fontSize:14}});
				placas = Ti.UI.createLabel({text:Ti.App.Properties.getString("placa"),color:'#a2a1b8',right:'2%',top:'22%',font:{fontSize:14}});
				estrellas = Ti.UI.createLabel({right:'2%', top:'28%',color:'#a2a1b8',text:dus.toFixed(2)+' KM',font:{fontSize:14}});
				lblAdicional = Ti.UI.createLabel({right:'2%', top:'40%',color:'#a2a1b8',font:{fontSize:14},visible:false});
				lblcosto = Ti.UI.createLabel({right:'2%', top:'46%',color:'#4ad34d',font:{fontSize:14},visible:false});
				telefonoimg = Titanium.UI.createButton({backgroundImage:'/images/telefono.png', top:'40%', right:'2%', width:'5%', height:'4%'}); 
    			
					
				
				telefonoimg.addEventListener('click', function(e) {
 				if(Ti.App.Properties.getInt('ingles')==1){
 				var dialog = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Accept', 'Cancel'],message: 'Do you want to make this call?',title: 'Call'});
   			
 				}else{
 				var dialog = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar', 'Cancelar'],message: '¿Desea usted realizar esta llamada?',title: 'LLamada'});
   		     	
 				}
					
   		     	
   		     
   				
dialog.addEventListener('click', function(ed) {
	if (ed.index === 0) {
		if (Titanium.Platform.version >= '6.0' || Titanium.Platform.version >= 6.0) {
			var intent = Ti.Android.createIntent({
				action : Ti.Android.ACTION_DIAL,
				data : 'tel:' + telefono.text
			});
			Ti.Android.currentActivity.startActivity(intent);
		} else {
			var intent = Ti.Android.createIntent({
				action : Ti.Android.ACTION_CALL,
				data : 'tel:' + telefono.text
			});
			Ti.Android.currentActivity.startActivity(intent);
		}

	}
}); 

         dialog.show();
	});				
				
				
				if(Ti.App.Properties.getInt('ingles')==1){
        				tarifa_inicial.text='Initial rate:';
        				modelo.text='Model';
        				conductor.text='Driver';
        				placaslbl.text='license plate';
        				dt.text='Total distance:';
        				kmadicional.text='Additional km:';
        				costoadicional.text='Additional cost:';
        				costoTOTAL.text='Total Approximate cost:';
        			}
				
				var nuevoKMT;var newCosto;var tarifaFinal;
						if (Ti.App.Properties.getString("tiSer")==1){
						
							if (dus.toFixed(2)>Ti.App.Properties.getDouble("km_inicial")) {
						
							var	newkmad=dus.toFixed(2)-Ti.App.Properties.getDouble("km_inicial");
					
							lblAdicional.text = newkmad;
							Ti.App.Properties.setDouble("kmAdicional",lblAdicional.text);
							//kmadicional.visible = true;
							//lblAdicional.visible = true;
							Ti.API.info('tipo newkmad: ' +newkmad);
					
							if (Ti.App.Properties.getInt("id_tipo_servicio") == 1) {
								newCosto = newkmad.toFixed(2) * Ti.App.Properties.getDouble("precio_km_adicional");
								
							}else if (Ti.App.Properties.getInt("id_tipo_servicio") == 2){
								newCosto = newkmad.toFixed(2) * Ti.App.Properties.getDouble("precio_km_adicional");
							
							}else if (Ti.App.Properties.getInt("id_tipo_servicio") == 3){
								newCosto = newkmad.toFixed(2) * Ti.App.Properties.getDouble("precio_km_adicional");
						
								
							}
						
							Ti.API.info('tipo newCosto: ' +newCosto);
							//lblcosto.text = '$'+newCosto.toFixed(2)+' MXN';
							//costoadicional.visible = true;
							//lblcosto.visible = true;
							tarifaA=newCosto;
							tarifaFinal= newCosto+ Ti.App.Properties.getDouble("tarifa_inicial")*1;
							//lblTotal.text = '$' + tarifaFinal.toFixed(2) +' MXN';
							Ti.App.Properties.setString("ta",tarifaA.toFixed(2));
							Ti.App.Properties.setString("ct",tarifaFinal.toFixed(2));
							Ti.App.Properties.setDouble("ctD",tarifaFinal.toFixed(2));
							//lblTotal.visible = true;
							costoTOTAL.visible = true;					
						}else if (dus.toFixed(2) <=Ti.App.Properties.getDouble("km_inicial")) {
							
							//lblTotal.visible = true;
							costoTOTAL.visible = true;	
							lblAdicional.text = '0';
							Ti.App.Properties.setDouble("kmAdicional",lblAdicional.text);
							tarifaA=0;
							Ti.App.Properties.setString("ta",tarifaA.toFixed(2));
							tarifaFinal=Ti.App.Properties.getDouble("tarifa_inicial")*1;
							//lblTotal.text = '$' + tarifaFinal.toFixed(2) +' MXN';
							Ti.App.Properties.setDouble("ctD",tarifaFinal.toFixed(2));
							Ti.App.Properties.setString("ct",tarifaFinal.toFixed(2));
				 			Ti.App.Properties.setDouble("kmCostoAdicional",tarifaA);
				 			}
					Ti.App.Properties.setDouble("kmCostoAdicional",tarifaA);
					}else if (Ti.App.Properties.getString("tiSer")==2){
							costoTOTAL.visible = true;	
						   	tarifaFinal=Ti.App.Properties.getDouble("ctD");
							Ti.App.Properties.setDouble("ctD",tarifaFinal.toFixed(2));
					}
				lblTotal = Ti.UI.createLabel({text:'$' + Ti.App.Properties.getDouble("ctD") +' MXN',right:'2%', top:'34%',color:'#4ad34d',font:{fontSize:14},visible:true});
		 		
		 		if(Ti.App.Properties.getInt('recotizado')==1){
		 			
		 			re=Ti.App.Properties.getDouble('ctD')-(parseFloat(Ti.App.Properties.getInt("Bono")))-Ti.App.Properties.getDouble("pagoInicial");
			
					if(re<=0){
						re=0;
					}
		 			Ti.App.Properties.setDouble("re",re.toFixed(2));
				
		 			
		 			endServReq.open("POST","http://subtmx.com/webservices/endServicios.php");
                    params = {
                    	id_servicio: Ti.App.Properties.getInt("id_servicio"),
                        km_adicional: Ti.App.Properties.getDouble("kmAdicional"),
                        costo_km_adicional:Ti.App.Properties.getDouble("kmCostoAdicional"),
                        hora_extra: "00:00:00",
                        costo_hora_extra: 0,
                        cargo_hora_pico: 0,
                        tiempo_espera_adicional:"00:00:00",
                        costo_espera_adicional: 0,
                        costo_total: Ti.App.Properties.getDouble("pagoInicial"),
                        id_eventualidad:idEv,
                        costo_pendiente:Ti.App.Properties.getDouble("re").toFixed(2),
                        status_promocion:bbit
                    };
      				endServReq.send(params);
		 			
		 			
		 			
		 			
		 			// registerUpdate.open("POST","http://subtmx.com//webservices/updateservicio.php");
                		// params = {
                    		// destino:Ti.App.Properties.getString("txtdestino"),
                        	// id_servicio: Ti.App.Properties.getInt("id_servicio"),
                        	// costo_total:Ti.App.Properties.getDouble("ctD").toFixed(2)
                    // };
                   	// registerUpdate.send(params);
		 			Ti.App.Properties.setInt('recotizado',0);
		 		
		 		}	
						
			
				viewP2.add(lugar_origen);
				viewP2.add(nombre_chofer);
				viewP2.add(placas);
				viewP2.add(estrellas);
				viewP2.add(modelo);
				viewP2.add(conductor);
				viewP2.add(placaslbl);
				viewP2.add(dt);
				viewP2.add(lblAdicional);
				viewP2.add(kmadicional);
				viewP2.add(lblcosto);
				viewP2.add(costoadicional);
				viewP2.add(lblTotal);
				viewP2.add(costoTOTAL);
				viewP2.add(lblTel);
				viewP2.add(telefonoimg);
			
				if(Ti.App.Properties.getString("tiSer")==1){
				
				windowC.add(btnCanViaje2);
				windowC.add(btnRecotizar2);
				}else if(Ti.App.Properties.getString("tiSer")==2){
				
				}
				
				windowC.add(ImgPerfil);
				windowC.add(viewP2);
				windowC.add(vistaMapa);
  				
  		
	Ti.App.Properties.setInt('recotizado2',0); 
	}	
			}else{
			}
		},	function() {}
	);
		
	if(Ti.App.Properties.getInt('ingles')==1){
		var dialogRecotizar = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Accept','Cancel'],message: 'Do you want to quote the trip?',title: 'Cancel trip'});
	}else{
		var dialogRecotizar = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar','Cancelar'],message: '¿Desea recotizar el viaje?',title: 'Recotizar viaje'});
	}
	
	
		dialogRecotizar.addEventListener('click',function(ed){
		Titanium.Geolocation.getCurrentPosition(function(e){
			if (e.error){
        	if (Ti.App.Properties.getInt('ingles')==1)
        	 {	alert('Unable to get current location.');}
        	else{alert('No se puede obtener ubicacion actual');}
        		return;
    		}
			Ti.App.Properties.setDouble("latActual",e.coords.latitude);
        	Ti.App.Properties.setDouble("longActual",e.coords.longitude);
        
		 });
	
		if(ed.index == 0){
					 my_timer3.start();	
				
					
					 viewP2.removeAllChildren();
				     windowC.remove(ImgPerfil);
						
					mapa2 = require('/interface/mapa2');
    				new mapa2().open({modal:true});
    				
				 	  
				 	
				 	 
   		}else {
		}
	});
	
	
   btnCanViaje.addEventListener('click', function(e){
  		dialogCancelar.show();
   });
   
   btnRecotizar.addEventListener('click', function(e){
  		 dialogRecotizar.show();  
   });
     btnCanViaje2.addEventListener('click', function(e){
  		dialogCancelar.show();
   });
   
   btnRecotizar2.addEventListener('click', function(e){
  		 dialogRecotizar.show();  
   });
   
  
	
		
	my_timer = new countDown(03, 00, function() {
	
	if (my_timer.time.m == 0 && my_timer.time.s == 0) {
		my_timer.stop();
		if(Ti.App.Properties.getInt('ingles')==1){
		var dialogChofer = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Accept'],message: 'Driver not found. Please try again',title: 'Search'});
				}else{
					var dialogChofer = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'],message: 'Chofer no encontrado. Favor de intentarlo nuevamente',title: 'Busqueda'});
				}
		dialogChofer.show();
		dialogChofer.addEventListener('click', function(ed) {

		var webViewCacheDir = Ti.Filesystem.getFile(Ti.Filesystem.applicationCacheDirectory, "webviewCache");
					Ti.API.info('CACHE DIR');
					Ti.API.info("cacheDiroctory=" + Ti.Filesystem.applicationCacheDirectory);
					Ti.API.info('CONTENTS');
					Ti.API.info(webViewCacheDir.getDirectoryListing());
					Ti.API.info('Array length before: ' + webViewCacheDir.getDirectoryListing().length);
					webViewCacheDir.deleteDirectory(true);
					Ti.API.info('Array length after: ' + webViewCacheDir.getDirectoryListing().length);
					mapa = require('/interface/mapa');
                  	new mapa().open({modal:true});
   					windowC.close();
                   	var viewChildren = windowC.children;
						Ti.API.info(viewChildren);
						for (var i=0; i < viewChildren.length; i++) {
							var child = viewChildren[i];
						windowC.remove(child);	
					}	
                  	windowC.removeAllChildren();
                   	windowC.remove(webviewC);
				 	webviewC.release();
				  	webviewC = null;
				 	Ti.API.info('webviewC: ' + webviewC);
				 	windowC = null;
				 	Ti.API.info('windowC: ' + windowC);
		});

		if (my_timer.time.s < 10) {
			display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
		} else {
			display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
		}
	} else if (my_timer.time.m == 2 && my_timer.time.s == 55) {
		
		windowC.add(webviewC);
		if (my_timer.time.s < 10) {
			display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
		} else {
			display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
		}
	} else if ((my_timer.time.m <= 3) && (my_timer.time.m == 2 && my_timer.time.s >= 10)) {
		Ti.API.info("id: " + idc);
		if (sa == 1) {
	
			my_timer.stop();
			getData();
		
		
		}
		if (my_timer.time.s < 10) {
			display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
		} else {
			display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
		}
	} else if (my_timer.time.m == 2 && my_timer.time.s == 9) {
		if (sa == 0) {
			
		// sisno.open("POST","http://iego.com.mx/webservices/enviarnotificacion.php");
          // params = {
           // idusuario:usuarioChofer,
           // mensaje: 'Servicio no aceptado',
           // titulo: 'Estado de servicio',
           // statussolicitud:0,
           // viajecancelado:0,
           // saldoinsuficiente:0,
           // noaceptado:1
       // };
       // sisno.send(params);
			Ti.App.fireEvent('fbController:noAceptado', {
				id_chofer : idc
			});
			idc='';
			var webViewCacheDir = Ti.Filesystem.getFile(Ti.Filesystem.applicationCacheDirectory, "webviewCache");
			Ti.API.info('CACHE DIR');
			Ti.API.info("cacheDiroctory=" + Ti.Filesystem.applicationCacheDirectory);
			Ti.API.info('CONTENTS');
			Ti.API.info(webViewCacheDir.getDirectoryListing());
			Ti.API.info('Array length before: ' + webViewCacheDir.getDirectoryListing().length);
			webViewCacheDir.deleteDirectory(true);
			Ti.API.info('Array length after: ' + webViewCacheDir.getDirectoryListing().length);
		}
		
		if (my_timer.time.s < 10) {
			display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
		} else {
			display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
		}
	} else if (my_timer.time.m == 2 && my_timer.time.s == 8) {
		
		var webViewCacheDir = Ti.Filesystem.getFile(Ti.Filesystem.applicationCacheDirectory, "webviewCache");
			Ti.API.info('CACHE DIR');
			Ti.API.info("cacheDiroctory=" + Ti.Filesystem.applicationCacheDirectory);
			Ti.API.info('CONTENTS');
			Ti.API.info(webViewCacheDir.getDirectoryListing());
			Ti.API.info('Array length before: ' + webViewCacheDir.getDirectoryListing().length);
			webViewCacheDir.deleteDirectory(true);
			Ti.API.info('Array length after: ' + webViewCacheDir.getDirectoryListing().length);
		
		windowC.remove(webviewC);
        webviewC.release();
        mapview.removeAllAnnotations();

		if (my_timer.time.s < 10) {
			display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
		} else {
			display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
		}
	} else if ((my_timer.time.m == 2 && my_timer.time.s >= 4) && (my_timer.time.m == 2 && my_timer.time.s < 8)) {

		if (my_timer.time.s < 10) {
			display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
		} else {
			display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
		}
	} else if (my_timer.time.m == 2 && my_timer.time.s == 3) {

		windowC.add(webviewC);
		if (my_timer.time.s < 10) {
			display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
		} else {
			display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
		}
	} else if (  (my_timer.time.m == 2 && my_timer.time.s < 3 && my_timer.time.m == 2 && my_timer.time.s >=0) || (my_timer.time.m == 1 && my_timer.time.s >= 17 && my_timer.time.m == 1 && my_timer.time.s <= 59)) {
		Ti.API.info("id: " + idc);
		if (sa == 1) {

			my_timer.stop();
			getData();
		

		}
		if (my_timer.time.s < 10) {
			display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
		} else {
			display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
		}
	} else if (my_timer.time.m == 1 && my_timer.time.s == 16) {
		if (sa == 0) {
				// sisno.open("POST","http://iego.com.mx/webservices/enviarnotificacion.php");
          // params = {
           // idusuario:usuarioChofer,
           // mensaje: 'Servicio no aceptado',
           // titulo: 'Estado de servicio',
           // statussolicitud:0,
           // viajecancelado:0,
           // saldoinsuficiente:0,
           // noaceptado:1
       // };
       // sisno.send(params);
			Ti.App.fireEvent('fbController:noAceptado', {
				id_chofer : idc
			});
	idc='';
	var webViewCacheDir = Ti.Filesystem.getFile(Ti.Filesystem.applicationCacheDirectory, "webviewCache");
			Ti.API.info('CACHE DIR');
			Ti.API.info("cacheDiroctory=" + Ti.Filesystem.applicationCacheDirectory);
			Ti.API.info('CONTENTS');
			Ti.API.info(webViewCacheDir.getDirectoryListing());
			Ti.API.info('Array length before: ' + webViewCacheDir.getDirectoryListing().length);
			webViewCacheDir.deleteDirectory(true);
			Ti.API.info('Array length after: ' + webViewCacheDir.getDirectoryListing().length);
		}
		if (my_timer.time.s < 10) {
			display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
		} else {
			display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
		}
	} else if (my_timer.time.m == 1 && my_timer.time.s == 15) {
		var webViewCacheDir = Ti.Filesystem.getFile(Ti.Filesystem.applicationCacheDirectory, "webviewCache");
			Ti.API.info('CACHE DIR');
			Ti.API.info("cacheDiroctory=" + Ti.Filesystem.applicationCacheDirectory);
			Ti.API.info('CONTENTS');
			Ti.API.info(webViewCacheDir.getDirectoryListing());
			Ti.API.info('Array length before: ' + webViewCacheDir.getDirectoryListing().length);
			webViewCacheDir.deleteDirectory(true);
			Ti.API.info('Array length after: ' + webViewCacheDir.getDirectoryListing().length);
		windowC.remove(webviewC);
		webviewC.release();
		mapview.removeAllAnnotations();

		if (my_timer.time.s < 10) {
			display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
		} else {
			display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
		}
	} else if ((my_timer.time.m == 1 && my_timer.time.s >= 11) && (my_timer.time.m == 1 && my_timer.time.s < 15)) {

		if (my_timer.time.s < 10) {
			display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
		} else {
			display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
		}
	} else if (my_timer.time.m == 1 && my_timer.time.s == 10) {

		windowC.add(webviewC);

		if (my_timer.time.s < 10) {
			display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
		} else {
			display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
		}
	} else if ((my_timer.time.m == 0 && my_timer.time.s >=34 && my_timer.time.m == 0 && my_timer.time.s <=59) || (my_timer.time.m == 1 && my_timer.time.s <10 && my_timer.time.m == 1 && my_timer.time.s >=0)) {
		Ti.API.info("id: " + idc);
		if (sa == 1) {

			my_timer.stop();
			getData();
		
		}
		if (my_timer.time.s < 10) {
			display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
		} else {
			display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
		}
	} else if (my_timer.time.m == 0 && my_timer.time.s == 33) {
		if (sa == 0) {
				// sisno.open("POST","http://iego.com.mx/webservices/enviarnotificacion.php");
          // params = {
           // idusuario:usuarioChofer,
           // mensaje: 'Servicio no aceptado',
           // titulo: 'Estado de servicio',
           // statussolicitud:0,
           // viajecancelado:0,
           // saldoinsuficiente:0,
           // noaceptado:1
       // };
       // sisno.send(params);
			Ti.App.fireEvent('fbController:noAceptado', {
				id_chofer : idc
			});
idc='';
var webViewCacheDir = Ti.Filesystem.getFile(Ti.Filesystem.applicationCacheDirectory, "webviewCache");
			Ti.API.info('CACHE DIR');
			Ti.API.info("cacheDiroctory=" + Ti.Filesystem.applicationCacheDirectory);
			Ti.API.info('CONTENTS');
			Ti.API.info(webViewCacheDir.getDirectoryListing());
			Ti.API.info('Array length before: ' + webViewCacheDir.getDirectoryListing().length);
			webViewCacheDir.deleteDirectory(true);
			Ti.API.info('Array length after: ' + webViewCacheDir.getDirectoryListing().length);
		}
		if (my_timer.time.s < 10) {
			display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
		} else {
			display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
		}
	} else if (my_timer.time.m == 0 && my_timer.time.s == 32) {
		var webViewCacheDir = Ti.Filesystem.getFile(Ti.Filesystem.applicationCacheDirectory, "webviewCache");
			Ti.API.info('CACHE DIR');
			Ti.API.info("cacheDiroctory=" + Ti.Filesystem.applicationCacheDirectory);
			Ti.API.info('CONTENTS');
			Ti.API.info(webViewCacheDir.getDirectoryListing());
			Ti.API.info('Array length before: ' + webViewCacheDir.getDirectoryListing().length);
			webViewCacheDir.deleteDirectory(true);
			Ti.API.info('Array length after: ' + webViewCacheDir.getDirectoryListing().length);
			windowC.remove(webviewC);
			webviewC.release();
			mapview.removeAllAnnotations();

		if (my_timer.time.s < 10) {
			display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
		} else {
			display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
		}
	} else if ((my_timer.time.m == 0 && my_timer.time.s >= 30) && (my_timer.time.m == 0 && my_timer.time.s < 32)) {

		if (my_timer.time.s < 10) {
			display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
		} else {
			display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
		}
	} else if (my_timer.time.m == 0 && my_timer.time.s ==29) {

		windowC.add(webviewC);

		if (my_timer.time.s < 10) {
			display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
		} else {
			display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
		}
	} else if ((my_timer.time.m == 0 && my_timer.time.s >=3) && (my_timer.time.m == 0 && my_timer.time.s < 29 )) {
		Ti.API.info("id: " + idc);
		if (sa == 1) {

			my_timer.stop();
			getData();
		
		}

		if (my_timer.time.s < 10) {
			display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
		} else {
			display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
		}
	} else if (my_timer.time.m == 0 && my_timer.time.s == 2) {
		if (sa == 0) {
			
				// sisno.open("POST","http://iego.com.mx/webservices/enviarnotificacion.php");
          // params = {
           // idusuario:usuarioChofer,
           // mensaje: 'Servicio no aceptado',
           // titulo: 'Estado de servicio',
           // statussolicitud:0,
           // viajecancelado:0,
           // saldoinsuficiente:0,
           // noaceptado:1
       // };
       // sisno.send(params);
			Ti.App.fireEvent('fbController:noAceptado', {
				id_chofer : idc
			});
idc='';
var webViewCacheDir = Ti.Filesystem.getFile(Ti.Filesystem.applicationCacheDirectory, "webviewCache");
			Ti.API.info('CACHE DIR');
			Ti.API.info("cacheDiroctory=" + Ti.Filesystem.applicationCacheDirectory);
			Ti.API.info('CONTENTS');
			Ti.API.info(webViewCacheDir.getDirectoryListing());
			Ti.API.info('Array length before: ' + webViewCacheDir.getDirectoryListing().length);
			webViewCacheDir.deleteDirectory(true);
			Ti.API.info('Array length after: ' + webViewCacheDir.getDirectoryListing().length);
		}
		if (my_timer.time.s < 10) {
			display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
		} else {
			display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
		}
	} else if (my_timer.time.m == 0 && my_timer.time.s == 1) {
		var webViewCacheDir = Ti.Filesystem.getFile(Ti.Filesystem.applicationCacheDirectory, "webviewCache");
			Ti.API.info('CACHE DIR');
			Ti.API.info("cacheDiroctory=" + Ti.Filesystem.applicationCacheDirectory);
			Ti.API.info('CONTENTS');
			Ti.API.info(webViewCacheDir.getDirectoryListing());
			Ti.API.info('Array length before: ' + webViewCacheDir.getDirectoryListing().length);
			webViewCacheDir.deleteDirectory(true);
			Ti.API.info('Array length after: ' + webViewCacheDir.getDirectoryListing().length);
		windowC.remove(webviewC);
		webviewC.release();
		mapview.removeAllAnnotations();

		if (my_timer.time.s < 10) {
			display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
		} else {
			display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
		}
	}
}, function() {
}); 
	

	windowC.addEventListener('open',function(){

	if(Ti.App.Properties.getInt("vistaActiva")==0){
		Ti.App.Properties.setDouble("Extra",0);
		Ti.App.Properties.setDouble("Extra2",0);
	
		progress.animate({ width: 0, duration: 180000 });my_timer.start();
	}else if(Ti.App.Properties.getInt("vistaActiva")==1){

		sdial = true;
		if(Ti.App.Properties.hasProperty('yallegue')){
			sdial2 = true;
		}
		if(Ti.App.Properties.hasProperty('viajeiniciado')){
			btnCanViaje2.enabled=false;
			btnCanViaje2.backgroundColor='#c2c2c2';
			sdial3 = true;
		
			
		}
		
		viewPro.visible=false;
		Ti.App.Properties.setInt('recotizado2',1);
		 my_timer3.start();	
	
	}	
	});	
	
	if(Ti.App.Properties.getInt("vistaActiva")==0){
	webviewC = Ti.UI.createWebView({url : '/firebase/index.html',width:0,height:0,top:0,left:0,visible:false, cacheMode:Titanium.UI.Android.WEBVIEW_LOAD_NO_CACHE});
	
	Ti.Geolocation.getCurrentPosition(function(e) {
		if (e.error){
        	if (Ti.App.Properties.getInt('ingles')==1)
        	 {	alert('Unable to get current location.');}
        	else{alert('No se puede obtener ubicacion actual');}
        		return;
    		}else{
    			
    			
    		webviewC.addEventListener('load', function() {
			Ti.App.fireEvent('firebase:init', {
				latitude :latInicio, 
				longitude : lngInicio,
				id_chofer:0,
				id_choferAc:0,
				latInicio:latInicio,
				lngInicio:lngInicio,
				latDestino:latDestino,
				lngDestino:lngDestino,
				idTipoServicio:tipo_servicio,
				taxiRosa:taxiRosaSelect,
				nombreM:Ti.App.Properties.getString("NMujer"),
				id_user:Ti.App.Properties.getInt("id_usuario"),
				tel : Ti.App.Properties.getString("telefono"),
				radiofirebase:Ti.App.Properties.getInt('radiofirebase')
			});
		});
    }
    		    		
		
	});
	}else if(Ti.App.Properties.getInt("vistaActiva")==1){
		webviewC = Ti.UI.createWebView({url : '/firebase/index2.html',width:0,height:0,top:0,left:0,visible:false, cacheMode:Titanium.UI.Android.WEBVIEW_LOAD_NO_CACHE});
					Ti.Geolocation.getCurrentPosition(function(e) {
					if (e.error){
        				if (Ti.App.Properties.getInt('ingles')==1)
        	 		{	alert('Unable to get current location.');}
        				else{alert('No se puede obtener ubicacion actual');}
        				return;
    			}else{
    				
    				
    				webviewC.addEventListener('load', function() {
    					Ti.App.fireEvent('firebase:init2', {
						latitude :Ti.App.Properties.getDouble("latIN"), 
						longitude : Ti.App.Properties.getDouble("longIN"),
						id_chofer:Ti.App.Properties.getInt("id_Chofer"),
						choferA:Ti.App.Properties.getInt("id_Chofer"),
						latInicio:latInicio,
						lngInicio:lngInicio,
						latDestino:latDestino,
						lngDestino:lngDestino,
						idTipoServicio:tipo_servicio,
						taxiRosa:taxiRosaSelect,
						nombreC: Ti.App.Properties.getString("nombreChoferA"),
						id_user:Ti.App.Properties.getInt("id_usuario"),
						latC:Ti.App.Properties.getDouble("latChoferA"),
						lonC:Ti.App.Properties.getDouble("longChoferA"),
						radiofirebase:Ti.App.Properties.getInt('radiofirebase')
					});
				});
    		}
    	});
	windowC.add(webviewC);
}
	
	 
		Ti.Geolocation.getCurrentPosition(function(e) {
			if (e.error){
        	if (Ti.App.Properties.getInt('ingles')==1)
        	 {	alert('Unable to get current location.');}
        	else{alert('No se puede obtener ubicacion actual');}
        		return;
    		}
			mapview = Map.createView({mapType : Map.NORMAL_TYPE,
				region : {latitude:e.coords.latitude,longitude:e.coords.longitude,latitudeDelta:0.009,longitudeDelta:0.009,userLocation:true,visible:true},
				animate : true,
				regionFit : true,
				userLocation : true
			});
			vistaMapa.add(mapview);
			
			if (!e.success || e.error) {
					Ti.API.info('error:' + JSON.stringify(e.error));
					return;
			}
			
		});
	
		var vistaAceptado=Ti.UI.createView({width:'90%',heigth:'30%',top:'35%',bottom:'35%',zIndex:30,backgroundColor:'white',borderColor:'black',borderRadius:3,visible:false,zIndex:11});
		var txtAceptTitle=Ti.UI.createLabel({text:'Ficha Tecnica',center:0,top:'10%',color:'black',font:{fontSize:16}});
		var txtAcept=Ti.UI.createLabel({text:'El chofer aceptó el servicio, verifique la ficha',center:0,top:'30%',color:'black',font:{fontSize:13}});
		var btndialogAceptado=Ti.UI.createButton({title:'Aceptar',color:'white',backgroundColor:'#007fff',bottom:'5%',center:0,borderRadius:4});
		
		vistaAceptado.add(txtAceptTitle);
		vistaAceptado.add(txtAcept);
		vistaAceptado.add(btndialogAceptado);
		
		var vistaYallegue=Ti.UI.createView({width:'90%',heigth:'30%',top:'35%',bottom:'35%',zIndex:30,backgroundColor:'white',borderColor:'black',borderRadius:3,visible:false,zIndex:11});
		var txtYallegueTitle=Ti.UI.createLabel({text:'¡Hola!,Ya llegué',center:0,top:'10%',color:'black',font:{fontSize:16}});
		var txtYallegue=Ti.UI.createLabel({text:'Tu chofer ha llegado.',center:0,top:'30%',color:'black',font:{fontSize:13}});
		var btndialoYallegue=Ti.UI.createButton({title:'Aceptar',color:'white',backgroundColor:'#007fff',bottom:'5%',center:0,borderRadius:4});
		
		vistaYallegue.add(txtYallegueTitle);
		vistaYallegue.add(txtYallegue);
		vistaYallegue.add(btndialoYallegue);

		var vistaViajeiniciado=Ti.UI.createView({width:'90%',heigth:'30%',top:'35%',bottom:'35%',zIndex:30,backgroundColor:'white',borderColor:'black',borderRadius:3,visible:false,zIndex:11});
		var txtViajeiniciadoTitle=Ti.UI.createLabel({text:'Viaje iniciado',center:0,top:'10%',color:'black',font:{fontSize:16}});
		var txtViajeiniciado=Ti.UI.createLabel({text:'Tu viaje inició, pongase comodo.',center:0,top:'30%',color:'black',font:{fontSize:13}});
		var btndialoViajeiniciado=Ti.UI.createButton({title:'Aceptar',color:'white',backgroundColor:'#007fff',bottom:'5%',center:0,borderRadius:4});
		
		vistaViajeiniciado.add(txtViajeiniciadoTitle);
		vistaViajeiniciado.add(txtViajeiniciado);
		vistaViajeiniciado.add(btndialoViajeiniciado);
		
		var vistaFinalizado=Ti.UI.createView({width:'90%',heigth:'30%',top:'35%',bottom:'35%',zIndex:30,backgroundColor:'white',borderColor:'black',borderRadius:3,visible:false,zIndex:11});
		var txtFinalizadoTitle=Ti.UI.createLabel({text:'Viaje finalizado',center:0,top:'10%',color:'black',font:{fontSize:16}});
		var txtFinalizado=Ti.UI.createLabel({text:'Tu viaje ha finalizado',center:0,top:'30%',color:'black',font:{fontSize:13}});
		var btndialoFinalizado=Ti.UI.createButton({title:'Aceptar',color:'white',backgroundColor:'#007fff',bottom:'5%',center:0,borderRadius:4});
		
		vistaFinalizado.add(txtFinalizadoTitle);
		vistaFinalizado.add(txtFinalizado);
		vistaFinalizado.add(btndialoFinalizado);
		
		if(Ti.App.Properties.getInt('ingles')==1){
		txtAceptTitle.text='Data sheet';
		txtAcept.text='The driver accept the service, check the tab';
		btndialogAceptado.title='Accept';
		txtYallegueTitle.text='Hello! already I arrived';
		txtYallegue.text='Your driver has arrived.';
		btndialoYallegue.title='Accept';
		txtViajeiniciadoTitle.text='Travel started';
		txtViajeiniciado.text='Your trip started';
		btndialoViajeiniciado.title='Accept';
		txtFinalizadoTitle.text='Trip ended';
		txtFinalizado.text='Your trip is complete';
		btndialoFinalizado.title='Accept';
		}
		
	windowC.addEventListener('android:back', function(e){
    	if(Ti.App.Properties.hasProperty('yallegue')){
     		var CMapa= Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar','Cancelar'],message: '¿Esta seguro de volver al mapa?, se perderá el proceso actual',title: 'Mensaje'});
	  			CMapa.show();
	  			CMapa.addEventListener('click',function(e){
	  				if(e.index==0){
	  				Titanium.App.Properties.removeProperty('vistaActiva');
	  				
	  				Ti.App.fireEvent('fbController:salirmapa',{ 
					id_chofer:Ti.App.Properties.getInt("id_Chofer"),
					});
	  				
	  				Ti.App.fireEvent('fbController:finalizarviaje2', { 
					id_chofer:Ti.App.Properties.getInt("id_Chofer"),
					id_user:Ti.App.Properties.getInt("id_usuario")
					});
					Ti.App.Properties.setDouble("pagoInicial",0);
	  				var webViewCacheDir = Ti.Filesystem.getFile(Ti.Filesystem.applicationCacheDirectory, "webviewCache");
					Ti.API.info('CACHE DIR');
					Ti.API.info("cacheDiroctory=" + Ti.Filesystem.applicationCacheDirectory);
					Ti.API.info('CONTENTS');
					Ti.API.info(webViewCacheDir.getDirectoryListing());
					Ti.API.info('Array length before: ' + webViewCacheDir.getDirectoryListing().length);
					webViewCacheDir.deleteDirectory(true);
					Ti.API.info('Array length after: ' + webViewCacheDir.getDirectoryListing().length);
					mapa = require('/interface/mapa');
                  new mapa().open({modal:true});
   					windowC.close();
                   	var viewChildren = windowC.children;
						Ti.API.info(viewChildren);
						for (var i=0; i < viewChildren.length; i++) {
							var child = viewChildren[i];
						windowC.remove(child);	
					}	
                  	windowC.removeAllChildren();
                   	windowC.remove(webviewC);
				 	webviewC.release();
				  	webviewC = null;
				 	Ti.API.info('webviewC: ' + webviewC);
				 	windowC = null;
				 	Ti.API.info('windowC: ' + windowC);
	  				}	
	  			});
     	}
     	
    });		
	
	
	
	Ti.App.addEventListener('fbController:addMarker', function(e){
		
		Ti.App.Properties.setString("nombreChoferA",e.nombre);
		Ti.App.Properties.setDouble("latChoferA",e.latitude);
		Ti.App.Properties.setDouble("longChoferA",e.longitude);
		
		annotations[e.id] = Map.createAnnotation({
			driverId : e.id,
			latitude : e.latitude,
			longitude : e.longitude,
			image : '/images/autochofer.png',
			title : 'Chofer: '+ e.nombre
		});
		idc = e.id;
		mapview.addAnnotation(annotations[e.id]);
		mapview.region = {latitude:e.latitude, longitude:e.longitude, latitudeDelta:0.009, longitudeDelta:0.009};
		
		
	});
	
	Ti.App.addEventListener('fbController:moveMarker', function(est){
			
		Titanium.Geolocation.getCurrentPosition(function(e){
			if (e.error){
        	return;
    		 }else{
    		Ti.API.info(est.latitude);
    		 	Ti.API.info(est.longitude);
    		 if(est.latitude!=undefined && est.longitude!=undefined && est.id!=undefined && idc!='' ){ 	
    		annotations[est.id].setLatitude(est.latitude);
			annotations[est.id].setLongitude(est.longitude);
			mapview.setRegion({latitude:est.latitude,longitude:est.longitude,latitudeDelta:0.003, longitudeDelta:0.003});
			}
    		}
			
		 });
			
			if(est.eventualidad==1){
				$eventualidad=1000;
				idEv=est.idEv;
				costo_minutos=est.costo_minutos;
			}else if(est.eventualidad==0){
				$eventualidad=0;
				idEv=est.idEv;
				costo_minutos=est.costo_minutos;
			}
					
			
			
			if (est.servicioaceptado == 1 && idc!='') {
				if (sdial == false) {
						sa=1;
					
   						if(vaceptado==0){
   							
   							// vistaAceptado.visible=true;
   							
   								// Titanium.Android.NotificationManager.notify(0, 
					 				// Titanium.Android.createNotification({
    									// contentTitle: 'NUEVA ALERTA',
				    					// contentText : 'El chofer acepto el servicio, verifique la ficha',
				   						// tickerText: 'mensaje de SUBT',
				    					// contentIntent: Ti.Android.createPendingIntent({
				    						// intent: Ti.Android.createIntent({
 											// action: Ti.Android.ACTION_MAIN,
				    						// className: 'com.subtmx.subt.SubtActivity',
				    						// packageName: 'com.subtmx.subt'
				    						// })
				  						// }),
									// }));
   						vaceptado=1;
   						sonidoP1=0;
   						
   						}
   					
   					
   						// player1.play();			
   					}
			}
			if (est.yallegue == 1) {
				
   				if (sdial2 == false) {
   					if(vyallegue==0){
   						Ti.App.Properties.setInt('yallegue',1);
   						vistaYallegue.visible=true;
   						Titanium.Android.NotificationManager.notify(1, 
						Titanium.Android.createNotification({
    							contentTitle: 'NUEVA ALERTA',
				    			contentText : 'Tu chofer ha llegado.',
				    			tickerText: 'mensaje de SUBT',
				    			contentIntent: Ti.Android.createPendingIntent({
				    				intent: Ti.Android.createIntent({
 									action: Ti.Android.ACTION_MAIN,
				    				className: 'com.subtmx.subt.SubtActivity',
				    				packageName: 'com.subtmx.subt'
				    				})
				  				}),
						}));
						vyallegue=1;
   					}
   				  player2.play();	
   				}
			}
			
			if (est.viajeiniciado == 1) {
			
				if (sdial3 == false) {
				btnCanViaje.enabled=false;
				btnCanViaje2.enabled=false;
				btnCanViaje.backgroundColor='#c2c2c2';
				btnCanViaje2.backgroundColor='#c2c2c2';
   					
   					if(	viniciado==0){
   					vistaViajeiniciado.visible=true;
   					Ti.App.Properties.setInt('viajeiniciado',1);
   					Titanium.Android.NotificationManager.notify(2, 
					Titanium.Android.createNotification({
    				contentTitle: 'NUEVA ALERTA',
				    contentText : 'Tu viaje inició, pongase comodo.',
				    tickerText: 'mensaje de SUBT',
				    contentIntent: Ti.Android.createPendingIntent({
				    intent: Ti.Android.createIntent({
 					action: Ti.Android.ACTION_MAIN,
				    className: 'com.subtmx.subt.SubtActivity',
				    packageName: 'com.subtmx.subt'
				    })
				  }),
			}));	
   					viniciado=1;
   			}
   						
   				player3.play();
   				}
   			}
   			
   			
			if (est.finalizarviaje == 1) {
   				if (sdial4 == false) {
   						
   				if(vfinalizado==0){
   							
   					vistaFinalizado.visible=true;
   					Titanium.Android.NotificationManager.notify(3, 
					 Titanium.Android.createNotification({
    				contentTitle: 'NUEVA ALERTA',
				    contentText : 'Tu viaje a finalizado, favor de pagar.',
				    tickerText: 'mensaje de SUBT',
				    contentIntent: Ti.Android.createPendingIntent({
				    intent: Ti.Android.createIntent({
 					action: Ti.Android.ACTION_MAIN,
				    className: 'com.subtmx.subt.SubtActivity',
				    packageName: 'com.subtmx.subt'
				    })
				  }),
			}));	
   					vfinalizado=1;
   					sonidoP4=0;
   					
   				}
   					player4.play();	
   				}
   			}
	});
	
	
	btndialogAceptado.addEventListener('click', function(ed){
		vistaAceptado.visible=false;
	
		// Ti.App.Properties.setInt("vistaActiva",1);
		// Titanium.Android.NotificationManager.cancelAll();
		sdial = true;
		sonidoP1=1;
		player1.stop();
		player1.release();
		player1.null;
		function clearMycache(){
		Titanium.Network.removeAllSystemCookies();
		}
			  
		
// registerServ.open("POST", "http://subtmx.com//webservices/createservice.php");
// params = {
	// id_tipo_servicio : tipo_servicio,
	// id_usuario : Ti.App.Properties.getInt("id_usuario"),
	// id_chofer : Ti.App.Properties.getInt("id_Chofer"),
	// latitud_origen : latInicio,
	// longitud_origen : lngInicio,
	// latitud_destino : latDestino,
	// longitud_destino : lngDestino,
	// lugar_origen : Ti.App.Properties.getString("txtorigen"),
	// opcion_servicio : ti_ser,
	// tarifa_inicial : Ti.App.Properties.getDouble("tarifa_inicial"),
	// id_tipo_pago : 1,
	// destino : Ti.App.Properties.getString("txtdestino"),
	// costo_total : Ti.App.Properties.getDouble("ctD").toFixed(2)
// };
// registerServ.send(params); 

		
// registerServ.onload = function() {
	// json = this.responseText;
	// response = JSON.parse(json);
	// if (response.logged == true) {
		// Ti.App.Properties.setInt("id_servicio", response.id_servicio);
		// Ti.API.info('id_servicio:  ' + response.id_servicio);
// 
		// Ti.App.fireEvent('fbController:idservicio', {
			// id_chofer : Ti.App.Properties.getInt("id_Chofer"),
			// idser : response.id_servicio,
			// iduser : Ti.App.Properties.getInt("id_usuario"),
			// tel : Ti.App.Properties.getString("telefono")
// 
		// });
	// }
// };

});
	
	btndialoYallegue.addEventListener('click', function(ed){
			vistaYallegue.visible=false;
			Ti.App.fireEvent('fbController:Llego', { 
				id_chofer:Ti.App.Properties.getInt("id_Chofer")
			});
			sdial2 = true;
			sonidoP2=1;
			player2.stop();
			player2.release();
			player2.null;
			function clearMycache(){
			Titanium.Network.removeAllSystemCookies();
			}
			Titanium.Android.NotificationManager.cancelAll();
			
		
	});
	btndialoViajeiniciado.addEventListener('click', function(ed){
			vistaViajeiniciado.visible=false;
			Ti.App.fireEvent('fbController:Inicio', { 
				id_chofer:Ti.App.Properties.getInt("id_Chofer")
			});
			sdial3 = true;
			sonidoP3=1;
			player3.stop();
			player3.release();
			player3.null;
			function clearMycache(){
			Titanium.Network.removeAllSystemCookies();
			}
			Titanium.Android.NotificationManager.cancelAll();
		
	});
	
	if(Ti.App.Properties.getInt('ingles')==1){
		var dialCalif = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Accept'],message: 'Payment Successful.The amount of the transaction was: ',title: 'Payment Successful'});
	}else{
		var dialCalif = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'],message: 'Pago realizado correctamente, el monto de la transaccion fue: ',title: 'Pago realizado'});
	}
	
	
	btndialoFinalizado.addEventListener('click', function(ed){
		vistaFinalizado.visible=false;
		
		// if(Ti.App.Properties.getDouble("apagar")>Ti.App.Properties.getDouble('ctD')){
			// Ti.App.Properties.setDouble('ctD',Ti.App.Properties.getDouble("apagar"));
		// }
		// final=Ti.App.Properties.getDouble('ctD')-(parseFloat(Ti.App.Properties.getInt("Bono")))-Ti.App.Properties.getDouble("pagoInicial");
		// final2=Ti.App.Properties.getDouble('ctD')-(parseFloat(Ti.App.Properties.getInt("Bono")));
// 		
		// if(final<=0){
			// final=0;
		// }
		// if(final2<=0){
			// final2=0;
		// }
		
		if(Ti.App.Properties.getString("tiSer")==2){
		costo_minutos=0;	
		}
		if(Ti.App.Properties.hasProperty('re')){
		Ti.App.Properties.setDouble("re",(Ti.App.Properties.getDouble("re")+parseFloat(costo_minutos)+parseFloat($eventualidad)));
		}else{
		Ti.App.Properties.setDouble("re",(parseFloat(costo_minutos)+parseFloat($eventualidad)));
		}
				
	
		
		
			Titanium.Android.NotificationManager.cancelAll();
			sdial4 = true;
			sonidoP4=1;
			player4.stop();
			player4.release();
			player4.null;
			function clearMycache(){
			Titanium.Network.removeAllSystemCookies();
			}
			
         	// pagoCan.open("POST","http://subtmx.com/webservices/updateservicio.php");
  //          	params3 = {
//                        id_servicio: Ti.App.Properties.getInt("id_servicio"),
//                        costo_total: final2.toFixed(2)
//                    	 };
//         	pagoCan.send(params3);
			if(Ti.App.Properties.getDouble("re")==0){
			
           		endServReq.open("POST","http://subtmx.com/webservices/endServicios.php");
                    params = {
                    	id_servicio: Ti.App.Properties.getInt("id_servicio"),
                        km_adicional: Ti.App.Properties.getDouble("kmAdicional"),
                        costo_km_adicional:Ti.App.Properties.getDouble("kmCostoAdicional"),
                        hora_extra: "00:00:00",
                        costo_hora_extra: 0,
                        cargo_hora_pico: 0,
                        tiempo_espera_adicional:"00:00:00",
                        costo_espera_adicional: 0,
                        costo_total: Ti.App.Properties.getDouble("pagoInicial"),
                        id_eventualidad:idEv,
                        costo_pendiente:0,
                        status_promocion:bbit
                    };
      			endServReq.send(params);
      			dialCalif.message = 'Pago realizado correctamente';
            	dialCalif.show();
           		Titanium.App.Properties.removeProperty('vistaActiva');
            	Titanium.App.Properties.removeProperty('yallegue');
            	Titanium.App.Properties.removeProperty('viajeiniciado');
            	Titanium.App.Properties.removeProperty('tiSer');
            	Titanium.App.Properties.removeProperty("id_tipo_servicio");
				Titanium.App.Properties.removeProperty("tarifa_inicial");
				Titanium.App.Properties.removeProperty("num_telefono_chofer");
				Titanium.App.Properties.removeProperty("fotografiaC");
				Titanium.App.Properties.removeProperty("modelo");
				Titanium.App.Properties.removeProperty("nombre_chofer");
				Titanium.App.Properties.removeProperty("placa");
            	Titanium.App.Properties.removeProperty("re");
            	Ti.App.Properties.setDouble("pagoInicial",0);
            	
			}else{
				if(Ti.App.Properties.hasProperty("paypal")){
				pagoReq3.open("POST","http://subtmx.com/webservices/paypal/samples/BillingAgreements/DoReferenceTransaction.php");
                    params = {
                       	destino:Ti.App.Properties.getString("txtdestino"),
                       	id_biling:Ti.App.Properties.getString("id_biling2"),
                        name: Ti.App.Properties.getString('nombre'),
                        lastname: Ti.App.Properties.getString('apellido'),
                        id_usuario: Ti.App.Properties.getInt('id_usuario'),
                        id_servicio: Ti.App.Properties.getInt("id_servicio"),
                        descripcion_viaje: 'Tipo de viaje es: ' + carroSelect + ', Km recorridos: ' + Ti.App.Properties.getString('kmt'),
                        costo_total: Ti.App.Properties.getDouble("re").toFixed(2) 
                    };
            	pagoReq3.send(params);	
			}else if(Ti.App.Properties.hasProperty("coneckta")){
				pagoReq3.open("POST","http://subtmx.com/webservices/php-conekta-master/conekta_card.php");
                    params = {
                    	id_usuario: Ti.App.Properties.getInt('id_usuario'),
                    	costo_total: Ti.App.Properties.getDouble("re").toFixed(2), 
                        descripcion_viaje: 'Tipo de viaje es: ' + carroSelect + ', Km recorridos: ' + Ti.App.Properties.getString('kmt'),
                        id_servicio: Ti.App.Properties.getInt("id_servicio"),
                    };
           		pagoReq3.send(params);	
			}
			}
			
		
		
           	
           	
           	Ti.App.fireEvent('fbController:finalizarviaje', { 
				id_chofer:Ti.App.Properties.getInt("id_Chofer"),
				id_user:Ti.App.Properties.getInt("id_usuario")
				
			});
			
    		
	});
	
	
	
		
	pagoReq3.onload = function(){
            json = this.responseText;
            response = JSON.parse(json);
            if (response.logged == true){
            	
            	
            		endServReq.open("POST","http://subtmx.com/webservices/endServicios.php");
                    params = {
                    	id_servicio: Ti.App.Properties.getInt("id_servicio"),
                        km_adicional: Ti.App.Properties.getDouble("kmAdicional"),
                        costo_km_adicional:Ti.App.Properties.getDouble("kmCostoAdicional"),
                        hora_extra: "00:00:00",
                        costo_hora_extra: 0,
                        cargo_hora_pico: 0,
                        tiempo_espera_adicional:"00:00:00",
                        costo_espera_adicional: 0,
                        costo_total: Ti.App.Properties.getDouble("pagoInicial"),
                        id_eventualidad:idEv,
                        costo_pendiente:0,
                        status_promocion:bbit
                    };
      				endServReq.send(params);
            	
            	
           		
               	Titanium.App.Properties.removeProperty('vistaActiva');
            	Titanium.App.Properties.removeProperty('yallegue');
            	Titanium.App.Properties.removeProperty('viajeiniciado');
            	Titanium.App.Properties.removeProperty('tiSer');
            	Titanium.App.Properties.removeProperty("id_tipo_servicio");
				Titanium.App.Properties.removeProperty("tarifa_inicial");
				Titanium.App.Properties.removeProperty("num_telefono_chofer");
				Titanium.App.Properties.removeProperty("fotografiaC");
				Titanium.App.Properties.removeProperty("modelo");
				Titanium.App.Properties.removeProperty("nombre_chofer");
				Titanium.App.Properties.removeProperty("placa");
            	Titanium.App.Properties.removeProperty("re");
            	Ti.App.Properties.setDouble("pagoInicial",0);
           	
            	
            	// if(Ti.App.Properties.getInt('ingles')==1){
					// dialCalif.message= 'Payment Successful.The amount of the transaction was: $'+ final2.toFixed(2)+' MX';
				// }else{
				
            	// }
  					dialCalif.message = 'Pago realizado correctamente';
            		 dialCalif.show();
               }else{
            	Titanium.App.Properties.removeProperty('vistaActiva');
            	Titanium.App.Properties.removeProperty('yallegue');
            	Titanium.App.Properties.removeProperty('viajeiniciado');
            	Titanium.App.Properties.removeProperty('tiSer');
            	Titanium.App.Properties.removeProperty("id_tipo_servicio");
				Titanium.App.Properties.removeProperty("tarifa_inicial");
				Titanium.App.Properties.removeProperty("num_telefono_chofer");
				Titanium.App.Properties.removeProperty("fotografiaC");
				Titanium.App.Properties.removeProperty("modelo");
				Titanium.App.Properties.removeProperty("nombre_chofer");
				Titanium.App.Properties.removeProperty("placa");
            	Titanium.App.Properties.removeProperty("re");
            	Ti.App.Properties.setDouble("pagoInicial",0);
            	if(Ti.App.Properties.getInt('ingles')==1){
            		dialCalif.message= 'Could not make the payment';
            		dialCalif.title='Pending payment';
		       	}else{
            		dialCalif.message="No cuenta con saldo suficiente,verifique su metodo de pago";
            		dialCalif.title='Pago pendiente';
            	}
            	dialCalif.show();	
            }
    };
    
    endServReq.onload = function(){
            json = this.responseText;
            response = JSON.parse(json);
            if (response.logged == true){
            //Ti.App.Properties.setDouble("pagoInicial",0);
            }
    };
    
     		
    	dialCalif.addEventListener('click', function(ed){ 
    			if (ed.index === 0){
    				
    				
    				var webViewCacheDir = Ti.Filesystem.getFile(Ti.Filesystem.applicationCacheDirectory, "webviewCache");
					Ti.API.info('CACHE DIR');
					Ti.API.info("cacheDiroctory=" + Ti.Filesystem.applicationCacheDirectory);
					Ti.API.info('CONTENTS');
					Ti.API.info(webViewCacheDir.getDirectoryListing());
					Ti.API.info('Array length before: ' + webViewCacheDir.getDirectoryListing().length);
					webViewCacheDir.deleteDirectory(true);
					Ti.API.info('Array length after: ' + webViewCacheDir.getDirectoryListing().length);
					modalWindow = require('/interface/modalWindow');
                  	new modalWindow().open({modal:true});
                  	windowC.close();
                   	var viewChildren = windowC.children;
						Ti.API.info(viewChildren);
						for (var i=0; i < viewChildren.length; i++) {
							var child = viewChildren[i];
						windowC.remove(child);	
					}	
                  	windowC.removeAllChildren();
                   	windowC.remove(webviewC);
				 	webviewC.release();
				  	webviewC = null;
				 	Ti.API.info('webviewC: ' + webviewC);
				 	windowC = null;
				 	Ti.API.info('windowC: ' + windowC);
            							
    			}else{
    				var webViewCacheDir = Ti.Filesystem.getFile(Ti.Filesystem.applicationCacheDirectory, "webviewCache");
					Ti.API.info('CACHE DIR');
					Ti.API.info("cacheDiroctory=" + Ti.Filesystem.applicationCacheDirectory);
					Ti.API.info('CONTENTS');
					Ti.API.info(webViewCacheDir.getDirectoryListing());
					Ti.API.info('Array length before: ' + webViewCacheDir.getDirectoryListing().length);
					webViewCacheDir.deleteDirectory(true);
					Ti.API.info('Array length after: ' + webViewCacheDir.getDirectoryListing().length);
					modalWindow = require('/interface/modalWindow');
                  	new modalWindow().open({modal:true});
                   	windowC.close();
                  var viewChildren = windowC.children;
						Ti.API.info(viewChildren);
						for (var i=0; i < viewChildren.length; i++) {
							var child = viewChildren[i];
						windowC.remove(child);	
					}	
                  	windowC.removeAllChildren();
                   	windowC.remove(webviewC);
				 	webviewC.release();
				  	webviewC = null;
				 	Ti.API.info('webviewC: ' + webviewC);
				 	windowC = null;
				 	Ti.API.info('windowC: ' + windowC);
            	
    			}
    	});
	
    	 args = arguments[0] || {};url = "http://subtmx.com/webservices/choferprueba.php";db = {};
    	function getData(){
			var xhr = Ti.Network.createHTTPClient({
				onload: function() {
					db = JSON.parse(this.responseText);
					if(db.servicios){
						setData(db);
					}
				},
				onerror: function(e) {
					Ti.API.debug("STATUS: " + this.status);
					Ti.API.debug("TEXT:   " + this.responseText);
					Ti.API.debug("ERROR:  " + e.error);
					if(Ti.App.Properties.getInt('ingles')==1){
        				alert ( 'Please check your internet connection');
        			}else{
        				alert('Comprueba tu conexion a internet');
        			}
					
				},
					timeout:5000
			});	
				xhr.open('POST', url);
						var params = {
    							id_chofer:idc    							
	   					};
				if(sa != 0){
    			xhr.send(params);
    			Ti.API.info(sa);
    			Ti.API.info(idc);
    		}
		}
		
		function setData(db){
			
			if(Ti.App.Properties.getInt("vistaActiva")==0){	
				
				for(var i = 0; i < db.servicios.length; i++){
			 			
						
				var kmformateado=Ti.App.Properties.getString("distancias").split(',');
		 		var kmWeb = kmformateado[0] + '.' + kmformateado[1];
		 
    			var dus= parseFloat(kmWeb) + Ti.App.Properties.getDouble("Extra2");
			
				Ti.App.Properties.setString("kmt",dus);
				choferactivo =db.servicios[i].id_chofer;
				Ti.App.Properties.setInt("id_Chofer",choferactivo);
				idch.text = db.servicios[i].id_chofer;
				viewP = Titanium.UI.createView({top:'10%', width:'100%',height:'auto',backgroundColor:'white',zIndex:3});
			
				tarifa_inicial = Ti.UI.createLabel({text:'Tarifa inicial:',left:'2%',top:'10%',color:'black'});
				modelo = Ti.UI.createLabel({text:'Modelo:',left:'2%',top:'10%',color:'black'});
				conductor = Ti.UI.createLabel({text:'Conductor:',left:'2%',top:'16%',color:'black'});
				placaslbl = Ti.UI.createLabel({text:'Placas:',left:'2%',top:'22%',color:'black'});
				dt = Ti.UI.createLabel({text:'Distancia total:',left:'2%',top:'28%',color:'black'});
				kmadicional = Ti.UI.createLabel({text:'Km adicional:',left:'2%',top:'40%',visible:false,color:'black'});
				costoadicional = Ti.UI.createLabel({text:'Costo adicional:',left:'2%',top:'46%',visible:false,color:'black'});
				costoTOTAL = Ti.UI.createLabel({text:'Costo aproximado:',left:'2%',top:'34%',visible:true,color:'black'});
				lblTel = Ti.UI.createLabel({text:'Telefono:',left:'2%',top:'40%',visible:true,color:'black'});
				horario_inicio = Ti.UI.createLabel({text:'$' + db.servicios[i].tarifa_inicial + ' MXN',color:'#4ad34d',right:'2%',top:'10%'});
				ti=Ti.UI.createLabel({text:db.servicios[i].tarifa_inicial});
				telefono.text = db.servicios[i].num_telefono_chofer;
				ImgPerfil.image = "http://subtmx.com/images/"+db.servicios[i].fotografia.replace(/\s/g, '%20');
				lugar_origen = Ti.UI.createLabel({text:db.servicios[i].modelo,color:'#a2a1b8',right:'2%',top:'10%',font:{fontSize:14}});
				nombre_chofer = Ti.UI.createLabel({text:db.servicios[i].nombre_chofer,color:'#a2a1b8',right:'2%',top:'16%',font:{fontSize:14}});
				placas = Ti.UI.createLabel({text:db.servicios[i].placa,color:'#a2a1b8',right:'2%',top:'22%',font:{fontSize:14}});
				estrellas = Ti.UI.createLabel({right:'2%', top:'28%',color:'#a2a1b8',text:dus.toFixed(1)+' KM',font:{fontSize:14}});
				lblAdicional = Ti.UI.createLabel({right:'2%', top:'40%',color:'#a2a1b8',font:{fontSize:14},visible:false});
				lblcosto = Ti.UI.createLabel({right:'2%', top:'46%',color:'#4ad34d',font:{fontSize:14},visible:false});
				lblTotal = Ti.UI.createLabel({right:'2%', top:'34%',color:'#4ad34d',font:{fontSize:14},visible:true});
				telefonoimg = Titanium.UI.createButton({backgroundImage:'/images/telefono.png', top:'40%', right:'2%', width:'5%', height:'4%'}); 
				Ti.App.Properties.setDouble("km_inicial",db.servicios[i].km_inicial);
				Ti.App.Properties.setInt("id_tipo_servicio",db.servicios[i].id_tipo_servicio);
				Ti.App.Properties.setDouble("tarifa_inicial",db.servicios[i].tarifa_inicial);
				Ti.App.Properties.setString("num_telefono_chofer",db.servicios[i].num_telefono_chofer);
				Ti.App.Properties.setString("fotografiaC",db.servicios[i].fotografia.replace(/\s/g, '%20'));
				Ti.App.Properties.setString("modelo",db.servicios[i].modelo);
				Ti.App.Properties.setString("nombre_chofer",db.servicios[i].nombre_chofer);
				Ti.App.Properties.setString("placa",db.servicios[i].placa);
				Ti.App.Properties.setDouble("precio_km_adicional",db.servicios[i].precio_km_adicional);
				
				telefonoimg.addEventListener('click', function(e) {
 		
				var dialog = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar', 'Cancelar'],message: '¿Desea usted realizar esta llamada?',title: 'LLamada'});
   		     
   				dialog.addEventListener('click', function(ed){    
            	if (ed.index === 0){
            	 if (Titanium.Platform.version >= '6.0') {
 					var intent = Ti.Android.createIntent({     action: Ti.Android.ACTION_DIAL,     data: 'tel:' + telefono.text }); Ti.Android.currentActivity.startActivity(intent);
 				}else{
 					var intent = Ti.Android.createIntent({     action: Ti.Android.ACTION_CALL,     data: 'tel:' + telefono.text }); Ti.Android.currentActivity.startActivity(intent);
 				}		
            
            	
   			}
         });
         dialog.show();
	});
				
				if(Ti.App.Properties.getInt('ingles')==1){
        				tarifa_inicial.text='Initial rate:';
        				modelo.text='Model';
        				conductor.text='Driver';
        				placaslbl.text='license plate';
        				dt.text='Total distance:';
        				kmadicional.text='Additional km:';
        				costoadicional.text='Additional cost:';
        				costoTOTAL.text='Total Approximate cost:';
        		}
				
				var nuevoKMT;var newCosto;var tarifaFinal;
				  if (Ti.App.Properties.getString("tiSer")==1){
					if (dus>db.servicios[i].km_inicial) {
						
						var	newkmad=dus-db.servicios[i].km_inicial;
							lblAdicional.text = newkmad;
							Ti.App.Properties.setDouble("kmAdicional",lblAdicional.text);
							//kmadicional.visible = true;
							//lblAdicional.visible = true;
							Ti.API.info('tipo newkmad: ' +newkmad);
							
							if (db.servicios[i].id_tipo_servicio == 1) {
								newCosto = newkmad * db.servicios[i].precio_km_adicional;
								Ti.App.Properties.setDouble("precio_km_adicional",db.servicios[i].precio_km_adicional);
							}else if (db.servicios[i].id_tipo_servicio == 2){
								newCosto = newkmad * db.servicios[i].precio_km_adicional;
								Ti.App.Properties.setDouble("precio_km_adicional",db.servicios[i].precio_km_adicional);
							}else if (db.servicios[i].id_tipo_servicio == 3){
								newCosto = newkmad * db.servicios[i].precio_km_adicional;
								Ti.App.Properties.setDouble("precio_km_adicional",db.servicios[i].precio_km_adicional);
							}
								Ti.API.info('tipo newCosto: ' +newCosto);
					
							tarifaA=newCosto;
							tarifaFinal=newCosto+db.servicios[i].tarifa_inicial*1;
							lblTotal.text = '$' + tarifaFinal.toFixed(2) +' MXN';
							Ti.App.Properties.setString("ta",tarifaA.toFixed(2));
							Ti.App.Properties.setString("ct",tarifaFinal.toFixed(2));
							Ti.App.Properties.setDouble("ctD",tarifaFinal.toFixed(2));
							lblTotal.visible = true;
							costoTOTAL.visible = true;					
						}else if (dus.toFixed(1) <=db.servicios[i].km_inicial) {
							
							lblTotal.visible = true;
							costoTOTAL.visible = true;	
							lblAdicional.text = '0';
							Ti.App.Properties.setDouble("kmAdicional",lblAdicional.text);
							tarifaA=0;
							Ti.App.Properties.setString("ta",tarifaA.toFixed(2));
							tarifaFinal=db.servicios[i].tarifa_inicial*1;
							lblTotal.text = '$' + tarifaFinal.toFixed(2) +' MXN';
							Ti.App.Properties.setDouble("ctD",tarifaFinal.toFixed(2));
							Ti.App.Properties.setString("ct",tarifaFinal.toFixed(2));
				 			Ti.App.Properties.setDouble("kmCostoAdicional",tarifaA);
				 			}
						Ti.App.Properties.setDouble("kmCostoAdicional",tarifaA);
					}else if (Ti.App.Properties.getString("tiSer")==2){
							lblTotal.visible = true;
							costoTOTAL.visible = true;	
						     	tarifaFinal=response.tarifa_hora * Ti.App.Properties.getInt("txtserviciohora");;
								lblTotal.text='$' + tarifaFinal.toFixed(2) +' MXN';
								Ti.App.Properties.setDouble("ctD",tarifaFinal.toFixed(2));
								Ti.App.Properties.setDouble("kmAdicional",0);
								Ti.App.Properties.setDouble("kmCostoAdicional",0);
					}
				
		}
			//viewP.add(horario_inicio);
				//viewP.add(tarifa_inicial);
				viewP.add(lugar_origen);
				viewP.add(nombre_chofer);
				viewP.add(placas);
				viewP.add(estrellas);
				viewP.add(modelo);
				viewP.add(conductor);
				viewP.add(placaslbl);
				viewP.add(dt);
				viewP.add(lblAdicional);
				viewP.add(kmadicional);
				viewP.add(lblcosto);
				viewP.add(costoadicional);
				viewP.add(lblTotal);
				viewP.add(costoTOTAL);
				viewP.add(lblTel);
				viewP.add(telefonoimg);
			
				if(Ti.App.Properties.getString("tiSer")==1){
				viewP.add(btnCanViaje);
				viewP.add(btnRecotizar);
				}else if(Ti.App.Properties.getString("tiSer")==2){
				
				}
				
				windowC.add(ImgPerfil);
				centerView.add(viewP);
				windowC.add(vistaMapa);
			
			var pagoinicial=Ti.App.Properties.getDouble('ctD')-(parseFloat(Ti.App.Properties.getInt("Bono")));
			if (pagoinicial<=0){
				pagoinicial=0;
			}
			Ti.App.Properties.setDouble("apagar",Ti.App.Properties.getDouble('ctD'));
			Ti.App.Properties.setDouble("pagoInicial",pagoinicial.toFixed(2));
			
			if (pagoinicial<=0){
			Ti.App.Properties.setInt("vistaActiva", 1);
			viewPro.visible=false;
			Titanium.Android.NotificationManager.notify(0, Titanium.Android.createNotification({
				contentTitle : 'NUEVA ALERTA',
				contentText : 'El chofer acepto el servicio, verifique la ficha',
				tickerText : 'mensaje de SUBT',
				contentIntent : Ti.Android.createPendingIntent({
					intent : Ti.Android.createIntent({
						action : Ti.Android.ACTION_MAIN,
						className : 'com.subtmx.subt.SubtActivity',
						packageName : 'com.subtmx.subt'
					})
				}),
			}));

			vistaAceptado.visible = true;
			player1.play();

			registerServ.open("POST", "http://subtmx.com//webservices/createservice.php");
			params = {
				id_tipo_servicio : tipo_servicio,
				id_usuario : Ti.App.Properties.getInt("id_usuario"),
				id_chofer : Ti.App.Properties.getInt("id_Chofer"),
				latitud_origen : latInicio,
				longitud_origen : lngInicio,
				latitud_destino : latDestino,
				longitud_destino : lngDestino,
				lugar_origen : Ti.App.Properties.getString("txtorigen"),
				opcion_servicio : ti_ser,
				tarifa_inicial : Ti.App.Properties.getDouble("tarifa_inicial"),
				id_tipo_pago : 1,
				destino : Ti.App.Properties.getString("txtdestino"),
				costo_total : Ti.App.Properties.getDouble("ctD").toFixed(2),
				status_promocion:bbit
			};
			registerServ.send(params);

			Ti.App.fireEvent('fbController:saldoinsuficiente', {
				id_chofer : Ti.App.Properties.getInt("id_Chofer"),
				saldo : 0
			});
			}else{
			if (Ti.App.Properties.hasProperty("paypal")) {
				pagoReq.open("POST", "http://subtmx.com/webservices/paypal/samples/BillingAgreements/DoReferenceAeropuerto.php");
				params = {
					id_biling : Ti.App.Properties.getString("id_biling2"),
					name : Ti.App.Properties.getString('nombre'),
					lastname : Ti.App.Properties.getString('apellido'),
					descripcion_viaje : 'Tipo de viaje es: ' + carroSelect + ', Km recorridos: ' + Ti.App.Properties.getString('kmt'),
					costo_total : pagoinicial.toFixed(2),
				};
				pagoReq.send(params);
			} else if (Ti.App.Properties.hasProperty("coneckta")) {
				pagoReq.open("POST", "http://subtmx.com/webservices/php-conekta-master/conekta_card.php");
				params = {
					id_usuario : Ti.App.Properties.getInt('id_usuario'),
					costo_total :pagoinicial.toFixed(2),
					descripcion_viaje : 'Tipo de viaje es: ' + carroSelect + ', Km recorridos: ' + Ti.App.Properties.getString('kmt'),

				};
				pagoReq.send(params);
		
			}
		}
		
		}else if(Ti.App.Properties.getInt("vistaActiva")==1){
			
			
		}
			
		
		}
		
		
pagoReq.onerror = function(event){
       alert('Error de conexion: ' + JSON.stringify(event));
};
pagoReq.onload = function() {
	json = this.responseText;
	response = JSON.parse(json);
	
	if (response.logged != null) {
		if (response.logged == true) {
			
			Ti.App.Properties.setInt("vistaActiva", 1);
			viewPro.visible=false;
			Titanium.Android.NotificationManager.notify(0, Titanium.Android.createNotification({
				contentTitle : 'NUEVA ALERTA',
				contentText : 'El chofer acepto el servicio, verifique la ficha',
				tickerText : 'mensaje de SUBT',
				contentIntent : Ti.Android.createPendingIntent({
					intent : Ti.Android.createIntent({
						action : Ti.Android.ACTION_MAIN,
						className : 'com.subtmx.subt.SubtActivity',
						packageName : 'com.subtmx.subt'
					})
				}),
			}));

			vistaAceptado.visible = true;
			player1.play();

			registerServ.open("POST", "http://subtmx.com//webservices/createservice.php");
			params = {
				id_tipo_servicio : tipo_servicio,
				id_usuario : Ti.App.Properties.getInt("id_usuario"),
				id_chofer : Ti.App.Properties.getInt("id_Chofer"),
				latitud_origen : latInicio,
				longitud_origen : lngInicio,
				latitud_destino : latDestino,
				longitud_destino : lngDestino,
				lugar_origen : Ti.App.Properties.getString("txtorigen"),
				opcion_servicio : ti_ser,
				tarifa_inicial : Ti.App.Properties.getDouble("tarifa_inicial"),
				id_tipo_pago : 1,
				destino : Ti.App.Properties.getString("txtdestino"),
				costo_total : Ti.App.Properties.getDouble("ctD").toFixed(2),
				status_promocion:bbit
			};
			registerServ.send(params);

			Ti.App.fireEvent('fbController:saldoinsuficiente', {
				id_chofer : Ti.App.Properties.getInt("id_Chofer"),
				saldo : 0
			});
		} else {

			Ti.App.fireEvent('fbController:saldoinsuficiente', {
				id_chofer : Ti.App.Properties.getInt("id_Chofer"),
				saldo : 1
			});

			if (Ti.App.Properties.getInt('ingles') == 1) {
				var dialogChofer = Ti.UI.createAlertDialog({
					cancel : 1,
					buttonNames : ['Accept'],
					message : 'Unable to process your request',
					title : 'Message'
				});
			} else {
				var dialogChofer = Ti.UI.createAlertDialog({
					cancel : 1,
					buttonNames : ['Aceptar'],
					message : 'No cuenta con saldo suficiente,verifique su metodo de pago',
					title : 'Mensaje'
				});
			}
			dialogChofer.show();
			dialogChofer.addEventListener('click', function(ed) {

				var webViewCacheDir = Ti.Filesystem.getFile(Ti.Filesystem.applicationCacheDirectory, "webviewCache");
				Ti.API.info('CACHE DIR');
				Ti.API.info("cacheDiroctory=" + Ti.Filesystem.applicationCacheDirectory);
				Ti.API.info('CONTENTS');
				Ti.API.info(webViewCacheDir.getDirectoryListing());
				Ti.API.info('Array length before: ' + webViewCacheDir.getDirectoryListing().length);
				webViewCacheDir.deleteDirectory(true);
				Ti.API.info('Array length after: ' + webViewCacheDir.getDirectoryListing().length);
				mapa = require('/interface/mapa');
				new mapa().open({
					modal : true
				});
				windowC.close();
				var viewChildren = windowC.children;
				Ti.API.info(viewChildren);
				for (var i = 0; i < viewChildren.length; i++) {
					var child = viewChildren[i];
					windowC.remove(child);
				}
				windowC.removeAllChildren();
				windowC.remove(webviewC);
				webviewC.release();
				webviewC = null;
				Ti.API.info('webviewC: ' + webviewC);
				windowC = null;
				Ti.API.info('windowC: ' + windowC);
			});

		}
	} else {
		alert("Error de conexión, intente nuevamente");
	}
}; 

	

registerServ.onload = function() {
	json = this.responseText;
	response = JSON.parse(json);
	if (response.logged == true) {
		Ti.App.Properties.setInt("id_servicio", response.id_servicio);
		Ti.API.info('id_servicio:  ' + response.id_servicio);

		Ti.App.fireEvent('fbController:idservicio', {
			id_chofer : Ti.App.Properties.getInt("id_Chofer"),
			idser : response.id_servicio,
			iduser : Ti.App.Properties.getInt("id_usuario"),
			tel : Ti.App.Properties.getString("telefono")

		});
		
	   				
		endServReq.open("POST","http://subtmx.com/webservices/endServicios.php");
                    params = {
                    	id_servicio: Ti.App.Properties.getInt("id_servicio"),
                        km_adicional: Ti.App.Properties.getDouble("kmAdicional"),
                        costo_km_adicional:Ti.App.Properties.getDouble("kmCostoAdicional"),
                        hora_extra: "00:00:00",
                        costo_hora_extra: 0,
                        cargo_hora_pico: 0,
                        tiempo_espera_adicional:"00:00:00",
                        costo_espera_adicional: 0,
                        costo_total: Ti.App.Properties.getDouble("pagoInicial"),
                        id_eventualidad:idEv,
                        costo_pendiente:0,
                        status_promocion:bbit
                    };
       endServReq.send(params);
		
	}
}; 
					
		 		
				
		
 		header.add(titulo);
 		centerView.add(header);
 		centerView.add(fondoP); 
 		
 		
		centerView.add(btnCall);
	
   		windowC.add(vistaAceptado);
   		windowC.add(vistaYallegue);
   		windowC.add(vistaViajeiniciado);
   		windowC.add(vistaFinalizado);
   		   		
        viewPro.add(display_lbl);
		viewPro.add(buscando_lbl);
		track.add(progress); 
 		viewPro.add(track); 
        windowC.add(centerView);
        windowC.add(viewPro);

	return windowC;
};
module.exports = chofer;