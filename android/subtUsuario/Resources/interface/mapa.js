function mapa(navwin){
	var Map = require('ti.map');
	var windowM,search,share,menu,navwin,mapview,btnPedir,winLeft,navController,logoLeft,mainwin,lblMisPedidos,lblMiPerfil,lblPromociones,lblPagos,pago,winNav,btnLista,vistaBottom,autoCheck,txtAddress,servSel;
  
    windowM= Ti.UI.createWindow({theme:'Theme.AppCompat.Translucent.NoTitleBar',backgroundColor:'black'});   
    windowM.orientationModes = [Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT];
    Ti.App.mapa=windowM;   
    menuView = Ti.UI.createView({backgroundImage:'/images/bgMenu.jpg',height: 'auto',left: '-75%',  top:0,width: '75%',zIndex: 9,   _toggle: false});
    lblVersion  = Ti.UI.createTextField({value:'Versión:'+Ti.App.version,color:'#151431',left:'13%',top:'5%',zIndex:4,font:{fontSize:'12%'},editable:false,focus:false});
 	
    lblUSer  = Ti.UI.createTextField({value:Ti.App.Properties.getString('usuario'),color:'#151431',left:'13%',top:'10%',zIndex:4,font:{fontSize:'17%'},editable:false,focus:false});
 	var destinoSeleccionado=0,origenSeleccionado=0;
 		var vistaCotizacion= Ti.UI.createView({width:'100%', height:'100%', visible:false,zIndex: 20,backgroundColor:'#B3000000'});
   		var vistaCotizar= Ti.UI.createView({width:'80%', height:Ti.UI.SIZE,zIndex: 20,backgroundColor:'white',borderColor:'black',borderRadius:6,layout:'vertical'});
   		var vistatxt= Ti.UI.createView({width:'90%', height:Ti.UI.SIZE, layout:'vertical',top:30});
   		var txtCotizar=Titanium.UI.createLabel({text:'COTIZACIÓN',top:0,color:'black',font:{fontSize:18,fontFamily: 'Menlo Bold Italic'}});
   		var txtCosto=Titanium.UI.createLabel({text:'Costo Aproximado',top:10,color:'black'});
   		var txtPrecio=Titanium.UI.createLabel({text:'$450.00',top:10,font:{fontSize: 25,fontFamily: 'SanFranciscoText-Regular'},color:'#3498db'});
   		var txtAuto=Titanium.UI.createLabel({text:'Tipo de Auto:',top:10,color:'black'});
   		var txtviajeOrigen=Titanium.UI.createLabel({text:'Origen:',top:10,color:'black'});
   		var txtviajeDestino=Titanium.UI.createLabel({text:'Destino:',top:10,color:'black'});
   		var txtmensaje=Titanium.UI.createLabel({text:'¿Desea solicitar el servicio?',top:10,color:'#2980b9',font:{fontSize: 15,fontFamily: 'Menlo Bold Italic'},textAlign:'center'});
   		var vistaBotones= Ti.UI.createView({width:Ti.UI.SIZE, height:Ti.UI.SIZE,layout:'horizontal',top:20});
   		var vistaBotones2= Ti.UI.createView({width:'100%', height:30,layout:'horizontal',top:0});
   		btnCotizar = Titanium.UI.createButton({title:'Aceptar',center:0,backgroundColor:'#0c83c7',borderRadius:5});
    	btnCotizar2 = Titanium.UI.createButton({title:'Cancelar',left:4,backgroundColor:'#0c83c7',borderRadius:5});
 		var player = Ti.Media.createAudioPlayer({url:"/sound/sonido.mp3"});
 		var random=Math.floor(Math.random() * 1000000);
 		
 	leftButtonOrigen = Titanium.UI.createButton({backgroundImage:'/images/green.png',width:30,height:30,left:'3%'});
    leftButtonDestino = Titanium.UI.createButton({backgroundImage:'/images/red.png',width:30,height:30,left:'3%'});
 	origenView =Ti.UI.createView({height: '8%',center:0,top: '12%',width:'90%',zIndex:1,backgroundColor:'white',zIndex:12});
    destinoView =Ti.UI.createView({height: '8%',center:0,top: '22%',width:'90%',zIndex:1,backgroundColor:'white',zIndex:12});
    txtOrigen = Titanium.UI.createTextField({backgroundColor:'white',borderRadius:5,color:'black',hintTextColor:'gray',hintText:"Ingrese su Origen",top:'0%',left:'15%',width:'70%',height:'100%'});
    txtDestino = Titanium.UI.createTextField({backgroundColor:'white',borderRadius:5,color:'black',hintTextColor:'gray',hintText:"Ingrese su Destino",top:'0%',left:'15%',width:'70%',height:'100%',editable:false});
  	var origen;
    var destino;
    logoLeft = Titanium.UI.createImageView({image:'/images/logo.png',width:'55%',height:'auto',zIndex:2,left:'15%',top:'20%',opacity:1});
    lblHome = Titanium.UI.createLabel({color:'blue',text:'Home',top:'52%',left:'13%'});
    lblMisPedidos = Titanium.UI.createLabel({color:'black',text:'Historial',top:'60%',left:'13%'});
    lblMiPerfil = Titanium.UI.createLabel({color:'black',text:'Mi Perfil',top:'68%',left:'13%'});
    lblPromociones = Titanium.UI.createLabel({color:'black',text:'Promociones',top:'76%',left:'13%'});
    lblPagos = Titanium.UI.createLabel({color:'black',text:'Pago',top:'84%',left:'13%'});  
    lblConfiguración = Titanium.UI.createLabel({color:'black',text:'Configuración',top:'92%',left:'13%'}); 
    var vistavistaTaxiRosa= Ti.UI.createView({width:'100%', height:'100%', visible:false,zIndex: 20,backgroundColor:'#B3000000'});
    vistaTaxiRosa=Ti.UI.createView({width:'90%',height:Ti.UI.SIZE,zIndex:13,backgroundColor:'white',borderColor:'black',layout:'vertical'});
   	vistaTaxiRosa2=Ti.UI.createView({width:Ti.UI.SIZE,height:Ti.UI.SIZE,layout:'horizontal',center:0,top:15});
   	txtvistaTaxiRosa = Titanium.UI.createTextField({center:0,hintText:'Escribe el nombre de la mujer y parentesco',width:'80%',height:'auto',top:15,color:'black',hintTextColor:'gray',font:{fontSize:'12%'}});
   	btnvistaTaxiRosa =Ti.UI.createButton({title:'Aceptar',width:'auto',height:'auto',center:0,backgroundColor:'#0c83c7',bottom:20,borderRadius:6});   
   	btnvistaTaxiRosa2 =Ti.UI.createButton({title:'Cancelar',width:'auto',height:'auto',left:20,backgroundColor:'#0c83c7',bottom:20,borderRadius:6});         
    centerView =Ti.UI.createView({height: 'auto',right:0,top: 0,width:'100%',zIndex:1});
    header = Ti.UI.createView({backgroundColor: 'white',height: '10%',right:0,top: 0,width:'100%',zIndex: 3});
    titulo=Ti.UI.createLabel({text:'SUBT', color:'#232323',center:0,bottom:'25%',zIndex:2,font:{fontSize:'15.5%'}});
	var vistavistaAeropuerto= Ti.UI.createView({width:'100%', height:'100%', visible:false,zIndex: 10,backgroundColor:'#B3000000'});
	vistaAeropuerto = Ti.UI.createView({backgroundColor:'white',height:Ti.UI.SIZE,width:'90%',zIndex:13,borderColor:'black',layout:'vertical'});
	vistaAeropuerto2 = Ti.UI.createView({ height:Ti.UI.SIZE,width:'100%',top:20});
	txtvistaAeropuerto = Titanium.UI.createTextField({left:"3%",hintText:'N° de vuelo',width:'30%',height:'auto',color:'black',hintTextColor:'gray',font:{fontSize:'15%'},textAlign:'center'});
   	txtvistaAeropuerto2 = Titanium.UI.createTextField({right:"3%",hintText:'Hora de vuelo',width:'50%',height:'auto',color:'black',hintTextColor:'gray',font:{fontSize:'15%'},editable:false,textAlign:'center'});
   	btnvistaAeropuerto =Ti.UI.createButton({title:'Aceptar',width:'auto',height:'auto', top:20,center:0,font:{size:'10%'},backgroundColor:'#0c83c7',bottom:20,borderRadius:6});   
  	player = Ti.Media.createSound({url:"/sound/sonido.mp3"});
	
    vistaBottom = Ti.UI.createView({backgroundColor:'white',height:'10%',bottom:0,width:'100%',zIndex: 3});
    btnLista = Titanium.UI.createButton({width:'8%',height:'80%',left:'3%',zIndex:3,backgroundImage:'/images/list.png'});
 	autoCheck = Ti.UI.createSwitch({value:false,top:'30%',onTintColor:'#f9c1e5',tintColor:'#D90019',right:'2%'});
    listText= Ti.UI.createLabel({text:'Tipo viaje',left:'20%',color:'black'});
  	autoRosa= Ti.UI.createLabel({text:'Tipo rosa',right:'20%',color:'black'}); 	
    iconHeader = Ti.UI.createButton({   backgroundImage:'/images/menuicon.png', left:'3%',zIndex:2,height:'42%',width:'8.5%'});
    iconShare = Ti.UI.createButton({ backgroundImage:'/images/Info.png',  right:'3%',zIndex:10,height:'48%',width:'7%'});
    btnPedir = Titanium.UI.createButton({width:'70%',height:'auto',title:'Pedir taxi',textAlign:'center',center:0,bottom:'15%',color:'white',zIndex:3,backgroundColor:'#0c83c7',borderRadius:6});
  	pinUsuario = Map.createAnnotation({title:"Origen actual",image:'/images/pinOrigen.png',draggable:true});
    pinDestino = Map.createAnnotation({title:"Destino actual",image:"/images/pinDestino.png",draggable:true}); 
   // txtAddress = Titanium.UI.createTextField({hintText:'Ingrese su destino',hintTextColor:'gray',left:'14.5%',width:'60%',height:'auto',borderRadius:5,zIndex:5,color:'black'});
   	showBtn = Ti.UI.createButton({width:'7%',height:'47%',right:'13%',borderRadius:5,backgroundImage:'/images/iconsearch.png',zIndex:5});
   	pedirKm= Titanium.Network.createHTTPClient();
   	bonoHttp=Titanium.Network.createHTTPClient();
   	idPpal=Titanium.Network.createHTTPClient();
   	registerServ = Titanium.Network.createHTTPClient();
   	pagoReq = Titanium.Network.createHTTPClient();
   	pagoReq2 = Titanium.Network.createHTTPClient();
	PagoPen= Titanium.Network.createHTTPClient();
	endServReq = Titanium.Network.createHTTPClient();
	recibo = Titanium.Network.createHTTPClient();
	rango = Titanium.Network.createHTTPClient();
	rango2 = Titanium.Network.createHTTPClient();
	tarifasAero= Titanium.Network.createHTTPClient();
	cotizarauto= Titanium.Network.createHTTPClient();
	disbloqueado= Titanium.Network.createHTTPClient();
	disbloqueado2= Titanium.Network.createHTTPClient();
	idconeckta = Titanium.Network.createHTTPClient();
	versionApp = Titanium.Network.createHTTPClient();
	versionApp2 = Titanium.Network.createHTTPClient();
   	Ti.App.Properties.setString("NMujer",0);
   	var versionI=Ti.App.version;
   	versionI=versionI.split('.');
   	versionI=versionI[0]+''+versionI[1]+''+versionI[2];
    var version= Ti.UI.createView({width:'100%', height:'100%', visible:false,zIndex: 20,backgroundColor:'#B3000000'});
     var vistaversion= Ti.UI.createView({width:'80%', height:Ti.UI.SIZE,zIndex: 20,backgroundColor:'white',borderColor:'black',borderRadius:6,layout:'vertical'});
   	 var Tituloversion=Titanium.UI.createLabel({text:'VERSION ANTIGUA',top:20,color:'black',font:{fontSize:18,fontFamily: 'Menlo Bold Italic'}});
   	 var txtversion=Titanium.UI.createLabel({text:"Actualiza a la ultima versión",top:20,color:'#2980b9',font:{fontSize: 15,fontFamily: 'Menlo Bold Italic'},textAlign:'center',width:'80%'});
   	 var btnversion = Titanium.UI.createButton({title:'Aceptar',center:0,backgroundColor:'#0c83c7',top:10,borderRadius:5});
     var vistaversion2= Ti.UI.createView({width:'100%', height:30,layout:'horizontal',top:0});
    	
	version.add(vistaversion);
	vistaversion.add(Tituloversion);
	vistaversion.add(txtversion);
	vistaversion.add(btnversion);
	vistaversion.add(vistaversion2);
	
 
	btnversion.addEventListener('click', function() {
		var opcion_Actualizar = Ti.UI.createAlertDialog({
			buttonNames : ['Actualizar', 'Salir'],
			message : '¿Que desea hacer?',
			title : 'Mensaje'
		});
		opcion_Actualizar.show();
		opcion_Actualizar.addEventListener('click', function(e) {
			if (e.index == 0) {
				Ti.Platform.openURL("https://play.google.com/store/apps/details?id=com.subtmx.subt");
			} else if (e.index == 1) {
				Titanium.App.Properties.removeProperty('coneckta');
				Titanium.App.Properties.removeProperty('paypal');
				Titanium.App.Properties.removeProperty('id_usuario');
				Titanium.App.Properties.removeProperty('usuario');
				Titanium.App.Properties.removeProperty('email');
				Titanium.App.Properties.removeProperty('telefono');
				Titanium.App.Properties.removeProperty('rfc');
				Titanium.App.Properties.removeProperty('razon');
				inicio = require('/interface/inicio');
				new inicio().open({
					modal : true
				});
				windowM.close();
			} else {
			}
		});

	});

 
   	var bono;
   	var billing;
   	var pagar;
   	var dest;
    var hor;
    var b;
    var ids;
    var desc;
    var costo;
    var costoTo;
    var kma;
    var ckma;
    var Ideve;
   	
   	var countDown =  function( m , s, fn_tick, fn_end  ) {
		return {
			total_sec:m*60+s,
			timer:this.timer,
			set: function(m,s) {
				this.total_sec = parseInt(m)*60+parseInt(s);
				this.time = {m:m,s:s};
				return this;
			},
			start: function() {
				var self1 = this;
				this.timer = setInterval( function() {
					if (self1.total_sec) {
						self1.total_sec--;
						self1.time = { m : parseInt(self1.total_sec/60), s: (self1.total_sec%60) };
						fn_tick();
					}else {
						self1.stop();
						fn_end();
					}
		   		}, 1000 );
				return this;
			},
			stop: function() {
				clearInterval(this.timer);
				this.time = {m:0,s:0};
				this.total_sec = 0;
				return this;
			}
		};
	};
var my_timer = new countDown(00,08, 
		function() {
			if (my_timer.time.s ==0){
				
	 Ti.App.fireEvent('fbController:mensajeUsuario', { 
				id_usuario:Ti.App.Properties.getInt("id_usuario"),
				move:random
				
	});
				my_timer.stop();	
				
            }	
		},	function() {}
	);
   	
   	
   	
   	if(Ti.App.Properties.getInt('ingles')==1){
   		var dialogBono = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Accept'],message: 'Received a discount of $ 50 MXN by  make your first trip with us',title: 'Congratulations'});
   	}else{
   		var dialogBono = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'],message: 'Has recibido un descuento de $50 MXN por realizar tu primer viaje con nosotros.',title: 'Felicidades'});
   	}
   	
   	iconShare.addEventListener('click',function(){
   		dialogBono.show();
   	});
   	 
   	 windowM.addEventListener('android:back', function(){
   	 	if (Ti.App.Properties.hasProperty('id_usuario')) {
		}else{			
	 		inicio= require('/interface/inicio');
        	new inicio().open();
        	windowM.close();
		}
     });
     
     var dispositivoBloqueado= Ti.UI.createView({width:'100%', height:'100%', visible:false,zIndex: 20,backgroundColor:'#B3000000'});
     var vistamensajedispositivoBloqueado= Ti.UI.createView({width:'80%', height:Ti.UI.SIZE,zIndex: 20,backgroundColor:'white',borderColor:'black',borderRadius:6,layout:'vertical'});
   	 var TitulomensajedispositivoBloqueado=Titanium.UI.createLabel({text:'NOTIFICACIÓN',top:20,color:'black',font:{fontSize:18,fontFamily: 'Menlo Bold Italic'}});
   	 var txtdispositivoBloqueado=Titanium.UI.createLabel({text:"Tu dispositivo fue bloqueado.\n Consulte a soporte técnico:\n soporte@subtmx.com",top:20,color:'#2980b9',font:{fontSize: 15,fontFamily: 'Menlo Bold Italic'},textAlign:'center',width:'80%'});
   	 var btnmensajedispositivoBloqueado = Titanium.UI.createButton({title:'Aceptar',center:0,backgroundColor:'#0c83c7',top:10,borderRadius:5});
     var vistamensajedispositivoBloqueado2= Ti.UI.createView({width:'100%', height:30,layout:'horizontal',top:0});
    btnmensajedispositivoBloqueado.addEventListener('click',function(){
    	Titanium.App.Properties.removeProperty('coneckta');
		Titanium.App.Properties.removeProperty('paypal');
		Titanium.App.Properties.removeProperty('id_usuario');
		Titanium.App.Properties.removeProperty('usuario');
		Titanium.App.Properties.removeProperty('email');
		Titanium.App.Properties.removeProperty('telefono');
		Titanium.App.Properties.removeProperty('rfc');
		Titanium.App.Properties.removeProperty('razon');
    	inicio = require('/interface/inicio');
		new inicio().open({modal:true});
		windowM.close();
    });
   	 windowM.addEventListener('open',function(){
   	 	Titanium.App.Properties.removeProperty('re');
   	   	Titanium.App.Properties.removeProperty('recotizado');
   	 	Titanium.App.Properties.removeProperty('viajeiniciado');
   	 	Titanium.App.Properties.removeProperty('tiSer');
   	 	Titanium.App.Properties.removeProperty('yallegue');
   	 	Titanium.App.Properties.removeProperty('vistaActiva');
   	 	Titanium.App.Properties.removeProperty('id_Chofer');
   	 	Titanium.App.Properties.removeProperty('tipo_servicio');
   	 	Titanium.App.Properties.removeProperty('taxiRosaSelect');
   	 	Titanium.App.Properties.removeProperty('carroSelect');
   	 	Titanium.App.Properties.removeProperty('numPasajero');
   	 	Titanium.App.Properties.removeProperty('distancias');
   	 	Titanium.App.Properties.removeProperty('tipo_servicio');
   	 	Titanium.App.Properties.removeProperty('taxiRosaSelect');
   	 	Titanium.App.Properties.removeProperty("id_Chofer");
   	 	Titanium.App.Properties.removeProperty("tiSer");
   	 	
   	 
		if (Ti.App.Properties.hasProperty('id_usuario')) {
			bonoHttp.open("POST", "http://subtmx.com/webservices/bono.php");
					params = {
						id_usuario : Ti.App.Properties.getInt("id_usuario")
					};
			bonoHttp.send(params);
			
			versionApp.open("POST", "http://subtmx.com/webservices/version.php");
					params = {
						version :versionI,
						dispositivo : "android"
					};
			versionApp.send(params);
			
			

		}

   		
   	 });
   	 
   	 versionApp.onload = function() {
		json = this.responseText;
		response = JSON.parse(json);
		if (response.logged == true) {
			Ti.App.Properties.setInt("radiofirebase",response.radiofirebase);
			disbloqueado.open("POST", "http://subtmx.com/webservices/registrodispositivo.php");
			params = {
				user_id : Ti.App.Properties.getInt("id_usuario"),
				device_token:Titanium.Platform.id,
                device_type:"android"
			};
			disbloqueado.send(params);
			
		
		} else {
			version.visible=true;
		}
	};
	
	disbloqueado.onload = function() {
		json = this.responseText;
		response = JSON.parse(json);
		if (response.logged != null) {
			if (response.logged == true) {
					if (Ti.App.Properties.hasProperty('coneckta')){
						idconeckta.open("POST", "http://subtmx.com/webservices/conektausuario.php");
						params = {
						id_usuario : Ti.App.Properties.getInt("id_usuario")
						};
						idconeckta.send(params);
					}
					
					
				if (Ti.App.Properties.hasProperty('coneckta') || Ti.App.Properties.hasProperty('paypal')) {
					bonoHttp.open("POST", "http://subtmx.com/webservices/bono.php");
					params = {
						id_usuario : Ti.App.Properties.getInt("id_usuario")
					};
					bonoHttp.send(params);
					PagoPen.open("POST", "http://subtmx.com/webservices/statuspago.php");
					params = {
						id_usuario : Ti.App.Properties.getInt("id_usuario")
					};

					PagoPen.send(params);

				} else {
					paypal = require('/interface/tipoPago');
					new paypal().open();
						PagoPen.open("POST", "http://subtmx.com/webservices/statuspago.php");
					params = {
						id_usuario : Ti.App.Properties.getInt("id_usuario")
					};

					PagoPen.send(params);
				}
			} else {
				dispositivoBloqueado.visible = true;
			}
		} else {
			alert("Error de conexión, intente nuevamente");
		}
	};
	
	
	idconeckta.onload = function() {
		json = this.responseText;
		response = JSON.parse(json);
		if (response.logged == true) {
		
			Ti.App.Properties.setString("coneckta", 'coneckta');
		
		} else {
		
			paypal = require('/interface/pago');
			new paypal().open();
			windowM.close();
		}
	};

	
	PagoPen.onload = function() {
		json = this.responseText;
		response = JSON.parse(json);
		if (response.logged != null) {
			if (response.logged == true) {
				dest = response.destino;
				hor = response.horario_precio;
				b = response.id_biling;
				ids = response.id_servicio;
				desc = response.descripcion_viaje;
				costo = response.costo_total;
				kma = response.km_adicional;
				ckma = response.costo_km_adicional;
				Ideve = response.id_eventualidad;

				if (response.status_pago == 1) {

					if (costo == 0) {
						endServReq.open("POST", "http://subtmx.com/webservices/endServicios.php");
						params = {
							id_servicio : ids,
							km_adicional : kma,
							costo_km_adicional : ckma,
							hora_extra : "00:00:00",
							costo_hora_extra : 0,
							cargo_hora_pico : 0,
							tiempo_espera_adicional : "00:00:00",
							costo_espera_adicional : 0,
							costo_total : (parseFloat(costo).toFixed(2)),
							id_eventualidad : Ideve,
							costo_pendiente : 0,
							status_promocion : 0
						};
						endServReq.send(params);
					} else {

						pagar = 1;
						if (Ti.App.Properties.getInt('ingles') == 1) {
							var dialogPagoPendiente = Ti.UI.createAlertDialog({
								cancel : 1,
								buttonNames : ['Pagar'],
								message : 'Remember that you have a pay pending for: $' + parseFloat(costo).toFixed(2),
								title : 'Message'
							});
						} else {
							var dialogPagoPendiente = Ti.UI.createAlertDialog({
								cancel : 1,
								buttonNames : ['Pagar'],
								message : 'Le recordamos que tiene un pago pendiente por la cantidad de: $' + parseFloat(costo).toFixed(2),
								title : 'Aviso'
							});
						}
						dialogPagoPendiente.show();

						dialogPagoPendiente.addEventListener('click', function(e) {
							if (e.index == 0) {
								if (Ti.App.Properties.hasProperty('coneckta')) {
									pagoReq2.open("POST", "http://subtmx.com/webservices/php-conekta-master/conekta_card.php");
									params = {
										id_usuario : Ti.App.Properties.getInt('id_usuario'),
										id_servicio : ids,
										descripcion_viaje : desc,
										costo_total : parseFloat(costo).toFixed(2)
									};
									pagoReq2.send(params);
									Ti.API.info(Ti.App.Properties.getInt('id_usuario'));
									Ti.API.info(ids);
									Ti.API.info(desc);
									Ti.API.info(parseFloat(costo).toFixed(2));
								} else if (Ti.App.Properties.hasProperty('paypal')) {
									pagoReq2.open("POST", "http://subtmx.com/webservices/paypal/samples/BillingAgreements/DoReferenceTransaction.php");
									params = {
										destino : dest,
										horario_precio : hor,
										id_biling : b,
										name : Ti.App.Properties.getString('nombre'),
										lastname : Ti.App.Properties.getString('apellido'),
										id_usuario : Ti.App.Properties.getInt('id_usuario'),
										id_servicio : ids,
										descripcion_viaje : desc,
										costo_total : parseFloat(costo).toFixed(2)
									};
									pagoReq2.send(params);
								}
								btnLista.enabled = false;
							} else {

							}
						});
					}
				} else {

				}

			} else {

			}
		} else {
			alert("Error de conexión, intente nuevamente");
		}
	}; 



   	 
   	    var dialCalif = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'],message: 'Pago realizado correctamenta',title: 'Pago realizado'});

    pagoReq2.onload = function(){
            json = this.responseText;
            response = JSON.parse(json);
          
        if (response.logged != null) {
            if (response.logged == true){
            	
            	endServReq.open("POST","http://subtmx.com/webservices/endServicios.php");
                    params = {
                    	id_servicio: ids,
                        km_adicional: kma,
                        costo_km_adicional:ckma,
                        hora_extra: "00:00:00",
                        costo_hora_extra: 0,
                        cargo_hora_pico: 0,
                        tiempo_espera_adicional:"00:00:00",
                        costo_espera_adicional: 0,
                        costo_total: (parseFloat(costo).toFixed(2)),
                        id_eventualidad:Ideve,
                        costo_pendiente:0,
                        status_promocion:0
                    };
      			endServReq.send(params);
      
           
            pagar=0;
           	if(Ti.App.Properties.getInt('ingles')==1){
					dialCalif.message= 'Payment Successful.The amount of the transaction was: $'+response.costo_total+' MX';
				}else{
					dialCalif.message = 'Pago realizado correctamente, el monto de la transaccion fue: $'+response.costo_total+' MX';
            		}
            		dialCalif.show();
            		btnLista.enabled=true;
            }else{  
            		if(Ti.App.Properties.getInt('ingles')==1){
            		dialCalif.message= 'Could not make the payment';
            		dialCalif.title='Pending payment';
		       	}else{
            		dialCalif.message="No se pudo realizar el pago";
            		dialCalif.title='Pago pendiente';
            	}
            	dialCalif.show();
            	btnLista.enabled=true;
            }
            }else{
            	alert("Error de conexión, intente nuevamente");
            }
     };
     
   	  idPpal.onload = function(){
            json = this.responseText;
            response = JSON.parse(json);
             if (response.logged != null) {
            if (response.logged == true){
            	Ti.App.Properties.setString("id_biling2",response.id_biling);
            	if(Ti.App.Properties.getString("id_biling2")==''){
            		Ti.API.info(Ti.App.Properties.getString("id_biling2"));
            		billing=0;
   					Ti.App.Properties.setInt("biling",billing);
    				paypal= require('/interface/paypal');
        			new paypal().open();
        			windowM.close();
    			}else{
    				billing=1;
    				Ti.App.Properties.setInt("biling",billing);
    				Ti.API.info(Ti.App.Properties.getString("id_biling2"));
    		 	}
           }else{
           }
           }else{
           	alert("Error de conexión, intente nuevamente");
           }
    	};
   	
   	bonoHttp.onload = function(){
            json = this.responseText;
            response = JSON.parse(json);
              if (response.logged != null) {
            if (response.logged == true){
               	bono=0;
            	Ti.App.Properties.setInt("Bono",bono);
            }else{
            	header.add(iconShare);
               	bono=50;
            	Ti.App.Properties.setInt("Bono",bono);
    		}
    		}else{
    			alert("Error de conexión, intente nuevamente");
    		}
    };
   	
   	var tarifaFinal;
  
	pedirKm.onload = function() {
		json = this.responseText;
		response = JSON.parse(json);
		 if (response.logged != null) {
		if (response.logged == true) {
			Ti.App.Properties.setString("distancias", response.km_total);
			
			if (bandera.text == 4) {
				var kmformateado = Ti.App.Properties.getString("distancias").split(',');
				var kmWeb = kmformateado[0] + '.' + kmformateado[1];
				var dus = parseFloat(kmWeb);
				var lblTotal = Ti.UI.createLabel();

				var numeroP;
				if (numPasajero.text == 3) {
					numeroP = 0;
				} else if (numPasajero.text == 8) {
					numeroP = 1;
				}

				tarifasAero.open("POST", "http://subtmx.com/webservices/costoAeropuerto.php");
				params = {
					num_pasajeros : numeroP
				};
				tarifasAero.send(params);

				tarifasAero.onload = function() {
					json = this.responseText;
					response = JSON.parse(json);
	 				if (response.logged != null) {
					if (response.logged == true) {
						var tarifa_inicial = response.tarifa_inicial;
						var km_inicial = response.km_inicial;
						var precio_km_adicional_1 = response.precio_km_adicional_1;
						var km_adicional_1 = response.km_adicional_1;
						var precio_km_adicional_2 = response.precio_km_adicional_2;
						var km_adicional_2 = response.km_adicional_2;
						var precio_km_adicional_3 = response.precio_km_adicional_3;
						var km_adicional_3 = response.km_adicional_3;
						var precio_km_adicional_4 = response.precio_km_adicional_4;
						var km_adicional_4 = response.km_adicional_4;
						var precio_km_adicional_5 = response.precio_km_adicional_5;
						var km_adicional_5 = response.km_adicional_5;

						if (dus > parseFloat(km_adicional_5)) {
							if (Ti.App.Properties.getInt('ingles') == 1) {
								var dialogDus = Ti.UI.createAlertDialog({
									cancel : 1,
									buttonNames : ['Accept'],
									message : 'Service not available for this area',
									title : 'Check'
								});
							} else {
								var dialogDus = Ti.UI.createAlertDialog({
									cancel : 1,
									buttonNames : ['Acceptar'],
									message : 'Servicio no disponible para esa area',
									title : 'Verificación'
								});
							}
							
							dialogDus.show();
							dialogDus.addEventListener('click',function(){
							vistaCotizacion.visible = false;	
							});
						} else {
							Ti.App.Properties.setDouble('dus', dus);
							var nuevoKMT;
							var newCosto;

							if (dus.toFixed(2) <= parseFloat(km_inicial)) {
								tarifaFinal = tarifa_inicial * 1;
								lblTotal.text = '$' + tarifaFinal.toFixed(2) + ' MXN';
							} else if (dus.toFixed(2) > parseFloat(km_inicial) && dus.toFixed(2) <= parseFloat(km_adicional_1)) {
								tarifaFinal = precio_km_adicional_1 * 1;
								lblTotal.text = '$' + tarifaFinal.toFixed(2) + ' MXN';

							} else if (dus.toFixed(2) > parseFloat(km_adicional_1) && dus.toFixed(2) <= parseFloat(km_adicional_2)) {
								tarifaFinal = precio_km_adicional_2 * 1;
								lblTotal.text = '$' + tarifaFinal.toFixed(2) + ' MXN';

							} else if (dus.toFixed(2) >= parseFloat(km_adicional_2) && dus.toFixed(2) <= parseFloat(km_adicional_3)) {

								tarifaFinal = precio_km_adicional_3 * 1;
								lblTotal.text = '$' + tarifaFinal.toFixed(2) + ' MXN';

							} else if (dus.toFixed(2) >= parseFloat(km_adicional_3) && dus.toFixed(2) <= parseFloat(km_adicional_4)) {

								tarifaFinal = precio_km_adicional_4 * 1;
								lblTotal.text = '$' + tarifaFinal.toFixed(2) + ' MXN';

							} else if (dus.toFixed(2) >= parseFloat(km_adicional_4) && dus.toFixed(2) <= parseFloat(km_adicional_5)) {

								tarifaFinal = precio_km_adicional_5 * 1;
								lblTotal.text = '$' + tarifaFinal.toFixed(2) + ' MXN';

							}

							Ti.App.Properties.setDouble("tarifaFinal", tarifaFinal);
							txtPrecio.text = lblTotal.text;
							Ti.API.info('id_tipo_servicio' + bandera.text);
							Ti.API.info('id_usuario' + Ti.App.Properties.getInt("id_usuario"));
							Ti.API.info('id_chofer' + 16);
							Ti.API.info('latitud_origen' + latInicio.text);
							Ti.API.info('longitud_origen' + lngInicio.text);
							Ti.API.info('latitud_destino' + latDestino.text);
							Ti.API.info('longitud_destino' + lngDestino.text);
							Ti.API.info('lugar_origen' + 'Aeropuerto internacional de Acapulco');
							Ti.API.info('opcion_servicio' + 1);
							Ti.API.info('tarifa_inicial' + tarifaFinal);
							Ti.API.info('id_tipo_pago' + 1);
							
						cotizarauto.open("POST", "http://subtmx.com/webservices/cotizarauto.php");
						params = {
							id_tipo_servicio : bandera.text
						};
						cotizarauto.send(params);
						}
						
					} else {
						if (Ti.App.Properties.getInt('ingles') == 1) {
							alert("Unable to process your request");
						} else {
							alert("No se pudo Realizar el pedido");
						}

					}
					}else{
					alert("Error de conexión, intente nuevamente");
					}
				};

			}
			if (ti_ser.text == 2) {
				cotizarauto.open("POST", "http://subtmx.com/webservices/cotizarauto.php");
				params = {
					id_tipo_servicio : bandera.text
				};
				cotizarauto.send(params);
			} else {

				if (Ti.App.Properties.getString("distancias") == '') {
					if (Ti.App.Properties.getInt('ingles') == 1) {
						var dialogAlerta = Ti.UI.createAlertDialog({
							cancel : 1,
							buttonNames : ['Accept'],
							message : 'Sorry, we could not process your request, try you again',
							title : 'Message'
						});
					}else{
						var dialogAlerta = Ti.UI.createAlertDialog({
							cancel : 1,
							buttonNames : ['Aceptar'],
							message : 'Lo sentimos, no pudimos procesar su solicitud, intentelo de nuevo',
							title : 'Mensaje'
						});
					}
					dialogAlerta.show();
				} else {

					cotizarauto.open("POST", "http://subtmx.com/webservices/cotizarauto.php");
					params = {
						id_tipo_servicio : bandera.text
					};
					cotizarauto.send(params);

				}
			}

		}
		}else{
			alert("Error de conexión, intente nuevamente");
		}

	}; 

      
     
	
      
      	
	rango.onload = function() {
		json = this.responseText;
		response = JSON.parse(json);
		Ti.API.info(response.logged);
		if (response.logged != null) {
			if (response.logged == true) {
				var RangoOrigen = response.status_origen;
				var RangoDestino = response.status_destino;

				if (RangoOrigen == 1) {
					if (Ti.App.Properties.getInt('ingles') == 1) {
						var dialogAlertaRuta = Ti.UI.createAlertDialog({
							cancel : 1,
							buttonNames : ['Accept'],
							message : 'Service not available in this area , please select another origin',
							title : 'Message'
						});
					} else {
						var dialogAlertaRuta = Ti.UI.createAlertDialog({
							cancel : 1,
							buttonNames : ['Aceptar'],
							message : 'Servicio no disponible en esta área, favor de seleccionar otro origen',
							title : 'Mensaje'
						});
					}
					dialogAlertaRuta.show();
				} else {
					if (RangoDestino == 1) {
						if (Ti.App.Properties.getInt('ingles') == 1) {
							var dialogAlertaRuta = Ti.UI.createAlertDialog({
								cancel : 1,
								buttonNames : ['Accept'],
								message : 'Service not available in this area , please select another destination',
								title : 'Message'
							});
						} else {
							var dialogAlertaRuta = Ti.UI.createAlertDialog({
								cancel : 1,
								buttonNames : ['Aceptar'],
								message : 'Servicio no disponible en esta área, favor de seleccionar otro destino',
								title : 'Mensaje'
							});
						}
						dialogAlertaRuta.show();
						txtDestino.editable = true;
					} else {

						if (bandera.text == 4) {
							pedirKm.open("POST", "http://subtmx.com/webservices/distanciag.php");
							params = {
								lat1 : latInicio.text,
								lng1 : lngInicio.text,
								lat2 : latDestino.text,
								lng2 : lngDestino.text
							};
							pedirKm.send(params);
						} else {

							rango2.open("POST", "http://subtmx.com/webservices/phpgeo-master/tests/geo2.php");
							params = {
								latitudo : latInicio.text,
								longitudo : lngInicio.text,
							};
							rango2.send(params);

						}

					}
				}
			} else {

			}
		} else {
			alert("Error de conexión, intente nuevamente");
		}
	}; 
	
	
	
	
	rango2.onload = function() {
		json = this.responseText;
		response = JSON.parse(json);
		Ti.API.info(response.logged);
		if (response.logged != null) {
			if (response.logged == true) {
			
				var RangoOrigen2 = response.status_origen;
		
				if (RangoOrigen2 == 1) {
					if (Ti.App.Properties.getInt('ingles') == 1) {
						var dialogAlertaRuta = Ti.UI.createAlertDialog({
							cancel : 1,
							buttonNames : ['Accept'],
							message : 'Type of service not available in this area , please select type airport',
							title : 'Message'
						});
					} else {
						var dialogAlertaRuta = Ti.UI.createAlertDialog({
							cancel : 1,
							buttonNames : ['Aceptar'],
							message : 'Tipo de servicio no disponible en esta área, favor de seleccionar tipo aeropuerto',
							title : 'Mensaje'
						});
					}
					dialogAlertaRuta.show();
				} else {
					pedirKm.open("POST", "http://subtmx.com/webservices/distanciag.php");
					params = {
						lat1 : latInicio.text,
						lng1 : lngInicio.text,
						lat2 : latDestino.text,
						lng2 : lngDestino.text
					};
					pedirKm.send(params);
				}
			} else {

			}
		} else {
			alert("Error de conexión, intente nuevamente");
		}
	}; 



   	
  
	cotizarauto.onload = function() {
		json = this.responseText;
		response = JSON.parse(json);
		if (response.logged != null) {
		if (response.logged == true) {

			vistaCotizacion.visible = true;

			if (bandera.text == 4) {
				
			} else {
				var Rtipo_servicio = response.tipo_servicio;
				var newkmad,
				    newCosto;
				var kmformateado2 = Ti.App.Properties.getString("distancias").split(',');
				var kmWeb2 = kmformateado2[0] + '.' + kmformateado2[1];
				var dus2 = parseFloat(kmWeb2);
				var lblTotal = Ti.UI.createLabel();
				Ti.API.info(dus2);
				
				if (ti_ser.text == 1) {
					if (dus2 > response.km_inicial) {
						newkmad = dus2 - response.km_inicial;
						newCosto = newkmad * response.precio_km_adicional;
						newCosto = parseFloat(newCosto) + parseFloat(response.tarifa_inicial);
						txtPrecio.text = '$' + parseFloat(newCosto).toFixed(2)+ ' MXN';
					} else if (dus2.toFixed(2) <= response.km_inicial) {
						Ti.API.info("menor");
						txtPrecio.text = '$' + response.tarifa_inicial + ' MXN';

					}

				} else if (ti_ser.text == 2) {
						txtPrecio.text = '$' + response.tarifa_hora * parseInt(txtserviciohora.value) + ' MXN';

				}
			}

		}
		}else{
			alert("Error de conexión, intente nuevamente");
		}
	}; 

   	
	var servicioAeropuerto,
	    idAeropuerto;
	registerServ.onload = function() {
		json = this.responseText;
		response = JSON.parse(json);
		if (response.logged != null) {
		if (response.logged == true) {
			servicioAeropuerto = response.folio_servicio;
			idAeropuerto = response.id_servicio;

			Ti.App.Properties.setInt("id_servicio", response.id_servicio);
			Ti.API.info('destino:  ' + txtDestino.value);
			Ti.API.info('id_biling:  ' + Ti.App.Properties.getString("id_biling2"));
			Ti.API.info('name:  ' + Ti.App.Properties.getString('nombre'));
			Ti.API.info('lastname:  ' + Ti.App.Properties.getString('apellido'));
			Ti.API.info('id_usuario:  ' + Ti.App.Properties.getInt('id_usuario'));
			Ti.API.info('id_servicio:  ' + response.id_servicio);
			Ti.API.info('descripcion_viaje:  ' + 'Tipo de viaje es: ' + carroSelect.text + ', Km recorridos: ' + Ti.App.Properties.getDouble('dus'));
			Ti.API.info('costo_total:  ' + Ti.App.Properties.getDouble("tarifaFinal"));

			var Folioformateado = servicioAeropuerto.split('-');
			var FolioNum = Folioformateado[1];
			Ti.App.Properties.setString("folioForm", FolioNum);

			var currentTime = new Date();
			var hours = currentTime.getHours();
			var minutes = currentTime.getMinutes();
			var seconds = currentTime.getSeconds();
			var month = currentTime.getMonth() + 1;
			var day = currentTime.getDate();
			var year = currentTime.getFullYear();

			if (month < 10) {
				month = "0" + month;
			}
			if (day < 10) {
				day = "0" + day;
			}
			if (hours < 10) {
				hours = "0" + hours;
			}
			if (minutes < 10) {
				minutes = "0" + minutes;
			}
			if (seconds < 10) {
				seconds = "0" + seconds;
			}
			var fecha = day + "-" + month + "-" + year;

			if (txtvistaAeropuerto.value == '') {
				txtvistaAeropuerto.value = 'No aplica';
			}
			if (txtvistaAeropuerto2.value == '') {
				txtvistaAeropuerto2.value = 'No aplica';
			}

			Ti.App.fireEvent('fbController:Aeropuerto', {
				genero : Ti.App.Properties.getString("genero"),
				nombre : Ti.App.Properties.getString("nombrecompleto"),
				email : Ti.App.Properties.getString("email"),
				tel : Ti.App.Properties.getString("telefono"),
				origen : 'Aeropuerto internacional de Acapulco',
				destino : txtDestino.value,
				id_usuario : Ti.App.Properties.getInt("id_usuario"),
				numPasajeros : numPasajero.text,
				status_respuesta : 0,
				id_servicio : idAeropuerto,
				folio_servicio : servicioAeropuerto,
				folioNum : FolioNum,
				cancelado : 0,
				fecha : fecha,
				hora : hours + ':' + minutes + ':' + seconds,
				numero_de_vuelo : txtvistaAeropuerto.value,
				hora_vuelo : txtvistaAeropuerto2.value
			});
			txtvistaAeropuerto.value = '';
			txtvistaAeropuerto2.value = '';
			if (Ti.App.Properties.getInt('ingles') == 1) {

				alert('Payment Successful.The amount of the transaction was: $' + Ti.App.Properties.getDouble("tarifaFinal") + ' MX, ' + 'You soon will get an email with the details of the driver');
			} else {
				alert('Estimado usuario,su transaccion fue realizada correctamente, su monto fue: $' + Ti.App.Properties.getDouble("tarifaFinal") + ' MX,' + 'le comentamos que en breve le llegará un correo electrónico con los datos del conductor');
			}

			recibo.open("POST", "http://subtmx.com/webservices/recibo.php");
			params = {
				destino : Ti.App.Properties.getString("txtdestino"),
				id_usuario : Ti.App.Properties.getInt('id_usuario'),
				id_servicio : idAeropuerto,
			};
			recibo.send(params);

		}
		}else{
			alert("Error de conexión, intente nuevamente");
		}
	}; 

    
    recibo.onload = function(){
            json = this.responseText;
            response = JSON.parse(json);
            if (response.logged != null) {
            if (response.logged == true){
            	
            	endServReq.open("POST","http://subtmx.com/webservices/endServicios.php");
                    		params = {
                    			id_servicio: idAeropuerto,
                        		km_adicional: 0,
                        		costo_km_adicional:0,
                        		hora_extra: "00:00:00",
                        		costo_hora_extra: 0,
                        		cargo_hora_pico: 0,
                        		tiempo_espera_adicional:"00:00:00",
                        		costo_espera_adicional: 0,
                        		costo_total: Ti.App.Properties.getDouble("tarifaFinal"),
                        		id_eventualidad:8
                      
                    		};
           		endServReq.send(params);
            
            }
            }else{ 
            alert("Error de conexión, intente nuevamente");
            }	
      };
  
   	var vistahora=Ti.UI.createView({width:'80%',borderRadius:4,borderColor:'black',height:Ti.UI.SIZE,visible:false,zIndex:20,backgroundColor:'white', layout:'vertical'});
   	var bvista=Ti.UI.createButton({title:'aceptar',center:0,bottom:10,width:'auto',height:'auto',color:'black',backgroundColor:'transparent'});
   	var picker = Ti.UI.createPicker({
  		top:0,
  		center:0,
  		width:'auto',
  		useSpinner: true
	});
	picker.selectionIndicator = true;

	var hora = [ '01', '02', '03', '04', '05', '06', '07', '08','09', '10', '11', '12'];
	var minutos = [ '01', '02', '03', '04','05', '06', '07', '08','09', '10', '11', '12','13', '14', '15', '16','17', '18', '19', '20',
	'21', '22', '23', '24','25', '26', '27', '28','29', '30', '31', '32','33', '34', '35', '36','37', '38', '39', '40','41', '42', '43', '44','45', '46', '47', '48',
	'49', '50', '51', '52','53', '54', '55', '56','57', '58', '59' ];
	var ampm = [ 'am', 'pm'];
	
	var column1 = Ti.UI.createPickerColumn({font:{fontSize:'38%'},width:'30%',backgroundColor:'#ffffff',center:0});

	for(var i=0, ilen=hora.length; i<ilen; i++){
  	var row = Ti.UI.createPickerRow({
    title: hora[i]
  	});
 	 column1.addRow(row);
	}
	var column2 = Ti.UI.createPickerColumn({font:{fontSize:'38%'},width:'30%',backgroundColor:'#ffffff',center:0});

	for(var i=0, ilen=minutos.length; i<ilen; i++){
  	var row = Ti.UI.createPickerRow({ title: minutos[i] });
	column2.addRow(row);
	}
	var column3 = Ti.UI.createPickerColumn({font:{fontSize:'38%'},width:'30%',backgroundColor:'#ffffff',center:0});

	for(var i=0, ilen=ampm.length; i<ilen; i++){
  	var row = Ti.UI.createPickerRow({ title: ampm[i] });
	column3.addRow(row);
	}
	picker.addEventListener('change',function(e){
				var hor=picker.getSelectedRow(0).title;
				var min=picker.getSelectedRow(1).title;
				var meridiano=picker.getSelectedRow(2).title;
				txtvistaAeropuerto2.value= hor + ':' + min + ' '+meridiano;
	});
	picker.add([column1,column2,column3]);

	vistahora.add(picker);
	vistahora.add(bvista);
   	windowM.add(vistahora);
   	txtvistaAeropuerto2.addEventListener('click',function(){
   		vistahora.visible=true;
   	});
   	bvista.addEventListener('click',function(){
   	 	vistahora.visible=false;							
   		
   	});
   	
  latInicio = Ti.UI.createLabel({});
	lngInicio = Ti.UI.createLabel({});
	latDestino = Ti.UI.createLabel({});
	lngDestino = Ti.UI.createLabel({});
	geocoderlbl = Ti.UI.createLabel({});
	drapDest = Ti.UI.createLabel({});
	ti_ser=Ti.UI.createLabel({});
	bandera=Ti.UI.createLabel({});
	taxiRosaSelect=0;servSel=0;	
	
	btnTitleSelect = Titanium.UI.createButton({title:'Tipo servicio'});
	
	if(Ti.App.Properties.getInt('ingles')==1){
   		lblMisPedidos.text='My books';
   		lblMiPerfil.text='Profile';
   		lblPromociones.text='Promotions';
   		lblPagos.text='Payment';
   		listText.text='Type trip';
   		autoRosa.text='Pink type';
   		btnPedir.title='Request a taxi';
   		pinUsuario.title="Current position";
   		pinDestino.title="Current destination";
   		txtOrigen.hintText='Enter your Origin';
   		txtDestino.hintText='Enter your destination';
   		btnTitleSelect.title='Service type';
   		lblConfiguración.text='Configuration';
   	}

	autoCheck.addEventListener('change',function(e){
		if (autoCheck.value == true) {
			if(Ti.App.Properties.getString("genero")=='masculino' ||  Ti.App.Properties.getString("genero")=='male'){
			iconHeader.enabled=false;
			vistavistaTaxiRosa.visible=true;	
			}
			taxiRosaSelect=1;
		}else if(autoCheck.value == false){
			Ti.App.Properties.setString("NMujer",'0');
			taxiRosaSelect=0;
		}
	});
	
	
		
	
	lblHome.addEventListener('click',function(){
	 clickMenu(null);
	});
	if(Ti.App.Properties.getInt('ingles')==1){
		var optsConfig = {options:  ['Help','cancellation airport'], selectedIndex: 2,destructive:0,title: 'Select an option'};
	}else{
	    var optsConfig = {options:  ['Ayuda','Cancelación aeropuerto'], selectedIndex: 2,destructive:0,title: 'Selecciona una opción'};	
	}
	
	
	var dialogConfiguracion = Ti.UI.createOptionDialog(optsConfig);
		
		dialogConfiguracion.addEventListener('click', function(ed){
	   	if (ed.index === 0){
	   		ayuda = require('/interface/ayuda');
			new ayuda().open();
		
	   	}else if (ed.index === 1){
	   		
	   		aeropuerto = require('/interface/aeropuerto');
			new aeropuerto().open();
	   	}
	});
	
	lblConfiguración.addEventListener('click',function(){
	if (Ti.App.Properties.hasProperty('id_usuario')) {
			dialogConfiguracion.show();
			}else{
			if(Ti.App.Properties.getInt('ingles')==1){
				alert('Only registered users');
			}else{
				alert('Solo usuarios registrados');
			}
	}
	});
	
	lblPagos.addEventListener('click',function(){
		if (Ti.App.Properties.hasProperty('id_usuario')) {
					paypal = require('/interface/tipoPago');
					new paypal().open();
			}else{
			if(Ti.App.Properties.getInt('ingles')==1){
				alert('Only registered users');
			}else{
				alert('Solo usuarios registrados');
			}
		}
	});
	
	lblPromociones.addEventListener('click',function(){
		if (Ti.App.Properties.hasProperty('id_usuario')) {
		
	        		function removeAllChildren(viewWeb){
       				var children = viewWeb.children.slice(0);
 
    				for (var i = 0; i < children.length; ++i) {
        			viewWeb.remove(children[i]);
    					}
					}
					var webViewCacheDir = Ti.Filesystem.getFile(Ti.Filesystem.applicationCacheDirectory, "webviewCache");
					webViewCacheDir.deleteDirectory(true);
					
					
			promociones = require('/interface/promociones');
			new promociones().open();
				windowM.close();
   					windowM.remove(webview);
				 	webview.release();
          
				 	webview = null;
				 	Ti.API.info('WebView: ' + webview);
   					windowM = null;
   					Ti.API.info('windowM: ' + windowM);
		}else{
			if(Ti.App.Properties.getInt('ingles')==1){
				alert('Only registered users');
			}else{
				alert('Solo usuarios registrados');
			}
			
		}
	});
	lblMisPedidos.addEventListener('click',function(){
		if (Ti.App.Properties.hasProperty('id_usuario')) {
		
	        		function removeAllChildren(viewWeb){
       				var children = viewWeb.children.slice(0);
 
    				for (var i = 0; i < children.length; ++i) {
        			viewWeb.remove(children[i]);
    					}
					}
					var webViewCacheDir = Ti.Filesystem.getFile(Ti.Filesystem.applicationCacheDirectory, "webviewCache");
					webViewCacheDir.deleteDirectory(true);
					
				
			pedidos = require('/interface/pedidos');
			new pedidos().open();
				windowM.close();
   					windowM.remove(webview);
				 	webview.release();
          
				 	webview = null;
				 	Ti.API.info('WebView: ' + webview);
   					windowM = null;
   					Ti.API.info('windowM: ' + windowM);
		}else{
			if(Ti.App.Properties.getInt('ingles')==1){
				alert('Only registered users');
			}else{
				alert('Solo usuarios registrados');
			}
		}
	});
	
	lblMiPerfil.addEventListener('click',function(){
		 		
	        		function removeAllChildren(viewWeb){
       				var children = viewWeb.children.slice(0);
 
    				for (var i = 0; i < children.length; ++i) {
        			viewWeb.remove(children[i]);
    					}
					}
					var webViewCacheDir = Ti.Filesystem.getFile(Ti.Filesystem.applicationCacheDirectory, "webviewCache");
					webViewCacheDir.deleteDirectory(true);
					
					
		perfil = require('/interface/perfil');
		new perfil().open();
			windowM.close();
   					windowM.remove(webview);
				 	webview.release();
          
				 	webview = null;
				 	Ti.API.info('WebView: ' + webview);
   					windowM = null;
   					Ti.API.info('windowM: ' + windowM);
	});
	
	
	if(Ti.App.Properties.getInt('ingles')==1){
		var opts = {options:  ['Classic','Premium','Airport'], selectedIndex: 2,destructive:0,title: 'Choose a type of service'};
		var opts22 = {options:  ['1 to 3 people','4 to 8 people'], selectedIndex: 2,destructive:0,title: 'Number of passengers'};

	}else{
		var opts = {options:  ['Clasic','Premium','Aeropuerto'], selectedIndex: 2,destructive:0,title: 'Elija un tipo de servicio'};
		var opts22 = {options:  ['1 a 3 personas(Sedan)','4 a 8 personas(Camioneta)'], selectedIndex: 2,destructive:0,title: 'Numeros de pasajeros\n(1 maleta por persona)'};
	}
	var banderaAero=0;
	var dialog;var dialog22;
	var carroSelect= Ti.UI.createLabel();
	var numPasajero= Ti.UI.createLabel();
	
	dialog = Ti.UI.createOptionDialog(opts);
	dialog22 = Ti.UI.createOptionDialog(opts22);
	dialog.addEventListener('click', function(ed){
		btnCotizar.enabled=true;
	   	if (ed.index === 0){
			txtOrigen.editable=true;
			if(banderaAero==1){
				txtOrigen.value='';	
	
				Titanium.Geolocation.getCurrentPosition(function(e) {
					if (e.error) {
						if (Ti.App.Properties.getInt('ingles') == 1) {
							alert('Unable to get current location.');
							return;
						} else {
							alert('No se puede obtener ubicacion actual');
						}
						return;
					}

					latInicio.text = e.coords.latitude;
					lngInicio.text = e.coords.longitude;
					Titanium.Geolocation.reverseGeocoder(e.coords.latitude, e.coords.longitude, function(evt) {
						if (evt.success) {
							var places = evt.places;
							if (places && places.length) {
								geocoderlbl.text = places[0].address;
								txtOrigen.value = geocoderlbl.text;
								Ti.App.Properties.setDouble("latIN", e.coords.latitude);
								Ti.App.Properties.setDouble("longIN", e.coords.longitude);
								Ti.App.Properties.setString("txtorigen", txtOrigen.value);

							} else {
								if (Ti.App.Properties.getInt('ingles') == 1) {
									geocoderlbl.text = "Check your internet connection";
								} else {
									geocoderlbl.text = "Verifique su conexión a internet";
								}
							}

						} else {
							Ti.UI.createAlertDialog({
								title : 'Conexión inestable',
								message : evt.error
							}).show();
						}
					});
				}); 
			}
			bandera.text=1;
			carroSelect.text="Clasic";
			txtAuto.text='Tipo de auto: Classic';
			servSel=1;
			dialog2.show();
			vistavistaAeropuerto.visible=false;
		// }else if (ed.index === 1){
			// txtOrigen.editable=true;
			// if(banderaAero==1){
				// txtOrigen.value='';	
			// }
			// bandera.text=2;
			// carroSelect.text="Lite";
			// servSel=1;
			// dialog2.show();
			// vistaAeropuerto.visible=false;
			}else if (ed.index === 1){
			txtOrigen.editable=true;
			if(banderaAero==1){
				txtOrigen.value='';
				Titanium.Geolocation.getCurrentPosition(function(e) {
					if (e.error) {
						if (Ti.App.Properties.getInt('ingles') == 1) {
							alert('Unable to get current location.');
							return;
						} else {
							alert('No se puede obtener ubicacion actual');
						}
						return;
					}

					latInicio.text = e.coords.latitude;
					lngInicio.text = e.coords.longitude;
					Titanium.Geolocation.reverseGeocoder(e.coords.latitude, e.coords.longitude, function(evt) {
						if (evt.success) {
							var places = evt.places;
							if (places && places.length) {
								geocoderlbl.text = places[0].address;
								txtOrigen.value = geocoderlbl.text;
								Ti.App.Properties.setDouble("latIN", e.coords.latitude);
								Ti.App.Properties.setDouble("longIN", e.coords.longitude);
								Ti.App.Properties.setString("txtorigen", txtOrigen.value);

							} else {
								if (Ti.App.Properties.getInt('ingles') == 1) {
									geocoderlbl.text = "Check your internet connection";
								} else {
									geocoderlbl.text = "Verifique su conexión a internet";
								}
							}

						} else {
							Ti.UI.createAlertDialog({
								title : 'Conexión inestable',
								message : evt.error
							}).show();
						}
					});
				});
			}
			bandera.text=3;
			carroSelect.text="Premium";
			txtAuto.text='Tipo de auto: Premium';
			servSel=1;
			dialog2.show();
			vistavistaAeropuerto.visible=false;
		}else if (ed.index === 2){
			
			banderaAero=1;
			latInicio.text = 16.762454;
			lngInicio.text = -99.754535;
			 Titanium.Geolocation.reverseGeocoder(16.762454,-99.754535,function(evt){
				if (evt.success) {
						var places = evt.places;
						if (places && places.length) {
							geocoderlbl.text = places[0].address;
							txtOrigen.value=geocoderlbl.text;
							Ti.App.Properties.setDouble("latIN",latInicio.text);
        					Ti.App.Properties.setDouble("longIN",lngInicio.text);
        					Ti.App.Properties.setString("txtorigen",txtOrigen.value);
						}else{
							if(Ti.App.Properties.getInt('ingles')==1){
									geocoderlbl.text = "Check your internet connection";
							}else{
									geocoderlbl.text = "Verifique su conexión a internet";
							}
							
						}
						
				 }else{
				 	
				 	if(Ti.App.Properties.getInt('ingles')==1){
									Ti.UI.createAlertDialog({title:'Unstable connection',message:evt.error}).show();
					}else{
									Ti.UI.createAlertDialog({title:'Conexión inestable',message:evt.error}).show();
					}
						
				 }
			 });		txtDestino.editable=true;
			 			txtOrigen.editable=false;
						carroSelect.text="Aeropuerto";
						txtAuto.text='Tipo de auto: Aeropuerto';
						bandera.text=4;
						dialog22.show();
		  				servSel=1;
	   	}
	});

	 webview = Ti.UI.createWebView({url : '/firebase/index.html',width:0,height:0,top:0,left:0,visible:false,cacheMode:Titanium.UI.Android.WEBVIEW_LOAD_NO_CACHE});
	
							 webview.addEventListener('load', function() {
							 Ti.App.fireEvent('firebase:init2', {
							 latitude : 16.762454,
							 longitude : -99.754535,
							 id_usuario:Ti.App.Properties.getInt("id_usuario")
						 });
					 });	
 				
	windowM.add(webview);
	my_timer.start();
	var dialogAeroAviso = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'],title: 'Chofer asignado'});
	Ti.App.addEventListener('fbController:AlertAeropuerto', function(e){
		Titanium.Android.NotificationManager.notify(1,Titanium.Android.createNotification({
    				contentTitle: 'Chofer de Aeropuerto asignado',
				    contentText :'Se le ha asignado un chofer' ,
				    tickerText: 'mensaje de SUBT',
				    contentIntent: Ti.Android.createPendingIntent({
				    intent: Ti.Android.createIntent({
 					action: Ti.Android.ACTION_MAIN,
				    className: 'com.subtmx.subt.SubtActivity',
				    packageName: 'com.subtmx.subt'
				    })
				  }),
			}));
		var mensaje = 'Se te ha asignado el siguiente chofer: '+ e.nombrechofer + ' con un auto modelo: ' + e.modelo + ', color: '+ e.color +' y numero economico: ' + e.numero;
		dialogAeroAviso.message = mensaje;
		dialogAeroAviso.show();
		dialogAeroAviso.addEventListener('click',function(e){
				if(e.index==0){
				Ti.App.fireEvent('fbController:Aeropuertoleido', {
						id_usuario:Ti.App.Properties.getInt("id_usuario"),
						folioNum:Ti.App.Properties.getString("folioForm"),
					});
				Titanium.Android.NotificationManager.cancelAll();	
				}
			
			});
		
		player.play();
	});
	
	dialog22.addEventListener('click', function(ed){
		
		if (ed.index === 0){
			Ti.API.info("latI: "+latInicio.text);
			Ti.API.info("CarS: "+carroSelect.text);
			numPasajero.text = 3;
			txtDestino.editable=true;
			
		}else{
			txtDestino.editable=true;
			numPasajero.text = 8;
		}
	vistavistaAeropuerto.visible=true;
	});
	
	 var serviciohora= Ti.UI.createView({width:'100%', height:'100%', visible:false,zIndex: 20,backgroundColor:'#B3000000'});
     var vistaserviciohora= Ti.UI.createView({width:'80%', height:Ti.UI.SIZE,zIndex: 20,backgroundColor:'white',borderColor:'black',borderRadius:6,layout:'vertical'});
   	 var Tituloserviciohora=Titanium.UI.createLabel({text:'¿Cuantas horas requieres de servicio?',top:20,color:'black',font:{fontSize:18},textAlign:'center'});
   	 var txtserviciohora=Titanium.UI.createTextArea({value:2,top:20,color:'black',font:{fontSize: 30},textAlign:'center'});
   	 var vistaserviciohoraBTN= Ti.UI.createView({center:0,width:Ti.UI.SIZE,height:Ti.UI.SIZE,zIndex: 20,layout:'horizontal', top:5});
   	 
   	 var btnserviciohora1 = Titanium.UI.createButton({backgroundImage:'/images/+.png',center:0,height:40,width:40});
   	 var btnserviciohora2 = Titanium.UI.createButton({backgroundImage:'/images/-.png',left:10,height:40,width:40});
   	 var btnserviciohora = Titanium.UI.createButton({title:'Aceptar',center:0,backgroundColor:'#0c83c7',top:15,borderRadius:5});
     var vistaserviciohora2= Ti.UI.createView({width:'100%', height:30,layout:'horizontal',top:0});
	
	serviciohora.add(vistaserviciohora);
	vistaserviciohora.add(Tituloserviciohora);
	vistaserviciohora.add(txtserviciohora);
	vistaserviciohoraBTN.add(btnserviciohora1);
	vistaserviciohoraBTN.add(btnserviciohora2);
	vistaserviciohora.add(vistaserviciohoraBTN);
	vistaserviciohora.add(btnserviciohora);
	vistaserviciohora.add(vistaserviciohora2);
	btnserviciohora1.addEventListener('click',function(){
	if(txtserviciohora.value<12){
	txtserviciohora.value=parseInt(txtserviciohora.value)+1;
	Ti.App.Properties.setInt("txtserviciohora",txtserviciohora.value);
	}	
	});
	btnserviciohora2.addEventListener('click',function(){
	if(txtserviciohora.value>2){
	txtserviciohora.value=parseInt(txtserviciohora.value)-1;
	Ti.App.Properties.setInt("txtserviciohora",txtserviciohora.value);
	}		
	});
	btnserviciohora.addEventListener('click',function(){
	serviciohora.visible=false;
	});
	var dialogHora = Ti.UI.createAlertDialog({buttonNames: ['Cancelar','Aceptar'],title: 'Verificación'});
	dialogHora.addEventListener('click', function(ed){
	   	if (ed.index === 0){
	   		servSel=0;
	   		txtDestino.value='';
	   		destinoSeleccionado = 0;
	   		
	   	}else if (ed.index === 1){
	   	serviciohora.visible=true;
	   	}
	 });
	
	
	if(Ti.App.Properties.getInt('ingles')==1){
		var opts2 = {options:  ['Simple','Hourly'], selectedIndex: 2,destructive:0,title: 'Choose a service option'};
	}else{
		var opts2 = {options:  ['Sencillo','Por hora'], selectedIndex: 2,destructive:0,title: 'Elija una opción de servicio'};
	}
	var dialog2;
	
	var table_data = [];
		var last_search = null;
		var timers = [];
		
		var ANDROID_API_KEY = 'AIzaSyB4xyTWJ6MAh1mO94AUfLF9WsuOiQt-OsA';
		
		var container = Ti.UI.createView({
			//layout : 'vertical',
			width:'90%',
			height : Ti.UI.SIZE,
			top:'35%',
			backgroundColor:'white',
			zIndex:10
			
		});

	var autocomplete_table = Ti.UI.createTableView({height: Ti.UI.SIZE});
	
	container.add(autocomplete_table);
	
	txtDestino.addEventListener('change', function(e) {
			if (txtDestino.value.length > 2 && txtDestino.value !=  last_search) {
				clearTimeout(timers['autocomplete']);
				timers['autocomplete'] = setTimeout(function() {
					last_search =txtDestino.value;
					auto_complete(txtDestino.value);
				}, 300);
			}
			return false;
		});
		
		autocomplete_table.addEventListener('click', function(e) {get_place(e.row.place_id);autocomplete_table.visible=false;container.visible=false;});
		
		function get_place(place_id) {
			var url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + place_id + "&location=16.8633600,-99.8901000&key=" + ANDROID_API_KEY;		
			var client = Ti.Network.createHTTPClient({
			onload : function(e) {
     			var obj = JSON.parse(this.responseText);
				var result = obj.result;
				container.place_id           = place_id;
				container.formatted_address  = result.formatted_address;
				container.latitude           = result.geometry.location.lat;
				container.longitude          = result.geometry.location.lng;
				txtDestino.value             = container.formatted_address;
				//destinoView.height='8%';
				//origenView.height='8%';
				//txtDestino.blur();
				Ti.API.info(txtDestino);
				table_data = [];
				autocomplete_table.setData(table_data);
				autocomplete_table.setHeight(0);
 			},	
 			onerror : function(e) {
				Ti.API.debug(e.error);
				alert('error');
			},
			timeout : 5000
			});
			client.open("GET", url);
			client.send();	
		}
		
		function get_locations(query) {
			var url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + query + "&location=16.8633600,-99.8901000&key=" + ANDROID_API_KEY;		
			var client = Ti.Network.createHTTPClient({
				onload : function(e) {
     			var obj = JSON.parse(this.responseText);
     			var predictions = obj.predictions;     		
     			table_data = [];            
            		autocomplete_table.removeAllChildren();
				autocomplete_table.setHeight(Ti.UI.SIZE);
				
				function omitirAcentos(text) {
					var acentos = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç";
					var original = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc";
					for (var i=0; i<acentos.length; i++) {
						text = text.replace(acentos.charAt(i), original.charAt(i));
					}
					return text;
				}

				var dest;
            		for (var i = 0; i < predictions.length; i++) {
            			var desc = omitirAcentos(predictions[i].description);
            			var row = Ti.UI.createTableViewRow({
						height: 40,
						title: desc,
						place_id: predictions[i].place_id,
						hasDetail:false,
						postId:desc,
						color:'gray'          		
            			});                        	
            			table_data.push(row);  
            		}
            		
            		
            		 
            		autocomplete_table.setData(table_data);
            		//txtDestino.focus();
 				 },
 				onerror : function(e) {
					Ti.API.debug(e.error);
					alert('error');
				},
				timeout : 5000  
			});
			client.open("GET", url);
			client.send();	
		}
		
		function auto_complete(search_term){
			if (search_term.length > 2) {
				get_locations(search_term);   	
   			}
		}
		
		var autocomplete_table2 = Ti.UI.createTableView({height: Ti.UI.SIZE});
	
	container.add(autocomplete_table2);
	
	txtOrigen.addEventListener('change', function(e) {
			if (txtOrigen.value.length > 2 && txtOrigen.value !=  last_search) {
				clearTimeout(timers['autocomplete2']);
				timers['autocomplete2'] = setTimeout(function() {
					last_search2 =txtOrigen.value;
					auto_complete2(txtOrigen.value);
				}, 300);
			}
			return false;
	});
		
		autocomplete_table2.addEventListener('click', function(e) {get_place2(e.row.place_id); autocomplete_table2.visible=false; container.visible=false;});
		
		function get_place2(place_id) {
			var url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + place_id + "&location=16.8633600,-99.8901000&key=" + ANDROID_API_KEY;		
			var client = Ti.Network.createHTTPClient({
			onload : function(e) {
     			var obj = JSON.parse(this.responseText);
				var result = obj.result;
				container.place_id           = place_id;
				container.formatted_address  = result.formatted_address;
				container.latitude           = result.geometry.location.lat;
				container.longitude          = result.geometry.location.lng;
				txtOrigen.value             = container.formatted_address;
				//destinoView.height='8%';
				//origenView.height='8%';
				//txtDestino.blur();
				Ti.API.info(txtOrigen);
				table_data2 = [];
				autocomplete_table2.setData(table_data2);
				autocomplete_table2.setHeight(0);
 			},	
 			onerror : function(e) {
				Ti.API.debug(e.error);
				alert('error');
			},
			timeout : 5000
			});
			client.open("GET", url);
			client.send();	
		}
		
		function get_locations2(query2) {
			var url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + query2 + "&location=16.8633600,-99.8901000&key=" + ANDROID_API_KEY;		
			var client = Ti.Network.createHTTPClient({
				onload : function(e) {
     			var obj = JSON.parse(this.responseText);
     			var predictions = obj.predictions;     		
     			table_data2 = [];            
            		autocomplete_table2.removeAllChildren();
				autocomplete_table2.setHeight(Ti.UI.SIZE);
				
				function omitirAcentos2(text) {
					var acentos = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç";
					var original = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc";
					for (var i=0; i<acentos.length; i++) {
						text = text.replace(acentos.charAt(i), original.charAt(i));
					}
					return text;
				}

				var orig;
            		for (var i = 0; i < predictions.length; i++) {
            			var desc = omitirAcentos2(predictions[i].description);
            			var row = Ti.UI.createTableViewRow({
						height: 40,
						title: desc,
						place_id: predictions[i].place_id,
						hasDetail:false,
						postId:desc,
						color:'gray'          		
            			});                        	
            			table_data2.push(row);  
            		}
            		
            	
            		 
            		autocomplete_table2.setData(table_data2);
            		//txtDestino.focus();
 				 },
 				onerror : function(e) {
					Ti.API.debug(e.error);
					alert('error');
				},
				timeout : 5000  
			});
			client.open("GET", url);
			client.send();	
		}
		
		function auto_complete2(search_term2){
			if (search_term2.length > 2) {
				get_locations2(search_term2);   	
   			}
		}
		
	
	dialog2 = Ti.UI.createOptionDialog(opts2);
	dialog2.addEventListener('click', function(ed){
	   	if (ed.index === 0){
			ti_ser.text=1;
			Ti.App.Properties.setString("tiSer",1);
			txtDestino.editable=true;
		}else if (ed.index == 1){
			txtserviciohora.value=2;
			Ti.App.Properties.setInt("txtserviciohora",txtserviciohora.value);
			Ti.App.Properties.setString("distancias",0);
			destinoSeleccionado=1; origenSeleccionado=1;			
						ti_ser.text=2;
						Ti.App.Properties.setString("tiSer",2);
						latDestino.text = latInicio.text;
						lngDestino.text = lngInicio.text;
						Ti.App.Properties.setDouble("latDES",latDestino.text);
       					Ti.App.Properties.setDouble("longDES",lngDestino.text);
       					txtDestino.value=txtOrigen.value;
						destino=txtDestino.value;
       		      		Ti.App.Properties.setString("txtdestino",destino);
       		      		
       		      		
			if(bandera.text==1){
				if(Ti.App.Properties.getInt('ingles')==1){
					dialogHora.message="Dear user, we remind you that this service has a minimum hiring of 2 hours.\n you like to continue?";
				}else{
					dialogHora.message="Estimado usuario le recordamos que este servicio tiene una contratación minima de 2 horas.\n¿Desea Continuar?";
				}
				dialogHora.show();
						
			}else if(bandera.text==2){
				if(Ti.App.Properties.getInt('ingles')==1){
					dialogHora.message="Dear user, we remind you that this service has a minimum hiring of 2 hours.\n you like to continue?";
				}else{
					dialogHora.message="Estimado usuario le recordamos que este servicio tiene una contratación minima de 2 horas.\n¿Desea Continuar?";
				}
				dialogHora.show();
			}else if(bandera.text==3){
				if(Ti.App.Properties.getInt('ingles')==1){
					dialogHora.message="Dear user, we remind you that this service has a minimum hiring of 2 hours.\n you like to continue?";
				}else{
					dialogHora.message="Estimado usuario le recordamos que este servicio tiene una contratación minima de 2 horas.\n¿Desea Continuar?";
				}
				dialogHora.show();
			}
		}
	});
	
	
	
	btnLista.addEventListener('click',function(){
	if(pagar==1){
		PagoPen.open("POST","http://subtmx.com/webservices/statuspago.php");
        params = {
        id_usuario:Ti.App.Properties.getInt("id_usuario")
        };
       	PagoPen.send(params);
	}else{
		if (Ti.App.Properties.hasProperty('id_usuario')) {
			
				versionApp2.open("POST", "http://subtmx.com/webservices/version.php");
					params = {
						version :versionI,
						dispositivo : "android"
					};
				versionApp2.send(params);
				
		
	   }else{
	   		if(Ti.App.Properties.getInt('ingles')==1){
					alert('Only registered users');
					}else{
					alert('Solo usuarios registrados');
			}
	   }
	}
		
	});
	
	
	versionApp2.onload = function() {
		json = this.responseText;
		response = JSON.parse(json);
		if (response.logged == true) {
			Ti.App.Properties.setInt("radiofirebase",response.radiofirebase);
			disbloqueado2.open("POST", "http://subtmx.com/webservices/registrodispositivo.php");
			params = {
				user_id : Ti.App.Properties.getInt("id_usuario"),
				device_token : Titanium.Platform.id,
				device_type : "android"
			};
			disbloqueado2.send(params);

		} else {
			version.visible = true;
		}
	}; 

		
		
	disbloqueado2.onload = function() {
		json = this.responseText;
		response = JSON.parse(json);
		if (response.logged != null) {
			if (response.logged == true) {

				if (Ti.App.Properties.hasProperty('id_usuario')) {
					dialog.show();
				} else {
					if (Ti.App.Properties.getInt('ingles') == 1) {
						alert('Only registered users');
					} else {
						alert('Solo usuarios registrados');
					}
				}
			} else {
				dispositivoBloqueado.visible = true;
			}
		} else {
			alert("Error de conexión, intente nuevamente");
		}
	};

	btnPedir.addEventListener('click',function(){
		//vistaCotizacion.visible=true;
		
		if (destinoSeleccionado == 0 || origenSeleccionado == 0) {
			if (Ti.App.Properties.getInt('ingles') == 1) {
				alert('Please select a trip type');
			} else {
				alert('Favor de seleccionar su origen y destino');
			}
		} else {
			txtviajeOrigen.text = 'Origen: ' + txtOrigen.value;
			txtviajeDestino.text = 'Destino: ' + txtDestino.value;
			rango.open("POST", "http://subtmx.com/webservices/phpgeo-master/tests/geo.php");
			params = {
				latitudo : latInicio.text,
				longitudo : lngInicio.text,
				latitudd : latDestino.text,
				longitudd : lngDestino.text,
			};
			rango.send(params);
		}

	});
	
	
	Titanium.Geolocation.getCurrentPosition(function(e){
		if (e.error){
        	if (Ti.App.Properties.getInt('ingles')==1){
        		//alert('Unable to get current location.');
        		return;
        	 }
        	else{
        		//alert('No se puede obtener ubicacion actual');
        		}
        		return;
    		}
		if(Ti.App.Properties.getInt('ingles')==1){
			pinUsuario = Map.createAnnotation({
			latitude:e.coords.latitude, 
			longitude:e.coords.longitude,
       		title:"My location",
    			subtitle:'User',
    			image:'/images/pinOrigen.png',
    			myid:1,
    			draggable: true
		});
		}else{
			pinUsuario = Map.createAnnotation({
			latitude:e.coords.latitude, 
			longitude:e.coords.longitude,
       		title:"Mi ubicacion",
    			subtitle:'Usuario',
    			image:'/images/pinOrigen.png',
    			myid:1,
    			draggable: true
		});
		}
	
		
		latInicio.text=e.coords.latitude;
        lngInicio.text=e.coords.longitude;
        Titanium.Geolocation.reverseGeocoder(e.coords.latitude,e.coords.longitude,function(evt){
					if (evt.success) {
						var places = evt.places;
						if (places && places.length) {
							geocoderlbl.text = places[0].address;
							
							txtOrigen.value=geocoderlbl.text;
							Ti.App.Properties.setDouble("latIN",e.coords.latitude);
        					Ti.App.Properties.setDouble("longIN",e.coords.longitude);
        					Ti.App.Properties.setString("txtorigen",txtOrigen.value);
						
						} else {
							if(Ti.App.Properties.getInt('ingles')==1){
									geocoderlbl.text = "Check your internet connection";
							}else{
									geocoderlbl.text = "Verifique su conexión a internet";
							}
						}
						
					}
					else {
						Ti.UI.createAlertDialog({
							title:'Conexión inestable',
							message:evt.error
						}).show();
					}
				});
        if(Ti.App.Properties.getInt('ingles')==1){
			pinDestino = Map.createAnnotation({
			latitude:e.coords.latitude + 0.0009999, 
			longitude:e.coords.longitude -0.0009999 ,
    			title:"Destination",
    			subtitle:'Place the pin on your destination',
    			image:"/images/pinDestino.png",
    			myid:1,
    			draggable: true
    		});
		}else{
			pinDestino = Map.createAnnotation({
			latitude:e.coords.latitude + 0.0009999, 
			longitude:e.coords.longitude -0.0009999 ,
    			title:"Destino",
    			subtitle:'Coloca el pin en tu destino',
    			image:"/images/pinDestino.png",
    			myid:1,
    			draggable: true
    		});
		}
				
        mapview = Map.createView({
        		mapType: Map.NORMAL_TYPE,
        		region: {latitude:e.coords.latitude, longitude:e.coords.longitude, latitudeDelta:0.003, longitudeDelta:0.003},      
        		animate:true,
        		regionFit:true,
        		userLocation:true,
        		annotations:[pinUsuario,pinDestino]
        });
        centerView.add(mapview);
	 });
	var xhrLocationCode = Ti.Network.createHTTPClient();var requestUrl;
		xhrLocationCode.setTimeout(120000);
		
		autocomplete_table.addEventListener("click", function(e){
			//txtDestino.focus();
		});
		
		txtDestino.addEventListener('click',function(){
			if(servSel==0){
				if(Ti.App.Properties.getInt('ingles')==1){
				alert('Please select a trip type');
				}else{
				alert('Favor de seleccionar un tipo de viaje');
				}
			}else{
			container.visible=true;
			autocomplete_table.visible=true;
			autocomplete_table2.visible=false;
			destinoView.height='13%';
			origenView.height='13%';
			// vistaTaxiRosa.height='35%';
			// *vistaAeropuerto.height='35%';
			}
		});
		
		txtOrigen.addEventListener('click',function(){
			container.visible=true;
			autocomplete_table2.visible=true;
			autocomplete_table.visible=false;
			destinoView.height='13%';
			origenView.height='13%';
			// vistaTaxiRosa.height='35%';
			// *vistaAeropuerto.height='35%';
			
		});
		
			
		txtDestino.addEventListener('focus',function(){
			container.visible=true;
			autocomplete_table.visible=true;
			autocomplete_table2.visible=false;
		   destinoView.height='13%';
			origenView.height='13%';
			// vistaTaxiRosa.height='35%';
			// *vistaAeropuerto.height='35%';
		});
		txtOrigen.addEventListener('focus',function(){
			container.visible=true;
			autocomplete_table2.visible=true;
			autocomplete_table.visible=false;
		  /* destinoView.height='13%';
			origenView.height='13%';
			vistaTaxiRosa.height='35%';
			vistaAeropuerto.height='35%';*/
		});
		
	txtOrigen.addEventListener('return', function(e){
  			txtOrigen.blur();
  			bdrBusqueda = 0;
  			requestUrl = "http://maps.google.com/maps/api/geocode/json?address=" + txtOrigen.value.replace(' ', '+');
			requestUrl += "&sensor=" + (Ti.Geolocation.locationServicesEnabled == true);
			xhrLocationCode.open("GET", requestUrl);
			xhrLocationCode.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
			xhrLocationCode.send();		
		});
		
			 windowM.addEventListener('android:back', function(){
     		 	destinoView.height='8%';
     		 	if(txtDestino.blur()==true){
     		 		alert("cerrado");
     		 	};
     		});
			txtDestino.addEventListener('return', function(e){
  			txtDestino.blur();
  			bdrBusqueda = 1;
  			requestUrl = "http://maps.google.com/maps/api/geocode/json?address=" + txtDestino.value.replace(' ', '+');
			requestUrl += "&sensor=" + (Ti.Geolocation.locationServicesEnabled == true);
			xhrLocationCode.open("GET", requestUrl);
			xhrLocationCode.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
			xhrLocationCode.send();		
		});
			  
	  xhrLocationCode.onerror = function(e) {};
		xhrLocationCode.onload = function(e) {
			// mapview.region = {latitude:latInicio.text, longitude:lngInicio.text, latitudeDelta:0.03, longitudeDelta:0.03};	
			 var response = JSON.parse(this.responseText);
				if (response.status == 'OK' && response.results != undefined && response.results.length > 0) {
					if (bdrBusqueda == 0) {
						pinUsuario.latitude =response.results[0].geometry.location.lat;
						pinUsuario.longitude = response.results[0].geometry.location.lng;
						latInicio.text = response.results[0].geometry.location.lat;
						lngInicio.text = response.results[0].geometry.location.lng;
						Ti.App.Properties.setDouble("latIN",latInicio.text);
        				Ti.App.Properties.setDouble("longIN",lngInicio.text);
        				origen=txtOrigen.value;
			       		Ti.App.Properties.setString("txtorigen",origen);
			       		txtviajeOrigen.text='Origen: '+origen;
			       		Titanium.Geolocation.reverseGeocoder(latInicio.text,lngInicio.text,function(evt){
							if (evt.success) {
								var places = evt.places;
								if (places && places.length) {
									geocoderlbl.text = places[0].address;
								}else{
									geocoderlbl.text = "No address found";
								}
							}else{
								Ti.UI.createAlertDialog({
									title:'Reverse geo error',
									message:evt.error
								}).show();
						}
					});
			       	mapview.region = {latitude:latInicio.text, longitude:lngInicio.text, latitudeDelta:0.03, longitudeDelta:0.03};	
			
					}else if (bdrBusqueda == 1) {
						
						drapDest.text='1';
						pinDestino.latitude =response.results[0].geometry.location.lat;
						pinDestino.longitude = response.results[0].geometry.location.lng;
						latDestino.text = response.results[0].geometry.location.lat;
						lngDestino.text = response.results[0].geometry.location.lng;
						Ti.App.Properties.setDouble("latIN",latInicio.text);
        				Ti.App.Properties.setDouble("longIN",lngInicio.text);
        				origen=txtOrigen.value;
			       		Ti.App.Properties.setString("txtorigen",origen);
						Ti.App.Properties.setDouble("latDES",latDestino.text);
       					Ti.App.Properties.setDouble("longDES",lngDestino.text);
						destino=txtDestino.value;
       		      		Ti.App.Properties.setString("txtdestino",destino);
       		      		txtviajeOrigen.text='Origen: '+origen;
       		      		txtviajeDestino.text='Destino: '+destino;
       		      		if(destinoSeleccionado==0){
       		      			var dialogDes = Ti.UI.createAlertDialog({
									cancel : 1,
									buttonNames : ['Acceptar'],
									message : 'Si la dirección de destino no es el correcto, puedes arrastrar el pin al lugar exacto',
									title : 'Mensaje'
								});
       		      			dialogDes.show();
       		      		}
       		      		destinoSeleccionado=1; origenSeleccionado=1;
						destinoView.height='8%';
						origenView.height='8%';
						// vistaTaxiRosa.height='20%';
						// *vistaAeropuerto.height='20%';
						container.visible=false;
				
					if(bandera.text==4){
					txtDestino.editable=false;
					}else{
							
					}
					
				mapview.region = {latitude:latDestino.text, longitude:lngDestino.text, latitudeDelta:0.03, longitudeDelta:0.03};	
		
				}
			}
			response = null;
		};

	Titanium.Geolocation.getCurrentPosition(function(e) {
					if (e.error) {
						if (Ti.App.Properties.getInt('ingles') == 1) {
							alert('Unable to get current location.');
							return;
						} else {
							alert('No se puede obtener ubicacion actual,verifique que tenga prendido su gps');
						}
						return;
					}
	mapview.addEventListener('regionchanged', function(e) {

		Titanium.Geolocation.reverseGeocoder(e.latitude, e.longitude, function(evt) {
			if (evt.success) {
				var places = evt.places;
				if (places && places.length) {
					if (destinoSeleccionado == 1) {
						destino = places[0].address;
						txtDestino.value = places[0].address;
						Ti.App.Properties.setString("txtdestino", destino);
					}

				} else {
					geocoderlbl.text = "Verifique su conexión a internet";
				}
			} else {
				Ti.UI.createAlertDialog({
					title : 'Conexión inestable',
					message : evt.error
				}).show();
			}
		});

		latDestino.text = e.latitude;
		lngDestino.text = e.longitude;
		Ti.App.Properties.setDouble("latDES", e.latitude);
		Ti.App.Properties.setDouble("longDES", e.longitude);
		txtviajeDestino.text = 'Destino: ' + destino;
		pinDestino.setLatitude(e.latitude);
		pinDestino.setLongitude(e.longitude);
	}); 
});

		
    iconHeader.addEventListener('click',function(event){
   
    		clickMenu(null);
	});
    function clickMenu(direction){
    		if(menuView._toggle === false && (direction==null || direction=='right')){
        		centerView.animate({right:'-75%' ,duration: 100,curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT});
            	menuView.animate({left: 0,duration: 100,curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT});
            	menuView._toggle=true;
        }else if(direction==null || direction=='left'){
            centerView.animate({right:0 ,duration: 100,curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT});
            menuView.animate({left: '-75%',duration: 100,curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT});
            menuView._toggle=false;
        };
	}
	
	btnvistaTaxiRosa2.addEventListener('click',function(){
	
		autoCheck.value=false;
		iconHeader.enabled=true;
		vistavistaTaxiRosa.visible=false;
	});
	
	btnvistaTaxiRosa.addEventListener('click',function(){
		if(	txtvistaTaxiRosa.value==''){
			alert("Debe escribir el nombre de la mujer");
		}else{
		Ti.App.Properties.setString("NMujer",txtvistaTaxiRosa.value);
		iconHeader.enabled=true;
		vistavistaTaxiRosa.visible=false;
		}
	});
	btnvistaAeropuerto.addEventListener('click',function(){
		if(txtvistaAeropuerto.value=='' || txtvistaAeropuerto2.value==''){
    		 if(Ti.App.Properties.getInt('ingles')==1){
    		alert("All data are required");
    		}else{alert("Todos los datos son obligatorios");
    		}
    										
    	}else{
    		vistavistaAeropuerto.visible=false;
    	}
		
	});
	
	btnCotizar2.addEventListener('click', function() {
			vistaCotizacion.visible=false;
	});
	
	
	btnCotizar.addEventListener('click', function() {
		btnCotizar.enabled=false;
		if (bandera.text == 4) {
			if (Ti.App.Properties.hasProperty("paypal")) {
				pagoReq.open("POST", "http://subtmx.com/webservices/paypal/samples/BillingAgreements/DoReferenceAeropuerto.php");
				params = {
					id_biling : Ti.App.Properties.getString("id_biling2"),
					name : Ti.App.Properties.getString('nombre'),
					lastname : Ti.App.Properties.getString('apellido'),
					descripcion_viaje : 'Tipo de viaje es: ' + carroSelect.text + ', Km recorridos: ' + Ti.App.Properties.getDouble('dus'),
					costo_total : Ti.App.Properties.getDouble("tarifaFinal")
				};
				pagoReq.send(params);
			} else if (Ti.App.Properties.hasProperty("coneckta")) {
				pagoReq.open("POST", "http://subtmx.com/webservices/php-conekta-master/conekta_card_aeropuerto.php");
				params = {
					id_usuario : Ti.App.Properties.getInt('id_usuario'),
					costo_total : Ti.App.Properties.getDouble("tarifaFinal"),
					descripcion_viaje : 'Tipo de viaje es: ' + carroSelect + ', Km recorridos: ' + Ti.App.Properties.getString('kmt'),

				};
				pagoReq.send(params);
			}

		} else {

			var webViewCacheDir = Ti.Filesystem.getFile(Ti.Filesystem.applicationCacheDirectory, "webviewCache");
			webViewCacheDir.deleteDirectory(true);

			numPasajero.text = 0;
			Ti.App.Properties.setInt("vistaActiva", 0);
			chofer = require('/interface/chofer');
			new chofer(bandera.text, latInicio.text, lngInicio.text, latDestino.text, lngDestino.text, geocoderlbl.text, ti_ser.text, taxiRosaSelect, carroSelect.text, numPasajero.text).open({
				modal : true
			});
			windowM.close();
			windowM.remove(webview);
			webview.release();

			webview = null;
			Ti.API.info('WebView: ' + webview);
			windowM = null;
			Ti.API.info('windowM: ' + windowM);
		}
		vistaCotizacion.visible = false;
	}); 
 

	
pagoReq.onload = function() {
		json = this.responseText;
		response = JSON.parse(json);
		 if (response.logged != null) {
		if (response.logged == true) {

			registerServ.open("POST", "http://subtmx.com//webservices/createservice.php");
			params = {
				id_tipo_servicio : bandera.text,
				id_usuario : Ti.App.Properties.getInt("id_usuario"),
				id_chofer : 16,
				latitud_origen : latInicio.text,
				longitud_origen : lngInicio.text,
				latitud_destino : latDestino.text,
				longitud_destino : lngDestino.text,
				lugar_origen : 'Aeropuerto internacional de Acapulco',
				opcion_servicio : 1,
				tarifa_inicial : Ti.App.Properties.getDouble("tarifaFinal"),
				id_tipo_pago : 1,
				destino : Ti.App.Properties.getString("txtdestino"),
				costo_total : Ti.App.Properties.getDouble("tarifaFinal")
			};
			registerServ.send(params);
		} else {
			if (Ti.App.Properties.getInt('ingles') == 1) {
				alert("Unable to process your request");
			} else {
				alert("No se pudo Realizar el pedido");
			}

		}
		}else{
			alert("Error de conexión, intente nuevamente");
		}
	}; 

	//
 		
 		//
	var mensajeUsuarios= Ti.UI.createView({width:'100%', height:'100%', visible:false,zIndex: 20,backgroundColor:'#B3000000'});
   	
	Ti.App.addEventListener('fbController:alerta', function(e) {
		var vistamensajeUsuarios= Ti.UI.createView({width:'80%', height:Ti.UI.SIZE,zIndex: 20,backgroundColor:'white',borderColor:'black',borderRadius:6,layout:'vertical'});
   		var TitulomensajeUsuarios=Titanium.UI.createLabel({text:'NOTIFICACIÓN',top:20,color:'black',font:{fontSize:18,fontFamily: 'Menlo Bold Italic'}});
   		var txtmensaje=Titanium.UI.createLabel({text:e.mensaje,top:20,color:'#2980b9',font:{fontSize: 15,fontFamily: 'Menlo Bold Italic'},textAlign:'center',width:'80%'});
   		btnmensajeUsuarios = Titanium.UI.createButton({title:'Aceptar',center:0,backgroundColor:'#0c83c7',top:10,borderRadius:5});
    	var vistamensajeUsuarios2= Ti.UI.createView({width:'100%', height:30,layout:'horizontal',top:0});
    	mensajeUsuarios.add(vistamensajeUsuarios);
    	vistamensajeUsuarios.add(TitulomensajeUsuarios);
    	vistamensajeUsuarios.add(txtmensaje);
    	vistamensajeUsuarios.add(btnmensajeUsuarios);
    	vistamensajeUsuarios.add(vistamensajeUsuarios2);
		
		player.play();
		Titanium.Android.NotificationManager.notify(0, Titanium.Android.createNotification({
			contentTitle : 'NUEVA ALERTA',
			contentText : e.mensaje,
			tickerText : 'mensaje de SUBT',
			contentIntent : Ti.Android.createPendingIntent({
				intent : Ti.Android.createIntent({
					action : Ti.Android.ACTION_MAIN,
					className : 'com.subtmx.subt.SubtActivity',
					packageName : 'com.subtmx.subt'
				})
			}),
		}));
		
		
		
		mensajeUsuarios.visible=true;
		btnmensajeUsuarios.addEventListener('click', function(e) {
				mensajeUsuarios.visible=false;
				
				mensajeUsuarios.remove(vistamensajeUsuarios);
				player.stop();
				 Ti.App.fireEvent('fbController:Rleido', {
				id_usuario : Ti.App.Properties.getInt("id_usuario")
				});
				Titanium.Android.NotificationManager.cancelAll();
		});
	}); 

    header.add(iconHeader);
    header.add(titulo);
    vistaBottom.add(btnLista);
    vistaBottom.add(autoCheck);
    vistaBottom.add(listText);
    vistaBottom.add(autoRosa);
	centerView.add(header);
	centerView.add(vistaBottom);
	centerView.add(btnPedir);
	centerView.add(destinoView);
  	centerView.add(origenView);
  	
   	origenView.add(leftButtonOrigen);
 	destinoView.add(leftButtonDestino);
  	origenView.add(txtOrigen);
	destinoView.add(txtDestino);
	menuView.add(logoLeft);
	menuView.add(lblHome);
	menuView.add(lblMisPedidos);
	menuView.add(lblMiPerfil);
	menuView.add(lblPromociones);
	menuView.add(lblPagos);
	menuView.add(lblConfiguración);
	menuView.add(lblUSer);
	menuView.add(lblVersion);
	vistavistaTaxiRosa.add(vistaTaxiRosa);
	vistaTaxiRosa.add(txtvistaTaxiRosa);
	vistaTaxiRosa2.add(btnvistaTaxiRosa);
	vistaTaxiRosa2.add(btnvistaTaxiRosa2);
	vistaTaxiRosa.add(vistaTaxiRosa2);
	vistaAeropuerto2.add(txtvistaAeropuerto);
	vistaAeropuerto2.add(txtvistaAeropuerto2);
	vistaAeropuerto.add(vistaAeropuerto2);
	vistaAeropuerto.add(btnvistaAeropuerto);
	windowM.add(vistavistaTaxiRosa);
	vistavistaAeropuerto.add(vistaAeropuerto);
	windowM.add(vistavistaAeropuerto);
	windowM.add(centerView);
	windowM.add(menuView);
	centerView.add(container);
	vistaBotones.add(btnCotizar);
   	vistaBotones.add(btnCotizar2);
   	
   	
   	vistatxt.add(txtCotizar);
   	vistatxt.add(txtCosto);
   	vistatxt.add(txtPrecio);
   	vistatxt.add(txtAuto);
   	vistatxt.add(txtviajeOrigen);
   	vistatxt.add(txtviajeDestino);
   	vistatxt.add(txtmensaje);
   	vistaCotizar.add(vistatxt);
   	vistaCotizar.add(vistaBotones);
   	vistaCotizar.add(vistaBotones2);
  	vistaCotizacion.add(vistaCotizar);	
  	
  	dispositivoBloqueado.add(vistamensajedispositivoBloqueado);
    vistamensajedispositivoBloqueado.add(TitulomensajedispositivoBloqueado);
    vistamensajedispositivoBloqueado.add(txtdispositivoBloqueado);
    vistamensajedispositivoBloqueado.add(btnmensajedispositivoBloqueado);
    vistamensajedispositivoBloqueado.add(vistamensajedispositivoBloqueado2);
	centerView.add(vistaCotizacion);
	centerView.add(mensajeUsuarios);
	centerView.add(dispositivoBloqueado);
	centerView.add(serviciohora);
	centerView.add(version);
    return windowM;
	
};
module.exports = mapa;