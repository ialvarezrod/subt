function promociones(){
  
    var window,lblCuenta,lblVencimiento,lblTarjeta,txtCuenta,txtVencimiento,txtTarjeta,cmbTarjeta,cmbVencimiento,btnCuenta,centerView,titulo,btTodas,btNuevas;
    var tableData = [],tableView,centerView;
      
    window= Ti.UI.createWindow({theme:'Theme.AppCompat.Translucent.NoTitleBar',backgroundColor:'white'});
	window .orientationModes = [Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT];
	
	/* Vista Principal */
	centerView =Ti.UI.createView({height: 'auto',right:0,top: 0,width:'100%',zIndex:1});
	header = Ti.UI.createView({backgroundImage: '/images/barra.jpg',height: '19%',right:0,top: 0,width:'100%',zIndex: 3});
	iconHeader = Ti.UI.createButton({	backgroundImage:'/images/menuicon.png', top:'20%',left:'4%',zIndex:2,height:'22%',width:'9.5%'});
	titulo=Ti.UI.createLabel({text:'Promociones', color:'#232323',center:0,top:'15%',zIndex:2,font:{fontSize:'15.5%'}});
	btTodas = Ti.UI.createButton({backgroundColor:'#007aff',title:'Todas', top:'40%',left:'22.5%',zIndex:2,height:'25%',width:'28%'});
    btNuevas = Ti.UI.createButton({backgroundColor:'transparent',color:'#007aff',borderColor:'#007aff',title:'Nuevas', top:'40%',left:'49%',zIndex:2,height:'25%',width:'28%'});

		/* Menu Lateral */
	menuView = Ti.UI.createView({backgroundImage:'/images/bgmenu.jpg',height: 'auto',left: '-75%',	top:0,width: '75%',zIndex: 9,	_toggle: false});
	logoLeft = Titanium.UI.createImageView({image:'/images/logo.png',width:'55%',height:'auto',zIndex:2,left:'15%',top:'20%',opacity:1});
	lblHome = Titanium.UI.createLabel({color:'black',text:'Home',top:'52%',left:'13%'});
	lblMisPedidos = Titanium.UI.createLabel({color:'black',text:'Mis Pedidos',top:'60%',left:'13%'});
	lblMiPerfil = Titanium.UI.createLabel({color:'black',text:'Mi Perfil',top:'68%',left:'13%'});
	lblPromociones = Titanium.UI.createLabel({color:'blue',text:'Promociones',top:'76%',left:'13%'});
	lblConfiguración = Titanium.UI.createLabel({color:'black',text:'Configuración',top:'92%',left:'13%'}); 
	lblPagos = Titanium.UI.createLabel({color:'black',text:'Pago',top:'84%',left:'13%'});	
   	lblUSer  = Ti.UI.createTextField({value:Ti.App.Properties.getString('usuario'),color:'#151431',left:'13%',top:'10%',zIndex:4,font:{fontSize:'17%'},editable:false,focus:false});

   if(Ti.App.Properties.getInt('ingles')==1){
   			titulo.text='Promotions';
   			lblMisPedidos.text='My books';
   			lblMiPerfil.text='Profile';
   			lblPromociones.text='Promotions';
   			lblPagos.text='Payment';
   			lblConfiguración.text='Configuration';
   		}
   		
   		
   		if(Ti.App.Properties.getInt('ingles')==1){
		var optsConfig = {options:  ['Help','cancellation airport'], selectedIndex: 2,destructive:0,title: 'Select an option'};
	}else{
	    var optsConfig = {options:  ['Ayuda','Cancelación aeropuerto'], selectedIndex: 2,destructive:0,title: 'Selecciona una opción'};	
	}
	
   		var dialogConfiguracion = Ti.UI.createOptionDialog(optsConfig);
		
		dialogConfiguracion.addEventListener('click', function(ed){
	   	if (ed.index === 0){
	   		ayuda = require('/interface/ayuda');
			new ayuda().open({modal:true});
		
	   	}else if (ed.index === 1){
	   		aeropuerto = require('/interface/aeropuerto');
			new aeropuerto().open({modal:true});
	   	}
	});
   	lblConfiguración.addEventListener('click',function(){
		if (Ti.App.Properties.hasProperty('id_usuario')) {
			dialogConfiguracion.show();
			}else{
			if(Ti.App.Properties.getInt('ingles')==1){
				alert('Only registered users');
			}else{
				alert('Solo usuarios registrados');
			}
	}
	});
   	lblHome.addEventListener('click',function(){
    	mapa = require('/interface/mapa');
  		new mapa().open({modal:true});
       	window.close();
 	});
 	
 	lblMisPedidos.addEventListener('click',function(){
        if (Ti.App.Properties.hasProperty('id_usuario')) {
            pedidos = require('/interface/pedidos');
           	new pedidos().open({modal:true});
            window.close();
        }else{
            if(Ti.App.Properties.getInt('ingles')==1){
        				alert ( 'Only registered');
        			}else{
        				alert('Solo usuarios registrados');
        			}
        }
    });
      
      lblMiPerfil.addEventListener('click',function(){
        	perfil = require('/interface/perfil');
       		new perfil().open({modal:true});  
    	   	window.close();
 	  });
   	
   		lblPagos.addEventListener('click',function(){
		if (Ti.App.Properties.hasProperty('id_usuario')) {
					paypal = require('/interface/tipoPago');
					new paypal().open();
			}else{
			if(Ti.App.Properties.getInt('ingles')==1){
				alert('Only registered users');
			}else{
				alert('Solo usuarios registrados');
			}
		}
	});
   	
       lblPromociones.addEventListener('click', function(){
     		clickMenu(null);
        }); 
 	
 		args = arguments[0] || {};url = "http://subtmx.com/webservices/promociones.php";db = {};
        function getData(){
            var xhr = Ti.Network.createHTTPClient({
                onload: function() {
                    db = JSON.parse(this.responseText);
                    if(db.servicios){
                        setData(db);
                    }
                },
                onerror: function(e) {
                    Ti.API.debug("STATUS: " + this.status);
                    Ti.API.debug("TEXT:   " + this.responseText);
                    Ti.API.debug("ERROR:  " + e.error);
                    alert('Comprueba tu conexion a internet');
                },
                timeout:5000
            }); 
            xhr.open('POST', url);
            var params = {
                    id_cliente:Ti.App.Properties.getInt('id_cliente')
                };
            xhr.send(params);
        }
               function setData(db){
            for(var i = 0; i < db.servicios.length; i++){
                row = Ti.UI.createTableViewRow({bottom:'5%',height:"390dp",backgroundColor:"white",backgroundSelectedColor:"blue"});
                logo = Ti.UI.createImageView({image:'/images/logo2.png',width:'15%',height:'auto',left:'2%',top:'2%'});
                titulo = Ti.UI.createLabel({text:db.servicios[i].titulo_promocion,color:'#566673',left:'18%',top:'2%'});
                fecha = Ti.UI.createLabel({text:db.servicios[i].fecha_inicio,color:'#bac1c8',right:'2%',top:'2%'});
                politica = Ti.UI.createLabel({text:db.servicios[i].politicas_promocion,color:'#007fff',left:'18%',top:'8%'});
                imgPromo = Ti.UI.createImageView({image:"http://subtmx.com/images/"+db.servicios[i].img_promocion.replace(/\s/g, '%20'),width:'95%',height:230,top:'20%',center:0});
                descPromo = Ti.UI.createLabel({text:db.servicios[i].desc_promocion,color:'#657582',left:'2%',top:'80%',right:'2%'});
                
                row.add(logo);
                row.add(titulo);
                row.add(fecha);
                row.add(politica);
                 row.add(imgPromo);
                row.add(descPromo);
                tableData.push(row);
            }
            tableView = Ti.UI.createTableView({data:tableData,top:'19%',bottom:0,scrollable:true});
            centerView.add(tableView);
        }
         getData();
         
         iconHeader.addEventListener('click',function(){
    		clickMenu(null);
		});
		centerView.addEventListener('swipe',function(event){
			if(event.direction=='left' || event.direction=='right' ){
			clickMenu(null);
			}	
		});
				
		function clickMenu(direction){
	 		if(menuView._toggle === false && (direction==null || direction=='right')){
				centerView.animate({right:'-75%',duration: 100,curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT});
				menuView.animate({left: 0,duration: 100,curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT});
				menuView._toggle=true;
			}else if(direction==null || direction=='left'){
				centerView.animate({right:0,duration: 100,	curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT	});
				menuView.animate({left: '-75%',duration: 100,curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT});
				menuView._toggle=false;
			};
		}
		 
		 header.add(iconHeader);
		 header.add(titulo);
         header.add(btTodas);
         header.add(btNuevas);
         centerView.add(header);
         centerView.add(titulo);
         window.add(menuView);
         window.add(centerView);
		menuView.add(logoLeft);
		menuView.add(lblHome);
		menuView.add(lblMisPedidos);
		menuView.add(lblMiPerfil);
		menuView.add(lblPromociones);
		menuView.add(lblConfiguración);
		menuView.add(lblPagos);
		menuView.add(lblUSer);
		return window;
}
module.exports = promociones;