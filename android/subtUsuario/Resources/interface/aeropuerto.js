
	function aeropuerto(){

	var window,vista,ImgPerfil,btnGuardar,txtPlaca,btnCancelar;

		
	window = Ti.UI.createWindow({theme:'Theme.AppCompat.Translucent.NoTitleBar',backgroundColor :'transparent',opacity: 2});
	window .orientationModes = [Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT];

	vista=Ti.UI.createView({left:'5%',right:'5%',top:'30%',bottom:'30%',backgroundColor:'white'});
	texto= Ti.UI.createLabel({ center:0,width :'auto', height : 'auto', top : '3%' ,text : 'Cancelación Aeropuerto',color:'black',font  : { fontSize : '25%'}});
	ImgPerfil= Ti.UI.createImageView({width:'21%',height:'26.5%',borderRadius:50,top:'21.5%',center:0,image:"/images/avion.jpg",zIndex:3});
	txtPlaca= Ti.UI.createTextField({hintText:'Escriba el folio de cancelación',top:'50%',center:0,width:'80%',heigth:'auto',color:'black',hintTextColor:'gray'});
    btnCancelar = Ti.UI.createButton({title:('Cancelar'),bottom:'5%',left:'5%',width:'auto',heigth:'auto',textAlign:'center',backgroundColor:'#0c83c7',color:'white'});
  	btnGuardar = Ti.UI.createButton({title:('Aceptar'),bottom:'5%',right:'5%',width:'auto',heigth:'auto',textAlign:'center',backgroundColor:'#0c83c7',color:'white'});
    	
    if(Ti.App.Properties.getInt('ingles')==1){
		texto.text='Cancellation airport';
		txtPlaca.hintText='Write the folio to cancellation';
		btnCancelar.title='Cancel';	
		btnGuardar.title='Accept';	
	}	
  	webview = Ti.UI.createWebView({url : '/firebase/index.html',width:0,height:0,top:0,left:0,visible:false,cacheMode:Titanium.UI.Android.WEBVIEW_LOAD_NO_CACHE});
	
							webview.addEventListener('load', function() {
							Ti.App.fireEvent('firebase:init2', {
							latitude : 16.762454,
							longitude : -99.754535,
							id_chofer:Ti.App.Properties.getInt("id_usuario")
						});
					});		
	window.add(webview);  
	vista.add(texto);
    vista.add(ImgPerfil);
    vista.add(txtPlaca);
    vista.add(btnGuardar);
    vista.add(btnCancelar);
  
    window.add(vista);
    
     window.addEventListener('android:back', function(){
     });
    
    	
	btnGuardar.addEventListener('click', function(e){
				var Canceladoformateado=txtPlaca.value.split('-');
				var Foliocancelado = Canceladoformateado[1];
					var webViewCacheDir = Ti.Filesystem.getFile(Ti.Filesystem.applicationCacheDirectory, "webviewCache");
					webViewCacheDir.deleteDirectory(true);
				
				Ti.App.fireEvent('fbController:Cancelacion', {
						folio:Foliocancelado,
						id_usuario:Ti.App.Properties.getInt("id_usuario"),
				});
	      			window.close();
   					window.remove(webview);
				 	webview.release();
          
				 
				 	Ti.API.info('WebView: ' + webview);
   				
   					Ti.API.info('windowM: ' + window);
	});
	btnCancelar.addEventListener('click', function(e){
					var webViewCacheDir = Ti.Filesystem.getFile(Ti.Filesystem.applicationCacheDirectory, "webviewCache");
					webViewCacheDir.deleteDirectory(true);
				
					window.close();
   					window.remove(webview);
				 	webview.release();
          
				 
				 	Ti.API.info('WebView: ' + webview);
   				
   					Ti.API.info('windowM: ' + window);
	});
	

	return window;
	};
	module.exports = aeropuerto;