function pedidos(){
	var window,lblCuenta,lblVencimiento,lblTarjeta,txtCuenta,txtVencimiento,txtTarjeta,cmbTarjeta,cmbVencimiento,btnCuenta;
	var tableData = [],tableView;
	
	window= Ti.UI.createWindow({theme:'Theme.AppCompat.Translucent.NoTitleBar',backgroundColor:'white'});
	window .orientationModes = [Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT];
	// Vista Lateral
	menuView = Ti.UI.createView({backgroundImage:'/images/bgMenu.jpg',height: 'auto',left: '-75%',	top:0,width: '75%',zIndex: 9,	_toggle: false});
	logoLeft = Titanium.UI.createImageView({image:'/images/logo.png',width:'55%',height:'auto',zIndex:2,left:'15%',top:'20%',opacity:1});
	lblHome = Titanium.UI.createLabel({color:'black',text:'Home',top:'52%',left:'13%'});
	lblMisPedidos = Titanium.UI.createLabel({color:'blue',text:'Mis Pedidos',top:'60%',left:'13%'});
	lblMiPerfil = Titanium.UI.createLabel({color:'black',text:'Mi Perfil',top:'68%',left:'13%'});
	lblPromociones = Titanium.UI.createLabel({color:'black',text:'Promociones',top:'76%',left:'13%'});
	lblPagos = Titanium.UI.createLabel({color:'black',text:'Pago',top:'84%',left:'13%'});	
	lblConfiguración = Titanium.UI.createLabel({color:'black',text:'Configuración',top:'92%',left:'13%'}); 
	titulo=Ti.UI.createLabel({text:'Mis Pedidos', color:'#232323',center:0,bottom:'25%',zIndex:2,font:{fontSize:'15.5%'}});
  	lblUSer  = Ti.UI.createTextField({value:Ti.App.Properties.getString('usuario'),color:'#151431',left:'13%',top:'10%',zIndex:4,font:{fontSize:'17%'},editable:false,focus:false});

	deleteServiciosReq = Titanium.Network.createHTTPClient();
		
		if(Ti.App.Properties.getInt('ingles')==1){
   			titulo.text='My books';
   			lblMisPedidos.text='My books';
   			lblMiPerfil.text='Profile';
   			lblPromociones.text='Promotions';
   			lblPagos.text='Payment';
   			lblConfiguración.text='Configuration';
   		}
   		if(Ti.App.Properties.getInt('ingles')==1){
		var optsConfig = {options:  ['Help','cancellation airport'], selectedIndex: 2,destructive:0,title: 'Select an option'};
	}else{
	    var optsConfig = {options:  ['Ayuda','Cancelación aeropuerto'], selectedIndex: 2,destructive:0,title: 'Selecciona una opción'};	
	}
	
   		
   		var dialogConfiguracion = Ti.UI.createOptionDialog(optsConfig);
		
		dialogConfiguracion.addEventListener('click', function(ed){
	   	if (ed.index === 0){
	   		ayuda = require('/interface/ayuda');
			new ayuda().open({modal:true});
		
	   	}else if (ed.index === 1){
	   		aeropuerto = require('/interface/aeropuerto');
			new aeropuerto().open({modal:true});
	   	}
	});
   		lblConfiguración.addEventListener('click',function(){
	if (Ti.App.Properties.hasProperty('id_usuario')) {
			dialogConfiguracion.show();
			}else{
			if(Ti.App.Properties.getInt('ingles')==1){
				alert('Only registered users');
			}else{
				alert('Solo usuarios registrados');
			}
	}
	});
		
		lblHome.addEventListener('click',function(){
			mapa = require('/interface/mapa');
			new mapa().open({modal:true});
			window.close();
		});
	
		lblMiPerfil.addEventListener('click',function(){
			perfil = require('/interface/perfil');
			new perfil().open({modal:true});
			window.close();
		});
	
		lblPromociones.addEventListener('click', function(){
			if (Ti.App.Properties.hasProperty('id_usuario')) {
				promo= require('/interface/promociones');
				new promo().open({modal:true});
				window.close();
			}else{
				if(Ti.App.Properties.getInt('ingles')==1){
        				alert ( 'Only registered');
        			}else{
        				alert('Solo usuarios registrados');
        			}
			}
		});	
		
		lblPagos.addEventListener('click',function(){
		if (Ti.App.Properties.hasProperty('id_usuario')) {
					paypal = require('/interface/tipoPago');
					new paypal().open();
			}else{
			if(Ti.App.Properties.getInt('ingles')==1){
				alert('Only registered users');
			}else{
				alert('Solo usuarios registrados');
			}
		}
	});
	
	     lblMisPedidos.addEventListener('click', function(){
     		clickMenu(null);
        }); 
	
		centerView =Ti.UI.createView({height: 'auto',right:0,top: 0,width:'100%',zIndex:1});
		header = Ti.UI.createView({backgroundImage: '/images/barra.jpg',height: '10%',right:0,top: 0,width:'100%',zIndex: 10});
		iconHeader = Ti.UI.createButton({	backgroundImage:'/images/menuicon.png', left:'3%',zIndex:2,height:'42%',width:'8.5%'});
		iconEdit = Ti.UI.createButton({	backgroundImage:'/images/editarP.png', left:'88%',zIndex:2,height:'53%',width:'8%'});
  			iconEdit2 = Ti.UI.createButton({	backgroundImage:'/images/palomita.png', left:'88%',zIndex:2,height:'53%',width:'8%',visible:false,enabled:false});
		fondoP = Ti.UI.createView({backgroundColor: '#eaeaea',height: '20%',bottom:0,width:'auto',zIndex: 2});
	 	logo = Titanium.UI.createImageView({image:'/images/logo.png',width:'20%',height:'auto',zIndex:2,center:0,top:'10%',bottom:'10%',opacity:1});
   
		args = arguments[0] || {};url = "http://subtmx.com/webservices/pedidos.php";db = {};
		
		function getData(){
			var xhr = Ti.Network.createHTTPClient({
				onload: function() {
					db = JSON.parse(this.responseText);
					if(db.servicios){
						setData(db);
					}
				},
				onerror: function(e) {
					Ti.API.debug("STATUS: " + this.status);
					Ti.API.debug("TEXT:   " + this.responseText);
					Ti.API.debug("ERROR:  " + e.error);
					alert('Comprueba tu conexion a internet');
				},
				timeout:5000
			});	
			xhr.open('POST', url);
			var params = {
    				id_usuario:Ti.App.Properties.getInt('id_usuario')
    			};
			xhr.send(params);
		}
		
		function setData(db){
			for(var i = 0; i < db.servicios.length; i++){
				row = Ti.UI.createTableViewRow({bottom:'40%',height:"100dp",backgroundColor:"white",backgroundSelectedColor:"blue"});
				horario_inicio = Ti.UI.createLabel({text:db.servicios[i].hora_inicio,color:'#151431',left:'10%',top:'45%',font:{fontSize:15}});
				vistaimg= Ti.UI.createView({width:65,height:65,borderRadius:30, top:'15%', bottom:15, left:'35%'});
				imgChofer = Ti.UI.createImageView({image:"http://subtmx.com/images/"+db.servicios[i].fotografia.replace(/\s/g, '%20'),width:65,height:97});
				folio = Ti.UI.createLabel({text:'FOLIO: '+db.servicios[i].folio,color:'#151431',left:'55%',top:'15%',font:{fontSize:12,fontWeight:'bold'}});
				lugar_origen = Ti.UI.createLabel({text:db.servicios[i].lugar_destino,color:'#151431',left:'55%',top:'28%',font:{fontSize:12}});
				nombre_chofer = Ti.UI.createLabel({text:db.servicios[i].nombre_chofer,color:'#a2a1b8',left:'55%',top:'70%',font:{fontSize:13}});
				status = Ti.UI.createView({width:15,height:15,borderRadius:7,top:'34%', left:'2%'});
				fecha = Ti.UI.createLabel({text:db.servicios[i].fecha,color:'#151431 ',left:'10%',top:'30%',font:{fontSize:15,fontWeight:'bold'}});
                precio = Ti.UI.createLabel({text:'$ '+db.servicios[i].costo + ' MXN',color:'#151431 ',left:'10%',top:'60%',font:{fontSize:14}});
			
				
				
			
				
				
				if (db.servicios[i].status_servicio == 1) {
						status.backgroundColor = '#09c863';
				}else if (db.servicios[i].status_servicio == 0){
						status.backgroundColor = '#a00a1c';
				}
				
				vistaimg.add(imgChofer);
				row.add(status);
				row.add(vistaimg);
				row.add(horario_inicio);
				row.add(lugar_origen);
				row.add(nombre_chofer);
				tableData.push(row);
				row.add(folio);
				row.add(status);
				row.add(vistaimg);
				row.add(fecha);
				row.add(precio);
				
			}
			tableView = Ti.UI.createTableView({data:tableData,top:'10%',bottom:'20%',scrollable:true});
			centerView.add(tableView);
				
			tableView.addEventListener('delete',function(e){
				deleteServiciosReq.open("POST","http://subtmx.com/webservices/deleteservicios.php");
        			params = {
            			id_servicio:e.rowData.postIdServicio
        			};
        			deleteServiciosReq.send(params);
			});	
			
		}
		deleteServiciosReq.onload = function(){
    			json = this.responseText;
    			response = JSON.parse(json);
    			if (response.logged == true){
    				
    			}else{
        			alert(response.message);
    			}
		};
		
		iconEdit.addEventListener('click',function(){
	  		 if (Ti.App.Properties.hasProperty('id_usuario')) {
	  	  		iconEdit2.visible=true;
	  	  		iconEdit2.enabled=true;
	  	  		iconEdit.visible=false;
	  	  		iconEdit.enabled=false;
	  		}else{
	  			alert('Solo usuarios registrados');
	  		}
	  
	  	});
	  	  
	  	  iconEdit2.addEventListener('click',function(){
	  	  iconEdit.visible=true;
	  	   iconEdit.enabled=true;
	      iconEdit2.visible=false;
	      iconEdit2.enabled=false;
	      });
	    
		 iconHeader.addEventListener('click',function(){
    		clickMenu(null);
		});
		centerView.addEventListener('swipe',function(event){
			if(event.direction=='left' || event.direction=='right' ){
			clickMenu(null);
			}	
		});
				
		function clickMenu(direction){
	 		if(menuView._toggle === false && (direction==null || direction=='right')){
				centerView.animate({right:'-75%',duration: 100,curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT});
				menuView.animate({left: 0,duration: 100,curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT});
				menuView._toggle=true;
			}else if(direction==null || direction=='left'){
				centerView.animate({right:0,duration: 100,	curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT	});
				menuView.animate({left: '-75%',duration: 100,curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT});
				menuView._toggle=false;
			};
		}
	
		fondoP.add(logo);	
		header.add(iconHeader);
		//header.add(iconEdit);
		//header.add(iconEdit2);
	   	header.add(titulo);
	  	centerView.add(fondoP); 
	  	centerView.add(header);
		menuView.add(logoLeft);
		menuView.add(lblHome);
		menuView.add(lblMisPedidos);
		menuView.add(lblMiPerfil);
		menuView.add(lblPromociones);
		menuView.add(lblPagos);
		menuView.add(lblUSer);
		menuView.add(lblConfiguración);
		window.add(menuView);
	   	window.add(centerView);	
		getData();
		return window;
};
module.exports = pedidos;