function login(){
	var window,lblUser,lblpass,btnlogin,scrollView,txtUser,txtPass,lblreset,btnFacebook,loginReq,json,response,resetReq,mapa;
	 
	window = Ti.UI.createWindow({backgroundImage:'/images/bgLogin.png'});
    window .orientationModes = [Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT];

 	lblUser = Titanium.UI.createLabel({text:'Usuario/Correo',center:0,top:'20%',color:'#49a6ff'});
    txtUser = Titanium.UI.createTextField({color:'#0066cc',hintText:'Usuario',center:0,top:'26%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,value:"abc@gmail.com",textAlign:'center'});
    lblpass= Titanium.UI.createLabel({text:'Contraseña',center:0,top:'36%',color:'#49a6ff'});
    txtPass = Titanium.UI.createTextField({passwordMask:true,hintText:'***********',center:0,top:'42%',color:'#0066cc',center:0,width:'80%',height:40,leftButtonPadding:10,borderRadius:6,value:"***********",textAlign:'center'});
    btnlogin = Titanium.UI.createButton({title:'Inicia Sesión',color:'white',borderRadius:30,borderColor:'white',top:'55%',width:'85%',height:60,backgroundColor:'#0c83c7'});
    lblreset = Titanium.UI.createLabel({text:'Recuperar Contraseña',color:'#075582',top:'70%',center:0});
    loginReq = Titanium.Network.createHTTPClient();
    resetReq = Titanium.Network.createHTTPClient();
    codigo = Titanium.Network.createHTTPClient();
     var codigoAcceso= Ti.UI.createView({width:'100%', height:'100%', visible:false,zIndex: 20,backgroundColor:'#B3000000'});
     var vistacodigoAcceso= Ti.UI.createView({width:'80%', height:Ti.UI.SIZE,zIndex: 20,backgroundColor:'white',borderColor:'black',borderRadius:6,layout:'vertical'});
   	 var TitulocodigoAcceso=Titanium.UI.createLabel({text:'CÓDIGO DE ACCESO',top:20,color:'black',font:{fontSize:18,fontFamily: 'Menlo Bold Italic'}});
   	 var txtcodigoAcceso=Titanium.UI.createTextField({hintText:"0000",top:20,color:'#2980b9',hintTextColor:'gray',font:{fontSize: 15,fontFamily: 'Menlo Bold Italic'},textAlign:'center',width:'80%'});
   	 var vistacodigoAcceso1= Ti.UI.createView({width:Ti.UI.SIZE, height:Ti.UI.SIZE,layout:'horizontal',top:10,center:0});
	 var btncodigoAcceso = Titanium.UI.createButton({title:'Aceptar',center:0,backgroundColor:'#0c83c7',borderRadius:5});
   	 var btncodigoAcceso2 = Titanium.UI.createButton({title:'Cancelar',left:10,backgroundColor:'#0c83c7',borderRadius:5});
     var vistacodigoAcceso2= Ti.UI.createView({width:'100%', height:30,layout:'horizontal',top:0});
	
	codigoAcceso.add(vistacodigoAcceso);
	vistacodigoAcceso.add(TitulocodigoAcceso);
	vistacodigoAcceso.add(txtcodigoAcceso);
	vistacodigoAcceso.add(vistacodigoAcceso1);
	vistacodigoAcceso1.add(btncodigoAcceso);
	vistacodigoAcceso1.add(btncodigoAcceso2);
	vistacodigoAcceso.add(vistacodigoAcceso2);
    window.add(codigoAcceso);
    
    
   
	btncodigoAcceso.addEventListener('click', function() {
		txtcodigoAcceso.value=txtcodigoAcceso.value.replace(/^\s+/g, '').replace(/\s+$/g, '');
			if(txtcodigoAcceso.value!=''){
					codigo.open("POST", "http://subtmx.com/webservices/codigo.php");
						params = {
							usuario : Ti.App.Properties.getInt("id_usuario"),
							codigo : txtcodigoAcceso.value
						};
					codigo.send(params);		
			}else{
				Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'],message: 'Debe introducir el código de seguridad' ,title: 'Mensaje'}).show();
			}
	}); 

    btncodigoAcceso2.addEventListener('click', function() {
		txtcodigoAcceso.value='';
		codigoAcceso.visible=false;
		  Titanium.App.Properties.removeProperty("id_usuario");
          Titanium.App.Properties.removeProperty("id_cliente");
          Titanium.App.Properties.removeProperty("usuario2");
          Titanium.App.Properties.removeProperty("nombre");
          Titanium.App.Properties.removeProperty("apellido");
          Titanium.App.Properties.removeProperty("email");
          Titanium.App.Properties.removeProperty("telefono");
          Titanium.App.Properties.removeProperty("rfc");
          Titanium.App.Properties.removeProperty("razon");
          Titanium.App.Properties.removeProperty("genero");
          Titanium.App.Properties.removeProperty("nombrecompleto");
	});
    
    if(Ti.App.Properties.getInt("ingles")==1){
    	lblUser.text='User';
    	txtUser.hintText='User';
    	lblpass.text='Password';
    	btnlogin.title='Log in';
    	lblreset.text='Recover password';
    }
    
    
    window.addEventListener('android:back', function(){
    	 Titanium.App.Properties.removeProperty("id_usuario");
          Titanium.App.Properties.removeProperty("id_cliente");
          Titanium.App.Properties.removeProperty("usuario2");
          Titanium.App.Properties.removeProperty("nombre");
          Titanium.App.Properties.removeProperty("apellido");
          Titanium.App.Properties.removeProperty("email");
          Titanium.App.Properties.removeProperty("telefono");
          Titanium.App.Properties.removeProperty("rfc");
          Titanium.App.Properties.removeProperty("razon");
          Titanium.App.Properties.removeProperty("genero");
          Titanium.App.Properties.removeProperty("nombrecompleto");
	 	inicio= require('/interface/inicio');
        new inicio().open({modal:true});
        window.close();
	 });
    txtUser.addEventListener('focus',function(){
			txtUser.value = '';
    	});
    	txtPass.addEventListener('focus',function(){
        	txtPass.value = '';
    	});
    	
    	btnlogin.addEventListener('click',function(e){
            if (txtUser.value != '' && txtPass.value != ''){
                    loginReq.open("POST","http://subtmx.com/webservices/loginResquest.php");
                    params = {
                        username: txtUser.value,
                        password: Ti.Utils.md5HexDigest(txtPass.value)
                    };
                    loginReq.send(params);
            }else{
            	 if(Ti.App.Properties.getInt('ingles')==1){
            	 	alert('Username and password are required');
            	 }else{
            	 	alert("Usuario y password son requeridos");
            	 }
                    
			}
    });
    	
      
    
    loginReq.onload = function(){
            json = this.responseText;
            response = JSON.parse(json);
            if (response.logged == true){
                Ti.App.Properties.setInt("id_usuario", response.id_usuario);
                Ti.App.Properties.setInt("id_cliente", response.id_cliente);
                Ti.App.Properties.setString("usuario2", response.usuario);
                Ti.App.Properties.setString("nombre", response.nombre);
                Ti.App.Properties.setString("apellido", response.apellido);
                Ti.App.Properties.setString("email", response.email);
                Ti.App.Properties.setString("telefono", response.telefono);
                Ti.App.Properties.setString("rfc", response.rfc);
                Ti.App.Properties.setString("razon", response.razon);
                Ti.App.Properties.setString("genero", response.sexo);
                Ti.App.Properties.setString("nombrecompleto",response.nombre_completo);
                Ti.App.Properties.setInt("BonoActual",0);
                Ti.App.Properties.setDouble("pagoInicial",0);
                if(Ti.App.Properties.getInt('ingles')==1){
                 	
                 	dialogMessage = Ti.UI.createAlertDialog({message: 'Welcome to SUBT '+response.nombre+' '+response.apellido,buttonNames: ['Accept'],title: 'Message'});
              	 }else{
              	 	dialogMessage = Ti.UI.createAlertDialog({message: 'Bienvenido a SUBT '+response.nombre+' '+response.apellido,buttonNames: ['Aceptar'],title: 'Mensaje'});
              	 }
                if(response.status_codigo==1){
                	codigoAcceso.visible=true;
                }else{
                	Ti.App.Properties.setString("usuario", response.usuario);
                	dialogMessage.show();
                }
                              
                	dialogMessage.addEventListener('click', function(e){
   					mapa = require('/interface/mapa');
       	 			new mapa().open({modal:true});
        				window.close();
  				   });
            }else{
            	  if(Ti.App.Properties.getInt('ingles')==1){
            	  	dialogMessage = Ti.UI.createAlertDialog({message:response.message,buttonNames: ['Accept'],title: 'Message'});
            	  }else{
            	  	dialogMessage = Ti.UI.createAlertDialog({message:response.message,buttonNames: ['Aceptar'],title: 'Mensaje'});
            	  }
            		dialogMessage.show();
            }
    	};
    
    codigo.onload = function() {
		 json = this.responseText;
         response = JSON.parse(json);
		  if (response.logged == true){
			Ti.App.Properties.setString("usuario",Ti.App.Properties.getString("usuario2"));
			mapa = require('/interface/mapa');
       	 	new mapa().open({modal:true});
        	window.close();
		  } else {
			Ti.UI.createAlertDialog({
				cancel : 1,
				buttonNames : ['Aceptar'],
				message : response.message,
				title : 'Mensaje'
			}).show();
		}
	};
    
    	
	lblreset.addEventListener('click', function(){
        	if (txtUser.value == ''){
        		 if(Ti.App.Properties.getInt('ingles')==1){
        		 	alert('The user is required to reset the password');
        		 }
        		 else{
        		 	alert('Se requiere su usuario para restaurar la contraseña');
        		 }
                
            }else{
                resetReq.open('POST','http://subtmx.com/webservices/sendemail.php');
            params = {
                    names: txtUser.value
            };
                resetReq.send(params);
            }
    	});
	resetReq.onload = function(){
            	json = this.responseText;
            	response = JSON.parse(json);
            	if (response.logged == true){
            		 if(Ti.App.Properties.getInt('ingles')==1){
            		 	alert('It sent you an email to reset your password');
            		 }
            		 else{
            		 	alert(response.message);
            		 }
                	
            	}else{
            	    alert(response.message);    
            	}
    	};
	resetReq.onerror = function(event){
		if(Ti.App.Properties.getInt('ingles')==1){
			alert ('Connection Error: '+ JSON.stringify (event));
		}
		else{
			alert('Error de conexion: ' + JSON.stringify(event));
		}
            		
    	};
    	
    	window.addEventListener("open", function(evtop) { 
        		var theActionBar = window.activity.actionBar; 
        		if (theActionBar != undefined) {
                 theActionBar.backgroundImage = '/images/barra.jpeg';
                 if(Ti.App.Properties.getInt('ingles')==1){
                 	theActionBar.title = "Log in";
                 }else{ 
                 	theActionBar.title = "Inicio de sesion";
                 }
        			
         		}
            window.activity.onCreateOptionsMenu = function(emenu) { 
            	menu = emenu.menu;
            	 if(Ti.App.Properties.getInt('ingles')==1){
                 	 itemSearch = menu.add({
                		  title:'Register',
                		  showAsAction : Ti.Android.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW
            		 });
                	 itemMapa = menu.add({
                		  title:'Map',
                		  showAsAction : Ti.Android.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW
            		 }); 
                 }else{ 
                 	 itemSearch = menu.add({
                	      title:'Registro',
                		  showAsAction : Ti.Android.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW
            		 });
                	 itemMapa = menu.add({
                		  title:'Mapa',
                		  showAsAction : Ti.Android.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW
            		  }); 
                 }
            	
               
                itemSearch.addEventListener('click', function(){        
                menu1 = require('interface/registro');
                new menu1().open();
                window.close();
            });
                itemMapa.addEventListener('click', function(){      
                mapa = require('interface/mapa');
                new mapa().open();
                window.close();
            });
           };
    });
    
    window.add(lblUser);
    	window.add(txtUser);
    	window.add(lblpass);
    	window.add(txtPass);
   	window.add(lblreset);
    	window.add(btnlogin);
    	
    	return window;
};
module.exports = login;