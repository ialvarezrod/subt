function inicio(){
	var window,logo,btnLogin,btnRegistrar,lblOmitir,slider1,slider2,slider3,scrollableView,navwin,login,winNav,registro,mapa,animateWin;
	
	window = Ti.UI.createWindow({theme:'Theme.AppCompat.Translucent.NoTitleBar'});
    window .orientationModes = [Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT];
    
    video1 = Titanium.Media.createVideoPlayer({top : 0,width : '100%',scalingMode : Titanium.Media.VIDEO_SCALING_MODE_FILL,url:'/images/videoHome.mp4',mediaControlStyle: Titanium.Media.VIDEO_CONTROL_HIDDEN});   
    logo = Titanium.UI.createImageView({image:'/images/logo.png',width:'30%',height:'auto',zIndex:2,center:0,top:'0%',opacity:0.1});
    btnLogin = Titanium.UI.createButton({height:'10%',width:'50%',left:0,title:'INICIA SESIÓN',textAlign:'center',backgroundColor:'#3ca9cb',color:'white',bottom:0,zIndex:3});
    btnRegistrar = Titanium.UI.createButton({height:'10%',width:'50%',right:0,title:'REGÍSTRATE',textAlign:'center',backgroundColor:'#305caf',color:'white',bottom:0,zIndex:3});
    lblOmitir = Titanium.UI.createButton({title:'Omitir',top:'5%',right:15,color:'white',backgroundColor:'transparent',zIndex:3});
    var dialogIdioma = Ti.UI.createAlertDialog({buttonNames: ['Español','English'],message: 'Seleccione un idioma/Select a language',title: 'Idioma/language'});
	dialogIdioma.addEventListener('click', function(ed){
		
		if (ed.index === 0){
			Ti.App.Properties.setInt("ingles",0);
		}else if (ed.index === 1){
			btnLogin.title='Log in';
			btnRegistrar.title='Sign up';
			lblOmitir.title='Skip';
			Ti.App.Properties.setInt("ingles",1);
		}
	});
    window.addEventListener('open',function(){
    	dialogIdioma.show();
    	
    	
    		function getGeoPosition(_event) {
            
    		}
    		Ti.Geolocation.purpose = "Please allow us to find you.";
    		Ti.Geolocation.accuracy = Ti.Geolocation.ACCURACY_BEST;
    		Ti.Geolocation.distanceFilter = 10;
    		function geolocation() {
    			var hasLocationPermissions = Ti.Geolocation.hasLocationPermissions(Ti.Geolocation.AUTHORIZATION_ALWAYS);
    			if (hasLocationPermissions) {
        			Titanium.Geolocation.getCurrentPosition(getGeoPosition);
    			}
    			Ti.Geolocation.requestLocationPermissions(Ti.Geolocation.AUTHORIZATION_ALWAYS, function(e) {
        			if (e.success) {
        			}else if (Ti.Platform.osname == 'android') { 
        				   if(Ti.App.Properties.getInt("ingles")==1){
        				   	alert ( 'You must allow the application to access your location.');
        				   }else{
        				   	alert('Debe permitir a la aplicacion acceder a su localización.');
        				   }
        				      
            			
        			}else {
        				 if(Ti.App.Properties.getInt("ingles")==1){
        				   		Ti.UI.createAlertDialog({title : 'You have denied permission localization.',
                				message : e.error
            					}).show();
        				   }else{
        				   		Ti.UI.createAlertDialog({title : 'Has denegado el permiso de localizacion.',
                				message : e.error
            					}).show();
        				   }
            		}
    			});
		}
		geolocation();
		animateLogo = Titanium.UI.createAnimation();
        animateLogo.top = '18%';
        animateLogo.duration = 1800;
        animateLogo.opacity = 1;
        logo.animate(animateLogo);
    });
   
	video1.addEventListener('complete', function(e){
		video1.play();
	});
	
	btnLogin.addEventListener('click', function(){
        login = require('/interface/login');
        new login().open({modal:true});
        window.close();
    });
    
    btnRegistrar.addEventListener('click', function(){
    	registro = require('/interface/registro');
     	new registro().open({modal:true});
     	window.close();
    });
    
    lblOmitir.addEventListener('click', function(){               
        Titanium.App.Properties.removeProperty('id_cliente');
        Titanium.App.Properties.removeProperty('id_usuario');
        Titanium.App.Properties.removeProperty('id_chofer');
        Titanium.App.Properties.removeProperty('id_servicio');
        Titanium.App.Properties.removeProperty('usuario');
        Titanium.App.Properties.removeProperty('nombre');
        Titanium.App.Properties.removeProperty('apellido');
        Titanium.App.Properties.removeProperty('email');
        Titanium.App.Properties.removeProperty('telefono');
        Titanium.App.Properties.removeProperty('rfc');
        Titanium.App.Properties.removeProperty('razon');
        mapa = require('/interface/mapa');
        new mapa().open({modal:true});
        window.close();
    });
    
    window.add(logo);
	window.add(lblOmitir);
	window.add(btnLogin);
	window.add(btnRegistrar);
	window.add(video1);
    
    return window;
};
module.exports = inicio;