function pago(){
	var window = Titanium.UI.createWindow({theme:'Theme.AppCompat.Translucent.NoTitleBar',backgroundColor:'black'});
  	window .orientationModes = [Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT];
  	var name=Ti.App.Properties.getString('nombre');
  	var lastname=Ti.App.Properties.getString("apellido");
  	var usuario= Ti.App.Properties.getInt("id_usuario");

  	Ti.API.info(name);
  	Ti.API.info(lastname);
  	Ti.API.info(usuario);
	
	var txtRegistrate=Ti.UI.createLabel({text:'Registre su metodo de pago',font:{fontSize:'15%'},top:'5%',center:0});
	var cerrar = Ti.UI.createButton({title:'cerrar',widht:'auto',height:'auto',top:0,right:'5%',backgroundColor:'transparent'});
	var webview = Titanium.UI.createWebView({top:'10%',url:'http://subtmx.com/webservices/conekta-php-master/token.php?id='+usuario});
  	idPpal = Titanium.Network.createHTTPClient();
   
     if(Ti.App.Properties.getInt("ingles")==1){
    	txtRegistrate.text='Register your account';
     }
   
   idPpal.open("POST", "http://subtmx.com/webservices/conektausuario.php");
			params = {
				id_usuario : Ti.App.Properties.getInt("id_usuario")
			};
	idPpal.send(params);
	
	idPpal.onload = function() {
		json = this.responseText;
		response = JSON.parse(json);
		if (response.logged == true) {
		Ti.App.Properties.setString("coneckta", 'coneckta');
		} 
	};


	
   window.addEventListener('open',function(){
 
   });
   
  
    cerrar.addEventListener('click',function(){
    		mapa= require('/interface/mapa');
     	    new mapa().open({modal:true});
    	    window.close();	
    
    });
    
    
	window.addEventListener('android:back', function() {
		if (Ti.App.Properties.hasProperty('coneckta')) {
			//my_timer.stop();
			mapa = require('/interface/mapa');
			new mapa().open({
			modal : true
			});
			window.close();

		} else {
			var confirmClear = Titanium.UI.createAlertDialog({
				message : 'Cerrar sesión',
				buttonNames : ['aceptar', 'cancelar']
			});
			confirmClear.show();

			confirmClear.addEventListener('click', function(e) {
				if (e.index === 0) {
			
					Titanium.App.Properties.removeProperty("coneckta");
					Titanium.App.Properties.removeProperty('id_usuario');
					Titanium.App.Properties.removeProperty('usuario');
					Titanium.App.Properties.removeProperty('nombre');
					Titanium.App.Properties.removeProperty('apellido');
					Titanium.App.Properties.removeProperty('email');
					Titanium.App.Properties.removeProperty('telefono');
					Titanium.App.Properties.removeProperty('rfc');
					Titanium.App.Properties.removeProperty('razon');
					Titanium.App.Properties.removeProperty('numero_tarjeta');
					inicio = require('/interface/inicio');
					new inicio().open({
						modal : true
					});
					window.close();

				}
			});

		}

	}); 

    
     window.add(cerrar);
     window.add(webview);
     window.add(txtRegistrate);
     
		return window;
}
module.exports = pago;