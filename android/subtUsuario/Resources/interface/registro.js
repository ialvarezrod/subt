	function registro(){
	var window,tc,lblUser,lblCorreo,lblPass,lblTel,txtUser,txtCorreo,txtPass,txtTel,chkFactura,chkTerminos,btnRegistrar,lblTermino,lblFactura,scrollView,registerReq;
	var json,response,alertDialog;
	
	window = Ti.UI.createWindow({backgroundImage:'/images/bgLogin.png'});
	window .orientationModes = [Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT];
	scrollView = Titanium.UI.createScrollView({contentWidth:'auto',contentHeight:'auto',top:0,showVerticalScrollIndicator:true,showHorizontalScrollIndicator:false,layout:'vertical'});

	vista1 = Ti.UI.createView({backgroundColor:'transparent',height:Ti.UI.SIZE,zIndex:10,layout:'vertical'});
	vista2 = Ti.UI.createView({backgroundColor:'transparent',height:Ti.UI.SIZE,zIndex:10,top:-2,layout:'vertical' });
	lblUser = Titanium.UI.createLabel({text:'Nombre',center:0,top:'4%',color:'#49a6ff'});
	txtUser = Titanium.UI.createTextField({autocapitalization:Titanium.UI.TEXT_AUTOCAPITALIZATION_WORDS,opacity:0.3,hintText:'Nombre del usuario',color:'#0066cc',hintTextColor:'gray',center:0,top:'1%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,textAlign:'center',backgroundFocusedColor:'transparent',font:{fontSize:12}});
	lblapellidoP = Titanium.UI.createLabel({text:'Apellido Paterno',center:0,top:'3%',color:'#49a6ff'});
	txtapellidoP = Titanium.UI.createTextField({autocapitalization:Titanium.UI.TEXT_AUTOCAPITALIZATION_WORDS,opacity:0.3,hintText:'Apellido paterno',color:'#0066cc',hintTextColor:'gray',center:0,top:'1%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,textAlign:'center',backgroundFocusedColor:'transparent',font:{fontSize:12}});
	lblapellidoM = Titanium.UI.createLabel({text:'Apellido Materno',center:0,top:'3%',color:'#49a6ff'});
	txtapellidoM = Titanium.UI.createTextField({autocapitalization:Titanium.UI.TEXT_AUTOCAPITALIZATION_WORDS,opacity:0.3,hintText:'Apellido materno',color:'#0066cc',hintTextColor:'gray',center:0,top:'1%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,textAlign:'center',backgroundFocusedColor:'transparent',font:{fontSize:12}});
	lblCorreo = Titanium.UI.createLabel({text:'Correo Electrónico',center:0,top:'3%',color:'#49a6ff'});
	txtCorreo = Titanium.UI.createTextField({hintText:'correo',color:'#0066cc',hintTextColor:'gray',opacity:0.3,center:0,top:'1%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,textAlign:'center',backgroundFocusedColor:'transparent',font:{fontSize:12}});
	lblPass = Titanium.UI.createLabel({text:'Contraseña',center:0,top:'3%',color:'#49a6ff'});
	txtPass = Titanium.UI.createTextField({hintText:'*************',passwordMask:true,color:'#0066cc',hintTextColor:'gray',opacity:0.3,center:0,top:'1%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,textAlign:'center',backgroundFocusedColor:'transparent',font:{fontSize:12}});	
	lblPass2 = Titanium.UI.createLabel({text:'Repetir Contraseña',center:0,top:'3%',color:'#49a6ff'});
	txtPass2 = Titanium.UI.createTextField({hintText:'*************',passwordMask:true,color:'#0066cc',hintTextColor:'gray',opacity:0.3,center:0,top:'1%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,textAlign:'center',backgroundFocusedColor:'transparent',font:{fontSize:12}});	
	
	lblTel = Titanium.UI.createLabel({text:'Telefono',center:0,top:'3%',color:'#49a6ff'});
	txtTel = Titanium.UI.createTextField({keyboardType: Titanium.UI.KEYBOARD_NUMBER_PAD,hintText:'0000000000',color:'#0066cc',hintTextColor:'gray',opacity:0.3,center:0,top:'1%',width:'80%',leftButtonPadding:10,borderRadius:6,textAlign:'center',backgroundFocusedColor:'transparent',font:{fontSize:12}});
	lblSexo = Titanium.UI.createLabel({text:'Sexo',center:0,top:0,color:'#49a6ff'});
	txtSexo = Titanium.UI.createTextField({hintText:'Masculino/Femenino',color:'#0066cc',hintTextColor:'gray',opacity:0.3,center:0,top:'1%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,textAlign:'center',backgroundFocusedColor:'transparent',font:{fontSize:12},editable:false});
	var ter=Ti.UI.createView({backgroundColor:'transparent',height:Ti.UI.SIZE,layout:'horizontal',top:'2%',width:'100%'});
	chkTerminos = Ti.UI.createSwitch({value:false,left:'20%',onTintColor:'#62D337',tintColor:'#D90019'});
	lblTermino = Titanium.UI.createLabel({text:'Deseo facturar',left:'2%',color:'#49a6ff'});
	var fact=Ti.UI.createView({backgroundColor:'transparent',height:Ti.UI.SIZE,layout:'horizontal',top:'1%',width:'100%'});
	chkFactura = Ti.UI.createSwitch({value:false,left:'20%',onTintColor:'#62D337',tintColor:'#D90019'});
	lblFactura = Titanium.UI.createLabel({text:'Acepto términos y condiciones',left:'2%',color:'#49a6ff'});
	var btn=Ti.UI.createView({backgroundColor:'transparent',height:Ti.UI.SIZE,layout:'horizontal',top:'1%',width:'100%'});
	btnRegistrar = Titanium.UI.createButton({title:'Registrarme',color:'white',borderRadius:30,borderColor:'#49a6ff',backgroundColor:'#49a6ff',center:0,width:'85%', height:60, top:'3%'});
	registerReq = Titanium.Network.createHTTPClient();
	codigo = Titanium.Network.createHTTPClient();
	
	var codigoAcceso= Ti.UI.createView({width:'100%', height:'100%', visible:false,zIndex: 20,backgroundColor:'#B3000000'});
     var vistacodigoAcceso= Ti.UI.createView({width:'80%', height:Ti.UI.SIZE,zIndex: 20,backgroundColor:'white',borderColor:'black',borderRadius:6,layout:'vertical'});
   	 var TitulocodigoAcceso=Titanium.UI.createLabel({text:'CÓDIGO DE ACCESO',top:20,color:'black',font:{fontSize:18,fontFamily: 'Menlo Bold Italic'}});
   	 var txtcodigoAcceso=Titanium.UI.createTextField({hintText:"0000",top:20,color:'#2980b9',hintTextColor:'gray',font:{fontSize: 15,fontFamily: 'Menlo Bold Italic'},textAlign:'center',width:'80%'});
   	 var vistacodigoAcceso1= Ti.UI.createView({width:Ti.UI.SIZE, height:Ti.UI.SIZE,layout:'horizontal',top:10,center:0});
	 var btncodigoAcceso = Titanium.UI.createButton({title:'Aceptar',center:0,backgroundColor:'#0c83c7',borderRadius:5});
   	 var btncodigoAcceso2 = Titanium.UI.createButton({title:'En otro momento',left:10,backgroundColor:'#0c83c7',borderRadius:5});
     var vistacodigoAcceso2= Ti.UI.createView({width:'100%', height:30,layout:'horizontal',top:0});
	
	var vistaCargando=Ti.UI.createView({width:'100%',height:'100%',visible:false,zIndex: 20,backgroundColor:'#B3000000'});
	var lblCargando=Titanium.UI.createLabel({text:'Registrando...',center:0,color:'white',font:{fontSize:18,fontFamily: 'Menlo Bold Italic'}});
   	
   	vistaCargando.add(lblCargando);
   	window.add(vistaCargando);
	codigoAcceso.add(vistacodigoAcceso);
	vistacodigoAcceso.add(TitulocodigoAcceso);
	vistacodigoAcceso.add(txtcodigoAcceso);
	vistacodigoAcceso.add(vistacodigoAcceso1);
	vistacodigoAcceso1.add(btncodigoAcceso);
	vistacodigoAcceso1.add(btncodigoAcceso2);
	vistacodigoAcceso.add(vistacodigoAcceso2);
    window.add(codigoAcceso);
    
    
    btncodigoAcceso.addEventListener('click', function() {
		txtcodigoAcceso.value=txtcodigoAcceso.value.replace(/^\s+/g, '').replace(/\s+$/g, '');
			if(txtcodigoAcceso.value!=''){
					codigo.open("POST", "http://subtmx.com/webservices/codigo.php");
						params = {
							usuario : Ti.App.Properties.getInt("id_usuario"),
							codigo : txtcodigoAcceso.value
						};
					codigo.send(params);		
			}else{
				Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'],message:"Debe introducir el código de seguridad enviado a su correo" ,title: 'Mensaje'}).show();
			}
	}); 

    btncodigoAcceso2.addEventListener('click', function() {
		txtcodigoAcceso.value='';
		codigoAcceso.visible=false;
		 Titanium.App.Properties.removeProperty("id_usuario");
          Titanium.App.Properties.removeProperty("id_cliente");
          Titanium.App.Properties.removeProperty("usuario2");
          Titanium.App.Properties.removeProperty("nombre");
          Titanium.App.Properties.removeProperty("apellido");
          Titanium.App.Properties.removeProperty("email");
          Titanium.App.Properties.removeProperty("telefono");
          Titanium.App.Properties.removeProperty("rfc");
          Titanium.App.Properties.removeProperty("razon");
          Titanium.App.Properties.removeProperty("genero");
          Titanium.App.Properties.removeProperty("nombrecompleto");
          inicio= require('/interface/inicio');
        new inicio().open({modal:true});
        window.close();
	});
	
	  codigo.onload = function() {
		 json = this.responseText;
         response = JSON.parse(json);
		  if (response.logged == true){
			Ti.App.Properties.setString("usuario",Ti.App.Properties.getString("usuario2"));
			paypal= require('/interface/mapa');
        	new paypal().open({modal:true});
        	window.close();
		  } else {
			Ti.UI.createAlertDialog({
				cancel : 1,
				buttonNames : ['Aceptar'],
				message : response.message,
				title : 'Mensaje'
			}).show();
		}
	};
	
	lblFactura.addEventListener('click',function(){
   	Ti.Platform.openURL("http://subtmx.com/terminosycondiciones/index.html");
  	});
	
	if(Ti.App.Properties.getInt('ingles')==1){
		lblUser.text='Name';
		txtUser.hintText='Username';
		lblapellidoP.text='Last name';
		txtapellidoP.hintText='Last name';
		lblapellidoM.text='Mothers last name';
		txtapellidoM.hintText='Mothers last name';
		lblCorreo.text='Email';
		txtCorreo.hintText='Email';
		lblPass.text='Password';
		lblPass2.text='Repeat Password';
		lblTel.text='Telephone';
		txtTel.hintText='Telephone';
		lblSexo.text='Sex';
		txtSexo.hintText='Male/Female';
		lblTermino.text='Request invoice';
		lblFactura.text='I accept terms and conditions';
		btnRegistrar.title='Sign in';
	}
	
	
	window.addEventListener('android:back', function(){
		Titanium.App.Properties.removeProperty("id_usuario");
          Titanium.App.Properties.removeProperty("id_cliente");
          Titanium.App.Properties.removeProperty("usuario2");
          Titanium.App.Properties.removeProperty("nombre");
          Titanium.App.Properties.removeProperty("apellido");
          Titanium.App.Properties.removeProperty("email");
          Titanium.App.Properties.removeProperty("telefono");
          Titanium.App.Properties.removeProperty("rfc");
          Titanium.App.Properties.removeProperty("razon");
          Titanium.App.Properties.removeProperty("genero");
          Titanium.App.Properties.removeProperty("nombrecompleto");
	 	inicio= require('/interface/inicio');
        new inicio().open({modal:true});
        window.close();
	 });
	
	if(Ti.App.Properties.getInt('ingles')==1){
		var optsSexo = {options:  ['Male','Female'], selectedIndex: 2,destructive:0,title: 'Select your sex'};
	}
	else{
		var optsSexo = {options:  ['Masculino','Femenino'], selectedIndex: 2,destructive:0,title: 'Selecciona tu sexo'};
	}
	
		var dialogSexo = Ti.UI.createOptionDialog(optsSexo);
		
		dialogSexo.addEventListener('click', function(ed){
	   		if (ed.index === 0){
	   			if(Ti.App.Properties.getInt('ingles')==1){
	   			txtSexo.value='Male';	
	   			}else{
	   				txtSexo.value='Masculino';	
	   			}
	   		}if (ed.index === 1){
	   			if(Ti.App.Properties.getInt('ingles')==1){
	   			txtSexo.value='Female';	
	   			}else{
	   			txtSexo.value='Femenino';	
	   			}
	   		}
	   });
	   		
			
	txtUser.addEventListener('focus',function(){
		txtUser.value = '';
		txtUser.opacity=1;
	});
	txtapellidoP.addEventListener('focus',function(){
		txtapellidoP.value = '';
		txtapellidoP.opacity=1;
	});
	txtapellidoM.addEventListener('focus',function(){
		txtapellidoM.value = '';
		txtapellidoM.opacity=1;
	});
	txtCorreo.addEventListener('focus',function(){
		txtCorreo.value = '';
		txtCorreo.opacity=1;
	});
	
	txtPass.addEventListener('focus',function(){
		txtPass.value = '';
		txtPass.opacity=1;
	});
	txtPass2.addEventListener('focus',function(){
		txtPass2.value = '';
		txtPass2.opacity=1;
	});
	txtTel.addEventListener('focus',function(){
		txtTel.value = '';
		txtTel.opacity=1;
	});
	txtSexo.addEventListener('click',function(){
		dialogSexo.show();
	});

	function checkemail(emailAddress){
    		var str = emailAddress;
    		var filter = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    		if (filter.test(str)){
        		testresults = true;
    		}else{
        		testresults = false;
    		}
    		return (testresults);
	};
	

	btnRegistrar.addEventListener('click',function(e){
		txtCorreo.value=txtCorreo.value.replace(/^\s+/g, '').replace(/\s+$/g, '');
    		if (txtUser.value != '' && txtapellidoP.value != '' && txtapellidoM.value != '' && txtCorreo.value != '' && txtPass.value != '' && txtPass2.value != ''  && txtTel.value != ''  && txtSexo.value != '' && txtTel.value.length ==10 ){
        		if (!checkemail(txtCorreo.value)){
        			if(Ti.App.Properties.getInt('ingles')==1){
        				alert("Enter a valid email");
        			}else{
        				alert("Introduce un email valido");
        			}
          		}else{  
          			if(txtPass.value != txtPass2.value){
          				if(Ti.App.Properties.getInt('ingles')==1){
    						alertDialog = Titanium.UI.createAlertDialog({title: 'Register',message: 'Passwords are different, try again',buttonNames: ['OK']});
    					}else{
    						alertDialog = Titanium.UI.createAlertDialog({title: 'Registro',message: 'Las contraseñas son distintas,intente nuevamente',buttonNames: ['OK']});
    					}
    					alertDialog.show();
          			}else{
          				if(chkFactura.value==false){
          					if(Ti.App.Properties.getInt('ingles')==1){
    							alertDialog = Titanium.UI.createAlertDialog({title: 'Register',message: 'You must accept the terms and conditions',buttonNames: ['OK']});
    						}else{
    							alertDialog = Titanium.UI.createAlertDialog({title: 'Registro',message: 'Debe aceptar los términos y condiciones',buttonNames: ['OK']});
    						}
    						alertDialog.show();
          				}else{
          					if (chkTerminos.value == true) {
                				factura = require('/interface/factura');
                    			winNav = factura();
                    			winNav.fireEvent('foo',{
                    			username: txtUser.value,
                        		password: txtPass.value,
                        		correo: txtCorreo.value,
                        		telefono:txtTel.value,
                        		nombre:txtUser.value,
                        		apellido_p:txtapellidoP.value,
                        		apellido_m:txtapellidoM.value,
                        		sexo:txtSexo.value
                   				});
                    			winNav.open();
                    			window.close();
               				}else{
               				
               				Ti.API.info(txtUser.value);
               				Ti.API.info(Ti.Utils.md5HexDigest(txtPass.value));
               				Ti.API.info(txtCorreo.value);
               				Ti.API.info(txtTel.value);
               				Ti.API.info(txtUser.value);
               				Ti.API.info(txtapellidoP.value);
               				Ti.API.info(txtapellidoM.value);
               				Ti.API.info(txtSexo.value);
               				vistaCargando.visible=true;
                			registerReq.open("POST","http://subtmx.com/webservices/registerResquest.php");
                    		params = {
                        	username: txtUser.value,
                             password: Ti.Utils.md5HexDigest(txtPass.value),
                             correo: txtCorreo.value,
                             telefono:txtTel.value,
                             nombre:txtUser.value,
                        	 apellido_p:txtapellidoP.value,
                        	 apellido_m:txtapellidoM.value,
                        	 sexo:txtSexo.value,
                        	 rfc:'',
                             razon_social:'',
                             direccion_fiscal:'',
                             email_factura:'',
                             cp:'',
                        	 device_token:Titanium.Platform.id,
                        	 device_type:"android"
                        	
                      	};
                        registerReq.send(params);
              			}
          			}
          		}
        	}
       	}else{
       		if(Ti.App.Properties.getInt('ingles')==1){
       			alert('All fields are mandatory');
       		}else{
       			alert('Todos los campos son obligatorios');
       		}
        		
       	}    	
       	
    });
    
    	registerReq.onload = function(){
		json = this.responseText;
		
    	response = JSON.parse(json);
    
    		 if (response.message == "Insert failed" || response.message == 'El usuario o email ya existen' || response.message == 'Tu dispositivo fue bloqueado, consulte a soporte técnico: soporte@subtmx.com' || response.message == 'Su número de telefono ya fue registrado anteriormente'){
    		 	vistaCargando.visible=false;
        		btnRegistrar.opacity = 1;
        		alertDialog2 = Titanium.UI.createAlertDialog({title: 'Soporte',message: response.message,buttonNames: ['Aceptar']}).show();
        	
    		}else{
    			if(Ti.App.Properties.getInt('ingles')==1){
    				alertDialog = Titanium.UI.createAlertDialog({title: 'Register',message:"Debe introducir el código de seguridad enviado a su correo",buttonNames: ['OK']});
    		
    			}else{
    				alertDialog = Titanium.UI.createAlertDialog({title: 'Registro',message:"Debe introducir el código de seguridad enviado a su correo",buttonNames: ['OK']});
    			}
    				alertDialog.show();
    				vistaCargando.visible=false;
    				alertDialog.addEventListener('click',function(e){
    					Ti.App.Properties.setInt("id_usuario", response.id_usuario);
                		Ti.App.Properties.setInt("id_cliente", response.id_cliente);
               			Ti.App.Properties.setString("usuario2", response.usuario);
                		Ti.App.Properties.setString("email", response.email);
                		Ti.App.Properties.setString("telefono", response.telefono);
                		Ti.App.Properties.setString("apellido", txtapellidoP.value);
                		Ti.App.Properties.setString("genero", response.sexo);
               			Ti.App.Properties.setInt("BonoActual",0);
                		Ti.App.Properties.setDouble("pagoInicial",0);
                		
                		codigoAcceso.visible=true;
                	
                		
    					// paypal= require('/interface/mapa');
        		 		// new paypal().open({modal:true});
        		 		// window.close();
    				});
    				
				}
 	};
    
    	window.addEventListener("open", function(evtop) { 
		var theActionBar = window.activity.actionBar; 
		if (theActionBar != undefined) {
			theActionBar.backgroundImage = '/images/barra.jpeg';
        			if(Ti.App.Properties.getInt('ingles')==1){
        				theActionBar.title = "Register";
        			}else{
        				theActionBar.title = "Registro";
        			}
        			
        		
        }	
		window.activity.onCreateOptionsMenu = function(emenu) { 
				menu = emenu.menu;
				if(Ti.App.Properties.getInt('ingles')==1){
					itemSearch = menu.add({
					title:'Login',
					showAsAction : Ti.Android.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW
				});
				itemMapa = menu.add({
					title:'Map',
					showAsAction : Ti.Android.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW
				});	
				}else{
					itemSearch = menu.add({
					title:'Inicio de sesion',
					showAsAction : Ti.Android.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW
				});
				itemMapa = menu.add({
					title:'Mapa',
					showAsAction : Ti.Android.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW
				});	
				}
			   	
	         	itemSearch.addEventListener('click', function(){		
					menu1 = require('interface/login');
					new menu1().open();
					window.close();
				});
				itemMapa.addEventListener('click', function(){		
					mapa = require('interface/mapa');
					new mapa().open();
					window.close();
				});
		};
	});
    
   	
	vista1.add(lblUser);
	vista1.add(txtUser);
	vista1.add(lblapellidoP);
	vista1.add(txtapellidoP);
	vista1.add(lblapellidoM);
	vista1.add(txtapellidoM);
	vista1.add(lblCorreo);
	vista1.add(txtCorreo);
	vista1.add(lblPass);
	vista1.add(txtPass);
	vista1.add(lblPass2);
	vista1.add(txtPass2);
	vista1.add(lblTel);
	vista1.add(txtTel);
	vista2.add(lblSexo);
	vista2.add(txtSexo);
	ter.add(chkTerminos);
	ter.add(lblTermino);
	fact.add(chkFactura);
	fact.add(lblFactura);
	vista2.add(ter);
	vista2.add(fact);
	vista2.add(btnRegistrar);
	scrollView.add(vista1);
	scrollView.add(vista2);
	//scrollView.add(vista1);
	
	window.add(scrollView);
	
	return window;
};
module.exports = registro;

