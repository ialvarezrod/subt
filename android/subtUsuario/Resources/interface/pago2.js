function pago(){
		var self,header,iconHeader,iconShare,menuView,logoLeft,lblHome,lblMisPedidos,lblMiPerfil,lblPromociones,lblPagos,lblCuenta,
		txtCuenta,lblVencimiento,txtVencimiento,lblTarjeta,txtTarjeta,btnCuenta,lblNCuenta,lblCVV,pagoReq;
	
		self= Ti.UI.createWindow({theme:'Theme.AppCompat.Translucent.NoTitleBar',backgroundImage:'/images/bgLogin.png'});
		self .orientationModes = [Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT];
		centerView =Ti.UI.createView({height: '100%',right:0,top: 0,width:'100%',zIndex:1});
		header = Ti.UI.createView({height: '10%',right:0,top: 0,width:'100%',zIndex: 3 ,backgroundColor:'white'});
		
		btCancelar = Ti.UI.createButton({title:'Cancelar',left:'2%',top:'4%', height:'auto',widht:'auto',backgroundColor:'transparent',color:'blue'});
		btAceptar =Ti.UI.createButton({title:'Aceptar',right:'2%',top:'4%', height:'auto',widht:'auto',backgroundColor:'transparent',color:'blue'});
				
		titulo=Ti.UI.createLabel({text:'Pagos', color:'#232323',center:0,bottom:'25%',zIndex:2,font:{fontSize:'15.5%'}});
		iconHeader = Ti.UI.createButton({	backgroundImage:'/images/menuicon.png', left:'3%',zIndex:2,height:'42%',width:'8.5%'});
   		iconShare = Ti.UI.createButton({	backgroundImage:'/images/scan.png',  left:'88%',zIndex:2,height:'25',width:'25',titleid:'manual'});
	
		menuView = Ti.UI.createView({backgroundImage:'/images/bgMenu.jpg',height: 'auto',left: '-75%',	top:0,width: '75%',zIndex: 9,	_toggle: false});
	 	logoLeft = Titanium.UI.createImageView({image:'/images/logo.png',width:'55%',height:'auto',zIndex:2,left:'15%',top:'20%',opacity:1});
		lblHome = Titanium.UI.createLabel({color:'black',text:'Home',top:'52%',left:'13%'});
		lblMisPedidos = Titanium.UI.createLabel({color:'black',text:'Mis Pedidos',top:'60%',left:'13%'});
		lblMiPerfil = Titanium.UI.createLabel({color:'black',text:'Mi Perfil',top:'68%',left:'13%'});
		lblPromociones = Titanium.UI.createLabel({color:'black',text:'Promociones',top:'76%',left:'13%'});
		lblPagos = Titanium.UI.createLabel({color:'blue',text:'Pagos',top:'84%',left:'13%'});	
		
		lblCuenta = Ti.UI.createLabel({text:'Número de cuenta',center:0,top:'15%',color:'#0c83c7',visible:false});
		txtCuenta = Ti.UI.createTextField({color:'#c2c2c2',hintText:'64531-4300-8710-9922',center:0,top:'20%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,value:"64531-4300-8710-9922",textAlign:'center',editable:false,visible:false});
		lblVencimiento = Ti.UI.createLabel({text:'Vencimiento',center:0,top:'30%',color:'#0c83c7',editable:false,visible:false});
		txtVencimiento = Ti.UI.createTextField({color:'#c2c2c2',hintText:'05/19',center:0,top:'35%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,value:"05/19",textAlign:'center',editable:false,visible:false});
        lblTarjeta = Ti.UI.createLabel({text:'Tipo de tarjeta',center:0,top:'50%',color:'#0c83c7'});
    	txtTarjeta = Titanium.UI.createTextField({color:'#c2c2c2',hintText:'Seleccionar',center:0,top:'55%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,value:"Seleccionar",textAlign:'center',backgroundColor:'white',editable:false});
        
        scrollView = Titanium.UI.createScrollView({contentWidth:'auto',contentHeight:'auto',top:0,showVerticalScrollIndicator:true,showHorizontalScrollIndicator:false});


		vista1=Ti.UI.createView({width:'100%',height:Ti.UI.SIZE , layout:'vertical',top:'10%'});
		
			
        lblNCuenta = Ti.UI.createLabel({text:'Número de cuenta',center:0,top:'10%',color:'#0c83c7'});
        txtNCuenta = Ti.UI.createTextField({color:'#c2c2c2',hintTextColor:'#c2c2c2',hintText:'64531-4300-8710-9922',center:0,top:'10',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,value:"64531-4300-8710-9922",textAlign:'center'});
        
        titulosMAC=Ti.UI.createView({width:'100%',height:Ti.UI.SIZE ,top:15});
        lblMES = Ti.UI.createLabel({text:'Mes',left:'3%',color:'#0c83c7'});
        lblANIO = Ti.UI.createLabel({text:'Año',center:'0',color:'#0c83c7'});
     	lblCVV = Ti.UI.createLabel({text:'CVV',right:'3%',color:'#0c83c7'});
       	
       	txtMAC=Ti.UI.createView({width:'100%',height:Ti.UI.SIZE , top:15});
        txtMes = Ti.UI.createTextField({color:'#c2c2c2',hintText:'02',left:'3%',width:'30%',leftButtonPadding:10,borderRadius:6,height:40,value:"02",textAlign:'center'});
        txtAnio = Ti.UI.createTextField({color:'#c2c2c2',hintText:'29',center:'0',width:'30%',leftButtonPadding:10,borderRadius:6,height:40,value:"29",textAlign:'center'});
        txtCVV = Ti.UI.createTextField({color:'#c2c2c2',hintText:'568',right:'3%',width:'30%',leftButtonPadding:10,borderRadius:6,height:40,value:"568",textAlign:'center'});
       	
       	txtcallesT=Ti.UI.createView({width:'100%',height:Ti.UI.SIZE , top:15});
      	txtcallesT2=Ti.UI.createView({width:'50%',left:'0',height:Ti.UI.SIZE ,top:0});
      	txtcallesT3=Ti.UI.createView({width:'50%',right:'0',height:Ti.UI.SIZE , top:0});
       	
       	lblCalle = Ti.UI.createLabel({text:'Calle1',top:'0',left:5,color:'#0c83c7',visible:true});
        txtCalle  = Ti.UI.createTextField({color:'#c2c2c2',left:5,top:15,hintText:'',width:'100%',leftButtonPadding:10,borderRadius:6,height:40,textAlign:'center',visible:true});
        lblCalle2 = Ti.UI.createLabel({text:'Calle2',top:'0',left:5,color:'#0c83c7',visible:true});
        txtCalle2  = Ti.UI.createTextField({color:'#c2c2c2',top:15,left:5,hintText:'',width:'100%',leftButtonPadding:10,borderRadius:6,height:40,textAlign:'center',visible:true});
       
       	titulosccs=Ti.UI.createView({width:'100%',height:Ti.UI.SIZE ,top:15});
		lblcity = Ti.UI.createLabel({text:'Ciudad',left:'3%',color:'#0c83c7',visible:true});
        txtcity = Ti.UI.createTextField({color:'#c2c2c2',hintText:'',left:'3%',width:'30%',leftButtonPadding:10,borderRadius:6,height:40,textAlign:'center',visible:true});
        lblstate = Ti.UI.createLabel({text:'Estado',center:'0',color:'#0c83c7',visible:true});
        
        txtccs=Ti.UI.createView({width:'100%',height:Ti.UI.SIZE ,top:15});
        txtstate = Ti.UI.createTextField({color:'#c2c2c2',hintText:'',center:'0',width:'30%',leftButtonPadding:10,borderRadius:6,height:40,textAlign:'center',visible:true});
        lblzip = Ti.UI.createLabel({text:'CP',right:'3%',color:'#0c83c7',visible:true});
        txtzip = Ti.UI.createTextField({color:'#c2c2c2',hintText:'',right:'3%',width:'30%',leftButtonPadding:10,borderRadius:6,height:40,textAlign:'center',visible:true});
       	btnCuenta = Titanium.UI.createButton({title:'Añadir metodo de pago',color:'white',borderRadius:10,borderColor:'white',backgroundColor:'#0c83c7',top:30,width:'75%',height:50});
            
        
        Ti.App.Properties.getInt('id_usuario');
        Ti.App.Properties.getString('usuario');
        Ti.App.Properties.getString('apellido');
        pagoReq = Titanium.Network.createHTTPClient();
  	
  	    self.addEventListener('android:back', function() {

		});
			var banco = [ 'Mastercard', 'Visa', 'Amex','Oxxo'];
			var opciones=Ti.UI.createOptionDialog({title:"Seleccione una opción",options:banco});
			
			opciones.addEventListener("click", function(evt){
						txtTarjeta.value=banco[evt.index];
			});
		
 
 	 var cardio = require('com.likelysoft.cardio');
	 
	 	  txtCuenta.addEventListener('click', function() {
	      	
	      	cardio.scanCard(function(data){
                if(data.success == 'true') {
                    entradaC = data.expiryYear.split('0');
                    mes = data.expiryMonth;
                    salida = entradaC[1];
                    if (salida < 10) {
                        salida = salida+'0';
                    }
                    if (mes < 10) {
                        mes = '0'+mes;
                    }
                    var cardN=data.cardNumber.split(' ');
                    var cardN3 = cardN[3];
                    Ti.API.info(cardN[0]);
                    Ti.API.info(cardN[1]);
                    Ti.API.info(cardN[2]);
                    Ti.API.info(cardN[3]);
                    txtCuenta.value = '***************'+cardN3;
                    txtVencimiento.value = mes+'/'+salida;
                    txtNCuenta.value = data.cardNumber;
                    txtCVV.value = data.cvv;
                    txtMes.value = mes;
                    txtAnio.value = data.expiryYear;
                    Ti.API.info("Card number: " + data.cardNumber);
                    Ti.API.info("Redacted card number: " + data.redactedCardNumber);
                    Ti.API.info("Expiration month: " + data.expiryMonth);
                    Ti.API.info("Expiration year: " + data.expiryYear);
                    Ti.API.info("CVV code: " + data.cvv);
                }
            });	
           });
	 
	 
	  iconShare.addEventListener('click', function() {
	
              if (iconShare.titleid == 'manual'){
          			txtCuenta.visible = true;
            		txtVencimiento.visible = true;
            		lblCuenta.visible = true;
            		lblVencimiento.visible = true;
            		
            		lblNCuenta.visible = false;
            		txtNCuenta.visible = false;
            		lblMES.visible = false;
            		txtMes.visible = false;
            		lblANIO.visible = false;
            		txtAnio.visible = false;
            		lblCVV.visible = false;
            		txtCVV.visible = false;
            		lblCalle.visible = true;
            		txtCalle.visible = true;
            		lblCalle2.visible = true;
            		txtCalle2.visible = true;
            		
            		lblcity.visible = true;
            		txtcity.visible = true;
            		lblstate.visible = true;
            		txtstate.visible = true;
            		lblzip.visible = true;
            		txtzip.visible = true;
            		
            		lblTarjeta.top='55%';
    				txtTarjeta.top='65%';
        			btnCuenta.top=15;
            		iconShare.titleid='escaner';
            		iconShare.backgroundImage='/images/write.png';
            }else  if (iconShare.titleid == 'escaner'){
            		iconShare.backgroundImage='/images/scan.png';   		
            		txtCuenta.visible = false;
            		txtVencimiento.visible = false;
            		lblCuenta.visible = false;
            		lblVencimiento.visible = false;
            		
            		lblNCuenta.visible = true;
            		txtNCuenta.visible = true;
            		lblMES.visible = true;
            		txtMes.visible = true;
            		lblANIO.visible = true;
            		txtAnio.visible = true;
            		lblCVV.visible = true;
            		txtCVV.visible = true;
            		lblCalle.visible = true;
            		txtCalle.visible = true;
            		lblCalle2.visible = true;
            		txtCalle2.visible = true;
            		
            		
            		lblcity.visible = true;
            		txtcity.visible = true;
            		lblstate.visible = true;
            		txtstate.visible = true;
            		lblzip.visible = true;
            		txtzip.visible = true;
            		
            		
            		lblTarjeta.top='85%';
    				txtTarjeta.top='90%';
        			btnCuenta.top=15;
        
       
         
            		iconShare.titleid='manual';
            		txtNCuenta.focus();
           	}
        
           
          });
	 
		txtAnio.addEventListener('focus',function(){
            txtAnio.value = '';
        	});
        	txtNCuenta.addEventListener('focus',function(){
            txtNCuenta.value = '';
        	});
        	txtCVV.addEventListener('focus',function(){
            txtCVV.value = '';
        	});
        	txtMes.addEventListener('focus',function(){
            txtMes.value = '';
        	});
        	txtCuenta.addEventListener('focus',function(){
            txtCuenta.value = '';
        	});
       
       	lblHome.addEventListener('click', function(){
			home= require('/interface/mapa');
			new home().open({modal:true});
			self.close();		
		});
		
		lblMisPedidos.addEventListener('click', function(){
			if (Ti.App.Properties.hasProperty('id_usuario')) {
			pedidos= require('/interface/pedidos');
			new pedidos().open({modal:true});
			self.close();
			}else{
			alert('Solo usuarios registrados');
			}   
		});	

		lblMiPerfil.addEventListener('click', function(){
			perfil= require('/interface/perfil');
			new perfil().open({modal:true});
			self.close();
		});
		
		lblPromociones.addEventListener('click', function(){
			if (Ti.App.Properties.hasProperty('id_usuario')) {
			promo= require('/interface/promociones');
			new promo().open({modal:true});
			self.close();
			}else{
			alert('Solo usuarios registrados');
			}
		});	
	
		btnCuenta.addEventListener('click', function(){
			
			 Ti.API.info("numero_tarjeta: " + txtNCuenta.value);
             Ti.API.info("Expiration month: " + txtMes.value);
             Ti.API.info("Expiration year: " + txtAnio.value);
             Ti.API.info("CVV code: " + txtCVV.value);
             Ti.API.info("name: " + Ti.App.Properties.getString('usuario'));
             Ti.API.info("lastname: " + Ti.App.Properties.getString('apellido'));
             Ti.API.info("id_usuario: " + Ti.App.Properties.getInt('id_usuario'));
             
		pagoReq.open("POST","http://subtmx.com/webservices/card.php");
                    params = {
                        month_expire: txtMes.value,
                        year_expire: txtAnio.value,
                        cvv: txtCVV.value,
                        id_usuario: Ti.App.Properties.getInt('id_usuario'),
                        numero_tarjeta: txtNCuenta.value,
                        street1:txtCalle.value,
                        stret2:txtCalle2.value,
                        city:txtcity.value,
                        state:txtstate.value,
                        zip:txtzip.value,
                        country:'México',
                       
                    };
           	pagoReq.send(params);
			
			
		});	
		pagoReq.onload = function(){
            json = this.responseText;
            response = JSON.parse(json);
            if (response.logged == true){
            		Ti.App.Properties.setString("coneckta", 'coneckta');
            		Ti.App.Properties.setString("numero_tarjeta", txtNCuenta.value);
            		Ti.App.Properties.setString("month_expire", txtMes.value);
            		Ti.App.Properties.setString("year_expire", txtAnio.value);
            		Ti.App.Properties.setString("cvv", txtCVV.value);
            		Ti.App.Properties.setString("id_car", response.id_car);
            		mapa = require('/interface/mapa');
                new mapa().open();
                self.close();
            }else{
            		alert("No se pudo agregar la tarjeta");
            }
    		};
	
	     lblPagos.addEventListener('click', function(){
     		clickMenu(null);
        }); 
		txtTarjeta.addEventListener('click',function(){
				opciones.show();
		});	
			
	
			
		iconHeader.addEventListener('click',function(event){
				clickMenu(null);
		});
	
		centerView.addEventListener('swipe',function(event){
			if(event.direction=='left' || event.direction=='right' ){
				clickMenu(null);
			}	
		});
	
			function clickMenu(direction){
	 			if(menuView._toggle === false && (direction==null || direction=='right')){
					centerView.animate({right:'-75%' ,	duration: 100,curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT});
					menuView.animate({left: 0,duration: 100,curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT});
					menuView._toggle=true;
				}else if(direction==null || direction=='left'){
					centerView.animate({right:0,duration: 100,curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT	});
					menuView.animate({left: '-75%',duration: 100,curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT});
					menuView._toggle=false;
				};
			}
	
			header.add(titulo);	
			header.add(iconHeader);
			// header.add(iconShare);
			centerView.add(header);
			
		
			menuView.add(logoLeft);
			menuView.add(lblHome);
			menuView.add(lblMisPedidos);
			menuView.add(lblMiPerfil);
			menuView.add(lblPromociones);
			menuView.add(lblPagos);
			centerView.add(scrollView);
			
			vista1.add(lblNCuenta);
			vista1.add(txtNCuenta);
			titulosMAC.add(lblMES);
			titulosMAC.add(lblANIO);
			titulosMAC.add(lblCVV);
			txtMAC.add(txtMes);
			txtMAC.add(txtAnio);
			txtMAC.add(txtCVV);
			txtcallesT2.add(lblCalle);
       		txtcallesT2.add(txtCalle);
       		txtcallesT3.add(lblCalle2);
       		txtcallesT3.add(txtCalle2);
       		txtcallesT.add(txtcallesT2);
       		txtcallesT.add(txtcallesT3);
       		titulosccs.add(lblcity);
        	titulosccs.add(lblstate);
        	titulosccs.add(lblzip);
        	txtccs.add(txtcity);
        	txtccs.add(txtstate);
        	txtccs.add(txtzip);  
       		vista1.add(titulosMAC);
			vista1.add(txtMAC);
			vista1.add(txtcallesT);
			vista1.add(titulosccs);
			vista1.add(txtccs);
			vista1.add(btnCuenta);
			scrollView.add(vista1);
			
			 scrollView.add(lblCuenta);
     		 scrollView.add(txtCuenta);
     		 scrollView.add(lblVencimiento);
     		 scrollView.add(txtVencimiento);
     		
     		// scrollView.add(btnCuenta);
     		// scrollView.add(lblNCuenta);
            // scrollView.add(txtNCuenta);
            // scrollView.add(lblMES);
            // scrollView.add(txtMes);
            // scrollView.add(lblANIO);
            // scrollView.add(txtAnio);
            // scrollView.add(lblCVV);
            // scrollView.add(txtCVV);
            // scrollView.add(lblCalle);
            // scrollView.add(txtCalle);
            // scrollView.add(lblCalle2);
            // scrollView.add(txtCalle2);
            // scrollView.add(lblcity);
            // scrollView.add(txtcity);
            // scrollView.add(lblstate);
            // scrollView.add(txtstate);
            // scrollView.add(lblzip);
            // scrollView.add(txtzip);
            
      		self.add(menuView);
			self.add(centerView);
			return self;
}

module.exports = pago;
