function mapa2(navwin){
	var Map = require('ti.map');
	var window,search,share,menu,navwin,mapview,btnPedir,winLeft,navController,logoLeft,mainwin,lblMisPedidos,lblMiPerfil,lblPromociones,lblPagos,pago,winNav,btnLista,vistaBottom,autoCheck,txtAddress,servSel;
  
    window= Ti.UI.createWindow({theme:'Theme.AppCompat.Translucent.NoTitleBar',backgroundColor:'black'});   
    window.orientationModes = [Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT];
        
   	   
   
 	leftButtonOrigen = Titanium.UI.createButton({backgroundImage:'/images/green.png',width:30,height:30,left:'3%'});
    leftButtonDestino = Titanium.UI.createButton({backgroundImage:'/images/red.png',width:30,height:30,left:'3%'});
 	origenView =Ti.UI.createView({height: '8%',center:0,top: '12%',width:'80%',zIndex:1,backgroundColor:'white'});
    destinoView =Ti.UI.createView({height: '8%',center:0,top: '22%',width:'80%',zIndex:1,backgroundColor:'white'});
    txtOrigen = Titanium.UI.createTextField({backgroundColor:'white',borderRadius:5,color:'black',hintTextColor:'gray',hintText:"Ingrese su Origen",top:'0%',left:'15%',width:'70%',height:'100%'});
    txtDestino = Titanium.UI.createTextField({backgroundColor:'white',borderRadius:5,color:'black',hintTextColor:'gray',hintText:"Ingrese su Destino",top:'0%',left:'15%',width:'70%',height:'100%'});
  	var origen;
    var destino,destinoSeleccionado=0;
               
    centerView =Ti.UI.createView({height: 'auto',right:0,top: 0,width:'100%',zIndex:1});
    header = Ti.UI.createView({backgroundColor: 'white',height: '10%',right:0,top: 0,width:'100%',zIndex: 3});
    titulo=Ti.UI.createLabel({text:'SUBT', color:'#232323',center:0,bottom:'25%',zIndex:2,font:{fontSize:'15.5%'}});
	
   	   
    btnPedir = Titanium.UI.createButton({width:'70%',height:'auto',title:'Pedir Taxi',textAlign:'center',center:0,bottom:'15%',color:'white',zIndex:3,backgroundColor:'#0c83c7',borderRadius:6});
  	pinUsuario = Map.createAnnotation({title:"Origen actual",image:'/images/pinOrigen.png',draggable:true});
    pinDestino = Map.createAnnotation({title:"Destino actual",image:"/images/pinDestino.png",draggable:true}); 
  	showBtn = Ti.UI.createButton({width:'7%',height:'47%',right:'13%',borderRadius:5,backgroundImage:'/images/iconsearch.png',zIndex:5});
   	pedirKm= Titanium.Network.createHTTPClient();
   	pedirKm2= Titanium.Network.createHTTPClient();
   	rango = Titanium.Network.createHTTPClient();
    var idch = Ti.UI.createLabel({}); 	 
   	idch.text=Ti.App.Properties.getInt("id_Chofer");
 	var bandera=Ti.UI.createLabel({});
	bandera.text=Ti.App.Properties.getInt("id_tipo_servicio");
	var ti_ser=Ti.UI.createLabel({});
    var contador;
	var taxiRosaSelect=Ti.App.Properties.getInt("taxiRosaSelect");
	var carroSelect= Ti.UI.createLabel();;
	carroSelect.text=Ti.App.Properties.getString("carroSelect");
	var numPasajero= Ti.UI.createLabel();
	numPasajero.text=Ti.App.Properties.getString("numPasajero");
      	var rutaCorrecta=0;
  	

	
   	 window.addEventListener('open',function(){
     	Ti.Geolocation.getCurrentPosition(function(e) {
   		if (e.error){
        	if (Ti.App.Properties.getInt('ingles')==1)
        	 {	alert('Unable to get current location.');}
        	else{alert('No se puede obtener ubicacion actual');}
        		return;
    		}
    		
    	
			
	});
	window.add(webviewC);
	
   	 	destinoView.height='13%';
		origenView.height='13%';
   	 	Titanium.App.Properties.removeProperty("latDES");
        Titanium.App.Properties.removeProperty("longDES");
       	Titanium.App.Properties.removeProperty("latIN");
        Titanium.App.Properties.removeProperty("longIN");
   	
       					 	
   	 	 pedirKm2.open("POST","http://subtmx.com/webservices/distanciag.php");
        					params = {
        				 	lat1:Ti.App.Properties.getDouble("lat_in_recalcular") ,
       					 	lng1:Ti.App.Properties.getDouble("long_in_recalcular"),
       					 	lat2:Ti.App.Properties.getDouble("latActual"),
       					 	lng2:Ti.App.Properties.getDouble("longActual")
        					};
       					pedirKm2.send(params);	
   	 
   	 	Ti.API.info(Ti.App.Properties.getDouble('ctD'));
   	 	
   	 	
   	 });
   	 
	
   	
   	pedirKm2.onload = function(){
            json = this.responseText;
            response = JSON.parse(json);
            if (response.logged == true){
             	var kmformateado=response.km_total.split(',');
		 		var kmWeb = kmformateado[0] + '.' + kmformateado[1];
		 		var dus= parseFloat(kmWeb);
		 		if(response.km_total==''){
		 			dus=0;
		 	
		 		}
		 	    contador= Ti.App.Properties.getDouble("Extra2")+ dus;
		 	   
		 	
		 		var newCosto,newkmad,tarifaFinal;
		 		if (dus>Ti.App.Properties.getDouble("km_inicial")) {
							var	newkmad=dus-Ti.App.Properties.getDouble("km_inicial");
							newCosto = newkmad * Ti.App.Properties.getDouble("precio_km_adicional");
							tarifaFinal=newCosto+ Ti.App.Properties.getDouble("tarifa_inicial")*1;
							
				}else if (dus.toFixed(1) <= Ti.App.Properties.getDouble("km_inicial")) {
							tarifaFinal=Ti.App.Properties.getDouble("tarifa_inicial")*1;
							
						
				}
		   }else{
    		}
        }; 
   	 
   	   	
   	
	pedirKm.onload = function() {
		json = this.responseText;
		response = JSON.parse(json);
		if (response.logged == true) {
			Ti.App.Properties.setString("distancias", response.km_total);

			if (Ti.App.Properties.getString("distancias") == '') {
				if (Ti.App.Properties.getInt('ingles') == 1) {
					var dialogAlerta = Ti.UI.createAlertDialog({
						cancel : 1,
						buttonNames : ['Accept'],
						message : 'Sorry, we could not process your request, try you again',
						title : 'Message'
					});
				} else {
					var dialogAlerta = Ti.UI.createAlertDialog({
						cancel : 1,
						buttonNames : ['Aceptar'],
						message : 'Lo sentimos, no pudimos procesar su solicitud, intentelo de nuevo',
						title : 'Mensaje'
					});
				}
				dialogAlerta.show();
			} else {
				Ti.App.Properties.setDouble("Extra2", contador);

				Ti.App.fireEvent('fbController:nuevaruta', {
					id_chofer : Ti.App.Properties.getInt("id_Chofer")
				});
				Ti.API.info(Ti.App.Properties.getInt("id_Chofer"));
				Ti.API.info(Ti.App.Properties.getDouble("latIN"));
				Ti.API.info(Ti.App.Properties.getDouble("longIN"));
				Ti.API.info(Ti.App.Properties.getDouble("latDES"));
				Ti.API.info(Ti.App.Properties.getDouble("longDES"));

				Ti.App.fireEvent('fbController:rutauserO', {
					latInicio : Ti.App.Properties.getDouble("latIN"),
					lngInicio : Ti.App.Properties.getDouble("longIN"),
					id_chofer : Ti.App.Properties.getInt("id_Chofer")
				});
				Ti.App.fireEvent('fbController:rutauserD', {
					latDestino : Ti.App.Properties.getDouble("latDES"),
					lngDestino : Ti.App.Properties.getDouble("longDES"),
					id_chofer : Ti.App.Properties.getInt("id_Chofer")
				});
				dialog2.show();
			}
		} else {
		}
	}; 

   	
    var latInicio = Ti.UI.createLabel({});
	var lngInicio = Ti.UI.createLabel({});
	var latDestino = Ti.UI.createLabel({});
	var lngDestino = Ti.UI.createLabel({});
	geocoderlbl = Ti.UI.createLabel({});
	drapDest = Ti.UI.createLabel({});
	
	if(Ti.App.Properties.getInt('ingles')==1){
   		btnPedir.title='Request a taxi';
   		pinUsuario.title="Current position";
   		pinDestino.title="Current destination";
   		txtOrigen.hintText='Enter your Origin';
   		txtDestino.hintText='Enter your destination';
   	}
	
	
	txtDestino.addEventListener('focus',function(){
			if(servSel==0){
				if(Ti.App.Properties.getInt('ingles')==1){
				alert('Please select a trip type');
				}else{
				alert('Favor de seleccionar un tipo de viaje');
				}
			}else{
				
			}
	});
	
	if(Ti.App.Properties.getInt('ingles')==1){
		var opts2 = {options:  ['Simple','Hourly'], selectedIndex: 2,destructive:0,title: 'Choose a service option'};
	}else{
		var opts2 = {options:  ['Sencillo','Por hora'], selectedIndex: 2,destructive:0,title: 'Elija una opción de servicio'};
	}
	var dialog2;
	
	var table_data = [];
		var last_search = null;
		var timers = [];
		
		var ANDROID_API_KEY = 'AIzaSyB4xyTWJ6MAh1mO94AUfLF9WsuOiQt-OsA';
		
		var container = Ti.UI.createView({
			//layout : 'vertical',
			width:'90%',
			height : Ti.UI.SIZE,
			top:'35%',
			backgroundColor:'white',
			zIndex:10
			
		});

	var autocomplete_table = Ti.UI.createTableView({height: Ti.UI.SIZE});
	
	container.add(autocomplete_table);
	
	txtDestino.addEventListener('change', function(e) {
			if (txtDestino.value.length > 2 && txtDestino.value !=  last_search) {
				clearTimeout(timers['autocomplete']);
				timers['autocomplete'] = setTimeout(function() {
					last_search =txtDestino.value;
					auto_complete(txtDestino.value);
				}, 300);
			}
			return false;
		});
		
		autocomplete_table.addEventListener('click', function(e) {get_place(e.row.place_id);autocomplete_table.visible=false;container.visible=false;});
		
		function get_place(place_id) {
			var url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + place_id + "&location=16.8633600,-99.8901000&key=" + ANDROID_API_KEY;		
			var client = Ti.Network.createHTTPClient({
			onload : function(e) {
     			var obj = JSON.parse(this.responseText);
				var result = obj.result;
				container.place_id           = place_id;
				container.formatted_address  = result.formatted_address;
				container.latitude           = result.geometry.location.lat;
				container.longitude          = result.geometry.location.lng;
				txtDestino.value             = container.formatted_address;
			
				Ti.API.info(txtDestino);
				table_data = [];
				autocomplete_table.setData(table_data);
				autocomplete_table.setHeight(0);
 			},	
 			onerror : function(e) {
				Ti.API.debug(e.error);
				alert('error');
			},
			timeout : 5000
			});
			client.open("GET", url);
			client.send();	
		}
		
		function get_locations(query) {
			var url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + query + "&location=16.8633600,-99.8901000&key=" + ANDROID_API_KEY;		
			var client = Ti.Network.createHTTPClient({
				onload : function(e) {
     			var obj = JSON.parse(this.responseText);
     			var predictions = obj.predictions;     		
     			table_data = [];            
            		autocomplete_table.removeAllChildren();
				autocomplete_table.setHeight(Ti.UI.SIZE);
				
				function omitirAcentos(text) {
					var acentos = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç";
					var original = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc";
					for (var i=0; i<acentos.length; i++) {
						text = text.replace(acentos.charAt(i), original.charAt(i));
					}
					return text;
				}

				var dest;
            		for (var i = 0; i < predictions.length; i++) {
            			var desc = omitirAcentos(predictions[i].description);
            			var row = Ti.UI.createTableViewRow({
						height: 40,
						title: desc,
						place_id: predictions[i].place_id,
						hasDetail:false,
						postId:desc,
						color:'gray'          		
            			});                        	
            			table_data.push(row);  
            		}
            		
            		
            		 
            		autocomplete_table.setData(table_data);
            	
 				 },
 				onerror : function(e) {
					Ti.API.debug(e.error);
					alert('error');
				},
				timeout : 5000  
			});
			client.open("GET", url);
			client.send();	
		}
		
		function auto_complete(search_term){
			if (search_term.length > 2) {
				get_locations(search_term);   	
   			}
		}
		
		var autocomplete_table2 = Ti.UI.createTableView({height: Ti.UI.SIZE});
	
	container.add(autocomplete_table2);
	
	txtOrigen.addEventListener('change', function(e) {
			if (txtOrigen.value.length > 2 && txtOrigen.value !=  last_search) {
				clearTimeout(timers['autocomplete2']);
				timers['autocomplete2'] = setTimeout(function() {
					last_search2 =txtOrigen.value;
					auto_complete2(txtOrigen.value);
				}, 300);
			}
			return false;
	});
		
		autocomplete_table2.addEventListener('click', function(e) {get_place2(e.row.place_id); autocomplete_table2.visible=false; container.visible=false;});
		
		function get_place2(place_id) {
			var url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + place_id + "&location=16.8633600,-99.8901000&key=" + ANDROID_API_KEY;		
			var client = Ti.Network.createHTTPClient({
			onload : function(e) {
     			var obj = JSON.parse(this.responseText);
				var result = obj.result;
				container.place_id           = place_id;
				container.formatted_address  = result.formatted_address;
				container.latitude           = result.geometry.location.lat;
				container.longitude          = result.geometry.location.lng;
				txtOrigen.value             = container.formatted_address;
				//destinoView.height='8%';
				//origenView.height='8%';
				//txtDestino.blur();
				Ti.API.info(txtOrigen);
				table_data2 = [];
				autocomplete_table2.setData(table_data2);
				autocomplete_table2.setHeight(0);
 			},	
 			onerror : function(e) {
				Ti.API.debug(e.error);
				alert('error');
			},
			timeout : 5000
			});
			client.open("GET", url);
			client.send();	
		}
		
		function get_locations2(query2) {
			var url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + query2 + "&location=16.8633600,-99.8901000&key=" + ANDROID_API_KEY;		
			var client = Ti.Network.createHTTPClient({
				onload : function(e) {
     			var obj = JSON.parse(this.responseText);
     			var predictions = obj.predictions;     		
     			table_data2 = [];            
            		autocomplete_table2.removeAllChildren();
				autocomplete_table2.setHeight(Ti.UI.SIZE);
				
				function omitirAcentos2(text) {
					var acentos = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç";
					var original = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc";
					for (var i=0; i<acentos.length; i++) {
						text = text.replace(acentos.charAt(i), original.charAt(i));
					}
					return text;
				}

				var orig;
            		for (var i = 0; i < predictions.length; i++) {
            			var desc = omitirAcentos2(predictions[i].description);
            			var row = Ti.UI.createTableViewRow({
						height: 40,
						title: desc,
						place_id: predictions[i].place_id,
						hasDetail:false,
						postId:desc,
						color:'gray'          		
            			});                        	
            			table_data2.push(row);  
            		}
            		
            	
            		 
            		autocomplete_table2.setData(table_data2);
            		
 				 },
 				onerror : function(e) {
					Ti.API.debug(e.error);
					alert('error');
				},
				timeout : 5000  
			});
			client.open("GET", url);
			client.send();	
		}
		
		function auto_complete2(search_term2){
			if (search_term2.length > 2) {
				get_locations2(search_term2);   	
   			}
		}
	
	
	dialog2 = Ti.UI.createOptionDialog(opts2);
	dialog2.addEventListener('click', function(ed){
	   	if (ed.index === 0){
			ti_ser.text=1;
			Ti.App.Properties.setString("tiSer",1);
		}else if (ed.index === 1){
			Ti.App.Properties.setString("tiSer",2);
			Ti.App.Properties.setString("distancias",0);
			ti_ser.text=2;
		}
					Ti.App.Properties.setInt('recotizado',1);
					Ti.App.Properties.setInt('recotizado2',1);
					window.close();
                  
   					window.remove(webviewC);
				 	webviewC.release();
          		
				 	Ti.API.info('WebView: ' + webviewC);
   					
   					
						
	});
	
	
	
	
	
	Titanium.Geolocation.getCurrentPosition(function(e){
		if (e.error){
        	if (Ti.App.Properties.getInt('ingles')==1)
        	 {	alert('Unable to get current location.');}
        	else{alert('No se puede obtener ubicacion actual');}
        		return;
    		}
		if(Ti.App.Properties.getInt('ingles')==1){
			pinUsuario = Map.createAnnotation({
			latitude:e.coords.latitude, 
			longitude:e.coords.longitude,
       		title:"My location",
    			subtitle:'User',
    			image:'/images/pinOrigen.png',
    			myid:1,
    			draggable: true
		});
		}else{
			pinUsuario = Map.createAnnotation({
			latitude:e.coords.latitude, 
			longitude:e.coords.longitude,
       		title:"Mi ubicacion",
    			subtitle:'Usuario',
    			image:'/images/pinOrigen.png',
    			myid:1,
    			draggable: true
		});
		}
	
		
		latInicio.text=e.coords.latitude;
        lngInicio.text=e.coords.longitude;
        Titanium.Geolocation.reverseGeocoder(e.coords.latitude,e.coords.longitude,function(evt){
					if (evt.success) {
						var places = evt.places;
						if (places && places.length) {
							geocoderlbl.text = places[0].address;
							
							txtOrigen.value=geocoderlbl.text;
							Ti.App.Properties.setDouble("latIN",latInicio.text);
        					Ti.App.Properties.setDouble("longIN",lngInicio.text);
						
						} else {
							geocoderlbl.text = "Verifique su conexión a internet";
						}
						
					}
					else {
						Ti.UI.createAlertDialog({
							title:'Conexión inestable',
							message:evt.error
						}).show();
					}
				});
        if(Ti.App.Properties.getInt('ingles')==1){
			pinDestino = Map.createAnnotation({
			latitude:e.coords.latitude + 0.0009999, 
			longitude:e.coords.longitude -0.0009999 ,
    			title:"Destination",
    			subtitle:'Place the pin on your destination',
    			image:"/images/pinDestino.png",
    			myid:1,
    			draggable: true
    		});
		}else{
			pinDestino = Map.createAnnotation({
			latitude:e.coords.latitude + 0.0009999, 
			longitude:e.coords.longitude -0.0009999 ,
    			title:"Destino",
    			subtitle:'Coloca el pin en tu destino',
    			image:"/images/pinDestino.png",
    			myid:1,
    			draggable: true
    		});
		}
				
        mapview = Map.createView({
        		mapType: Map.NORMAL_TYPE,
        		region: {latitude:e.coords.latitude, longitude:e.coords.longitude, latitudeDelta:0.003, longitudeDelta:0.003},      
        		animate:true,
        		regionFit:true,
        		userLocation:true,
        		annotations:[pinUsuario,pinDestino]
        });
        centerView.add(mapview);
	 });
	var xhrLocationCode = Ti.Network.createHTTPClient();var requestUrl;
		xhrLocationCode.setTimeout(120000);
	txtOrigen.addEventListener('return', function(e){
  			txtOrigen.blur();
  			bdrBusqueda = 0;
  			requestUrl = "http://maps.google.com/maps/api/geocode/json?address=" + txtOrigen.value.replace(' ', '+');
			requestUrl += "&sensor=" + (Ti.Geolocation.locationServicesEnabled == true);
			xhrLocationCode.open("GET", requestUrl);
			xhrLocationCode.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
			xhrLocationCode.send();		
		});
		
		txtDestino.addEventListener('click',function(){
			if(servSel==0){
				if(Ti.App.Properties.getInt('ingles')==1){
				alert('Please select a trip type');
				}else{
				alert('Favor de seleccionar un tipo de viaje');
				}
			}else{
			container.visible=true;
			autocomplete_table.visible=true;
			autocomplete_table2.visible=false;
			destinoView.height='13%';
			origenView.height='13%';
			}
		});
		
		txtOrigen.addEventListener('click',function(){
			if(servSel==0){
				if(Ti.App.Properties.getInt('ingles')==1){
				alert('Please select a trip type');
				}else{
				alert('Favor de seleccionar un tipo de viaje');
				}
			}else{
			container.visible=true;
			autocomplete_table2.visible=true;
			autocomplete_table.visible=false;
			destinoView.height='13%';
			origenView.height='13%';
			}
		});
		
			
		txtDestino.addEventListener('focus',function(){
			container.visible=true;
			autocomplete_table.visible=true;
			autocomplete_table2.visible=false;
		   destinoView.height='13%';
			origenView.height='13%';
		});
		txtOrigen.addEventListener('focus',function(){
			container.visible=true;
			autocomplete_table2.visible=true;
			autocomplete_table.visible=false;
		   destinoView.height='13%';
			origenView.height='13%';
		});
		
		
			txtDestino.addEventListener('return', function(e){
  			txtDestino.blur();
  			bdrBusqueda = 1;
  			requestUrl = "http://maps.google.com/maps/api/geocode/json?address=" + txtDestino.value.replace(' ', '+');
			requestUrl += "&sensor=" + (Ti.Geolocation.locationServicesEnabled == true);
			xhrLocationCode.open("GET", requestUrl);
			xhrLocationCode.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
			xhrLocationCode.send();		
		});
	  
	   window.addEventListener('android:back', function(){
     		 	destinoView.height='8%';
     		 	if(txtDestino.blur()==true){
     		 		alert("cerrado");
     		 	};
      });
	  
	  xhrLocationCode.onerror = function(e) {};
		xhrLocationCode.onload = function(e) {
			
			var response = JSON.parse(this.responseText);
				if (response.status == 'OK' && response.results != undefined && response.results.length > 0) {
					if (bdrBusqueda == 0) {
						pinUsuario.latitude =response.results[0].geometry.location.lat;
						pinUsuario.longitude = response.results[0].geometry.location.lng;
						latInicio.text = response.results[0].geometry.location.lat;
						lngInicio.text = response.results[0].geometry.location.lng;
						Ti.App.Properties.setDouble("latIN",latInicio.text);
        				Ti.App.Properties.setDouble("longIN",lngInicio.text);
        				origen=txtOrigen.value;
			       		Ti.App.Properties.setString("txtorigen",origen);
			       		mapview.region = {latitude:latInicio.text, longitude:lngInicio.text, latitudeDelta:0.03, longitudeDelta:0.03};	
					}else if (bdrBusqueda == 1) {
						drapDest.text='1';
						
						pinDestino.latitude =response.results[0].geometry.location.lat;
						pinDestino.longitude = response.results[0].geometry.location.lng;
						latDestino.text = response.results[0].geometry.location.lat;
						lngDestino.text = response.results[0].geometry.location.lng;
						Ti.App.Properties.setDouble("latIN",latInicio.text);
        				Ti.App.Properties.setDouble("longIN",lngInicio.text);
						Ti.App.Properties.setDouble("latDES",latDestino.text);
       					Ti.App.Properties.setDouble("longDES",lngDestino.text);
       					
						destino=txtDestino.value;
       		      		Ti.App.Properties.setString("txtdestino",destino);
						destinoSeleccionado=1;
						
						Titanium.Geolocation.reverseGeocoder(response.results[0].geometry.location.lat,response.results[0].geometry.location.lng,function(evt){
							if (evt.success) {
								var places = evt.places;
								if (places && places.length) {
									geocoderlbl.text = places[0].address;
								}else{
									geocoderlbl.text = "No address found";
								}
							}else{
								Ti.UI.createAlertDialog({
									title:'Reverse geo error',
									message:evt.error
								}).show();
						}
					});
				mapview.region = {latitude:latDestino.text, longitude:lngDestino.text, latitudeDelta:0.03, longitudeDelta:0.03};	
				}
			}
			response = null;
		};
		
		
	mapview.addEventListener('regionchanged', function(e) {

		Titanium.Geolocation.reverseGeocoder(e.latitude, e.longitude, function(evt) {
			if (evt.success) {
				var places = evt.places;
				if (places && places.length) {
						if(destinoSeleccionado==1){
						destino = places[0].address;
						txtDestino.value = places[0].address;
						Ti.App.Properties.setString("txtdestino", destino);	
						}
						
				
				} else {
					geocoderlbl.text = "Verifique su conexión a internet";
				}
			} else {
				Ti.UI.createAlertDialog({
					title : 'Conexión inestable',
					message : evt.error
				}).show();
			}
		});

		latDestino.text = e.latitude;
		lngDestino.text = e.longitude;
		Ti.App.Properties.setDouble("latDES", e.latitude);
		Ti.App.Properties.setDouble("longDES", e.longitude);
		pinDestino.setLatitude(e.latitude);
		pinDestino.setLongitude(e.longitude);
	}); 

 		
      	rango.onload = function(){
            json = this.responseText;
            response = JSON.parse(json);
            Ti.API.info(response.logged);
            if (response.logged == true){
            	 var RangoOrigen=response.status_origen;
            	 var RangoDestino=response.status_destino;
            	 
            	 if(RangoOrigen==1){
            	 	if(Ti.App.Properties.getInt('ingles')==1){
        				var dialogAlertaRuta = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Accept'],message: 'Service not available in this area , please select another origin',title: 'Message'});
					}else{
        				var dialogAlertaRuta = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'],message: 'Servicio no disponible en esta área, favor de seleccionar otro origen',title: 'Mensaje'});
					}
					dialogAlertaRuta.show();	
            	 }else{
            	 	if(RangoDestino==1){
            	 		if(Ti.App.Properties.getInt('ingles')==1){
        				var dialogAlertaRuta = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Accept'],message: 'Service not available in this area , please select another destination',title: 'Message'});
						}else{
        				var dialogAlertaRuta = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'],message: 'Servicio no disponible en esta área, favor de seleccionar otro destino',title: 'Mensaje'});
						}
						dialogAlertaRuta.show();
					txtDestino.editable=true;		
            	 	}else{
            	 	//rutaCorrecta=1;
            	 		pedirKm.open("POST","http://subtmx.com/webservices/distanciag.php");
        					params = {
        				 	lat1:latInicio.text ,
       					 	lng1:lngInicio.text,
       					 	lat2:latDestino.text,
       					 	lng2:lngDestino.text
        					};
       					pedirKm.send(params);		
       					
            	 	}
            	 }
           	}else{ 
               			
             }
    };



	
	
	btnPedir.addEventListener('click', function() {
		if (destinoSeleccionado == 0) {
			var dialogDes = Ti.UI.createAlertDialog({
				cancel : 1,
				buttonNames : ['Acceptar'],
				message : 'Debe seleccionar un destino',
				title : 'Mensaje'
			});
			dialogDes.show();

		} else {
			rango.open("POST", "http://subtmx.com/webservices/phpgeo-master/tests/geo.php");
			params = {
				latitudo : latInicio.text,
				longitudo : lngInicio.text,
				latitudd : latDestino.text,
				longitudd : lngDestino.text,
			};
			rango.send(params);
		}

	});

  
    header.add(titulo);
  	centerView.add(header);

	centerView.add(btnPedir);
	centerView.add(mapview);
	centerView.add(destinoView);
  	centerView.add(origenView);
   	origenView.add(leftButtonOrigen);
 	destinoView.add(leftButtonDestino);
  	origenView.add(txtOrigen);
	destinoView.add(txtDestino);
	window.add(centerView);
	centerView.add(container);
	
		
    return window;
	
};
module.exports = mapa2;