
function modalWindow(){
var ratingbar = require('titutorial.ratingbar');
var window,vista,ImgPerfil,btnCalificar,texto,valorS,calChofer,calAuto,calSeguridad,calificacion,comentario,id_servicio,id_usuario,id_chofer;
calificaServ = Titanium.Network.createHTTPClient();
	
	id_servicio1=Ti.App.Properties.getInt("id_servicio");
  	id_usuario1=Ti.App.Properties.getInt("id_usuario");
  	id_chofer1= Ti.App.Properties.getInt("id_Chofer");
  	
			
	window = Ti.UI.createWindow({theme:'Theme.AppCompat.Translucent.NoTitleBar',backgroundColor :'white',opacity: 1});
 	window .orientationModes = [Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT];

	vista=Ti.UI.createView({height:'100%',width:'100%'});
		
	var starSeguridad = ratingbar.createRatingBar({ bottom:'30%', center:0, rating : 1, stars : 5, stepSize : 1, isIndicator : false,center:0}); 
  
  	texto= Ti.UI.createLabel({ center:0,width :'auto', height : 'auto', top : '10%' ,text : 'Califica al Chofer',color:'black',font  : { fontSize : '35%'}});
	ImgPerfil= Ti.UI.createImageView({width:'31%',height:'19.5%',borderRadius:50,top:'21.5%',center:0,defaultImage:'/images/foto.png',image:'/images/foto.png',zIndex:3});
	calChofer= Ti.UI.createTextField({ center:0,width :'70%', height : 'auto', top : '40%' ,hintText:'Calificación Chofer',color:'black',hintTextColor:'gray'});
	btnComentario= Ti.UI.createTextField({hintText:'Dejar comentario',top:'50%',center:0,width:'80%',heigth:'auto',color:'black',hintTextColor:'gray'});
    btnCalificar = Ti.UI.createButton({title:('Aceptar'),bottom:'10%',center:0,width:'50%',heigth:25,textAlign:'center',backgroundColor:'#305caf',color:'white'});
  	
  	if(Ti.App.Properties.getInt('ingles')==1){
  		texto.text='Rate service';
  		calChofer.hintText='Driver qualification';
  		btnComentario.hintText='Leave comment';
  		btnCalificar.title='Accept';
  	}
  	
	vista.add(texto);
    vista.add(ImgPerfil);
    vista.add(btnComentario);
    vista.add(starSeguridad);
    vista.add(btnCalificar);
       
    starSeguridad.addEventListener('change', function(e) {
		calificacion=e.rating.toString();
	});
       
    window.add(vista);	


var dialog;
 	if(Ti.App.Properties.getInt('ingles')==1){
		dialog = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Accept'],message: 'Thank you for using our service.',title: 'Qualification'});
	}else{
		dialog = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'],message: 'Gracias por utilizar nuestro servicio.',title: 'Calificación'});
	}
			
dialog.addEventListener('click', function(ed){
							if (ed.index === 0){
								calificaServ.open("POST","http://subtmx.com/webservices/calificacion.php");
                                params = {
                                	calificacion:calificacion,
                                	comentario:btnComentario.value,
                                	id_servicio:id_servicio1,
                                	id_usuario:id_usuario1,
                                	id_chofer:id_chofer1
                                  };
                               calificaServ.send(params);
                               mapa = require('/interface/mapa');
    			new mapa().open({modal:true});
								window.close();
							}
				});	
btnCalificar.addEventListener('click', function(e){
		dialog.show();
		
            
		//window.close();

});
	






return window;
};
module.exports = modalWindow;