function perfil(){
	var NappSlideMenu = require('dk.napp.slidemenu');
	var window,lblCuenta,lblVencimiento,lblTarjeta,txtCuenta,txtVencimiento,txtTarjeta,cmbTarjeta,cmbVencimiento,btnCuenta;
	var tableData = [],tableView;var hidden=false;
	
	winLeft = Ti.UI.createWindow({backgroundImage:'/images/bgMenu.jpg'});
	logoLeft = Titanium.UI.createImageView({image:'/images/logo.png',width:'35%',height:'auto',zIndex:2,left:'15%',top:'20%',opacity:1});
	lblNombreCompleto = Titanium.UI.createLabel({color:'black',text:Ti.App.Properties.getString('nombre_completo'),top:'15%',left:'15%',font:{fontWeight:'bold'}});
	lblHome = Titanium.UI.createLabel({color:'#6485c3',text:'Home',top:'52%',left:'13%'});
	lblMisPedidos = Titanium.UI.createLabel({color:'#6485c3',text:'Mis Pedidos',top:'60%',left:'13%'});
	lblMiPerfil = Titanium.UI.createLabel({color:'#6485c3',text:'Mi Perfil',top:'68%',left:'13%',font:{fontWeight:'bold'}});
	lblPromociones = Titanium.UI.createLabel({color:'#6485c3',text:'Promociones',top:'76%',left:'13%'});
	lblPagos = Titanium.UI.createLabel({color:'#6485c3',text:'Pagos',top:'84%',left:'13%'});
	lblConfi = Titanium.UI.createLabel({color:'#6485c3',text:'Configuración',top:'92%',left:'13%'});
	navwin = createCenterNavWindow();
	tipopago = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Paypal','Tarjeta de credito/debito'],message: 'Seleccione el metodo de pago de su preferencia',title: 'Tipo de pago'});
	optsEditPagos = {cancel: 2,options: ['Editar', 'Mantener', 'Cancelar'],selectedIndex: 2,destructive: 0,title: 'Seleccione una opción'};
	dialogEditPagos = Ti.UI.createOptionDialog(optsEditPagos);	
	
	lblHome.addEventListener('click',function(){
		if (Ti.App.Properties.hasProperty('id_usuario')) {
			mapa = require('/interface/mapa');
			winNav = new mapa();
			winNav.open();
		}else{
			if (Ti.App.Properties.hasProperty('idioma')) {
				Ti.UI.createAlertDialog({title:'Message',message:'Only users register'}).show();
			}else{
				Ti.UI.createAlertDialog({title:'Mensaje',message:'Solo usuarios registrados'}).show();
			}
		}
	});
	lblPagos.addEventListener('click',function(){
		if (Ti.App.Properties.hasProperty('id_usuario')) {
			tipopago.show();
		}else{
			if (Ti.App.Properties.hasProperty('idioma')) {
				Ti.UI.createAlertDialog({title:'Message',message:'Only users register'}).show();
			}else{
				Ti.UI.createAlertDialog({title:'Mensaje',message:'Solo usuarios registrados'}).show();
			}
		}
	});
	
	tipopago.addEventListener('click', function(e) {
			if (e.index == 0) {
				Titanium.App.Properties.removeProperty('coneckta');
				if (Ti.App.Properties.hasProperty('paypal')) {
					dialogEditPagos.show();
				}else{
						
				}
			}else{
				Titanium.App.Properties.removeProperty('paypal');
				if (Ti.App.Properties.hasProperty('coneckta')) {
					dialogEditPagos.show();
				}else{
					
				}
			}
		});
		
		dialogEditPagos.addEventListener('click', function(ed){
			if (ed.index == 0){
				if (Ti.App.Properties.hasProperty('paypal')) {
					paypal= require('/interface/paypal');
        			new paypal().open();
        			window.close();
				}else if (Ti.App.Properties.hasProperty('coneckta')){
					paypal= require('/interface/pago');
        			new paypal().open();
        			window.close();
				}
			}else if (ed.index == 1){
				
			}
		});
	
	lblPromociones.addEventListener('click',function(){
		if (Ti.App.Properties.hasProperty('idioma')) {
			Ti.UI.createAlertDialog({title:'Message',message:'No disponibles'}).show();
		}else{
			Ti.UI.createAlertDialog({title:'Mensage',message:'Not available'}).show();
		}
	});
	lblMisPedidos.addEventListener('click',function(){
		if (Ti.App.Properties.hasProperty('id_usuario')) {
			pedidos = require('/interface/pedidos');
			winNav = new pedidos();
			winNav.open();
		}else{
			if (Ti.App.Properties.hasProperty('idioma')) {
				Ti.UI.createAlertDialog({title:'Message',message:'Only users register'}).show();
			}else{
				Ti.UI.createAlertDialog({title:'Mensaje',message:'Solo usuarios registrados'}).show();
			}
		}
	});
	lblMiPerfil.addEventListener('click',function(){
		mainwin.closeOpenView();
	});
	
	function createCenterNavWindow(){
		tc = Ti.UI.createLabel({text:'Mi Perfil',color:'#000000'});
		window = Ti.UI.createWindow({backgroundColor:'white',titleControl:tc});
		navwin = Titanium.UI.iOS.createNavigationWindow({window: window});
		menu = Ti.UI.createButton({backgroundImage:'/images/menuicon.png', left:10,zIndex:2,height:23,width:30});
		share = Ti.UI.createButton({backgroundImage:'/images/iconEdit.png', right:50,height:28,width:28,zIndex:5});
		done =  Titanium.UI.createButton({backgroundImage:'/images/palomita.png', right:50,height:28,width:28,zIndex:5});
		viewLogo = Titanium.UI.createImageView({width:'100%',height:'auto',image:'/images/bgPerfil.jpg',top:0,left:0,zIndex:3});
		vistaimg= Ti.UI.createView({width:124,height:124,borderRadius:65, top:'5%', bottom:15, center:0,zIndex:3});
		imgUser = Ti.UI.createImageView({image:"/images/usuario.jpg",width:124,height:120});
		scrollView = Titanium.UI.createScrollView({contentWidth:'auto',contentHeight:'auto',top:0,showVerticalScrollIndicator:true,showHorizontalScrollIndicator:false});
		lblUsuario = Ti.UI.createLabel({text:'Usuario',color:'#aeaec1',top:'40%',left:'2%'});
		txtUsuario = Ti.UI.createTextField({value:Ti.App.Properties.getString('usuario'),color:'#151431',right:'2%',top:'40%',editable:false});
		separador = Ti.UI.createView({width:'100%',height:1,top:'47%',backgroundColor:'#eeeeee'});
		lblCorreo = Ti.UI.createLabel({text:'Email',color:'#aeaec1',top:'50%',left:'2%'});
		txtCorreo = Ti.UI.createTextField({value:Ti.App.Properties.getString('email'),color:'#151431',right:'2%',top:'50%',editable:false});
		separador2 = Ti.UI.createView({width:'100%',height:1,top:'57%',backgroundColor:'#eeeeee'});
		lblTelefono = Ti.UI.createLabel({text:'Telefono',color:'#aeaec1',top:'60%',left:'2%'});
		txtTelefono = Ti.UI.createTextField({value:Ti.App.Properties.getString('telefono'),color:'#151431',right:'2%',top:'60%',editable:false});
		separador3 = Ti.UI.createView({width:'100%',height:1,top:'67%',backgroundColor:'#eeeeee'});
		lblRfc= Ti.UI.createLabel({text:'RFC',color:'#aeaec1',top:'60%',left:'2%'});
		txtRfc = Ti.UI.createTextField({value:Ti.App.Properties.getString('rfc'),color:'#151431',right:'2%',top:'60%',editable:false});
		separador4 = Ti.UI.createView({width:'100%',height:1,top:'77%',backgroundColor:'#eeeeee'});
		lblRazon= Ti.UI.createLabel({text:'Razon Social',color:'#aeaec1',top:'70%',left:'2%'});
		txtRazon = Ti.UI.createTextField({value:Ti.App.Properties.getString('razon'),color:'#151431',right:'2%',top:'70%',editable:false});
		separador5 = Ti.UI.createView({width:'100%',height:1,top:'87%',backgroundColor:'#eeeeee'});
		lblCerrar = Ti.UI.createLabel({text:'Cerrar Sesión',color:'#aeaec1',top:'80%',left:'2%'});
		btnCerrar = Ti.UI.createButton({backgroundImage:'/images/iconClose.png',right:'2%',zIndex:2,height:38,width:43,top:'80%'});
		updateClienteReq = Titanium.Network.createHTTPClient();
		closeSesion=Titanium.Network.createHTTPClient();
		if (Ti.App.Properties.hasProperty('telefono')) {
			scrollView.add(lblTelefono);
			scrollView.add(txtTelefono);
			scrollView.add(separador5);
			lblRfc.top = '70%';
			txtRfc.top = '70%';
			lblRazon.top = '80%';
			txtRazon.top = '80%';
			lblCerrar.top = '90%';
			btnCerrar.top = '90%';
		}
		
		if (Ti.App.Properties.hasProperty('idioma')) {
			tc.text = 'Profile';
			lblUsuario.text = 'User';
			lblTelefono.text = 'Telephone';
			lblRazon.text = 'Business name';
			lblMisPedidos.text='My books';
   			lblMiPerfil.text='Profile';
   			lblPromociones.text='Promotions';
   			lblPagos.text='Payment';
   			lblConfiguración.text='Configuration';
   			lblCerrar.text = 'Sign off';
		}
		
		closeSesion.open("POST","http://subtmx.com/webservices/statususuario.php");
        	params = {
        		id_usuario:Ti.App.Properties.getInt('id_usuario'),
        	};
                		
		btnCerrar.addEventListener('click',function(){
            closeSesion.send(params);
			Titanium.App.Properties.removeProperty('id_usuario');
			Titanium.App.Properties.removeProperty('usuario');
			Titanium.App.Properties.removeProperty('email');
			Titanium.App.Properties.removeProperty('telefono');
			Titanium.App.Properties.removeProperty('rfc');
			Titanium.App.Properties.removeProperty('razon');
			inicio = require('/interface/inicio');
			winNav = new inicio();
			winNav.open();
			window.close();
		});
		
		menu.addEventListener('click',function(){
			mainwin.toggleLeftView();
			mainwin.setCenterhiddenInteractivity("TouchEnabled");
		});
		share.addEventListener('click',function(){
			if (!hidden) {
				window.setRightNavButton(done);
				txtRfc.editable = true;
				txtRfc.focus();
				txtRazon.editable = true;
				hidden = true;
			}else{
				window.setRightNavButton(share);
				
				hidden = false;
			}
		});
		done.addEventListener('click',function(){
			if (!hidden) {
				window.setRightNavButton(done);
				hidden = true;
			}else{
				txtRfc.editable = false;
				txtRazon.editable = false;
				window.setRightNavButton(share);
				hidden = false;
			}
			if (Ti.App.Properties.hasProperty('id_cliente')) {
				updateClienteReq.open("POST","http://subtmx.com/webservices/updateclientes.php");
        			params = {
            			id_cliente:Ti.App.Properties.getInt('id_cliente'),
            			rfc: txtRfc.value,
            			razon_social: txtRazon.value
        			};
        			updateClienteReq.send(params);
			}else{
				alertDialog = Titanium.UI.createAlertDialog({title: 'Registro/Login',message:'Registrece o Inicie Sesión',buttonNames: ['Aceptar']});
    				alertDialog.show();
			}
			
		});
		updateClienteReq.onload = function(){
    			json = this.responseText;
    			response = JSON.parse(json);
    			if (response.logged == true){
    				Ti.App.Properties.setString("rfc", txtRfc.value);
    				Ti.App.Properties.setString("razon", txtRazon.value);
    				alert(response.message);
    			}else{
        			alert(response.message);
    			}
		};
		
		window.containingNav = navwin;
		window.setLeftNavButton(menu);
		window.setRightNavButton(share);
		window.add(viewLogo);
		window.add(vistaimg);
		vistaimg.add(imgUser);
		scrollView.add(lblUsuario);
		scrollView.add(txtUsuario);
		scrollView.add(separador);
		scrollView.add(lblCorreo);
		scrollView.add(txtCorreo);
		scrollView.add(separador2);
		scrollView.add(separador3);
		scrollView.add(lblRfc);
		scrollView.add(txtRfc);
		scrollView.add(separador4);
		scrollView.add(lblRazon);
		scrollView.add(txtRazon);
		scrollView.add(lblCerrar);
		scrollView.add(btnCerrar);
		window.add(scrollView);
		
		return navwin;
	}
	
	mainwin = NappSlideMenu.createSlideMenuWindow({
		centerWindow: navwin,
		leftWindow:winLeft,
		statusBarStyle: NappSlideMenu.STATUSBAR_WHITE,
		leftLedge:100
	});
	
	winLeft.add(logoLeft);
	winLeft.add(lblHome);
	winLeft.add(lblMisPedidos);
	winLeft.add(lblMiPerfil);
	winLeft.add(lblPromociones);
	winLeft.add(lblPagos);
	winLeft.add(lblConfi);
	winLeft.add(lblNombreCompleto);
	
	return mainwin;
};
module.exports = perfil;