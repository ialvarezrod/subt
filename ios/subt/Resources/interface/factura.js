function factura(color, _title, _nav){
	var window,tc,lblUser,lblpass,btnlogin,scrollView,txtUser,txtPass,loginReq,json,response,resetReq,mapa,registerReq;
	
	tc = Ti.UI.createLabel({text: _title,color:'white'});
	window = Ti.UI.createWindow({backgroundImage:'/images/bgRegistro.png',navTintColor:'white', nav:_nav, titleControl: tc, title: _title});
	scrollView = Titanium.UI.createScrollView({contentWidth:'auto',contentHeight:'auto',top:0,showVerticalScrollIndicator:true,showHorizontalScrollIndicator:false});
	lblUser = Titanium.UI.createLabel({text:'RFC',center:0,top:'5%',color:'white'});
	txtUser = Titanium.UI.createTextField({color:'#0066cc',hintText:'CRTP891004BR4',center:0,top:'10%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,value:"",textAlign:'center',keyboardType:Titanium.UI.KEYBOARD_TYPE_ASCII, returnKeyType:Titanium.UI.RETURNKEY_NEXT});
	lblpass= Titanium.UI.createLabel({text:'Razón Social',center:0,top:'18%',color:'white'});
	txtPass = Titanium.UI.createTextField({hintText:'Manzur y Asociados SA de CV',center:0,top:'23%',color:'#0066cc',center:0,width:'80%',height:40,leftButtonPadding:10,borderRadius:6,value:"",textAlign:'center',keyboardType:Titanium.UI.KEYBOARD_TYPE_ASCII, returnKeyType:Titanium.UI.RETURNKEY_NEXT});
	
	lblDireccion= Titanium.UI.createLabel({text:'Direccion fiscal',center:0,top:'31%',color:'white'});
	txtDireccion = Titanium.UI.createTextField({hintText:'Direccion fiscal',center:0,top:'36%',color:'#0066cc',center:0,width:'80%',height:40,leftButtonPadding:10,borderRadius:6,value:"",textAlign:'center',keyboardType:Titanium.UI.KEYBOARD_TYPE_ASCII, returnKeyType:Titanium.UI.RETURNKEY_NEXT});
	
	lblCp= Titanium.UI.createLabel({text:'Codigo Postal',center:0,top:'44%',color:'white'});
	txtCp = Titanium.UI.createTextField({hintText:'Codigo Postal',center:0,top:'52%',color:'#0066cc',center:0,width:'80%',height:40,leftButtonPadding:10,borderRadius:6,value:"",textAlign:'center',keyboardType:Titanium.UI.KEYBOARD_TYPE_ASCII, returnKeyType:Titanium.UI.RETURNKEY_NEXT});
	
	lblCorreo= Titanium.UI.createLabel({text:'Correo de facturación',center:0,top:'60%',color:'white'});
	txtCorreo = Titanium.UI.createTextField({hintText:'Correo de facturación',center:0,top:'65%',color:'#0066cc',center:0,width:'80%',height:40,leftButtonPadding:10,borderRadius:6,value:"",textAlign:'center',keyboardType:Titanium.UI.KEYBOARD_TYPE_EMAIL, returnKeyType:Titanium.UI.RETURNKEY_NEXT});
	
	btnlogin = Titanium.UI.createButton({title:'Registrarme',color:'white',borderRadius:10,borderColor:'white',top:'85%',width:'80%',height:60,backgroundColor:'#0c83c7'});
	chkFactura = Ti.UI.createSwitch({value:false,top:'73%',onTintColor:'#62D337',tintColor:'#0c83c7',left:'10%'});
	lblFactura = Titanium.UI.createLabel({text:'Acepto términos y condiciones',right:'10%',top:'74%',color:'white'});
	registerReq = Titanium.Network.createHTTPClient();
	codigoReq = Titanium.Network.createHTTPClient();
	dialogCodigo = Ti.UI.createAlertDialog({title: 'Introduzca el codigo enviado a su correo',style:Ti.UI.iPhone.AlertDialogStyle.PLAIN_TEXT_INPUT,buttonNames: ['Entrar','Cancelar']});
	
	
	txtUser.addEventListener('return', function(e) {
		txtPass.focus();
	});
	txtPass.addEventListener('return', function(e) {
		txtDireccion.focus();
	});
	txtDireccion.addEventListener('return', function(e) {
		txtCp.focus();
	});
	txtCp.addEventListener('return', function(e) {
		txtCorreo.focus();
	});
	
	window.addEventListener('open', function(e) {
		if (Ti.App.Properties.hasProperty('idioma')) {
			lblpass.text = 'Corporate name';
			lblDireccion = 'Fiscal address';
			txtDireccion.value = 'fiscal address';
			lblCp.text = 'Postal Code';
			txtCp.value = 'postal Code';
			lblCorreo.text = 'Invoice email';
			txtCorreo.value = 'invoice email';
			btnlogin.title = 'Sign in';
			lblFactura.text = 'Agree to terms and conditions';
			tc.text = 'Invoice';
		}
	});
	
	window.addEventListener('foo', function(e){
		registerReq.onload = function(){
			json = this.responseText;
    			response = JSON.parse(json);
    			if (response.message == "Insert failed" || response.message == 'El usuario o email ya existen' || response.message == 'Tu dispositivo fue bloqueado, consulte a soporte técnico: soporte@subtmx.com'){
        			alertDialog = Titanium.UI.createAlertDialog({title: 'Soporte',message: response.message,buttonNames: ['Aceptar']}).show();
    			}else{
    				alertDialog2 = Titanium.UI.createAlertDialog({title: 'Mensaje',message: response.message,buttonNames: ['OK']});
    				alertDialog2.show();
    				alertDialog2.addEventListener('click',function(e){
    					Ti.App.Properties.setInt("id_usuario2", response.id_usuario);
    					Ti.App.Properties.setInt("id_cliente", response.id_cliente);
    					Ti.App.Properties.setString("usuario", response.usuario);
    					Ti.App.Properties.setString("email", response.email);
    					Ti.App.Properties.setString("telefono", response.telefono);
    					Ti.App.Properties.setString("nombre", response.nombre);
    					Ti.App.Properties.setString("nombre_completo", response.nombre_completo);
    					Ti.App.Properties.setString("apellido", response.apellido);
    					Ti.App.Properties.setString("rfc", response.rfc);
    					Ti.App.Properties.setString("razon", response.razon);
    					Ti.App.Properties.setString("sexo", response.sexo);
    					Ti.App.Properties.setInt("BonoActual", 0);
   						Ti.App.Properties.setInt("pagoInicial", 0);
    					dialogCodigo.show();
    					/*mapa = require('/interface/mapa');
						  winNav = new mapa();
						  winNav.open();
					      window.close();*/
    				});
    			}
		};
		
		btnlogin.addEventListener('click',function(){
			if (txtUser.value != '' && txtPass.value != ''){
				registerReq.open("POST","http://subtmx.com/webservices/registerResquest.php");
                		params = {
                    		username: e.username,
                    		password: Ti.Utils.md5HexDigest(e.password),
                    		correo: e.correo,
                    		telefono:e.telefono,
                    		nombre:e.nombre,
                    		apellido_p:e.apellido_p,
                    		apellido_m:e.apellido_m,
                    		rfc:txtUser.value,
                    		razon_social:txtPass.value,
                    		direccion_fiscal:txtDireccion.value,
                    		email_factura:txtCorreo.value,
                    		cp:txtCp.value,
                    		sexo:e.sexo,
                    		device_token:Titanium.Platform.id,
                        	 device_type:"ios"
                		};
                	registerReq.send(params);
			}else{
				dialog = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'],message: 'Todos los campos son obligatorios',title: 'Mensaje'});
				if (Ti.App.Properties.hasProperty('idioma')) {
					dialog.buttonNames = ['Accept'];
					dialog.message = 'All fields are required';
					dialog.title = 'Message';
				}
				dialog.show();
			}
		});
		
		dialogCodigo.addEventListener('click', function(e) {
			if (e.index == 0) {
				//dialogCodigo.value = dialogCodigo.value.replace(/^\s+/g, '').replace(/\s+$/g, '');
				if (e.text != '') {
					codigoReq.open("POST","http://subtmx.com/webservices/codigo.php");
           			params = {
            			usuario: Ti.App.Properties.getInt("id_usuario2"),
                		codigo: e.text
          			};
            		codigoReq.send(params);
				} else {
					Ti.UI.createAlertDialog({
						cancel : 1,
						buttonNames : ['Aceptar'],
						message : 'Debe introducir el código de seguridad',
						title : 'Error'
					}).show();
				}	
			}
		});
	
		codigoReq.onload = function(){
			json = this.responseText;
    		response = JSON.parse(json);
    		if (response.logged == false){
        		alertDialog2 = Titanium.UI.createAlertDialog({title:'Error',message: response.message,buttonNames: ['Aceptar']}).show();
    		}else{
    			Ti.App.Properties.setInt("id_usuario",Ti.App.Properties.getInt("id_usuario2"));
    			alertDialog = Titanium.UI.createAlertDialog({title: 'Registro',message: response.message,buttonNames: ['OK']});
    			alertDialog.show();
    			alertDialog.addEventListener('click',function(e){
    					mapa = require('/interface/mapa');
						winNav = new mapa();
						winNav.open();
						window.close();
    			});
    		}
		};	
	});
	
	scrollView.add(lblUser);
	scrollView.add(txtUser);
	scrollView.add(lblpass);
	scrollView.add(txtPass);
	scrollView.add(chkFactura);
	scrollView.add(lblFactura);
	scrollView.add(lblDireccion);
	scrollView.add(txtDireccion);
	scrollView.add(lblCp);
	scrollView.add(txtCp);
	scrollView.add(lblCorreo);
	scrollView.add(txtCorreo);
	scrollView.add(btnlogin);
	
	window.barColor = color;
	window.backButtonTitle = '';
	window.add(scrollView);
	
	return window;
};
module.exports = factura;