function login(color, _title, _nav){
	
	var window,tc,lblUser,lblpass,btnlogin,scrollView,txtUser,txtPass,lblreset,btnFacebook,loginReq,json,response,resetReq,mapa;
	
	tc = Ti.UI.createLabel({text: _title,color:'white'});
	window = Ti.UI.createWindow({backgroundImage:'/images/bgLogin.png',navTintColor:'white', nav:_nav, titleControl: tc, title: _title});
	scrollView = Titanium.UI.createScrollView({contentWidth:'auto',contentHeight:'auto',top:0,showVerticalScrollIndicator:true,showHorizontalScrollIndicator:false});
	lblUser = Titanium.UI.createLabel({text:'Usuario',center:0,top:'20%',color:'#49a6ff'});
	txtUser = Titanium.UI.createTextField({color:'#0066cc',hintText:'usuario',center:0,top:'26%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,textAlign:'center',returnKeyType:Titanium.UI.RETURNKEY_NEXT});
	lblpass= Titanium.UI.createLabel({text:'Contraseña',center:0,top:'36%',color:'#49a6ff'});
	txtPass = Titanium.UI.createTextField({hintText:'***********',center:0,top:'42%',color:'#0066cc',center:0,width:'80%',height:40,leftButtonPadding:10,borderRadius:6,textAlign:'center',passwordMask:true});
	btnlogin = Titanium.UI.createButton({title:'Iniciar Sesión',color:'white',borderRadius:10,borderColor:'white',top:'55%',width:'80%',height:60,backgroundColor:'#0c83c7'});
	lblreset = Titanium.UI.createLabel({text:'Recuperar Contraseña',color:'#28759b',top:'70%',center:0});
	btnFacebook = Titanium.UI.createButton({title:'Iniciar con Facebook', center:0, bottom:'10%',backgroundImage:'/images/facebook.png',width:'75%',height:45,borderRadius:2,textAlign:'center'});
	loginReq = Titanium.Network.createHTTPClient();
	resetReq = Titanium.Network.createHTTPClient();
	codigoReq = Titanium.Network.createHTTPClient();
	dialogCodigo = Ti.UI.createAlertDialog({title: 'Introduzca el codigo enviado a su correo',style:Ti.UI.iPhone.AlertDialogStyle.PLAIN_TEXT_INPUT,buttonNames: ['Entrar','Cancelar']});
	
	txtUser.addEventListener('return', function(e) {
		txtPass.focus();
	});
	
	window.addEventListener('open', function(e) {
		if (Ti.App.Properties.hasProperty('idioma')) {
			lblUser.text = 'User';
			txtUser.hintText = 'user';
			lblpass.text = 'Password';
			btnlogin.title = 'Log in';
			lblreset.text = 'Recover password';
			tc.text = 'Log in';
		}
	});
	
	btnlogin.addEventListener('click',function(e){
    		if (txtUser.value != '' && txtPass.value != ''){
        			loginReq.open("POST","http://subtmx.com/webservices/loginResquest.php");
        			params = {
            			username: txtUser.value,
            			password: Ti.Utils.md5HexDigest(txtPass.value)
        			};
        			loginReq.send(params);
    		}else{
    				dialog = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'],message: 'Usuario y password son requeridos',title: 'Mensaje'});
				if (Ti.App.Properties.hasProperty('idioma')) {
					dialog.buttonNames = ['Accept'];
					dialog.message = 'Username and password are required';
					dialog.title = 'Message';
				}
				dialog.show();
    			}
	});
	
	loginReq.onload = function(){
    	json = this.responseText;
    	response = JSON.parse(json);
    	if (response.logged == true){
    		Ti.App.Properties.setInt("id_usuario2", response.id_usuario);
    		Ti.App.Properties.setInt("id_cliente", response.id_cliente);
    		Ti.App.Properties.setString("usuario", response.usuario);
    		Ti.App.Properties.setString("nombre", response.nombre);
    		Ti.App.Properties.setString("apellido", response.apellido);
    		Ti.App.Properties.setString("nombre_completo", response.nombre_completo);
    		Ti.App.Properties.setString("email", response.email);
    		Ti.App.Properties.setString("telefono", response.telefono);
    		Ti.App.Properties.setString("rfc", response.rfc);
    		Ti.App.Properties.setString("razon", response.razon);
    		Ti.App.Properties.setString("sexo", response.sexo);
   			Ti.App.Properties.setInt("BonoActual", 0);
   			Ti.App.Properties.setInt("pagoInicial", 0);
    		if (response.status_codigo == 1){
    			dialogCodigo.show();
    		}else{
    			Ti.App.Properties.setInt("id_usuario", response.id_usuario);
    			mapa = require('/interface/mapa');
				winNav = new mapa();
				winNav.open();
				window.close();
    		}
    	}else{
    		dialog = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'],message: response.message,title: 'Mensaje'});
			if (Ti.App.Properties.hasProperty('idioma')) {
				dialog.buttonNames = ['Accept'];
				dialog.message = 'Welcome to SUBT';
				dialog.title = 'Message';
			}
			dialog.show();
    	}
	};
	
	dialogCodigo.addEventListener('click', function(e) {
		if (e.index == 0) {
				//dialogCodigo.value = dialogCodigo.value.replace(/^\s+/g, '').replace(/\s+$/g, '');
				if (e.text != '') {
					codigoReq.open("POST","http://subtmx.com/webservices/codigo.php");
           			params = {
            			usuario: Ti.App.Properties.getInt("id_usuario2"),
                		codigo: e.text
          			};
            		codigoReq.send(params);
				} else {
					alertDialog3 = Titanium.UI.createAlertDialog({title:'Error',message: 'Debe introducir el código de seguridad',buttonNames: ['Aceptar']}).show();
				}	
			}
	});
	
	codigoReq.onload = function(){
			json = this.responseText;
    		response = JSON.parse(json);
    		if (response.logged == false){
        		alertDialog2 = Titanium.UI.createAlertDialog({title:'Error',message: response.message,buttonNames: ['Aceptar']}).show();
    		}else{
    			Ti.App.Properties.setInt("id_usuario", Ti.App.Properties.getInt("id_usuario2"));
    			alertDialog = Titanium.UI.createAlertDialog({title: 'Registro',message: response.message,buttonNames: ['OK']});
    			alertDialog.show();
    			alertDialog.addEventListener('click',function(e){
    					mapa = require('/interface/mapa');
						winNav = new mapa();
						winNav.open();
						window.close();
    			});
    		}
	};
	
	resetReq.onerror = function(event){
    	alert('Error de conexion: ' + JSON.stringify(event));
	};
	
	lblreset.addEventListener('click', function(){
		if (txtUser.value == ''){
			dialog = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'],message: 'Se requiere su usuario para restaurar la contraseña',title: 'Alerta'});
			if (Ti.App.Properties.hasProperty('idioma')) {
				dialog.buttonNames = ['Accept'];
				dialog.message = 'The user is required to reset the password';
				dialog.title = 'Message';
			}
			dialog.show();
    	}else{
        	resetReq.open('POST','http://subtmx.com/webservices/sendemail.php');
         	params = {
            	names: txtUser.value
        	};
        	resetReq.send(params);
    	}
	});
	
	resetReq.onload = function(){
    	json = this.responseText;
    	response = JSON.parse(json);
    	if (response.mail == true){
    		dialog = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'],message: 'Se le ha enviado un correo para restaurar su contraseña',title: 'Mensaje'});
			if (Ti.App.Properties.hasProperty('idioma')) {
				dialog.buttonNames = ['Accept'];
				dialog.message = 'It sent you an email to reset your password';
				dialog.title = 'Message';
			}
			dialog.show();
    	}else{
        	alert(response.message);    
    	}
	};
	
	resetReq.onerror = function(event){
    	alert('Error de conexion: ' + JSON.stringify(event));
	};
	
	scrollView.add(lblUser);
	scrollView.add(txtUser);
	scrollView.add(lblpass);
	scrollView.add(txtPass);
	scrollView.add(lblreset);
	scrollView.add(btnlogin);
	
	window.barColor = color;
	window.backButtonTitle = '';
	window.add(scrollView);
	
	return window;
};
module.exports = login;