function pedidos(){
	var NappSlideMenu = require('dk.napp.slidemenu');
	var window,lblCuenta,lblVencimiento,lblTarjeta,txtCuenta,txtVencimiento,txtTarjeta,cmbTarjeta,cmbVencimiento,btnCuenta;
	var tableData = [],tableView;
	winLeft = Ti.UI.createWindow({backgroundImage:'/images/bgMenu.jpg'});
	logoLeft = Titanium.UI.createImageView({image:'/images/logo.png',width:'35%',height:'auto',zIndex:2,left:'15%',top:'20%',opacity:1});
	lblHome = Titanium.UI.createLabel({color:'#6485c3',text:'Home',top:'52%',left:'13%'});
	lblMisPedidos = Titanium.UI.createLabel({color:'#6485c3',text:'Mis Pedidos',top:'60%',left:'13%',font:{fontWeight:'bold'}});
	lblMiPerfil = Titanium.UI.createLabel({color:'#6485c3',text:'Mi Perfil',top:'68%',left:'13%'});
	lblPromociones = Titanium.UI.createLabel({color:'#6485c3',text:'Promociones',top:'76%',left:'13%'});
	lblPagos = Titanium.UI.createLabel({color:'#6485c3',text:'Pagos',top:'84%',left:'13%'});
	lblConfi = Titanium.UI.createLabel({color:'#6485c3',text:'Configuración',top:'92%',left:'13%'});
	navwin = createCenterNavWindow();
	tipopago = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Paypal','Tarjeta de credito/debito'],message: 'Seleccione el metodo de pago de su preferencia',title: 'Tipo de pago'});
	optsEditPagos = {cancel: 2,options: ['Editar', 'Mantener', 'Cancelar'],selectedIndex: 2,destructive: 0,title: 'Seleccione una opción'};
	dialogEditPagos = Ti.UI.createOptionDialog(optsEditPagos);	
	
	
	lblHome.addEventListener('click',function(){
		if (Ti.App.Properties.hasProperty('id_usuario')) {
			mapa = require('/interface/mapa');
			winNav = new mapa();
			winNav.open();
		}else{
			
		}
	});
	lblPagos.addEventListener('click',function(){
		if (Ti.App.Properties.hasProperty('id_usuario')) {
			tipopago.show();
		}else{
			Ti.UI.createAlertDialog({title:'Mensage',message:'Solo usuarios registrados'}).show();
		}
	});
	
	tipopago.addEventListener('click', function(e) {
			if (e.index == 0) {
				Titanium.App.Properties.removeProperty('coneckta');
				if (Ti.App.Properties.hasProperty('paypal')) {
					dialogEditPagos.show();
				}else{
						
				}
			}else{
				Titanium.App.Properties.removeProperty('paypal');
				if (Ti.App.Properties.hasProperty('coneckta')) {
					dialogEditPagos.show();
				}else{
					
				}
			}
		});
		
		dialogEditPagos.addEventListener('click', function(ed){
			if (ed.index == 0){
				if (Ti.App.Properties.hasProperty('paypal')) {
					paypal= require('/interface/paypal');
        			new paypal().open();
        			window.close();
				}else if (Ti.App.Properties.hasProperty('coneckta')){
					paypal= require('/interface/pago');
        			new paypal().open();
        			window.close();
				}
			}else if (ed.index == 1){
				
			}
		});
	
	lblPromociones.addEventListener('click',function(){
			Ti.UI.createAlertDialog({title:'Mensage',message:'No disponible'}).show();
	});
	lblMisPedidos.addEventListener('click',function(){
		mainwin.closeOpenView();
	});
	lblMiPerfil.addEventListener('click',function(){
		if (Ti.App.Properties.hasProperty('id_usuario')) {
			perfil = require('/interface/perfil');
			winNav = new perfil();
			winNav.open();
		}else{
			Ti.UI.createAlertDialog({title:'Mensage',message:'Solo usuarios registrados'}).show();
		}
	});
	
	function createCenterNavWindow(){
		tc = Ti.UI.createLabel({text:'Mis pedidos',color:'#000000'});
		window = Ti.UI.createWindow({backgroundImage:'/images/bgRegistro.png',titleControl:tc});
		navwin = Titanium.UI.iOS.createNavigationWindow({window: window});
		menu = Ti.UI.createButton({backgroundImage:'/images/menuicon.png', left:10,zIndex:2,height:23,width:30});
		centerView =Ti.UI.createView({height: 'auto',right:0,top: 0,width:'100%',zIndex:1});
		fondoP = Ti.UI.createView({backgroundColor: '#eaeaea',height: '20%',bottom:0,width:'auto',zIndex: 2});
	 	logo = Titanium.UI.createImageView({image:'/images/logo.png',width:'20%',height:'auto',zIndex:2,center:0,top:'10%',bottom:'10%',opacity:1});
		deleteServiciosReq = Titanium.Network.createHTTPClient();
		
		args = arguments[0] || {};url = "http://subtmx.com/webservices/pedidos.php";db = {};
		function getData(){
			var xhr = Ti.Network.createHTTPClient({
				onload: function() {
					db = JSON.parse(this.responseText);
					if(db.servicios){
						setData(db);
					}
				},
				onerror: function(e) {
					Ti.API.debug("STATUS: " + this.status);
					Ti.API.debug("TEXT:   " + this.responseText);
					Ti.API.debug("ERROR:  " + e.error);
					alert('Comprueba tu conexion a internet');
				},
				timeout:5000
			});	
			xhr.open('POST', url);
			var params = {
    				id_usuario:Ti.App.Properties.getInt('id_usuario')
    			};
			xhr.send(params);
		}
		
		function setData(db){
			for(var i = 0; i < db.servicios.length; i++){
				row = Ti.UI.createTableViewRow({bottom:'40%',height:"100dp",backgroundColor:"white",backgroundSelectedColor:"blue"});
				horario_inicio = Ti.UI.createLabel({text:db.servicios[i].hora_inicio,color:'#151431',left:'10%',top:'30%',font:{fontSize:18,fontWeight:'bold'}});
				vistaimg= Ti.UI.createView({width:65,height:65,borderRadius:30, top:'15%', bottom:15, left:'35%'});
				imgChofer = Ti.UI.createImageView({image:"http://subtmx.com/images/"+db.servicios[i].fotografia.replace(/\s/g, '%20'),width:65,height:97});
				lugar_origen = Ti.UI.createLabel({text:db.servicios[i].lugar_origen,color:'#151431',left:'55%',top:'23%',font:{fontSize:12,fontWeight:'bold'}});
				nombre_chofer = Ti.UI.createLabel({text:db.servicios[i].nombre_chofer,color:'#a2a1b8',left:'55%',top:'62%',font:{fontSize:13}});
				status = Ti.UI.createView({width:15,height:15,borderRadius:7,top:'34%', left:'2%'});
				if (db.servicios[i].status_servicio == 1) {
						status.backgroundColor = '#09c863';
				}else if (db.servicios[i].status_servicio == 0){
						status.backgroundColor = '#a00a1c';
				}
				
				vistaimg.add(imgChofer);
				row.add(status);
				row.add(vistaimg);
				row.add(horario_inicio);
				row.add(lugar_origen);
				row.add(nombre_chofer);
				tableData.push(row);
			}
			tableView = Ti.UI.createTableView({data:tableData,top:'0%',bottom:'17%',scrollable:true});
			centerView.add(tableView);
				
			tableView.addEventListener('delete',function(e){
				deleteServiciosReq.open("POST","http://subtmx.com/webservices/deleteservicios.php");
        			params = {
            			id_servicio:e.rowData.postIdServicio
        			};
        			deleteServiciosReq.send(params);
			});
		}
		
		deleteServiciosReq.onload = function(){
    			json = this.responseText;
    			response = JSON.parse(json);
    			if (response.logged == true){
    				
    			}else{
        			alert(response.message);
    			}
		};
		
		menu.addEventListener('click',function(){
			mainwin.toggleLeftView();
			mainwin.setCenterhiddenInteractivity("TouchEnabled");
		});
		
		fondoP.add(logo);
		centerView.add(fondoP);
		window.add(centerView);	
		getData();
		window.containingNav = navwin;
		window.setLeftNavButton(menu);
		return navwin;
	}
	
	mainwin = NappSlideMenu.createSlideMenuWindow({
		centerWindow: navwin,
		leftWindow:winLeft,
		statusBarStyle: NappSlideMenu.STATUSBAR_WHITE,
		leftLedge:100
	});
	
	winLeft.add(logoLeft);
	winLeft.add(lblHome);
	winLeft.add(lblMisPedidos);
	winLeft.add(lblMiPerfil);
	winLeft.add(lblPromociones);
	winLeft.add(lblPagos);
	winLeft.add(lblConfi);
	
	return mainwin;
};
module.exports = pedidos;