function inicio(){
	var window,logo,btnLogin,btnRegistrar,lblOmitir,navwin,login,winNav,registro,mapa,animateWin,bgVideo;
	
	window = Ti.UI.createWindow({navBarHidden:true});
	bgVideo = Titanium.Media.createVideoPlayer({top:0,autoplay:true,width:'100%',scalingMode:Titanium.Media.VIDEO_SCALING_MODE_FILL,url:'/images/videoHome.mp4',mediaControlStyle: Titanium.Media.VIDEO_CONTROL_HIDDEN,repeatMode:Titanium.Media.VIDEO_REPEAT_MODE_ONE});	
	logo = Titanium.UI.createImageView({image:'/images/logo.png',width:'35%',height:'auto',zIndex:2,center:0,top:'0%',opacity:0.1});
	btnLogin = Titanium.UI.createButton({height:'10%',width:'50%',left:0,title:'INICIAR SESIÓN',textAlign:'center',backgroundColor:'#3ca9cb',color:'white',bottom:0,zIndex:3});
	btnRegistrar = Titanium.UI.createButton({height:'10%',width:'50%',right:0,title:'REGÍSTRATE',textAlign:'center',backgroundColor:'#305caf',color:'white',bottom:0,zIndex:3});
	lblOmitir = Titanium.UI.createButton({title:'Omitir',top:'5%',right:15,color:'white',backgroundColor:'transparent',zIndex:3});
	navwin = Titanium.UI.iOS.createNavigationWindow({window: window});
	dialogIdioma = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Español','English'],message: 'Seleccione un idioma/Select a language',title: 'Idioma/Language'});
	
	window.addEventListener('open', function(e) {
		dialogIdioma.show();
		animateLogo = Titanium.UI.createAnimation();
		animateLogo.top = '14%';
		animateLogo.duration = 2000;
		animateLogo.opacity = 1;
		logo.animate(animateLogo);
		Titanium.App.Properties.removeProperty('idioma');
	});
	
	dialogIdioma.addEventListener('click',function(ed){
		if (ed.index === 1){
			btnLogin.title = 'LOG IN';
			btnRegistrar.title = 'SIGN IN';
			lblOmitir.title = 'Skip';
			Ti.App.Properties.setString("idioma", 'ingles');
		}
	});
	
	btnLogin.addEventListener('click', function(){
		login = require('/interface/login');
		winNav = new login('#53e3ff','Iniciar Sesión', navwin);
		winNav.containingNav = navwin;
		navwin.openWindow(winNav);
	});
	
	btnRegistrar.addEventListener('click', function(){
		registro = require('/interface/registro');
		winNav = new registro('#1288ec','Registrar', navwin);
		winNav.containingNav = navwin;
		navwin.openWindow(winNav);
	});
	
	lblOmitir.addEventListener('click', function(){
		bgVideo.release();
		mapa = require('/interface/mapa');
		winNav = new mapa(navwin);
		winNav.containingNav = navwin;
		navwin.openWindow(winNav);
	});
	
	window.containingNav = navwin;
	window.barColor = 'transparent';
	window.navTintColor = 'transparent';
	window.add(bgVideo);
	window.add(logo);
	window.add(lblOmitir);
	window.add(btnLogin);
	window.add(btnRegistrar);
	
	return navwin;
};
module.exports = inicio;