function chofer(latInicio,lngInicio,latDestino,lngDestino,geocoderlbl,idOpcionServicio,taxiRosa,idTipoCarro,nombreTipoCarro,numeroPasajero,geoDestino){
	var Map = require('ti.map');
	var NappSlideMenu = require('dk.napp.slidemenu');
	
	
	var window,lblModelo,lblPlaca,lblConductor,lblCostoTotal,lblLlamar,mapview,menu,navwin,winLeft,logoLeft,
		mainwin,lblMisPedidos,lblMiPerfil,lblPromociones,lblPagos,pago,winNav,horarioTarifa,tarifaInicial,
		my_timer,viewWeb,dialogAceptado,btnCanViaje,btnRecotizar,dialogFinalizado,dialogYallegue,dialogViajeiniciado,
		dialCalif,dialogChofer,dialogRecotizar;var final,final2;
		
	navwin = createCenterNavWindow();
	
	function createCenterNavWindow(){
		
		Ti.App.Properties.setString("carroSelect",nombreTipoCarro);
		Ti.App.Properties.setString("numPasajero",numeroPasajero);
		Ti.App.Properties.setInt("tipo_servicio",idTipoCarro);
		Ti.App.Properties.setInt("taxiRosaSelect",taxiRosa);
		Ti.App.Properties.setString("lat1",latInicio);
		Ti.App.Properties.setString("lon1",lngInicio);
		Ti.App.Properties.setString("lat2",latDestino);
		Ti.App.Properties.setString("lon2",lngDestino);
		Ti.App.Properties.setDouble("lat_in_recalcular",latInicio);
		Ti.App.Properties.setDouble("long_in_recalcular",lngInicio);
		Ti.App.Properties.setInt("BonoActual",Ti.App.Properties.getInt("bono"));
				
		tc = Ti.UI.createLabel({text:'Busqueda',color:'white'});
		window = Ti.UI.createWindow({backgroundColor:'white',navTintColor:'#007fff',titleControl:tc});
		navwin = Titanium.UI.iOS.createNavigationWindow({window: window});
		
		//variables locales
		var mapview;var annotations = [];var annotation;var locationAdded = false;var latitude,ti,idc;
		var longitude;var sdial=false;var sdial2=false;var sdial3=false;var sdial4=false;var webview;
		var servicioAceptado = 0;var tipo=1;var final = 0;var precio = 0;var yallegue = 0;
		var idevent = 8;var idch;var eventualidad = 0; var idEV=8;var costo_minutos =0;
		
		//temporizador
		viewPro= Ti.UI.createView({backgroundColor:'WHITE', widht:'100%',height:'100%',zIndex:10});
		logo = Titanium.UI.createImageView({image:'/images/logo.png',width:'25%',height:'auto',zIndex:2,center:0,top:'0%',opacity:0.1});
		track = Ti.UI.createView({ width: "80%", height: "2%", borderRadius:10, backgroundColor: '#c9ecfd',top:'50%'});
  		progress = Ti.UI.createView({ borderRadius:10, left: 0, width: '100%', height: "100%", backgroundColor : '#007fff'});
 		display_lbl =  Titanium.UI.createLabel({	text:"03:00",font:{fontSize:'65%'},top:'55%',center:0,color:'#c2c2c2 ',borderRadius:10,textAlign:'center'});
		buscando_lbl =  Titanium.UI.createLabel({text:"Buscando chofer...",font:{fontSize:'26%'},top:'40%',center:0,color:'#c2c2c2 ',	borderRadius:10,textAlign:'center'});
		idch =  Titanium.UI.createLabel({	text:"02:00",font:{fontSize:'65%'},top:'55%',center:0,color:'#c2c2c2 ',borderRadius:10,textAlign:'center'});
		
		//interfaz
		centerView =Ti.UI.createView({height: 'auto',right:0,top: 0,width:'100%',zIndex:2});
		viewWeb= Ti.UI.createView({backgroundColor:'WHITE'});  
		webview = Ti.UI.createWebView({url : '/firebase/index.html',width:0,height:0,top:0,left:0,visible:false});
		vistaMapa =Ti.UI.createView({height: '37%',right:0,bottom: 0,width:'100%',zIndex:6});
		btnCanViaje = Titanium.UI.createButton({title:'Cancelar Viaje',color:'white',backgroundColor:'#007fff', left:'2%',height:50,width:'35%',top:'52%',zIndex:10,enabled:true,borderRadius:7});
		btnRecotizar = Titanium.UI.createButton({title:'Recotizar Viaje',color:'white',backgroundColor:'#007fff', right:'2%',height:50,width:'35%',top:'52%',zIndex:10,enabled:true,borderRadius:7});
		player = Ti.Media.createSound({url:"/sound/sonido.mp3"});
		btnCall = Titanium.UI.createButton({backgroundImage:'/images/call.png',height:28,width:28,right:'2%',top:'41%',color:'#a2a1b8',zIndex:6});
		
		//webservices
		args = arguments[0] || {};url = "http://subtmx.com/webservices/choferprueba.php";db = {};
		pagoCan = Titanium.Network.createHTTPClient();
		pagoReq = Titanium.Network.createHTTPClient();
		pagoConeckta = Titanium.Network.createHTTPClient();
		registerServ = Titanium.Network.createHTTPClient();
		endServReq = Titanium.Network.createHTTPClient();var sa;
		pagoReq3 = Titanium.Network.createHTTPClient();
		reciboReq = Titanium.Network.createHTTPClient();
		
		//alertas
		dialogAceptado = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'],message: 'El chofer acepto el servicio, verifique la ficha',title: 'Ficha tecnica'});
		dialogYallegue = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'],message: 'Tu chofer ha llegado.',title: 'Mensaje'});
		dialogViajeiniciado = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'],message: 'Tu viaje inició, pongase comodo.',title: 'Viaje iniciado'});
		dialogCancelar = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar','Cancelar'],message: '¿Desea cancelar el viaje?',title: 'Cancelar viaje'});
		dialogFinalizado = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'],message: 'Tu viaje a finalizado.',title: 'Finalizar viaje'});
		dialCalif = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'],message: 'Pago realizado correctamenta, el monto de la transaccion fue: ',title: 'Pago realizado'});
		dialogChofer = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'],message: 'Chofer no encontrado. Favor de intentarlo nuevamente',title: 'Busqueda'});
		dialogRecotizar = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar','Cancelar'],message: '¿Desea recotizar el viaje?',title: 'Recotizar viaje'});var bbit, re;
		
		if(Ti.App.Properties.getInt("bono")==0){
			bbit=0;
		}else{
			bbit=1;
		}
		
						
		var countDown =  function( m , s, fn_tick, fn_end ){
			return {
				total_sec:m*60+s,
				timer:this.timer,
				set: function(m,s) {
					this.total_sec = parseInt(m)*60+parseInt(s);
					this.time = {m:m,s:s};
					return this;
				},
				start: function() {
					var self1 = this;
					this.timer = setInterval( function() {
						if (self1.total_sec) {
							self1.total_sec--;
							self1.time = { m : parseInt(self1.total_sec/60), s: (self1.total_sec%60) };
							fn_tick();
						}else {
							self1.stop();
							fn_end();
						}
		   			}, 1000 );
					return this;
				},
				stop: function() {
					clearInterval(this.timer);
					this.time = {m:0,s:0};
					this.total_sec = 0;
					return this;
				}
			};
		};
		
		my_timer = new countDown(03, 00, function() {
		if (my_timer.time.m == 0 && my_timer.time.s == 0) {
			my_timer.stop();
			dialogChofer.show();
			display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
		}else if (my_timer.time.m == 2 && my_timer.time.s == 55) {
			viewWeb.add(webview);
			if (my_timer.time.s < 10) {
				display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
			} else {
				display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
			}
		}else if ((my_timer.time.m <= 3) && (my_timer.time.m == 2 && my_timer.time.s >= 10)) {
			if (sa == 1) {
				my_timer.stop();
				getData();
			}
			if (my_timer.time.s < 10) {
				display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
			} else {
				display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
			}
		}else if (my_timer.time.m == 2 && my_timer.time.s == 9) {
			if (sa == 0) {
				Ti.App.fireEvent('fbController:noAceptado', {
					id_chofer : idc
				});
        		idc = "";
			}
			if (my_timer.time.s < 10) {
				display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
			} else {
				display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
			}
		} else if (my_timer.time.m == 2 && my_timer.time.s == 8) {
			viewWeb.remove(webview);
			mapview.removeAllAnnotations();
			if (my_timer.time.s < 10) {
				display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
			}else {
				display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
			}
		}else if ((my_timer.time.m == 2 && my_timer.time.s >= 4) && (my_timer.time.m == 2 && my_timer.time.s < 8)) {
			if (my_timer.time.s < 10) {
				display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
			} else {
				display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
			}
		}else if (my_timer.time.m == 2 && my_timer.time.s == 3) {
			viewWeb.add(webview);
			if (my_timer.time.s < 10) {
				display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
			} else {
				display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
			}
		} else if (  (my_timer.time.m == 2 && my_timer.time.s < 3 && my_timer.time.m == 2 && my_timer.time.s >=0) || (my_timer.time.m == 1 && my_timer.time.s >= 17 && my_timer.time.m == 1 && my_timer.time.s <= 59)) {
			if (sa == 1) {
				my_timer.stop();
				getData();
			}
			if (my_timer.time.s < 10) {
				display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
			} else {
				display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
			}
		}else if (my_timer.time.m == 1 && my_timer.time.s == 16) {
			if (sa == 0) {
				Ti.App.fireEvent('fbController:noAceptado', {
					id_chofer : idc
				});
        		idc = "";
			}
			if (my_timer.time.s < 10) {
				display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
			} else {
				display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
			}
		}else if (my_timer.time.m == 1 && my_timer.time.s == 15) {
			viewWeb.remove(webview);
			mapview.removeAllAnnotations();
			if (my_timer.time.s < 10) {
				display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
			} else {
				display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
			}
		}else if ((my_timer.time.m == 1 && my_timer.time.s >= 11) && (my_timer.time.m == 1 && my_timer.time.s < 15)) {
			if (my_timer.time.s < 10) {
				display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
			} else {
				display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
			}
		}else if (my_timer.time.m == 1 && my_timer.time.s == 10) {
			viewWeb.add(webview);
			if (my_timer.time.s < 10) {
				display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
			} else {
				display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
			}
		} else if ((my_timer.time.m == 0 && my_timer.time.s >=34 && my_timer.time.m == 0 && my_timer.time.s <=59) || (my_timer.time.m == 1 && my_timer.time.s <10 && my_timer.time.m == 1 && my_timer.time.s >=0)) {
			if (sa == 1) {
				my_timer.stop();
				getData();
			}
			if (my_timer.time.s < 10) {
				display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
			}else {
				display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
			}
		}else if (my_timer.time.m == 0 && my_timer.time.s == 33) {
			if (sa == 0) {
				Ti.App.fireEvent('fbController:noAceptado', {
					id_chofer : idc
				});
        		idc = "";
			}
			if (my_timer.time.s < 10) {
				display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
			}else {
				display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
			}
		} else if (my_timer.time.m == 0 && my_timer.time.s == 32) {
			viewWeb.remove(webview);
			mapview.removeAllAnnotations();
			if (my_timer.time.s < 10) {
				display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
			} else {
				display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
			}
		}else if ((my_timer.time.m == 0 && my_timer.time.s >= 30) && (my_timer.time.m == 0 && my_timer.time.s < 32)) {
			if (my_timer.time.s < 10) {
				display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
			} else {
				display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
			}
		}else if (my_timer.time.m == 0 && my_timer.time.s == 29) {
			window.add(webview);
			if (my_timer.time.s < 10) {
				display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
			} else {
				display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
			}
		}else if ((my_timer.time.m == 0 && my_timer.time.s >=3 ) && (my_timer.time.m == 0 && my_timer.time.s < 29)) {
			if (sa == 1) {
				my_timer.stop();
				getData();
			}
			if (my_timer.time.s < 10) {
				display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
			} else {
				display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
			}
		}else if (my_timer.time.m == 0 && my_timer.time.s == 2) {
			if (sa == 0) {
				Ti.App.fireEvent('fbController:noAceptado', {
					id_chofer : idc
				});
        		idc = "";
			}
			if (my_timer.time.s < 10) {
				display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
			} else {
				display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
			}
		}else if (my_timer.time.m == 0 && my_timer.time.s == 1) {
			viewWeb.remove(webview);
			mapview.removeAllAnnotations();
			if (my_timer.time.s < 10) {
				display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
			} else {
				display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
			}
		}
	}, function() {
	}); 
		
		dialogChofer.addEventListener('click', function(ed){
			viewWeb=null;
            window.remove(viewWeb);
			home= require('/interface/mapa');
        	new home().open();
        	window.close();
		});
		
		Ti.Geolocation.getCurrentPosition(function(e) {
			mapview = Map.createView({mapType : Map.NORMAL_TYPE,
				region : {latitude:e.coords.latitude,longitude:e.coords.longitude,latitudeDelta:0.009,longitudeDelta:0.009,userLocation:true,visible:true},
				animate : true,
				regionFit : true,
				userLocation : true
			});
			vistaMapa.add(mapview);
			if (!e.success || e.error) {
				Ti.API.info('error:' + JSON.stringify(e.error));
				return;
			}	
		});
		
		//Eventos
	Ti.Geolocation.getCurrentPosition(function(e) {
		if (Ti.App.Properties.hasProperty('viajeactivo')) {
			viewWeb.add(webview);
			webview.addEventListener('load', function() {
				Ti.App.fireEvent('firebase:init', {
					latitude : e.coords.latitude,
					longitude : e.coords.longitude,
					id_chofer:Ti.App.Properties.getInt('id_ChoferA'),
					latInicio:latInicio,
					lngInicio:lngInicio,
					latDestino:latDestino,
					lngDestino:lngDestino,
					idTipoCarro:idTipoCarro,
					taxiRosa:taxiRosa,
					viajeactivo:0,
					telefono:Ti.App.Properties.getString('telefono'),
					id_usuario:Ti.App.Properties.getInt('id_usuario_chofer'),
					conductor:Ti.App.Properties.getString("conductor"),
					id_user:Ti.App.Properties.getInt('id_usuario'),
					radiofirebase:Ti.App.Properties.getInt('radiofirebase')
				});
			});
			sdial = true;
			servicioAceptado = 1;
			idc = Ti.App.Properties.getInt("id_chofer");
			if(Ti.App.Properties.getInt("vistaActiva")==1){
				Ti.App.fireEvent('fbController:nuevaRuta', {
					id_chofer:Ti.App.Properties.getInt("id_chofer")
				});
					
				if(Ti.App.Properties.getInt('yallegueRec') == 1){
					sdial2 = true;
				}
				if(Ti.App.Properties.getInt('viajeiniciadoRec') == 1){
					sdial4 = true;
				}
			};
		}else{
			webview.addEventListener('load', function() {
				Ti.App.fireEvent('firebase:init', {
					latitude : latInicio,
					longitude :lngInicio,
					id_chofer:0,
					latInicio:latInicio,
					lngInicio:lngInicio,
					latDestino:latDestino,
					lngDestino:lngDestino,
					idTipoCarro:idTipoCarro,
					taxiRosa:taxiRosa,
					telefono:Ti.App.Properties.getString('telefono'),
					viajeactivo:1,
					nombreM:Ti.App.Properties.getString("NMujer"),
					id_user:Ti.App.Properties.getInt('id_usuario'),
					radiofirebase:Ti.App.Properties.getInt('radiofirebase')
				});
			});
		}
	});
		
		Ti.App.addEventListener('fbController:addMarker', function(e){
			annotations[e.id] = Map.createAnnotation({
				driverId : e.id,
				latitude : e.latitude,
				longitude : e.longitude,
				image : '/images/autochofer.png',
				title : 'Chofer: '+ e.nombre
			});
			mapview.addAnnotation(annotations[e.id]);
			mapview.region = {latitude:e.latitude, longitude:e.longitude, latitudeDelta:0.009, longitudeDelta:0.009};
			idc = e.id;Ti.App.Properties.setInt('id_usuario_chofer',e.id_usuario);Ti.App.Properties.setString('nombrechofer',e.nombre);
		});
		
		Ti.App.addEventListener('fbController:moveMarker2', function(est){
		if (est.finalizarviaje == 1) {
   			if (sdial3 == false) {
   				sdial3 = true;
   				Titanium.Media.audioSessionCategory = Ti.Media.AUDIO_SESSION_CATEGORY_PLAYBACK;
   				notification = Ti.App.iOS.scheduleLocalNotification({
    					alertAction: "ver",
    					alertBody: dialogFinalizado.message,
    					badge: 1,
    					date: new Date(new Date().getTime() + 3000),
    					sound: "/sound/sonido.mp3",
    					userInfo: {}
				}); 
   				dialogFinalizado.show();	
   				player.play();
   			}
   		}else if (est.finalizarviaje == 0){
			sdial3 = false;
		}
		if (est.eventualidad != 0) {
			eventualidad=1000;
			idEV=est.idEV;
			costo_minutos = est.costo_minutos;	
		}else if (est.eventualidad == 0) {
			eventualidad=0;
			idEV=est.idEV;
			costo_minutos = est.costo_minutos;
		}
	});
	
	Ti.App.addEventListener('fbController:moveMarker', function(est){
		annotations[est.id].setLatitude(est.latitude);
		annotations[est.id].setLongitude(est.longitude);
		mapview.setRegion({latitude:est.latitude,longitude:est.longitude,latitudeDelta:0.003, longitudeDelta:0.003});
		id_usuario = est.id_usuario;
		if (est.servicioaceptado == 1) {
			sa=1; 	
		}else if (est.servicioaceptado == 0){
			sdial = false;
			sa=0;
		}
		if (est.yallegue == 1) {
			if (sdial2 == false) {
   				sdial2 = true;
   				Titanium.Media.audioSessionCategory = Ti.Media.AUDIO_SESSION_CATEGORY_PLAYBACK;
   				notification = Ti.App.iOS.scheduleLocalNotification({
    					alertAction: "ver",
    					alertBody: dialogYallegue.message,
    					badge: 1,
    					date: new Date(new Date().getTime() + 3000),
    					sound: "/sound/sonido.mp3",
    					userInfo: {}
				}); 
   				dialogYallegue.show();
   				player.play();
   			}
		}else if (est.yallegue == 0){
			sdial2 = false;
		}
		if (est.viajeiniciado == 1) {
   			if (sdial4 == false) {
   				sdial4 = true;
   				Titanium.Media.audioSessionCategory = Ti.Media.AUDIO_SESSION_CATEGORY_PLAYBACK;
   				notification = Ti.App.iOS.scheduleLocalNotification({
    					alertAction: "ver",
    					alertBody: dialogViajeiniciado.message,
    					badge: 1,
    					date: new Date(new Date().getTime() + 3000),
    					sound: "/sound/sonido.mp3",
    					userInfo: {}
				}); 
   				dialogViajeiniciado.show();
   				player.play();
   			}
   		}else if (est.viajeiniciado == 0){
			sdial4 = false;
		}
	});
		
		dialogYallegue.addEventListener('click', function(ed){
			sdial2 = true;
			player.stop();
			Ti.App.Properties.setInt('yallegueRec',1);
			Titanium.App.Properties.removeProperty('yallegue');
			Ti.App.Properties.setString('viajeiniciado',0);
		});
		
		dialogViajeiniciado.addEventListener('click', function(ed){
			sdial4 = true;
			player.stop();
			Ti.App.Properties.setInt('viajeiniciadoRec',1);
			Titanium.App.Properties.removeProperty('viajeiniciado');
			Ti.App.Properties.setString('finalizar',0);
		});
		
		dialogAceptado.addEventListener('click', function(ed){
			my_timer.stop();
			Ti.App.fireEvent('fbController:id_user', {
				id_chofer:idch.text,
				id_user:Ti.App.Properties.getInt('id_usuario')
			});
			sdial = true;player.stop();var tipo=1; 
			Ti.App.Properties.setString('viajeactivo',0);
			Ti.App.Properties.setString('yallegue',0);
			Ti.App.Properties.setString('latInicio',latInicio);
			Ti.App.Properties.setString('lngInicio',lngInicio);
			Ti.App.Properties.setString('latDestino',latDestino);
			Ti.App.Properties.setString('lngDestino',lngDestino);
			Ti.App.Properties.setString('geocoderlbl',geocoderlbl);
			Ti.App.Properties.setString('idOpcionServicio',idOpcionServicio);
			Ti.App.Properties.setString('taxiRosa',taxiRosa);
			Ti.App.Properties.setString('idTipoCarro',idTipoCarro);
			Ti.App.Properties.setString('nombreTipoCarro',nombreTipoCarro);
			Ti.App.Properties.setString('numeroPasajero',numeroPasajero);
			Ti.App.Properties.setString('geoDestino',geoDestino);
		});
		
		btnCanViaje.addEventListener('click', function(e){
  			dialogCancelar.show();
   		});
   		
   		dialogCancelar.addEventListener('click',function(ed){
   			if (ed.index == 0) {
   				if (yallegue == 0) {
   					//Ti.App.Properties.setDouble("ctD",0.00);
   				}else if (yallegue == 1) {
   					//Ti.App.Properties.setDouble("ctD",0.00);
   				}
   				Ti.App.fireEvent('fbController:viajecancelado', {
					id_chofer:idc
				});
				dialogFinalizado.show();	
   			}
   		});
   		
   		btnRecotizar.addEventListener('click', function(e){
  		 	dialogRecotizar.show();
   		});
   		
   		dialogRecotizar.addEventListener('click',function(ed){
   			Titanium.Geolocation.getCurrentPosition(function(e){
				if (e.error){
					alert('No se puede obtener ubicacion actual');
					return;
				}
				Ti.App.Properties.setDouble("latActual",e.coords.latitude);
        		Ti.App.Properties.setDouble("longActual",e.coords.longitude);

        		recotizar = require('/interface/recotizar');
    			new recotizar().open();
   				window.close();
		 	});
   		});
   		var cfin = 0;
   		
   		dialogFinalizado.addEventListener('click', function(ed){
			if(Ti.App.Properties.hasProperty('re')){
				Ti.App.Properties.setDouble("re",(Ti.App.Properties.getDouble("re")+parseFloat(costo_minutos)+parseFloat(eventualidad)));
			}else{
				Ti.App.Properties.setDouble("re",(parseFloat(costo_minutos)+parseFloat(eventualidad)));
			}
			
			sdial3 = true;var a=0;var b =0;var c=0;
			player.stop();
			Ti.App.fireEvent('fbController:finalizarviaje', { 
				id_chofer:idc,
				id_userF:Ti.App.Properties.getInt("id_usuario")
			});

			if(Ti.App.Properties.getDouble("re")==0){
            	endServReq.open("POST","http://subtmx.com/webservices/endServicios.php");
               	params = {
                	id_servicio: Ti.App.Properties.getInt("id_servicio"),
                	km_adicional: Ti.App.Properties.getDouble("kmAdicional"),
                	costo_km_adicional:Ti.App.Properties.getDouble("kmCostoAdicional"),
                	hora_extra: "00:00:00",
                	costo_hora_extra: 0,
                	cargo_hora_pico: 0,
                	tiempo_espera_adicional:"00:00:00",
                	costo_espera_adicional: 0,
                	costo_total:Ti.App.Properties.getDouble("pagoInicial"),
            		id_eventualidad:idEV,
            		 costo_pendiente:0,
                     status_promocion:bbit
            	};
           		endServReq.send(params);
           		
           		Titanium.App.Properties.removeProperty('viajeactivo');
        		Titanium.App.Properties.removeProperty('finalizar');
        		Titanium.App.Properties.removeProperty('latInicio');
        		Titanium.App.Properties.removeProperty('lngInicio');
        		Titanium.App.Properties.removeProperty('latDestino');
        		Titanium.App.Properties.removeProperty('lngDestino');
        		Titanium.App.Properties.removeProperty('geocoderlbl');
        		Titanium.App.Properties.removeProperty('idOpcionServicio');
        		Titanium.App.Properties.removeProperty('taxiRosa');
        		Titanium.App.Properties.removeProperty('idTipoCarro');
        		Titanium.App.Properties.removeProperty('nombreTipoCarro');
        		Titanium.App.Properties.removeProperty('numeroPasajero');
        		Titanium.App.Properties.removeProperty('geoDestino');
        		Titanium.App.Properties.removeProperty('re');
            	viewWeb=null;
            	window.remove(viewWeb);
            	calificar = require('/interface/calificar');
                new calificar().open();
                window.close();
			}else{
				if (Ti.App.Properties.hasProperty('coneckta')) {
							pagoReq3.open("POST","http://subtmx.com/webservices/php-conekta-master/conekta_card.php");
            				params = {
                				id_usuario: Ti.App.Properties.getInt('id_usuario'),
                				id_servicio: Ti.App.Properties.getInt("id_servicio"),
                				descripcion_viaje: 'Tipo de viaje es: ' + nombreTipoCarro + ', Km recorridos: ' + Ti.App.Properties.getString('kmt'),
                				costo_total: Ti.App.Properties.getDouble("re").toFixed(2),
            				};
           					pagoReq3.send(params);
						}else if (Ti.App.Properties.hasProperty('paypal')){
							pagoReq3.open("POST","http://subtmx.com/webservices/paypal/samples/BillingAgreements/DoReferenceTransaction.php");
            				params = {
            					destino:geoDestino,
                				id_biling:Ti.App.Properties.getString("id_biling"),
                				name: Ti.App.Properties.getString('nombre'),
                				lastname: Ti.App.Properties.getString('apellido'),
                				id_usuario: Ti.App.Properties.getInt('id_usuario'),
                				id_servicio: Ti.App.Properties.getInt("id_servicio"),
                				descripcion_viaje: 'Tipo de viaje es: ' + nombreTipoCarro + ', Km recorridos: ' + Ti.App.Properties.getString('kmt'),
                				costo_total: Ti.App.Properties.getDouble("re").toFixed(2),
            				};
           					pagoReq3.send(params);
						}
			}
   		});
   		
   		pagoReq3.onload = function(){
           json = this.responseText;
           response = JSON.parse(json);
           if (response.logged == true){
           		reciboReq.open("POST","http://subtmx.com/webservices/recibo.php");
                    params4 = {
                    	id_servicio: Ti.App.Properties.getInt("id_servicio"),
                        id_usuario: Ti.App.Properties.getInt('id_usuario'),
                        destino:geoDestino
                    };
       			reciboReq.send(params4);
               endServReq.open("POST","http://subtmx.com/webservices/endServicios.php");
               	params = {
                	id_servicio: Ti.App.Properties.getInt("id_servicio"),
                	km_adicional: Ti.App.Properties.getDouble("kmAdicional"),
                	costo_km_adicional:Ti.App.Properties.getDouble("kmCostoAdicional"),
                	hora_extra: "00:00:00",
                	costo_hora_extra: 0,
                	cargo_hora_pico: 0,
                	tiempo_espera_adicional:"00:00:00",
                	costo_espera_adicional: 0,
                	costo_total: Ti.App.Properties.getDouble("pagoInicial"),
            		id_eventualidad:idEV,
            		costo_pendiente:0,
                    status_promocion:bbit
            	};
           		endServReq.send(params);
           		
           		Titanium.App.Properties.removeProperty('viajeactivo');
        		Titanium.App.Properties.removeProperty('finalizar');
        		Titanium.App.Properties.removeProperty('latInicio');
        		Titanium.App.Properties.removeProperty('lngInicio');
        		Titanium.App.Properties.removeProperty('latDestino');
        		Titanium.App.Properties.removeProperty('lngDestino');
        		Titanium.App.Properties.removeProperty('geocoderlbl');
        		Titanium.App.Properties.removeProperty('idOpcionServicio');
        		Titanium.App.Properties.removeProperty('taxiRosa');
        		Titanium.App.Properties.removeProperty('idTipoCarro');
        		Titanium.App.Properties.removeProperty('nombreTipoCarro');
        		Titanium.App.Properties.removeProperty('numeroPasajero');
        		Titanium.App.Properties.removeProperty('geoDestino');
        		Titanium.App.Properties.removeProperty("re");
            	//removeChildrens(viewWeb);
            	viewWeb=null;
            	window.remove(viewWeb);
            	dialCalif.message = 'Pago realizado correctamente.';
            	dialCalif.show();
              }else{
              	Titanium.App.Properties.removeProperty("re");
               Titanium.App.Properties.removeProperty('viajeactivo');
        		Titanium.App.Properties.removeProperty('finalizar');
        		Titanium.App.Properties.removeProperty('latInicio');
        		Titanium.App.Properties.removeProperty('lngInicio');
        		Titanium.App.Properties.removeProperty('latDestino');
        		Titanium.App.Properties.removeProperty('lngDestino');
        		Titanium.App.Properties.removeProperty('geocoderlbl');
        		Titanium.App.Properties.removeProperty('idOpcionServicio');
        		Titanium.App.Properties.removeProperty('taxiRosa');
        		Titanium.App.Properties.removeProperty('idTipoCarro');
        		Titanium.App.Properties.removeProperty('nombreTipoCarro');
        		Titanium.App.Properties.removeProperty('numeroPasajero');
        		Titanium.App.Properties.removeProperty('geoDestino');
        		viewWeb=null;
            	window.remove(viewWeb);
               	dialCalif.message = 'No cuenta con saldo suficiente, verifique su metodo de pago';
				dialCalif.title = 'Pago no realizado';
            	dialCalif.show();    
           }
   };
		
		dialCalif.addEventListener('click', function(ed){ 
    		if (ed.index === 0){
    			calificar = require('/interface/calificar');
                new calificar().open();
                window.close();
    		}
    	});
		
		function removeChildrens(objeto) {
    		for (i in objeto.children) {
        		var child=objeto.children[0];
        		removeChildrens(child);
        		objeto.remove(child);
        		child=null;
    		}
		}
		
		function getData(){
			var xhr = Ti.Network.createHTTPClient({
				onload: function() {
					db = JSON.parse(this.responseText);
					if(db.servicios){setData(db);}
				},
				onerror: function(e) {
					alert('Comprueba tu conexion a internet');
				},
				timeout:5000
			});	
			xhr.open('POST', url);
			if(Ti.App.Properties.getInt("vistaActiva")==1 || Ti.App.Properties.hasProperty('viajeactivo')){
				idc=Ti.App.Properties.getInt("id_ChoferA");
			}
			var params = {id_chofer:idc};
			if(sa != 0){xhr.send(params);}
		}
		function setData(db){
			for(var i = 0; i < db.servicios.length; i++){
				var kmformateado=Ti.App.Properties.getString("distancias").split(',');
                var kmWeb = kmformateado[0] + '.' + kmformateado[1];
                var dus= parseFloat(kmWeb) + Ti.App.Properties.getDouble("Extra2");
				choferactivo =db.servicios[i].id_chofer;
				idch.text = db.servicios[i].id_chofer;
		 		if (choferactivo != null){
		 			
		 			Ti.App.Properties.setString("kmt",dus);
					Ti.App.Properties.setInt("id_chofer",db.servicios[i].id_chofer);
					Ti.App.Properties.setInt("id_ChoferA",choferactivo);
					Ti.App.Properties.setString("id_choferS",db.servicios[i].id_chofer);
					
					viewP = Titanium.UI.createView({top:70, width:'100%',height:'auto',backgroundColor:'white',zIndex:3});
					ImgPerfil= Ti.UI.createImageView({width:112,height:112,borderRadius:55,top:'6%',center:0,image:"http://subtmx.com/images/"+db.servicios[i].fotografia.replace(/\s/g, '%20'),zIndex:4});
		
					//labels nombres
					modelolbl = Ti.UI.createLabel({text:'Modelo:',left:'2%',top:'10%',color:'black'});
					placaslbl = Ti.UI.createLabel({text:'Placas:',left:'2%',top:'16%',color:'black'});
					conductorlbl = Ti.UI.createLabel({text:'Conductor:',left:'2%',top:'22%',color:'black'});
					costoTOTAL = Ti.UI.createLabel({text:'Costo total:',left:'2%',top:'28%',visible:true,color:'black'});
					lblCall = Ti.UI.createLabel({text:'Llamar:',left:'2%',top:'34%',color:'black'});
					Ti.App.Properties.setString("telefonochofer",db.servicios[i].num_telefono_chofer);
				
					//labels valores
					modelo = Ti.UI.createLabel({text:db.servicios[i].modelo,color:'#a2a1b8',right:'2%',top:'10%',font:{fontSize:14}});
					placas = Ti.UI.createLabel({text:db.servicios[i].placa,color:'#a2a1b8',right:'2%',top:'16%',font:{fontSize:14}});
					conductor = Ti.UI.createLabel({text:db.servicios[i].nombre_chofer,color:'#a2a1b8',right:'2%',top:'22%',font:{fontSize:14}});
					lblTotal = Ti.UI.createLabel({right:'2%', top:'28%',color:'#4ad34d',font:{fontSize:14},visible:true});
					
					Ti.App.Properties.setString("conductor",conductor.text);
					
					//calculo de costos
					var nuevoKMT;var newCosto;var tarifaFinal,newkmad;
					if (idOpcionServicio == 1) {
						if (dus >db.servicios[i].km_inicial) {
							//alert('jajs');
							newkmad=dus-db.servicios[i].km_inicial;
							newCosto = newkmad * db.servicios[i].precio_km_adicional;
							tarifaFinal=newCosto+db.servicios[i].tarifa_inicial*1;
							lblTotal.text = '$' + tarifaFinal.toFixed(2) +' MXN';
					
							Ti.App.Properties.setDouble("kmAdicional",newkmad);
							Ti.App.Properties.setDouble("kmCostoAdicional",newCosto);
							Ti.App.Properties.setDouble("km_inicial",db.servicios[i].km_inicial);
							Ti.App.Properties.setDouble("precio_km_adicional",db.servicios[i].precio_km_adicional);
					
							Ti.App.Properties.setString("ta",newCosto);
							Ti.App.Properties.setDouble("ct",parseFloat(tarifaFinal).toFixed(2));
							Ti.App.Properties.setDouble("ctD",parseFloat(tarifaFinal).toFixed(2));
					
						}else if (dus <= db.servicios[i].km_inicial){
							Ti.App.Properties.setDouble("kmAdicional",0);
							Ti.App.Properties.setString("ta",0);
							tarifaFinal=db.servicios[i].tarifa_inicial;
							lblTotal.text = '$' + tarifaFinal +' MXN';
							Ti.App.Properties.setDouble("ctD",parseFloat(tarifaFinal).toFixed(2));
							Ti.App.Properties.setDouble("ct",parseFloat(tarifaFinal).toFixed(2));
							Ti.App.Properties.setDouble("kmCostoAdicional",0);
						}
						Ti.App.Properties.setDouble("tarifa_inicial",db.servicios[i].tarifa_inicial);
					}else{
						lblTotal.visible = true;
                    	costoTOTAL.visible = true;    
                    	tarifaFinal=response.tarifa_hora * Ti.App.Properties.getInt("txtserviciohora");;
                    	lblTotal.text='$' + tarifaFinal.toFixed(2) +' MXN';
                    	Ti.App.Properties.setDouble("ctD",tarifaFinal.toFixed(2));
                    	Ti.App.Properties.setDouble("kmAdicional",0);
                    	Ti.App.Properties.setDouble("kmCostoAdicional",0);
					}
					
		 			/*endServReq.open("POST","http://subtmx.com/webservices/endServicios.php");
                	params = {
                		id_servicio: Ti.App.Properties.getInt("id_servicio"),
                    	km_adicional: Ti.App.Properties.getDouble("kmAdicional"),
                    	costo_km_adicional:Ti.App.Properties.getDouble("kmCostoAdicional"),
                    	hora_extra: "00:00:00",
                    	costo_hora_extra: 0,
                    	cargo_hora_pico: 0,
                    	tiempo_espera_adicional:"00:00:00",
                    	costo_espera_adicional: 0,
                    	costo_total: Ti.App.Properties.getDouble("pagoInicial"),
                    	id_eventualidad:idEV,
                    	costo_pendiente:Ti.App.Properties.getDouble("re").toFixed(2),
                    	status_promocion:bbit
             		};
      				endServReq.send(params);*/
					
					centerView.add(ImgPerfil);
					viewP.add(modelolbl);
					viewP.add(placaslbl);
					viewP.add(conductorlbl);
					viewP.add(costoTOTAL);
					viewP.add(lblCall);
					viewP.add(modelo);
					viewP.add(placas);
					viewP.add(conductor);
					viewP.add(lblTotal);
					
					centerView.add(viewP);
            		
            		if (Ti.App.Properties.hasProperty('viajeactivo')) {
            			if(Ti.App.Properties.getInt("vistaActiva")==1) {
            				re=Ti.App.Properties.getDouble('ctD')-(parseFloat(Ti.App.Properties.getInt("bono")))-Ti.App.Properties.getDouble("pagoInicial");
							if(re<=0){
								re=0;
							}
		 					Ti.App.Properties.setDouble("re",re.toFixed(2));
		 					
		 					endServReq.open("POST","http://subtmx.com/webservices/endServicios.php");
                			params = {
                				id_servicio: Ti.App.Properties.getInt("id_servicio"),
                    			km_adicional: Ti.App.Properties.getDouble("kmAdicional"),
                    			costo_km_adicional:Ti.App.Properties.getDouble("kmCostoAdicional"),
                    			hora_extra: "00:00:00",
                    			costo_hora_extra: 0,
                    			cargo_hora_pico: 0,
                    			tiempo_espera_adicional:"00:00:00",
                    			costo_espera_adicional: 0,
                    			costo_total: Ti.App.Properties.getDouble("pagoInicial"),
                    			id_eventualidad:idEV,
                    			costo_pendiente:re.toFixed(2),
                    			status_promocion:bbit
             			};
      					endServReq.send(params);
            		}
            	}else{
            		var pagoinicial=Ti.App.Properties.getDouble('ctD')-(parseFloat(Ti.App.Properties.getInt("bono")));
            		if (pagoinicial<=0){
            			pagoinicial=0;
            		}
            		
            		Ti.App.Properties.setDouble("apagar",Ti.App.Properties.getDouble('ctD'));
            		Ti.App.Properties.setDouble("pagoInicial",pagoinicial.toFixed(2));
            			if (pagoinicial<=0){
            				viewPro.visible=false;
            				dialogAceptado.show();
            				player.play();
            				Ti.App.Properties.setString('viajeactivo',0);
            				registerServ.open("POST","http://subtmx.com//webservices/createservice.php");
                			params = {
                				id_tipo_servicio:idTipoCarro,
                    			id_usuario:Ti.App.Properties.getInt("id_usuario"),
                    			id_chofer:Ti.App.Properties.getInt("id_chofer"),
                    			latitud_origen:latInicio,
                    			longitud_origen:lngInicio,
                    			latitud_destino:latDestino,
                    			longitud_destino:lngDestino,
                    			lugar_origen:geocoderlbl,
                    			opcion_servicio:idOpcionServicio,
                    			tarifa_inicial:Ti.App.Properties.getDouble("tarifa_inicial"),
                    			id_tipo_pago:tipo,
                    			destino:geoDestino,
                    			costo_total:Ti.App.Properties.getDouble("ctD").toFixed(2),
                    			status_promocion:bbit
              				};
                			registerServ.send(params);
            				Ti.App.fireEvent('fbController:saldoinsuficiente', {
                				id_chofer : Ti.App.Properties.getInt("id_chofer"),
                				saldo : 0
            				});
            			}else{
            				if (Ti.App.Properties.hasProperty('coneckta')) {
								pagoReq.open("POST","http://subtmx.com/webservices/php-conekta-master/conekta_card.php");
            					params = {
                					id_usuario: Ti.App.Properties.getInt('id_usuario'),
                					id_servicio: Ti.App.Properties.getInt("id_servicio"),
                					descripcion_viaje: 'Tipo de viaje es: ' + nombreTipoCarro + ', Km recorridos: ' + Ti.App.Properties.getString('kmt'),
                					costo_total: parseFloat(pagoinicial).toFixed(2)
            					};
           						pagoReq.send(params);
							}else if (Ti.App.Properties.hasProperty('paypal')){
								pagoReq.open("POST","http://subtmx.com/webservices/paypal/samples/BillingAgreements/DoReferenceTransaction.php");
            					params = {
            						destino:geoDestino,
                					id_biling:Ti.App.Properties.getString("id_biling"),
                					name: Ti.App.Properties.getString('nombre'),
                					lastname: Ti.App.Properties.getString('apellido'),
                					id_usuario: Ti.App.Properties.getInt('id_usuario'),
                					id_servicio: Ti.App.Properties.getInt("id_servicio"),
                					descripcion_viaje: 'Tipo de viaje es: ' + nombreTipoCarro + ', Km recorridos: ' + Ti.App.Properties.getString('kmt'),
                					costo_total: parseFloat(pagoinicial).toFixed(2)
            					};
           						pagoReq.send(params);
							}
            			}
            		}
		 		}
			}
		}
		
		btnCall.addEventListener('click', function() {
 						var dialog = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Confirmar', 'Cancelar'],message: 'Desea usted realizar esta llamada?',title: 'Llamar'});
						dialog.addEventListener('click', function(ed){
							if (ed.index === 0){
								Ti.Platform.openURL('tel:'+ Ti.App.Properties.getString('telefonochofer'));
							}
						});
						dialog.show();
					});
		
		pagoReq.onload = function(){
   			json = this.responseText;
            response = JSON.parse(json);
            if (response.logged == true){
            	viewPro.visible=false;
            	dialogAceptado.show();
            	player.play();
            	Ti.App.Properties.setString('viajeactivo',0);
            	registerServ.open("POST","http://subtmx.com//webservices/createservice.php");
                params = {
                	id_tipo_servicio:idTipoCarro,
                    id_usuario:Ti.App.Properties.getInt("id_usuario"),
                    id_chofer:Ti.App.Properties.getInt("id_chofer"),
                    latitud_origen:latInicio,
                    longitud_origen:lngInicio,
                    latitud_destino:latDestino,
                    longitud_destino:lngDestino,
                    lugar_origen:geocoderlbl,
                    opcion_servicio:idOpcionServicio,
                    tarifa_inicial:Ti.App.Properties.getDouble("tarifa_inicial"),
                    id_tipo_pago:tipo,
                    destino:geoDestino,
                    costo_total:Ti.App.Properties.getDouble("ctD").toFixed(2),
                    status_promocion:bbit
              	};
                registerServ.send(params);
            	Ti.App.fireEvent('fbController:saldoinsuficiente', {
                	id_chofer : Ti.App.Properties.getInt("id_chofer"),
                	saldo : 0
            	});
            }else{
            	Ti.App.fireEvent('fbController:saldoinsuficiente', {
                	id_chofer : Ti.App.Properties.getInt("id_chofer"),
                	saldo : 1
            	});
				dialogChofer.message = 'No cuenta con saldo suficiente, verifique su metodo de pago';
				dialogChofer.title = 'Pago no realizado';
            	dialogChofer.show();
            }
   		};
   		
   		registerServ.onload = function(){
			json = this.responseText;
    		response = JSON.parse(json);
  			if (response.logged== true){
       	    	Ti.App.Properties.setInt("id_servicio", response.id_servicio);
       	    	Ti.App.fireEvent('fbController:idservicio', {
       	    		id_chofer:Ti.App.Properties.getInt("id_chofer"),
					id_servicio:response.id_servicio,
					id_usuario:Ti.App.Properties.getInt('id_usuario'),
					telefono:Ti.App.Properties.getString('telefono'),
					NMuj:Ti.App.Properties.getString("NMujer")
				});
				reciboReq.open("POST","http://subtmx.com/webservices/recibo.php");
                    params4 = {
                    	id_servicio: Ti.App.Properties.getInt("id_servicio"),
                        id_usuario: Ti.App.Properties.getInt('id_usuario'),
                        destino:geoDestino
                    };
       			reciboReq.send(params4);
				endServReq.open("POST","http://subtmx.com/webservices/endServicios.php");
                    params = {
                    	id_servicio: Ti.App.Properties.getInt("id_servicio"),
                        km_adicional: Ti.App.Properties.getDouble("kmAdicional"),
                        costo_km_adicional:Ti.App.Properties.getDouble("kmCostoAdicional"),
                        hora_extra: "00:00:00",
                        costo_hora_extra: 0,
                        cargo_hora_pico: 0,
                        tiempo_espera_adicional:"00:00:00",
                        costo_espera_adicional: 0,
                        costo_total: Ti.App.Properties.getDouble("pagoInicial"),
                        id_eventualidad:idEV,
                        costo_pendiente:0,
                        status_promocion:bbit
                    };
       			endServReq.send(params);
    		}
    	};
    	
    	endServReq.onload = function(){
           json = this.responseText;
           response = JSON.parse(json);
           if (response.logged == true){
           	
           	//Ti.App.Properties.setDouble("pagoInicial",0);
           }
   		};
		
		Ti.App.iOS.addEventListener('notification', function(e) {
    		if (e.badge > 0) {
        		Ti.App.iOS.scheduleLocalNotification({
            		date: new Date(new Date().getTime()),
            		badge: -1
        		});
    		}
		});
		
		window.addEventListener('open',function(){
			if (Ti.App.Properties.hasProperty('viajeactivo')) {
				idch.text = Ti.App.Properties.getInt('id_ChoferA');
				sa = 1;
				viewPro.visible = false;
				getData();
				if (Ti.App.Properties.getInt('yallegue')==0) {
					sdial2 = false;
				}
				if (Ti.App.Properties.getInt('viajeiniciado')==0) {
					sdial4 =  false;
				}
				if (Ti.App.Properties.getInt('finalizar')==0) {
					sdial3 = false;
				}
			}else{
				animateLogo = Titanium.UI.createAnimation();
        		animateLogo.top = '15%';
        		animateLogo.duration = 1800;
        		animateLogo.opacity = 1;
        		logo.animate(animateLogo);
        		progress.animate({ width: 0, duration: 180000 });
        		my_timer.start();	
			}
			if(Ti.App.Properties.getInt("vistaActiva")==0){
				Ti.App.Properties.setDouble("Extra2",0);
			}else if(Ti.App.Properties.getInt("vistaActiva")==1){
				viewPro.visible=false;
				
			}
		});
		
		window.containingNav = navwin;
		window.barColor = '#007fff';
		viewPro.add(display_lbl);
		viewPro.add(buscando_lbl);
		track.add(progress); 
 		viewPro.add(track);
 		viewPro.add(logo);
 		window.add(centerView);
 		window.add(viewWeb);
        window.add(viewPro);
        centerView.add(btnCall);
        centerView.add(btnCanViaje);
		centerView.add(btnRecotizar);
        centerView.add(vistaMapa);
		return navwin;
	}
	mainwin = NappSlideMenu.createSlideMenuWindow({
		centerWindow: navwin,
		leftWindow:winLeft,
		statusBarStyle: NappSlideMenu.STATUSBAR_WHITE,
		leftLedge:100
	});
	
	return mainwin;
};
module.exports = chofer;