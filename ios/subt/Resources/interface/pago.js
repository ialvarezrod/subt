function pagos(){
	var NappSlideMenu = require('dk.napp.slidemenu');
	var window,lblCuenta,lblVencimiento,lblTarjeta,txtCuenta,txtVencimiento,txtTarjeta,cmbTarjeta,cmbVencimiento,btnCuenta;
	var tableData = [],tableView;
	winLeft = Ti.UI.createWindow({backgroundImage:'/images/bgMenu.jpg'});
	logoLeft = Titanium.UI.createImageView({image:'/images/logo.png',width:'35%',height:'auto',zIndex:2,left:'15%',top:'20%',opacity:1});
	lblHome = Titanium.UI.createLabel({color:'#6485c3',text:'Home',top:'52%',left:'13%'});
	lblMisPedidos = Titanium.UI.createLabel({color:'#6485c3',text:'Mis Pedidos',top:'60%',left:'13%'});
	lblMiPerfil = Titanium.UI.createLabel({color:'#6485c3',text:'Mi Perfil',top:'68%',left:'13%'});
	lblPromociones = Titanium.UI.createLabel({color:'#6485c3',text:'Promociones',top:'76%',left:'13%'});
	lblPagos = Titanium.UI.createLabel({color:'#6485c3',text:'Pagos',top:'84%',left:'13%',font:{fontWeight:'bold'}});
	lblConfi = Titanium.UI.createLabel({color:'#6485c3',text:'Configuración',top:'92%',left:'13%'});
	navwin = createCenterNavWindow();
	
	lblHome.addEventListener('click',function(){
		if (Ti.App.Properties.hasProperty('id_usuario')) {
			mapa = require('/interface/mapa');
			winNav = new mapa();
			winNav.open();
		}else{
			
		}
	});
	lblPagos.addEventListener('click',function(){
		mainwin.closeOpenView();
	});
	lblPromociones.addEventListener('click',function(){
			Ti.UI.createAlertDialog({title:'Mensage',message:'No disponible'}).show();
	});
	lblMisPedidos.addEventListener('click',function(){
		if (Ti.App.Properties.hasProperty('id_usuario')) {
			pedidos = require('/interface/pedidos');
			winNav = new pedidos();
			winNav.open();
		}else{
			Ti.UI.createAlertDialog({title:'Mensage',message:'Solo usuarios registrados'}).show();
		}
	});
	lblMiPerfil.addEventListener('click',function(){
		if (Ti.App.Properties.hasProperty('id_usuario')) {
			perfil = require('/interface/perfil');
			winNav = new perfil();
			winNav.open();
		}else{
			Ti.UI.createAlertDialog({title:'Mensage',message:'Solo usuarios registrados'}).show();
		}
	});
	
	function createCenterNavWindow(){
		tc = Ti.UI.createLabel({text:'Credito o Debito',color:'#000000'});
		window = Ti.UI.createWindow({backgroundImage:'/images/bgRegistro.png',titleControl:tc});
		navwin = Titanium.UI.iOS.createNavigationWindow({window: window});
		menu = Ti.UI.createButton({backgroundImage:'/images/menuicon.png', left:10,zIndex:2,height:23,width:30});
		var webview = Titanium.UI.createWebView({url:'http://subtmx.com/webservices/conekta-php-master/token.php?id='+Ti.App.Properties.getInt("id_usuario")});
		
		menu.addEventListener('click',function(){
			mainwin.toggleLeftView();
			mainwin.setCenterhiddenInteractivity("TouchEnabled");
		});
		var banderaClose = 1;
		webview.addEventListener('load',function(){
			if (Titanium.UI.iOS.WEBVIEW_NAVIGATIONTYPE_RELOAD == 3) {
				banderaClose = banderaClose+1;
			}
			if (banderaClose == 3) {
				if (Ti.App.Properties.hasProperty('id_usuario')) {
					mapa = require('/interface/mapa');
					winNav = new mapa();
					winNav.open();
				}
			}
		});
		
		window.add(webview);	
		window.containingNav = navwin;
		window.setLeftNavButton(menu);
		return navwin;
	}
	
	mainwin = NappSlideMenu.createSlideMenuWindow({
		centerWindow: navwin,
		leftWindow:winLeft,
		statusBarStyle: NappSlideMenu.STATUSBAR_WHITE,
		leftLedge:100
	});
	
	winLeft.add(logoLeft);
	winLeft.add(lblHome);
	winLeft.add(lblMisPedidos);
	winLeft.add(lblMiPerfil);
	winLeft.add(lblPromociones);
	winLeft.add(lblPagos);
	winLeft.add(lblConfi);
	
	return mainwin;
};
module.exports = pagos;