function recotizar(){
	var Map = require('ti.map');
	var NappSlideMenu = require('dk.napp.slidemenu');
	Ti.Geolocation.preferredProvider = Titanium.Geolocation.PROVIDER_GPS;
	Titanium.Geolocation.accuracy = Titanium.Geolocation.ACCURACY_BEST;
	Titanium.Geolocation.distanceFilter = 10;
	
	var window,container,autocomplete_table;
	
	var table_data = [];var last_search = null;var timers = [];var ANDROID_API_KEY = 'AIzaSyB4xyTWJ6MAh1mO94AUfLF9WsuOiQt-OsA';
	var requestUrl;
	
	navwin = createCenterNavWindow();
	
	function createCenterNavWindow(){
		tc = Ti.UI.createLabel({text:'SUBT',color:'#0c83c7'});
		window = Titanium.UI.createWindow({backgroundColor:'white',titleControl:tc});
		
		leftButtonOrigen = Titanium.UI.createButton({backgroundImage:'/images/green.png',width:30,height:30});
		leftButtonDestino = Titanium.UI.createButton({backgroundImage:'/images/red.png',width:30,height:30});
		txtOrigen = Titanium.UI.createTextField({borderStyle:Ti.UI.INPUT_BORDERSTYLE_ROUNDED,color:'black',hintText:"Ingrese su Origen",top:20,width:'90%',height:40,leftButton:leftButtonOrigen,leftButtonMode:Titanium.UI.INPUT_BUTTONMODE_ALWAYS,zIndex:3});
		txtDestino = Titanium.UI.createTextField({borderStyle:Ti.UI.INPUT_BORDERSTYLE_ROUNDED,color:'black',hintText:"Ingrese su Destino",top:65,width:'90%',height:40,leftButton:leftButtonDestino,leftButtonMode:Titanium.UI.INPUT_BUTTONMODE_ALWAYS,zIndex:3});
		btnPedir = Titanium.UI.createButton({width:'90%',height:60,title:'Pedir Automóvil',textAlign:'center',center:0,bottom:'10%',color:'white',zIndex:3,backgroundColor:'#0c83c7',borderRadius:10});
		
		mapview = Map.createView({mapType: Map.NORMAL_TYPE,animate:true,regionFit:true,userLocation:true,top:0});
		pinUsuario = Map.createAnnotation({title:"Mi ubicacion",image:'/images/pinOrigen.png'});
		pinDestino = Map.createAnnotation({title:"Mi destino",image:'/images/pinDestino.png'});
		
		//webservices
		rango = Titanium.Network.createHTTPClient();
		xhrLocationCode = Ti.Network.createHTTPClient();
		xhrLocationCode.setTimeout(120000);
		pedirKm= Titanium.Network.createHTTPClient();
		pedirKm2= Titanium.Network.createHTTPClient();
		
		//constantes
		container = Ti.UI.createView({layout : 'vertical',height : Ti.UI.SIZE,top:'18%'});
		autocomplete_table = Ti.UI.createTableView({height: Ti.UI.SIZE});
		
		// Variables
		latInicio =  Titanium.UI.createLabel({});
		lngInicio =Titanium.UI.createLabel({});
		latDestino =Titanium.UI.createLabel({});
		lngDestino =Titanium.UI.createLabel({});
		nombreTipoCarro=Ti.UI.createLabel({text:Ti.App.Properties.getString("carroSelect")});
		var idTipoCarro = Ti.App.Properties.getInt("tipo_servicio"),idOpcionServicio = 1,bdrBusqueda,banderaCompletar = 1;
		var servicioAeropuerto,idAeropuerto,numeroPasajero=0,numvuelo,horavuelo,taxiRosa = Ti.App.Properties.getInt("taxiRosaSelect");
		
		// Alertas
		dialogTipoViaje = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Sencillo','Por hora'],message: 'Elija una opción de viaje',title: 'Opción de viaje'});
		
		Titanium.Geolocation.getCurrentPosition(function(e){
			if (e.error){
				Ti.UI.createAlertDialog({title:'GPS',message:"Señal de gps perdida"}).show();
        		return;
    		}else{
    			pinUsuario.latitude = Ti.App.Properties.getDouble("latActual");
    			pinUsuario.longitude = Ti.App.Properties.getDouble("longActual");
    			pinDestino.latitude = Ti.App.Properties.getDouble("latActual") + 0.0009999;
				pinDestino.longitude = Ti.App.Properties.getDouble("longActual") -0.0009999;
				mapview.region = {latitude:Ti.App.Properties.getDouble("latActual"), longitude:Ti.App.Properties.getDouble("longActual"), latitudeDelta:0.009, longitudeDelta:0.009};
    			mapview.annotations = [pinUsuario,pinDestino];
    			latInicio.text = Ti.App.Properties.getDouble("latActual");
				lngInicio.text = Ti.App.Properties.getDouble("longActual");
				
				Titanium.Geolocation.reverseGeocoder(Ti.App.Properties.getDouble("latActual"),Ti.App.Properties.getDouble("longActual"),function(evt){
					if (evt.success) {
						var places = evt.places;
						if (places && places.length) {
							txtOrigen.value = places[0].address;
						}else{
							txtOrigen.value = "No address found";
						}
					}else{
						Ti.UI.createAlertDialog({title:'GPS',message:"Señal de gps perdida"}).show();
					}
				});
    		}
		});
		
		function omitirAcentos(text) {
			var acentos = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç";
			var original = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc";
			for (var i=0; i<acentos.length; i++) {
				text = text.replace(acentos.charAt(i), original.charAt(i));
			}
			return text;
		}
		
		txtDestino.addEventListener('change', function(e) {
			banderaCompletar = 1;
			if (txtDestino.value.length > 6 && txtDestino.value !=  last_search) {
				clearTimeout(timers['autocomplete']);
				timers['autocomplete'] = setTimeout(function() {
					last_search =txtDestino.value;
					auto_complete(txtDestino.value);
				}, 300);
			}
			return false;
		});
		
		txtOrigen.addEventListener('return', function(e){
  			txtOrigen.blur();
  			bdrBusqueda = 0;
  			requestUrl = "http://maps.google.com/maps/api/geocode/json?address=" + txtOrigen.value.replace(' ', '+');
			requestUrl += "&sensor=" + (Ti.Geolocation.locationServicesEnabled == true);
			xhrLocationCode.open("GET", requestUrl);
			xhrLocationCode.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
			xhrLocationCode.send();		
		});
		
		txtDestino.addEventListener('return', function(e){
  			txtDestino.blur();
  			bdrBusqueda = 1;
  			requestUrl = "http://maps.google.com/maps/api/geocode/json?address=" + omitirAcentos(txtDestino.value.replace(' ', '+'));
			requestUrl += "&sensor=" + (Ti.Geolocation.locationServicesEnabled == true);
			xhrLocationCode.open("GET", requestUrl);
			Ti.API.info(requestUrl);
			xhrLocationCode.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
			xhrLocationCode.send();	
		});
		
		xhrLocationCode.onerror = function(e) {};
		xhrLocationCode.onload = function(e) {
			mapview.region = {latitude:latInicio.text, longitude:lngInicio.text, latitudeDelta:0.03, longitudeDelta:0.03};	
			var response = JSON.parse(this.responseText);
			if (response.status == 'OK' && response.results != undefined && response.results.length > 0) {
				if (bdrBusqueda == 0) {
					pinUsuario.latitude =response.results[0].geometry.location.lat;
					pinUsuario.longitude = response.results[0].geometry.location.lng;
					latInicio.text = response.results[0].geometry.location.lat;
					lngInicio.text = response.results[0].geometry.location.lng;
					Titanium.Geolocation.reverseGeocoder(response.results[0].geometry.location.lat,response.results[0].geometry.location.lng,function(evt){
						if (evt.success) {
							var places = evt.places;
							if (places && places.length) {
								txtOrigen.value = places[0].address;
							}else{
								txtOrigen.value = "No address found";
							}
						}else{
							Ti.UI.createAlertDialog({title:'Reverse geo error',message:evt.error}).show();
						}
					});
				}else{
					pinDestino.latitude =response.results[0].geometry.location.lat;
					pinDestino.longitude = response.results[0].geometry.location.lng;
					latDestino.text = response.results[0].geometry.location.lat;
					lngDestino.text = response.results[0].geometry.location.lng;
				}
				if (txtDestino.value == "") {
					Ti.UI.createAlertDialog({title:'Mensaje',message:'Ingrese su destino por favor'}).show();
				}else{
					rango.open("POST","http://subtmx.com/webservices/phpgeo-master/tests/geo.php");
                	params = {
                		latitudo:latInicio.text,
                		longitudo:lngInicio.text,
                    	latitudd:latDestino.text,
                    	longitudd:lngDestino.text,
               		};
                	rango.send(params);		
				}	
			}	
		};
		
		rango.onload = function(){
			 json = this.responseText;
             response = JSON.parse(json);
             if (response.logged == true){
             	var RangoOrigen=response.status_origen;
            	var RangoDestino=response.status_destino;
            	if(RangoOrigen==1){
            		if (Ti.App.Properties.hasProperty('idioma')) {
            	 		var dialogAlertaRuta = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Accept'],message: 'Service not available in this area , please select another origin',title: 'Message'});
					}else{
        				var dialogAlertaRuta = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'],message: 'Servicio no disponible en esta área, favor de seleccionar otro origen',title: 'Mensaje'});
            	 	}
            	 	dialogAlertaRuta.show();
            	 	txtOrigen.focus();
            	}else if(RangoDestino==1){
            		if (Ti.App.Properties.hasProperty('idioma')) {
            	 		var dialogAlertaRuta = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Accept'],message: 'Service not available in this area , please select another destination',title: 'Message'});
					}else{
        				var dialogAlertaRuta = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'],message: 'Servicio no disponible en esta área, favor de seleccionar otro destino',title: 'Mensaje'});
            	 	}
            	 	dialogAlertaRuta.show();
            	 	txtDestino.focus();
            	}else{
            	 	pedirKm.open("POST","http://subtmx.com/webservices/distanciag.php");
        			params = {
        				lat1:latInicio.text ,
       					lng1:lngInicio.text,
       					lat2:latDestino.text,
       					lng2:lngDestino.text
        			};
       				pedirKm.send(params);
            	}
             }
		};
		
		pedirKm.onload = function(){
			json = this.responseText;
            response = JSON.parse(json);
            if (response.logged == true){
            	Ti.App.Properties.setString("distancias",response.km_total);
				Ti.API.info(Ti.App.Properties.getString('distancias'));
				Ti.App.Properties.setDouble("Extra2",contador);
				Ti.App.Properties.setInt("vistaActiva",1);
				chofer= require('/interface/chofer');
        		new chofer(latInicio.text,lngInicio.text,latDestino.text,lngDestino.text,txtOrigen.value,idOpcionServicio,taxiRosa,idTipoCarro,nombreTipoCarro.text,numeroPasajero,txtDestino.value).open();
        		window.close();
            }
		};
		
		autocomplete_table.addEventListener('click', function(e) {
			get_place(e.row.place_id);
		});
		
		function get_place(place_id) {
			var url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + place_id + "&location=16.8633600,-99.8901000&key=" + ANDROID_API_KEY;		
			var client = Ti.Network.createHTTPClient({
				onload : function(e) {
     				var obj = JSON.parse(this.responseText);
					var result = obj.result;
					container.place_id           = place_id;
					container.formatted_address  = result.formatted_address;
					container.latitude           = result.geometry.location.lat;
					container.longitude          = result.geometry.location.lng;
					if (banderaCompletar == 0) {
						txtOrigen.value                  = container.formatted_address;	
					}else{
						txtDestino.value                  = container.formatted_address;	
					}		
					table_data = [];
					autocomplete_table.setData(table_data);
					autocomplete_table.setHeight(0);
 				},	
 				onerror : function(e) {
					Ti.API.debug(e.error);
					alert('error');
				},
				timeout : 5000
			});
			client.open("GET", url);
			client.send();	
		}
		
		function get_locations(query) {
			var url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + query + "&location=16.8633600,-99.8901000&key=" + ANDROID_API_KEY;		
			var client = Ti.Network.createHTTPClient({
				onload : function(e) {
     				var obj = JSON.parse(this.responseText);
     				var predictions = obj.predictions;     		
     				table_data = [];            
            		autocomplete_table.removeAllChildren();
					autocomplete_table.setHeight(Ti.UI.SIZE);
            		for (var i = 0; i < predictions.length; i++) {
            			var desc = omitirAcentos(predictions[i].description);
            			var row = Ti.UI.createTableViewRow({
						height: 40,
						title: desc,
						place_id: predictions[i].place_id,
						hasDetail:false,
						postId:desc,            		
            			});                        	
            			table_data.push(row);     	
            		}    		 
            		autocomplete_table.setData(table_data);
 				},
 				onerror : function(e) {
					Ti.API.debug(e.error);
					alert('error');
				},
				timeout : 5000
			});
			client.open("GET", url);
			client.send();	
		}
		
		function auto_complete(search_term){
			if (search_term.length > 2) {
				get_locations(search_term);   	
   			}
		}
		
		window.addEventListener('open',function(){     					 	
   	 		pedirKm2.open("POST","http://subtmx.com/webservices/distanciag.php");
        	params = {
        		lat1:Ti.App.Properties.getDouble("lat_in_recalcular") ,
       			lng1:Ti.App.Properties.getDouble("long_in_recalcular"),
       			lat2:Ti.App.Properties.getDouble("latActual"),
       			lng2:Ti.App.Properties.getDouble("longActual")
        	};
       		pedirKm2.send(params);	
   	 	});
   	 	
   	 	pedirKm2.onload = function(){
        	json = this.responseText;
            response = JSON.parse(json);
            if (response.logged == true){             	
            	var kmformateado=response.km_total.split(',');
		 		var kmWeb = kmformateado[0] + '.' + kmformateado[1];
		 		var dus= parseFloat(kmWeb);
		 		if(response.km_total==''){
		 			dus=0;
		 		}
		 		contador= Ti.App.Properties.getDouble("Extra2")+ dus;
		   }
       	};
		
		container.add(autocomplete_table);
		window.add(btnPedir);
		window.add(txtOrigen);
		window.add(txtDestino);
		window.add(mapview);
		window.add(container);
		return window;
	}
	
	mainwin = NappSlideMenu.createSlideMenuWindow({
		centerWindow: navwin,
		statusBarStyle: NappSlideMenu.STATUSBAR_WHITE,
		leftLedge:100
	});
	
	return mainwin;
};
module.exports = recotizar;
