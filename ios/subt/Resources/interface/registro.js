function registro(color, _title, _nav){
	var window,tc,lblUser,lblCorreo,lblPass,lblPass2,txtUser,txtCorreo,txtPass,txtPass2,chkFactura,chkTerminos,btnRegistrar,lblTermino,lblFactura,scrollView,
		registerReq,json,response,alertDialog;
		
	tc = Ti.UI.createLabel({text: _title,color:'white'});
	window = Ti.UI.createWindow({backgroundImage:'/images/bgRegistro.png',navTintColor:'white', nav:_nav, titleControl: tc, title: _title});
	scrollView = Titanium.UI.createScrollView({contentWidth:'auto',contentHeight:'auto',top:0,showVerticalScrollIndicator:true,showHorizontalScrollIndicator:false});
	view = Ti.UI.createView({top: 0,height: 616});
	view2 = Ti.UI.createView({top: '80%',height: 320});
	lblNombre = Titanium.UI.createLabel({text:'Nombre',center:0,top:'2%',color:'white'});
	txtNombre = Titanium.UI.createTextField({color:'#0066cc',hintText:'Nombre de usuario',center:0,top:'7%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,value:"",textAlign:'center',clearOnEdit:true, keyboardType:Titanium.UI.KEYBOARD_TYPE_ASCII, returnKeyType:Titanium.UI.RETURNKEY_NEXT});
	lblApellidoP = Titanium.UI.createLabel({text:'Apellido Paterno',center:0,top:'15%',color:'white'});
	txtApellidoP = Titanium.UI.createTextField({color:'#0066cc',hintText:'Apellido Paterno',center:0,top:'20%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,value:"",textAlign:'center',clearOnEdit:true,keyboardType:Titanium.UI.KEYBOARD_TYPE_ASCII, returnKeyType:Titanium.UI.RETURNKEY_NEXT});
	lblApellidoM = Titanium.UI.createLabel({text:'Apellido Materno',center:0,top:'28%',color:'white'});
	txtApellidoM = Titanium.UI.createTextField({color:'#0066cc',hintText:'Apellido Materno',center:0,top:'32%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,value:"",textAlign:'center',clearOnEdit:true,keyboardType:Titanium.UI.KEYBOARD_TYPE_ASCII, returnKeyType:Titanium.UI.RETURNKEY_NEXT});
	lblCorreo = Titanium.UI.createLabel({text:'Correo electrónico',center:0,top:'41%',color:'white'});
	txtCorreo = Titanium.UI.createTextField({color:'#0066cc',hintText:'Correo',center:0,top:'46%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,value:"",textAlign:'center',clearOnEdit:true,keyboardType:Titanium.UI.KEYBOARD_TYPE_EMAIL, returnKeyType:Titanium.UI.RETURNKEY_NEXT});
	lblPass = Titanium.UI.createLabel({text:'Contraseña',center:0,top:'54%',color:'white'});
	txtPass = Titanium.UI.createTextField({color:'#0066cc',hintText:'*********',center:0,top:'59%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,value:"",textAlign:'center',passwordMask:true,clearOnEdit:true,keyboardType:Titanium.UI.KEYBOARD_TYPE_ASCII, returnKeyType:Titanium.UI.RETURNKEY_NEXT});		
	lblrepeat = Titanium.UI.createLabel({text:'Repetir contraseña',center:0,top:'67%',color:'white'});
	txtrepeat = Titanium.UI.createTextField({color:'#0066cc',hintText:'*********',center:0,top:'72%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,value:"",textAlign:'center',passwordMask:true,clearOnEdit:true,keyboardType:Titanium.UI.KEYBOARD_TYPE_ASCII, returnKeyType:Titanium.UI.RETURNKEY_NEXT});	
	lblPass2 = Titanium.UI.createLabel({text:'Telefono',center:0,top:'80%',color:'white'});
	txtPass2 = Titanium.UI.createTextField({color:'#0066cc',hintText:'0000000000',center:0,top:'85%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,value:"",textAlign:'center',clearOnEdit:true, returnKeyType:Titanium.UI.RETURNKEY_NEXT});
	lblSexo = Titanium.UI.createLabel({text:'Sexo',center:0,top:'93%',color:'white'});
	txtSexo = Titanium.UI.createTextField({color:'#0066cc',hintText:'masculino/femenino',center:0,top:'96%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,value:"",textAlign:'center',editable:false});
	
	chkTerminos = Ti.UI.createSwitch({value:false,top:'0%',onTintColor:'#62D337',tintColor:'#0c83c7',left:'20%'});
	lblTermino = Titanium.UI.createLabel({text:'Deseo facturar',right:'20%',top:'1%',color:'white'});
	chkFactura = Ti.UI.createSwitch({value:false,top:'13%',onTintColor:'#62D337',tintColor:'#0c83c7',left:'10%'});
	lblFactura = Titanium.UI.createLabel({text:'Acepto términos y condiciones',right:'10%',top:'14%',color:'white'});
	btnRegistrar = Titanium.UI.createButton({title:'Registrarme',color:'white',borderRadius:10,borderColor:'white',backgroundColor:'#0c83c7',top:'28%',width:'80%', height:60});
	registerReq = Titanium.Network.createHTTPClient();
	codigoReq = Titanium.Network.createHTTPClient();
	dialogCodigo = Ti.UI.createAlertDialog({title: 'Introduzca el codigo enviado a su correo',style:Ti.UI.iPhone.AlertDialogStyle.PLAIN_TEXT_INPUT,buttonNames: ['Entrar','Cancelar']});
	view3 = Ti.UI.createView({height: 60, width:'80%', center:0, backgroundColor:'white', visible:false, borderRadius:5});
	var activityIndicator = Ti.UI.createActivityIndicator({color: 'black',font: {fontSize:16, fontWeight:'bold'},message: 'Registrando...',style: Ti.UI.ActivityIndicatorStyle.DARK,height:Ti.UI.SIZE,width:Ti.UI.SIZE, center:0, zIndex:6});
	
	txtNombre.addEventListener('return', function(e) {
		txtApellidoP.focus();
	});
	txtApellidoP.addEventListener('return', function(e) {
		txtApellidoM.focus();
	});
	txtApellidoM.addEventListener('return', function(e) {
		txtCorreo.focus();
	});
	txtCorreo.addEventListener('return', function(e) {
		txtPass.focus();
	});
	txtPass.addEventListener('return', function(e) {
		txtrepeat.focus();
	});
	txtrepeat.addEventListener('return', function(e) {
		txtPass2.focus();
	});
	txtPass2.addEventListener('return', function(e) {
		dialogConfi.show();
	});
	
	var optsConfi = {cancel: 2,options: ['Masculino','Femenino','Cancelar'],selectedIndex: 2,destructive: 0,title: 'Sexo'};
	var dialogConfi = Ti.UI.createOptionDialog(optsConfi);
	
	txtSexo.addEventListener('click', function(e) {
		dialogConfi.show();
	});
	
	dialogConfi.addEventListener('click', function(e) {
		if (e.index == 0) {
			if (Ti.App.Properties.hasProperty('idioma')) {
				txtSexo.value = 'Male';
			}else{
				txtSexo.value = 'Masculino';
			}
		}else{
			if (Ti.App.Properties.hasProperty('idioma')) {
				txtSexo.value = 'Female';
			}else{
				txtSexo.value = 'Femenino';
			}
		}
	});
	
	
	function checkemail(emailAddress){
    		var str = emailAddress;
    		var filter = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    		if (filter.test(str)){
        		testresults = true;
    		}else{
        		testresults = false;
    		}
    		return (testresults);
	};
	
	window.addEventListener('open', function(e) {
		if (Ti.App.Properties.hasProperty('idioma')) {
			lblNombre.text = 'Name';
			txtNombre.hintText = 'name';
			lblApellidoP.text = 'Last name';
			txtApellidoP.hintText = 'last name';
			lblApellidoM.text = 'Maternal surname';
			txtApellidoM.hintText = 'maternal surname';
			lblCorreo.text = 'Email Account';
			txtCorreo.hintText = 'email';
			lblPass.text = 'Password';
			lblPass2.text = 'Phone';
			lblTermino.text = 'Invoice request';
			lblFactura.text = 'Agree to terms and conditions';
			btnRegistrar.title = 'Sign in';
			tc.text = 'Sign in';
			lblSexo.text = 'Sex';
			optsConfi.options =['Male','Female','Cancel'];
			optsConfi.title = 'Sex';
		}
	});
	
	btnRegistrar.addEventListener('click',function(e){
			view3.visible = true;
			btnRegistrar.enabled = false;
        	btnRegistrar.opacity = 0.6;
			activityIndicator.show();
			if (txtNombre.value != '' && txtCorreo.value != '' && txtPass.value != '' && txtPass2.value != ''){
					if (!checkemail(txtCorreo.value.replace(/^\s+/g, '').replace(/\s+$/g, ''))){
						alert("Introduce un email valido");
					}else{
						if (txtPass.value == txtrepeat.value) {
							if (chkTerminos.value == true) {
							factura = require('/interface/factura');
							winNav = new factura('#1288ec','Factura', _nav);
							winNav.fireEvent('foo',{
								username: txtCorreo.value,
                    				password: txtPass.value,
                    				correo: txtCorreo.value.replace(/^\s+/g, '').replace(/\s+$/g, ''),
                    				telefono:txtPass2.value,
                    				nombre:txtNombre.value,
                    				apellido_p:txtApellidoP.value,
                    				apellido_m:txtApellidoM.value,
                    				sexo:txtSexo.value
							});
							window.nav.openWindow(winNav,{animate:true});
						}else{
							registerReq.open("POST","http://subtmx.com/webservices/registerResquest.php");
                			params = {
                    				username: txtCorreo.value,
                    				password: Ti.Utils.md5HexDigest(txtPass.value),
                    				correo: txtCorreo.value.replace(/^\s+/g, '').replace(/\s+$/g, ''),
                    				telefono:txtPass2.value,
                    				nombre:txtNombre.value,
                    				apellido_p:txtApellidoP.value,
                    				apellido_m:txtApellidoM.value,
                    				sexo:txtSexo.value,
                    				rfc:'',
                    				razon_social:'',
                    				direccion_fiscal:'',
                    				email_factura:'',
                    				cp:'',
                    				device_token:Titanium.Platform.id,
                        	 		device_type:"ios"
                				};
                			registerReq.send(params);
						}
					}else{
							dialog = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'],message: 'Las contraseñas no coinciden',title: 'Mensaje'});
							if (Ti.App.Properties.hasProperty('idioma')) {
								dialog.buttonNames = ['Accept'];
								dialog.message = 'Passwords do not match';
								dialog.title = 'Message';
							}
							dialog.show();
					}
				}
			}else{
				dialog = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'],message: 'Todos los campos son obligatorios',title: 'Mensaje'});
				if (Ti.App.Properties.hasProperty('idioma')) {
					dialog.buttonNames = ['Accept'];
					dialog.message = 'All fields are required';
					dialog.title = 'Message';
				}
				dialog.show();
			}
	});
	
	registerReq.onload = function(){
			json = this.responseText;
    		response = JSON.parse(json);
    		if (response.message == "Insert failed" || response.message == 'El usuario o email ya existen' || response.message == 'Tu dispositivo fue bloqueado, consulte a soporte técnico: soporte@subtmx.com' || response.message == 'Su número de telefono ya fue registrado anteriormente'){
        		view3.visible = false;
        		btnRegistrar.enabled = true;
        		btnRegistrar.opacity = 1;
        		activityIndicator.hide();
        		alertDialog = Titanium.UI.createAlertDialog({title: 'Soporte',message: response.message,buttonNames: ['Aceptar']}).show();
    		}else{
    			view3.visible = false;
    			activityIndicator.hide();
    			alertDialog2 = Titanium.UI.createAlertDialog({title: 'Registro',message: response.message,buttonNames: ['OK']});
    			alertDialog2.show();
    			alertDialog2.addEventListener('click',function(e){
    					Ti.App.Properties.setInt("id_usuario2", response.id_usuario);
    					Ti.App.Properties.setInt("id_cliente", response.id_cliente);
    					Ti.App.Properties.setString("usuario", response.usuario);
    					Ti.App.Properties.setString("email", response.email);
    					Ti.App.Properties.setString("telefono", response.telefono);
    					Ti.App.Properties.setString("nombre", response.nombre);
    					Ti.App.Properties.setString("nombre_completo", response.nombre_completo);
    					Ti.App.Properties.setString("apellido", response.apellido);
    					Ti.App.Properties.setString("sexo", response.sexo);
    					Ti.App.Properties.setInt("BonoActual", 0);
   						Ti.App.Properties.setInt("pagoInicial", 0);
   						dialogCodigo.show();
    			});
    		}
	};
	
	dialogCodigo.addEventListener('click', function(e) {
		if (e.index == 0) {
				//dialogCodigo.value = dialogCodigo.value.replace(/^\s+/g, '').replace(/\s+$/g, '');
				if (e.text != '') {
					codigoReq.open("POST","http://subtmx.com/webservices/codigo.php");
           			params = {
            			usuario: Ti.App.Properties.getInt("id_usuario2"),
                		codigo: e.text
          			};
            		codigoReq.send(params);
				} else {
					Ti.UI.createAlertDialog({
						cancel : 1,
						buttonNames : ['Aceptar'],
						message : 'Debe introducir el código de seguridad',
						title : 'Error'
					}).show();
				}	
			}
	});
	
	codigoReq.onload = function(){
			json = this.responseText;
    		response = JSON.parse(json);
    		if (response.logged == false){
        		alertDialog2 = Titanium.UI.createAlertDialog({title:'Error',message: response.message,buttonNames: ['Aceptar']}).show();
    		}else{
    			Ti.App.Properties.setInt("id_usuario",Ti.App.Properties.getInt("id_usuario2"));
    			alertDialog = Titanium.UI.createAlertDialog({title: 'Registro',message: response.message,buttonNames: ['OK']});
    			alertDialog.show();
    			alertDialog.addEventListener('click',function(e){
    					mapa = require('/interface/mapa');
						winNav = new mapa();
						winNav.open();
						window.close();
    			});
    		}
	};
	
	view.add(lblNombre);
	view.add(txtNombre);
	view.add(lblApellidoP);
	view.add(txtApellidoP);
	view.add(lblApellidoM);
	view.add(txtApellidoM);
	view.add(lblCorreo);
	view.add(txtCorreo);
	view.add(lblPass);
	view.add(txtPass);
	view.add(lblrepeat);
	view.add(txtrepeat);	
	view.add(lblPass2);
	view.add(txtPass2);
	view.add(lblSexo);
	view.add(txtSexo);
	view2.add(chkTerminos);
	view2.add(lblTermino);
	view2.add(chkFactura);
	view2.add(lblFactura);
	view2.add(btnRegistrar);
	view3.add(activityIndicator);
	
	window.barColor = color;
	window.backButtonTitle = '';
	window.add(scrollView);
	window.add(view3);
	scrollView.add(view);
	scrollView.add(view2);	
	
	return window;
};
module.exports = registro;