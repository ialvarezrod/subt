function mapa(navwin){
	var Map = require('ti.map');
	var NappSlideMenu = require('dk.napp.slidemenu');
	var Firebase = require('com.leftlanelab.firebase');
	Ti.Geolocation.preferredProvider = Titanium.Geolocation.PROVIDER_GPS;
	Titanium.Geolocation.accuracy = Titanium.Geolocation.ACCURACY_BEST;
	Titanium.Geolocation.distanceFilter = 10;
	
	var window,menu,navwin,mapview,btnPedir,winLeft,logoLeft,mainwin,lblMisPedidos,
		lblMiPerfil,lblPromociones,lblPagos,lblConfi,lblNombreCompleto,pago,winNav,
		latInicio,lngInicio,pagar,dialCalif,dest,hor,b,ids,desc,costo,kma,ckma,Ideve,
		dialogPagoPendiente,nombreTipoCarro,dialogConfirmarxhora,container,autocomplete_table,
		dialogNumeroVuelo,dialogHoraVuelo,dialogAeroAviso,dialogNombre,autoCheck;var costo;
		
	var table_data = [];var last_search = null;var timers = [];var ANDROID_API_KEY = 'AIzaSyB4xyTWJ6MAh1mO94AUfLF9WsuOiQt-OsA';
	var requestUrl;
	
	winLeft = Ti.UI.createWindow({backgroundImage:'/images/bgMenu.jpg'});
	logoLeft = Titanium.UI.createImageView({image:'/images/logo.png',width:'35%',height:'auto',zIndex:2,left:'15%',top:'20%',opacity:1});
	lblNombreCompleto = Titanium.UI.createLabel({color:'black',text:Ti.App.Properties.getString('nombre_completo'),top:'15%',left:'15%',font:{fontWeight:'bold'}});
	lblHome = Titanium.UI.createLabel({color:'#6485c3',text:'Home',top:'52%',left:'13%',font:{fontWeight:'bold'}});
	lblMisPedidos = Titanium.UI.createLabel({color:'#6485c3',text:'Mis Pedidos',top:'60%',left:'13%'});
	lblMiPerfil = Titanium.UI.createLabel({color:'#6485c3',text:'Mi Perfil',top:'68%',left:'13%'});
	lblPromociones = Titanium.UI.createLabel({color:'#6485c3',text:'Promociones',top:'76%',left:'13%'});
	lblPagos = Titanium.UI.createLabel({color:'#6485c3',text:'Pagos',top:'84%',left:'13%'});
	lblConfi = Titanium.UI.createLabel({color:'#6485c3',text:'Configuración',top:'92%',left:'13%'});
	lblVersion  = Ti.UI.createTextField({value:'Versión:'+Ti.App.version,color:'#151431',left:'13%',top:'5%',zIndex:4,font:{fontSize:'12%'},editable:false,focus:false});
	if(Ti.App.Properties.hasProperty('idioma')){
		var optsConfig = {options:  ['Help','cancellation airport','Cancel'], selectedIndex: 2,destructive:0,title: 'Select an option'};
	}else{
	    var optsConfig = {options:  ['Ayuda','Cancelación aeropuerto','Cancelar'], selectedIndex: 2,destructive:0,title: 'Selecciona una opción'};	
	}
	var dialogConfiguracion = Ti.UI.createOptionDialog(optsConfig);
	
	var vistaCotizacion= Ti.UI.createView({width:'100%', height:'100%', visible:false,zIndex: 20,backgroundColor:'#D9727374'});
    var vistaCotizar= Ti.UI.createView({width:'80%', height:Ti.UI.SIZE,zIndex: 20,backgroundColor:'white',borderColor:'black',borderRadius:6,layout:'vertical'});
    var vistatxt= Ti.UI.createView({width:'90%', height:Ti.UI.SIZE, layout:'vertical',top:30});
    var txtCotizar=Titanium.UI.createLabel({text:'COTIZACIÓN',top:0,color:'black',font:{fontSize:18,fontFamily: 'Menlo Bold Italic'}});
    var txtCosto=Titanium.UI.createLabel({text:'Costo Aproximado',top:10,color:'black'});
    var txtPrecio=Titanium.UI.createLabel({text:'$450.00',top:10,font:{fontSize: 25,fontFamily: 'SanFranciscoText-Regular'},color:'#3498db '});
    var txtAuto=Titanium.UI.createLabel({text:'Tipo de Auto:',top:10,color:'black'});
    var txtviajeOrigen=Titanium.UI.createLabel({text:'Origen:',top:10,color:'black'});
    var txtviajeDestino=Titanium.UI.createLabel({text:'Destino:',top:10,color:'black'});
    var txtmensaje=Titanium.UI.createLabel({text:'¿Desea solicitar el servicio?',top:10,color:'#2980b9 ',font:{fontSize: 15,fontFamily: 'Menlo Bold Italic'},textAlign:'center'});
    var vistaBotones= Ti.UI.createView({width:Ti.UI.SIZE, height:Ti.UI.SIZE,layout:'horizontal',top:20});
    var vistaBotones2= Ti.UI.createView({width:'100%', height:30,layout:'horizontal',top:0});
    btnCotizar = Titanium.UI.createButton({title:'Aceptar',center:0});
    btnCotizar2 = Titanium.UI.createButton({title:'Cancelar',left:60});
    
    
    //Vista del numerador servicio por hora
    var serviciohora= Ti.UI.createView({width:'100%', height:'100%', visible:false,zIndex: 20,backgroundColor:'#B3000000'});
    var vistaserviciohora= Ti.UI.createView({width:'80%', height:Ti.UI.SIZE,zIndex: 20,backgroundColor:'white',borderColor:'black',borderRadius:6,layout:'vertical'});
    var Tituloserviciohora=Titanium.UI.createLabel({text:'¿Cuantas horas requieres de servicio?',top:20,color:'black',font:{fontSize:16}, textAlign:'center', width:'75%'});
    var txtserviciohora=Titanium.UI.createTextArea({value:2,top:20,color:'black',font:{fontSize: 30},textAlign:'center'});
    var vistaserviciohoraBTN= Ti.UI.createView({center:0,width:Ti.UI.SIZE,height:Ti.UI.SIZE,zIndex: 20,layout:'horizontal', top:5});
       
    var btnserviciohora1 = Titanium.UI.createButton({backgroundImage:'/images/+.png',center:0,height:40,width:40});
    var btnserviciohora2 = Titanium.UI.createButton({backgroundImage:'/images/-.png',left:10,height:40,width:40});
    var btnserviciohora = Titanium.UI.createButton({title:'Aceptar',center:0,backgroundColor:'#0c83c7',top:15,height:50,width:120, borderRadius:5, color:'white'});
    var vistaserviciohora2= Ti.UI.createView({width:'100%', height:30,layout:'horizontal',top:0});
    
    serviciohora.add(vistaserviciohora);
    vistaserviciohora.add(Tituloserviciohora);
    vistaserviciohora.add(txtserviciohora);
    vistaserviciohoraBTN.add(btnserviciohora1);
    vistaserviciohoraBTN.add(btnserviciohora2);
    vistaserviciohora.add(vistaserviciohoraBTN);
    vistaserviciohora.add(btnserviciohora);
    vistaserviciohora.add(vistaserviciohora2);
    
    //Vista de la version
    var versionI=Ti.App.version;
   	versionI=versionI.split('.');
   	versionI=versionI[0]+''+versionI[1];
    var version= Ti.UI.createView({width:'100%', height:'100%', visible:false,zIndex: 20,backgroundColor:'#B3000000'});
    var vistaversion= Ti.UI.createView({width:'80%', height:Ti.UI.SIZE,zIndex: 20,backgroundColor:'white',borderColor:'black',borderRadius:6,layout:'vertical'});
   	var Tituloversion=Titanium.UI.createLabel({text:'VERSION ANTIGUA',top:20,color:'black',font:{fontSize:18,fontFamily: 'Menlo Bold Italic'}});
   	var txtversion=Titanium.UI.createLabel({text:"Actualiza a la ultima versión",top:20,color:'#2980b9',font:{fontSize: 15,fontFamily: 'Menlo Bold Italic'},textAlign:'center',width:'80%'});
   	var btnversion = Titanium.UI.createButton({title:'Aceptar',center:0,backgroundColor:'#0c83c7',top:10,borderRadius:5,height:40,width:'40%', color:'white'});
    var vistaversion2= Ti.UI.createView({width:'100%', height:30,layout:'horizontal',top:0});
    	
	version.add(vistaversion);
	vistaversion.add(Tituloversion);
	vistaversion.add(txtversion);
	vistaversion.add(btnversion);
	vistaversion.add(vistaversion2);
	
	btnversion.addEventListener('click', function() {
		var opcion_Actualizar = Ti.UI.createAlertDialog({
			buttonNames : ['Actualizar', 'Salir'],
			message : '¿Que desea hacer?',
			title : 'Mensaje'
		});
		opcion_Actualizar.show();
		opcion_Actualizar.addEventListener('click', function(e) {
			if (e.index == 0) {
				Ti.Platform.openURL("https://itunes.apple.com/us/app/subt/id1131779115?l=es&ls=1&mt=8");
			} else if (e.index == 1) {
				Titanium.App.Properties.removeProperty('coneckta');
				Titanium.App.Properties.removeProperty('paypal');
				Titanium.App.Properties.removeProperty('id_usuario');
				Titanium.App.Properties.removeProperty('usuario');
				Titanium.App.Properties.removeProperty('email');
				Titanium.App.Properties.removeProperty('telefono');
				Titanium.App.Properties.removeProperty('rfc');
				Titanium.App.Properties.removeProperty('razon');
				inicio = require('/interface/inicio');
				winNav = new inicio();
				winNav.open();
				window.close();
			} else {
			}
		});

	});
    
    
    btnserviciohora1.addEventListener('click',function(){
    	if(txtserviciohora.value<12){
    		txtserviciohora.value=parseInt(txtserviciohora.value)+1;
    		Ti.App.Properties.setInt("txtserviciohora",txtserviciohora.value);
    	}    
    });
    btnserviciohora2.addEventListener('click',function(){
    	if(txtserviciohora.value>2){
    		txtserviciohora.value=parseInt(txtserviciohora.value)-1;
    		Ti.App.Properties.setInt("txtserviciohora",txtserviciohora.value);
    	}        
    });
    btnserviciohora.addEventListener('click',function(){
    	serviciohora.visible=false;
    });
    
    var destinoSeleccionado =0, origenSeleccionado = 0;
	
	lblHome.addEventListener('click',function(){
		mainwin.closeOpenView();
	});
	
	lblPagos.addEventListener('click',function(){
		if (Ti.App.Properties.hasProperty('id_usuario')) {
				tipopago.show();
		}else{
			if (Ti.App.Properties.hasProperty('idioma')) {
				Ti.UI.createAlertDialog({title:'Message',message:'Only users register'}).show();
			}else{
				Ti.UI.createAlertDialog({title:'Mensaje',message:'Solo usuarios registrados'}).show();
			}
		}
	});
	
	lblPromociones.addEventListener('click',function(){
		if (Ti.App.Properties.hasProperty('idioma')) {
			Ti.UI.createAlertDialog({title:'Message',message:'Not available'}).show();
		}else{
			Ti.UI.createAlertDialog({title:'Mensage',message:'No disponibles'}).show();
		}
	});
	
	lblMisPedidos.addEventListener('click',function(){
		if (Ti.App.Properties.hasProperty('id_usuario')) {
			pedidos = require('/interface/pedidos');
			winNav = new pedidos();
			winNav.open();
		}else{
			if (Ti.App.Properties.hasProperty('idioma')) {
				Ti.UI.createAlertDialog({title:'Message',message:'Only users register'}).show();
			}else{
				Ti.UI.createAlertDialog({title:'Mensaje',message:'Solo usuarios registrados'}).show();
			}
		}
	});
	
	lblMiPerfil.addEventListener('click',function(){
		if (Ti.App.Properties.hasProperty('id_usuario')) {
			perfil = require('/interface/perfil');
			winNav = new perfil();
			winNav.open();
		}else{
			if (Ti.App.Properties.hasProperty('idioma')) {
				Ti.UI.createAlertDialog({title:'Message',message:'Only users register'}).show();
			}else{
				Ti.UI.createAlertDialog({title:'Mensaje',message:'Solo usuarios registrados'}).show();
			}
		}
	});
	
	lblConfi.addEventListener('click',function(){
		if (Ti.App.Properties.hasProperty('id_usuario')) {
			dialogConfiguracion.show();
		}else{
			if (Ti.App.Properties.hasProperty('idioma')) {
				Ti.UI.createAlertDialog({title:'Message',message:'Only users register'}).show();
			}else{
				Ti.UI.createAlertDialog({title:'Mensaje',message:'Solo usuarios registrados'}).show();
			}
		}
	});
	
	navwin = createCenterNavWindow();
	
	function createCenterNavWindow(){
		tc = Ti.UI.createLabel({text:'SUBT',color:'#0c83c7'});
		window = Titanium.UI.createWindow({backgroundColor:'white',titleControl:tc});
		navwin = Titanium.UI.iOS.createNavigationWindow({window: window});
		
		menu = Ti.UI.createButton({backgroundImage:'/images/menuicon.png', left:10,zIndex:2,height:23,width:30});
		info = Ti.UI.createButton({backgroundImage:'/images/info.png', right:50,height:28,width:28,zIndex:5});
		player = Ti.Media.createSound({url:"/sound/sonido.mp3"});
		
		dialogConfiguracion.addEventListener('click', function(ed){
	   		if (ed.index === 0){
	   			mainwin.closeOpenView();
	   			ayuda = require('/interface/ayuda');
				winNav = new ayuda('gray','Ayuda', navwin);
				winNav.containingNav = navwin;
				navwin.openWindow(winNav);
	   		}else if (ed.index === 1){
	   		
	   		}
		});
		
		// webservices
		PagoPen= Titanium.Network.createHTTPClient();
		pagoReq2 = Titanium.Network.createHTTPClient();
		endServReq = Titanium.Network.createHTTPClient();
		bonoReq = Titanium.Network.createHTTPClient();
		rango = Titanium.Network.createHTTPClient();
		rango2 = Titanium.Network.createHTTPClient();
		xhrLocationCode = Ti.Network.createHTTPClient();
		xhrLocationCode.setTimeout(120000);
		pedirKm= Titanium.Network.createHTTPClient();
		registerServ = Titanium.Network.createHTTPClient();
		pagoReq = Titanium.Network.createHTTPClient();
		idPpal=Titanium.Network.createHTTPClient();
		idconeckta=Titanium.Network.createHTTPClient();
		tarifasAero= Titanium.Network.createHTTPClient();
		cotizarauto= Titanium.Network.createHTTPClient();
		recibo = Titanium.Network.createHTTPClient();
		disbloqueado= Titanium.Network.createHTTPClient();
		disbloqueado2= Titanium.Network.createHTTPClient();
		versionApp = Titanium.Network.createHTTPClient();
		versionApp2 = Titanium.Network.createHTTPClient();
		reciboReq = Titanium.Network.createHTTPClient();
			
		leftButtonOrigen = Titanium.UI.createButton({backgroundImage:'/images/green.png',width:30,height:30});
		leftButtonDestino = Titanium.UI.createButton({backgroundImage:'/images/red.png',width:30,height:30});
		txtOrigen = Titanium.UI.createTextField({borderStyle:Ti.UI.INPUT_BORDERSTYLE_ROUNDED,color:'black',hintText:"Ingrese su Origen",top:20,width:'90%',height:40,leftButton:leftButtonOrigen,leftButtonMode:Titanium.UI.INPUT_BUTTONMODE_ALWAYS,zIndex:3,editable:false});
		txtDestino = Titanium.UI.createTextField({borderStyle:Ti.UI.INPUT_BORDERSTYLE_ROUNDED,color:'black',hintText:"Ingrese su Destino",top:65,width:'90%',height:40,leftButton:leftButtonDestino,leftButtonMode:Titanium.UI.INPUT_BUTTONMODE_ALWAYS,zIndex:3, editable:false});
		btnPedir = Titanium.UI.createButton({width:'90%',height:60,title:'Pedir Automóvil',textAlign:'center',center:0,bottom:'10%',color:'white',zIndex:3,backgroundColor:'#0c83c7',borderRadius:10});
		
		mapview = Map.createView({mapType: Map.NORMAL_TYPE,animate:true,regionFit:true,userLocation:true,top:0});
		pinUsuario = Map.createAnnotation({title:"Mi ubicacion",image:'/images/pinOrigen.png'});
		pinDestino = Map.createAnnotation({title:"Mi destino",image:'/images/pinDestino.png'});
		
		btnLista = Titanium.UI.createButton({width:40,height:40,zIndex:3,backgroundImage:'/images/list.png'});
		autoCheck = Ti.UI.createSwitch({value:false,onTintColor:'#f9c1e5',tintColor:'#0080fc',right:'2%'});
    	listText= Ti.UI.createLabel({text:'Tipo de viaje',color:'black'});
  		autoRosa= Ti.UI.createLabel({text:'Tipo rosa',color:'black'});
  		flexSpace = Titanium.UI.createButton({systemButton:Titanium.UI.iPhone.SystemButton.FLEXIBLE_SPACE});
		toolbar = Titanium.UI.iOS.createToolbar({items:[btnLista,listText,flexSpace,autoRosa,autoCheck],bottom:0,borderTop:true,borderBottom:false,translucent:true});	
		
		//constantes
		container = Ti.UI.createView({layout : 'vertical',height : Ti.UI.SIZE,top:'18%'});
		autocomplete_table = Ti.UI.createTableView({height: Ti.UI.SIZE});
		
		
		// Variables
		latInicio =  Titanium.UI.createLabel({});
		lngInicio =Titanium.UI.createLabel({});
		latDestino =Titanium.UI.createLabel({});
		lngDestino =Titanium.UI.createLabel({});
		nombreTipoCarro=Ti.UI.createLabel({text:'Clasic'});
		var idTipoCarro,idOpcionServicio = 0,bdrBusqueda,banderaCompletar = 1;
		var servicioAeropuerto,idAeropuerto,numeroPasajero=0,numvuelo,horavuelo,taxiRosa = 0;
		Ti.App.Properties.setString("NMujer",0);var notification;
		
		// Alertas
		optsNumPasajero = {cancel: 2,options: ['1 a 3 personas(Sedan)','4 a 8 personas(Camioneta)','Cancelar'],selectedIndex: 2,destructive: 0,title: 'Numeros de pasajeros'};
		dialogNumPasajero = Ti.UI.createOptionDialog(optsNumPasajero);
		dialCalif = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'],message: 'Pago realizado correctamenta',title: 'Pago realizado'});
		dialogPagoPendiente = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Pagar'],message: 'Remember that you have a service pending for pay, amount : $' ,title: 'Message'});
		optsServicios = {cancel: 2,options: ['Classic', 'Premium', 'Cancelar','Aeropuerto'],selectedIndex: 2,destructive: 0,title: 'Elija un tipo de servicio'};
		dialogOpcionServicio = Ti.UI.createOptionDialog(optsServicios);
		dialogTipoViaje = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Sencillo','Por hora'],message: 'Elija una opción de viaje',title: 'Opción de viaje'});
		dialogConfirmarxhora = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Cancelar','Aceptar'],message: 'Estimado usuario le recordamos que el servicio por hora, tiene un costo de $150 pesos por hora, con 2 horas minimas de contratacion',title: "!Importante¡"});
		dialogNumeroVuelo = Ti.UI.createAlertDialog({title: 'Introduzca el número de vuelo',style:Ti.UI.iPhone.AlertDialogStyle.PLAIN_TEXT_INPUT,buttonNames: ['Entrar']});
		dialogHoraVuelo = Ti.UI.createAlertDialog({title: 'Introduzca la hora de vuelo',style:Ti.UI.iPhone.AlertDialogStyle.PLAIN_TEXT_INPUT,buttonNames: ['Entrar']});
		dialogAeroAviso = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'], message: 'Se teo'});
		dialogMensajeGeneral = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'], message: 'Se teo'});
		dialogNombre = Ti.UI.createAlertDialog({title: 'Introduce el nombre de la mujer y parentesco',style:Ti.UI.iPhone.AlertDialogStyle.PLAIN_TEXT_INPUT,buttonNames: ['Enviar']});
		tipopago = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Paypal','Tarjeta de credito/debito'],message: 'Seleccione el metodo de pago de su preferencia',title: 'Tipo de pago'});
		optsEditPagos = {cancel: 2,options: ['Editar', 'Mantener', 'Cancelar'],selectedIndex: 2,destructive: 0,title: 'Seleccione una opción'};
		dialogEditPagos = Ti.UI.createOptionDialog(optsEditPagos);
		dialogPadis = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'], message: 'Por el momento este metodo de pago no esta disponible'});
		
		//Firebase
		var firebaseReference = Firebase.new('https://subtgeofire.firebaseio.com/');
		var fredNameRef = firebaseReference.child('Aeropuerto/'+Ti.App.Properties.getInt("id_usuario"));
		var fredMGRef = firebaseReference.child('Usuarios/'+Ti.App.Properties.getInt("id_usuario"));
		
		fredNameRef.on('child_changed', function(childSnapshot) {
			if(childSnapshot.val().statusleido == 0 && childSnapshot.val().status_respuesta == 1){
				var mensaje = 'Se te ha asignado el siguiente chofer: '+ childSnapshot.val().nombrechofer + ' con un auto modelo: ' + childSnapshot.val().modelo + ', color: '+ childSnapshot.val().color +' y numero economico: ' + childSnapshot.val().numero;
				dialogAeroAviso.message = mensaje;
				Titanium.Media.audioSessionCategory = Ti.Media.AUDIO_SESSION_CATEGORY_PLAYBACK;
				notification = Ti.App.iOS.scheduleLocalNotification({
    					alertAction: "ver",
    					alertBody: dialogAeroAviso.message,
    					badge: 1,
    					date: new Date(new Date().getTime() + 3000),
    					sound: "/sound/sonido.mp3",
    					userInfo: {}
				});
				dialogAeroAviso.show();
				player.play();
			}
		});
		
		Titanium.Geolocation.getCurrentPosition(function(e){
			if (e.error){
				Ti.UI.createAlertDialog({title:'GPS',message:"Señal de gps perdida"}).show();
        		return;
    		}else{
    			pinUsuario.latitude = e.coords.latitude;
    			pinUsuario.longitude = e.coords.longitude;
    			pinDestino.latitude = e.coords.latitude + 0.0009999;
				pinDestino.longitude = e.coords.longitude -0.0009999;
				mapview.region = {latitude:e.coords.latitude, longitude:e.coords.longitude, latitudeDelta:0.009, longitudeDelta:0.009};
    			mapview.annotations = [pinUsuario,pinDestino];
    			latInicio.text = e.coords.latitude;
				lngInicio.text = e.coords.longitude;
				
				Titanium.Geolocation.reverseGeocoder(e.coords.latitude,e.coords.longitude,function(evt){
					if (evt.success) {
						var places = evt.places;
						if (places && places.length) {
							txtOrigen.value = places[0].address;
						}else{
							txtOrigen.value = "No address found";
						}
					}else{
						Ti.UI.createAlertDialog({title:'GPS',message:"Señal de gps perdida"}).show();
					}
				});
    		}
		});
		
		function omitirAcentos(text) {
			var acentos = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç";
			var original = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc";
			for (var i=0; i<acentos.length; i++) {
				text = text.replace(acentos.charAt(i), original.charAt(i));
			}
			return text;
		}
		
		txtDestino.addEventListener('click',function(){
			if (idOpcionServicio != 0) {
				txtDestino.focus();
			}else{
				if (Ti.App.Properties.hasProperty('idioma')) {
					Ti.UI.createAlertDialog({title:'Message',message:"Please select a type of trip"}).show();
				}else{
					Ti.UI.createAlertDialog({title:'Mensaje',message:"Por favor seleccione un tipo de viaje"}).show();
				}
			}
		});
		
		txtOrigen.addEventListener('click',function(){
			if (idOpcionServicio != 0) {
				txtOrigen.focus();
			}else{
				if (Ti.App.Properties.hasProperty('idioma')) {
					Ti.UI.createAlertDialog({title:'Message',message:"Please select a type of trip"}).show();
				}else{
					Ti.UI.createAlertDialog({title:'Mensaje',message:"Por favor seleccione un tipo de viaje"}).show();
				}
			}
		});
		
		txtDestino.addEventListener('change', function(e) {
			banderaCompletar = 1;
			if (txtDestino.value.length > 6 && txtDestino.value !=  last_search) {
				clearTimeout(timers['autocomplete']);
				timers['autocomplete'] = setTimeout(function() {
					last_search =txtDestino.value;
					auto_complete(txtDestino.value);
				}, 300);
			}
			return false;
		});
		
		txtOrigen.addEventListener('return', function(e){
  			txtOrigen.blur();
  			bdrBusqueda = 0;
  			requestUrl = "http://maps.google.com/maps/api/geocode/json?address=" + txtOrigen.value.replace(' ', '+');
			requestUrl += "&sensor=" + (Ti.Geolocation.locationServicesEnabled == true);
			xhrLocationCode.open("GET", requestUrl);
			xhrLocationCode.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
			xhrLocationCode.send();		
		});
		
		txtDestino.addEventListener('return', function(e){
  			txtDestino.blur();
  			bdrBusqueda = 1;
  			requestUrl = "http://maps.google.com/maps/api/geocode/json?address=" + omitirAcentos(txtDestino.value.replace(' ', '+'));
			requestUrl += "&sensor=" + (Ti.Geolocation.locationServicesEnabled == true);
			xhrLocationCode.open("GET", requestUrl);
			Ti.API.info(requestUrl);
			xhrLocationCode.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
			xhrLocationCode.send();	
		});
		
		mapview.addEventListener('regionchanged', function(e) {
			Titanium.Geolocation.reverseGeocoder(e.latitude, e.longitude, function(evt) {
				if (evt.success) {
					var places = evt.places;
					if (places && places.length) {
							txtDestino.value = places[0].address;
							destinoSeleccionado=1; origenSeleccionado=1;
							pinDestino.latitude =e.latitude;
							pinDestino.longitude = e.longitude;
							latDestino.text = e.latitude;
							lngDestino.text = e.longitude;
					} else {
						geocoderlbl.text = "Verifique su conexión a internet";
					}
				} else {
					Ti.UI.createAlertDialog({
						title : 'Conexión inestable',
						message : evt.error
					}).show();
				}
			});
			txtviajeDestino.text = 'Destino: ' + txtDestino.value;
		}); 
		
		xhrLocationCode.onerror = function(e) {};
		xhrLocationCode.onload = function(e) {
			var response = JSON.parse(this.responseText);
			if (response.status == 'OK' && response.results != undefined && response.results.length > 0) {
				if (bdrBusqueda == 0) {
					pinUsuario.latitude =response.results[0].geometry.location.lat;
					pinUsuario.longitude = response.results[0].geometry.location.lng;
					latInicio.text = response.results[0].geometry.location.lat;
					lngInicio.text = response.results[0].geometry.location.lng;
					mapview.region = {latitude:latInicio.text, longitude:lngInicio.text, latitudeDelta:0.03, longitudeDelta:0.03};	
					Titanium.Geolocation.reverseGeocoder(response.results[0].geometry.location.lat,response.results[0].geometry.location.lng,function(evt){
						if (evt.success) {
							var places = evt.places;
							if (places && places.length) {
								txtOrigen.value = places[0].address;
							}else{
								txtOrigen.value = "No address found";
							}
						}else{
							Ti.UI.createAlertDialog({title:'Reverse geo error',message:evt.error}).show();
						}
					});
				}else{
					destinoSeleccionado=1; origenSeleccionado=1;
					pinDestino.latitude =response.results[0].geometry.location.lat;
					pinDestino.longitude = response.results[0].geometry.location.lng;
					latDestino.text = response.results[0].geometry.location.lat;
					lngDestino.text = response.results[0].geometry.location.lng;
					mapview.region = {latitude:latDestino.text, longitude:lngDestino.text, latitudeDelta:0.03, longitudeDelta:0.03};	
				}
				if (txtDestino.value == "") {
					Ti.UI.createAlertDialog({title:'Mensaje',message:'Ingrese su destino por favor'}).show();
				}
			}	
		};
		
		rango.onload = function(){
			 json = this.responseText;
             response = JSON.parse(json);
             if (response.logged == true){
             	var RangoOrigen=response.status_origen;
            	var RangoDestino=response.status_destino;
            	if(RangoOrigen==1){
            		if (Ti.App.Properties.hasProperty('idioma')) {
            	 		var dialogAlertaRuta = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Accept'],message: 'Service not available in this area , please select another origin',title: 'Message'});
					}else{
        				var dialogAlertaRuta = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'],message: 'Servicio no disponible en esta área, favor de seleccionar otro origen',title: 'Mensaje'});
            	 	}
            	 	dialogAlertaRuta.show();
            	 	txtOrigen.focus();
            	}else if(RangoDestino==1){
            		if (Ti.App.Properties.hasProperty('idioma')) {
            	 		var dialogAlertaRuta = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Accept'],message: 'Service not available in this area , please select another destination',title: 'Message'});
					}else{
        				var dialogAlertaRuta = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'],message: 'Servicio no disponible en esta área, favor de seleccionar otro destino',title: 'Mensaje'});
            	 	}
            	 	dialogAlertaRuta.show();
            	 	txtDestino.focus();
            	}else{
            		if (idTipoCarro == 4) {
                    	pedirKm.open("POST", "http://subtmx.com/webservices/distanciag.php");
                        params = {
                        	lat1 : latInicio.text,
                            lng1 : lngInicio.text,
                            lat2 : latDestino.text,
                            lng2 : lngDestino.text
                      	};
                        pedirKm.send(params);
                 	}else{
                    	rango2.open("POST", "http://subtmx.com/webservices/phpgeo-master/tests/geo2.php");
                        params = {
                        	latitudo : latInicio.text,
                            longitudo : lngInicio.text,
                       	};
                        rango2.send(params);
                   	}  
            	}
             }
		};
		
		rango2.onload = function() {
			json = this.responseText;
			response = JSON.parse(json);
			Ti.API.info(response.logged);
			if (response.logged != null) {
				if (response.logged == true) {
					var RangoOrigen2 = response.status_origen;
					if (RangoOrigen2 == 1) {
						if (Ti.App.Properties.getInt('ingles') == 1) {
							var dialogAlertaRuta = Ti.UI.createAlertDialog({
								cancel : 1,
								buttonNames : ['Accept'],
								message : 'Type of service not available in this area , please select type airport',
								title : 'Message'
							});
						} else {
							var dialogAlertaRuta = Ti.UI.createAlertDialog({
								cancel : 1,
								buttonNames : ['Aceptar'],
								message : 'Tipo de servicio no disponible en esta área, favor de seleccionar tipo aeropuerto',
								title : 'Mensaje'
							});
						}
						dialogAlertaRuta.show();
					} else {
						pedirKm.open("POST", "http://subtmx.com/webservices/distanciag.php");
						params = {
							lat1 : latInicio.text,
							lng1 : lngInicio.text,
							lat2 : latDestino.text,
							lng2 : lngDestino.text
						};
						pedirKm.send(params);
					}
				} else {

				}
			} else {
				alert("Error de conexión, intente nuevamente");
			}
		};
		
		var tarifaFinal;
		pedirKm.onload = function(){
			json = this.responseText;
            response = JSON.parse(json);
            if (response.logged == true){
            	Ti.App.Properties.setString("distancias",response.km_total);
            	if (nombreTipoCarro.text == 'Aeropuerto'){
            		var kmformateado=Ti.App.Properties.getString("distancias").split(',');
		 			var kmWeb = kmformateado[0] + '.' + kmformateado[1];
		 			var dus= parseFloat(kmWeb);var lblTotal=Ti.UI.createLabel();
		 			var nuevoKMT;var newCosto;var numeroP;
		 			if(numeroPasajero < 4){
		 				numeroP = 0;
		 			}else{
		 				numeroP = 1;
		 			}
		 			tarifasAero.open("POST", "http://subtmx.com/webservices/costoAeropuerto.php");
					params = {
						num_pasajeros : numeroP
					};
					tarifasAero.send(params);
		 			
		 			
					tarifasAero.onload = function() {
						json = this.responseText;
						response = JSON.parse(json);
						if (response.logged == true) {
							var tarifa_inicial = response.tarifa_inicial;
							var km_inicial = response.km_inicial;
							var precio_km_adicional_1 = response.precio_km_adicional_1;
							var km_adicional_1 = response.km_adicional_1;
							var precio_km_adicional_2 = response.precio_km_adicional_2;
							var km_adicional_2 = response.km_adicional_2;
							var precio_km_adicional_3 = response.precio_km_adicional_3;
							var km_adicional_3 = response.km_adicional_3;
							var precio_km_adicional_4 = response.precio_km_adicional_4;
							var km_adicional_4 = response.km_adicional_4;
							var precio_km_adicional_5 = response.precio_km_adicional_5;
							var km_adicional_5 = response.km_adicional_5;

							if (dus > parseFloat(km_adicional_5)) {
								if (Ti.App.Properties.hasProperty('idioma')) {
									var dialogDus = Ti.UI.createAlertDialog({
										cancel : 1,
										buttonNames : ['Accept'],
										message : 'Service not available for this area',
										title : 'Check'
									});
								} else {
									var dialogDus = Ti.UI.createAlertDialog({
										cancel : 1,
										buttonNames : ['Acceptar'],
										message : 'Servicio no disponible para esa area',
										title : 'Verificación'
									});
								}
								dialogDus.show();
							} else {
								Ti.App.Properties.setDouble('dus', dus);
								var nuevoKMT;
								var newCosto;

								if (dus.toFixed(2) <= parseFloat(km_inicial)) {
									tarifaFinal = tarifa_inicial * 1;
									lblTotal.text = '$' + tarifaFinal.toFixed(2) + ' MXN';
								} else if (dus.toFixed(2) > parseFloat(km_inicial) && dus.toFixed(2) <= parseFloat(km_adicional_1)) {
									tarifaFinal = precio_km_adicional_1 * 1;
									lblTotal.text = '$' + tarifaFinal.toFixed(2) + ' MXN';

								} else if (dus.toFixed(2) > parseFloat(km_adicional_1) && dus.toFixed(2) <= parseFloat(km_adicional_2)) {
									tarifaFinal = precio_km_adicional_2 * 1;
									lblTotal.text = '$' + tarifaFinal.toFixed(2) + ' MXN';

								} else if (dus.toFixed(2) >= parseFloat(km_adicional_2) && dus.toFixed(2) <= parseFloat(km_adicional_3)) {

									tarifaFinal = precio_km_adicional_3 * 1;
									lblTotal.text = '$' + tarifaFinal.toFixed(2) + ' MXN';

								} else if (dus.toFixed(2) >= parseFloat(km_adicional_3) && dus.toFixed(2) <= parseFloat(km_adicional_4)) {

									tarifaFinal = precio_km_adicional_4 * 1;
									lblTotal.text = '$' + tarifaFinal.toFixed(2) + ' MXN';

								} else if (dus.toFixed(2) >= parseFloat(km_adicional_4) && dus.toFixed(2) <= parseFloat(km_adicional_5)) {

									tarifaFinal = precio_km_adicional_5 * 1;
									lblTotal.text = '$' + tarifaFinal.toFixed(2) + ' MXN';

								}

								Ti.App.Properties.setDouble("tarifaFinal", tarifaFinal);
								txtPrecio.text = lblTotal.text;
							}
							cotizarauto.open("POST", "http://subtmx.com/webservices/cotizarauto.php");
							params = {
								id_tipo_servicio : idTipoCarro
							};
							cotizarauto.send(params);
						} else {
							if (Ti.App.Properties.hasProperty('idioma')) {
								alert("Unable to process your request");
							} else {
								alert("No se pudo Realizar el pedido");
							}

						}
					};
            	}
            	if(idOpcionServicio==2){
            		cotizarauto.open("POST", "http://subtmx.com/webservices/cotizarauto.php");
					params = {
						id_tipo_servicio : idTipoCarro
					};
					cotizarauto.send(params);
            	}else{
            		if (Ti.App.Properties.getString("distancias") == '') {
						if (Ti.App.Properties.hasProperty('idioma')) {
							var dialogAlerta = Ti.UI.createAlertDialog({
								cancel : 1,
								buttonNames : ['Accept'],
								message : 'Sorry, we could not process your request, try you again',
								title : 'Message'
							});
						}else{
							var dialogAlerta = Ti.UI.createAlertDialog({
								cancel : 1,
								buttonNames : ['Aceptar'],
								message : 'Lo sentimos, no pudimos procesar su solicitud, intentelo de nuevo',
								title : 'Mensaje'
							});
						}
						dialogAlerta.show();
					}else {
						cotizarauto.open("POST", "http://subtmx.com/webservices/cotizarauto.php");
						params = {
							id_tipo_servicio : idTipoCarro
						};
						cotizarauto.send(params);
					}
            	}
            }
		};
		
		
		cotizarauto.onload = function() {
			json = this.responseText;
			response = JSON.parse(json);
			if (response.logged == true) {
				vistaCotizacion.visible = true;
				if (idTipoCarro == 4) {
				} else {
					var Rtipo_servicio = response.tipo_servicio;
					var newkmad,
					    newCosto;
					var kmformateado2 = Ti.App.Properties.getString("distancias").split(',');
					var kmWeb2 = kmformateado2[0] + '.' + kmformateado2[1];
					var dus2 = parseFloat(kmWeb2);
					var lblTotal = Ti.UI.createLabel();
					Ti.API.info(dus2);
					if (idOpcionServicio == 1) {
						if (dus2 > response.km_inicial) {
							newkmad = dus2 - response.km_inicial;
							newCosto = newkmad * response.precio_km_adicional;
							newCosto = parseFloat(newCosto) + parseFloat(response.tarifa_inicial);
							txtPrecio.text = '$' + parseFloat(newCosto).toFixed(2) + ' MXN';
						} else if (dus2.toFixed(2) <= response.km_inicial) {
							txtPrecio.text = '$' + response.tarifa_inicial + ' MXN';
						}

					} else if (idOpcionServicio == 2) {
						txtPrecio.text = '$' + response.tarifa_hora * parseInt(txtserviciohora.value) + ' MXN';
					}
				}

			}
		}; 

		
		pagoReq.onload = function(){
			json = this.responseText;
            response = JSON.parse(json);
            if (response.logged == true){
            	registerServ.open("POST", "http://subtmx.com//webservices/createservice.php");
            	params = {
                	id_tipo_servicio : idTipoCarro,
                	id_usuario : Ti.App.Properties.getInt("id_usuario"),
                	id_chofer : 16,
                	latitud_origen : latInicio.text,
                	longitud_origen : lngInicio.text,
                	latitud_destino : latDestino.text,
                	longitud_destino : lngDestino.text,
                	lugar_origen : 'Aeropuerto internacional de Acapulco',
                	opcion_servicio : 1,
                	tarifa_inicial : Ti.App.Properties.getDouble("tarifaFinal"),
                	id_tipo_pago : 1,
                	destino : txtDestino.value,
                	costo_total : Ti.App.Properties.getDouble("tarifaFinal")
            	};
            	registerServ.send(params);
            }else{
            	if (Ti.App.Properties.hasProperty('idioma')) {
            		alert ( "Unable to add the card");
            	}else{
            		alert("No se pudo Realizar el pedido 2");
            	}	
            }
		};
		
		registerServ.onload = function(){
			json = this.responseText;
    		response = JSON.parse(json);
    		if (response.logged== true){
    			
    			Ti.API.info('servicioAeropuerto' + response.folio_servicio);
    			servicioAeropuerto=response.folio_servicio;
  				idAeropuerto=response.id_servicio;
  				
  				var Folioformateado=servicioAeropuerto.split('-');
		 		var FolioNum = Folioformateado[1];
		 		var currentTime = new Date();
    			var hours = currentTime.getHours();
    			var minutes = currentTime.getMinutes();
    			var seconds = currentTime.getSeconds();
    			var month = currentTime.getMonth() + 1;
    			var day = currentTime.getDate();
    			var year = currentTime.getFullYear();
    			if (month < 10) { month = "0" + month;}    
    			if (day < 10) {day = "0" + day;}
    			if (hours < 10) { hours = "0" + hours;}    
    			if (minutes < 10) {minutes = "0" + minutes;}
    			if (seconds < 10) {seconds = "0" + seconds;}
				var fecha=day + "-" + month + "-" + year;
				
				var AeroRef = firebaseReference.child('Aeropuerto/'+Ti.App.Properties.getInt("id_usuario")+"/"+FolioNum);	
				AeroRef.update({ 
					sexo:Ti.App.Properties.getString("sexo"),
					nombre_cliente:Ti.App.Properties.getString("nombre_completo"),
					email:Ti.App.Properties.getString("email"),
					telefono:Ti.App.Properties.getString("telefono"),
					origen:'Aeropuerto internacional de Acapulco',
					destino:txtDestino.value,
					id_usuario:Ti.App.Properties.getInt("id_usuario"),
					num_pasajeros:numeroPasajero,
					status_respuesta:0,
					id_servicio:idAeropuerto,
					folio_servicio:servicioAeropuerto,
					servicio_cancelado:0,
					fecha_servicio:fecha,
					hora_servicio:hours+':'+minutes+':'+seconds,
					numero_de_vuelo:numvuelo,
					statusleido:0,
					hora_de_vuelo:horavuelo
				});
				if (Ti.App.Properties.hasProperty('idioma')) {
					alert('Payment Successful.The amount of the transaction was: $'+Ti.App.Properties.getDouble("tarifaFinal")+' MX');
				}else{
					alert('Estimado usuario,su transaccion fue realizada correctamente, su monto fue: $'+Ti.App.Properties.getDouble("tarifaFinal")+' MX,'+'le comentamos que en breve le llegará un correo electrónico con los datos del conductor');
            	}
            	
            	recibo.open("POST", "http://subtmx.com/webservices/recibo.php");
            	params = {
                	destino : txtDestino.value,
                	id_usuario : Ti.App.Properties.getInt('id_usuario'),
                	id_servicio : idAeropuerto,
            	};
            	recibo.send(params);
    		}
		};
		
		recibo.onload = function(){
           json = this.responseText;
           response = JSON.parse(json);
           if (response.logged == true){
               endServReq.open("POST","http://subtmx.com/webservices/endServicios.php");
                params = {
                	id_servicio: idAeropuerto,
                    km_adicional: 0,
                    costo_km_adicional:0,
                    hora_extra: "00:00:00",
                    costo_hora_extra: 0,
                    cargo_hora_pico: 0,
                    tiempo_espera_adicional:"00:00:00",
                    costo_espera_adicional: 0,
                    costo_total: Ti.App.Properties.getDouble("tarifaFinal"),
                    id_eventualidad:8,
                    costo_pendiente:0,
                    status_promocion:0
                };
           		endServReq.send(params);
           }    
     };
		
		autocomplete_table.addEventListener('click', function(e) {
			get_place(e.row.place_id);
		});
		
		function get_place(place_id) {
			var url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + place_id + "&location=16.8633600,-99.8901000&key=" + ANDROID_API_KEY;		
			var client = Ti.Network.createHTTPClient({
				onload : function(e) {
     				var obj = JSON.parse(this.responseText);
					var result = obj.result;
					container.place_id           = place_id;
					container.formatted_address  = result.formatted_address;
					container.latitude           = result.geometry.location.lat;
					container.longitude          = result.geometry.location.lng;
					if (banderaCompletar == 0) {
						txtOrigen.value                  = container.formatted_address;	
					}else{
						txtDestino.value                  = container.formatted_address;	
					}		
					table_data = [];
					autocomplete_table.setData(table_data);
					autocomplete_table.setHeight(0);
 				},	
 				onerror : function(e) {
					Ti.API.debug(e.error);
					alert('error');
				},
				timeout : 5000
			});
			client.open("GET", url);
			client.send();	
		}
		
		function get_locations(query) {
			var url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + query + "&location=16.8633600,-99.8901000&key=" + ANDROID_API_KEY;		
			var client = Ti.Network.createHTTPClient({
				onload : function(e) {
     				var obj = JSON.parse(this.responseText);
     				var predictions = obj.predictions;     		
     				table_data = [];            
            		autocomplete_table.removeAllChildren();
					autocomplete_table.setHeight(Ti.UI.SIZE);
            		for (var i = 0; i < predictions.length; i++) {
            			var desc = omitirAcentos(predictions[i].description);
            			var row = Ti.UI.createTableViewRow({
						height: 40,
						title: desc,
						place_id: predictions[i].place_id,
						hasDetail:false,
						postId:desc,            		
            			});                        	
            			table_data.push(row);     	
            		}    		 
            		autocomplete_table.setData(table_data);
 				},
 				onerror : function(e) {
					Ti.API.debug(e.error);
					alert('error');
				},
				timeout : 5000
			});
			client.open("GET", url);
			client.send();	
		}
		
		function auto_complete(search_term){
			if (search_term.length > 2) {
				get_locations(search_term);   	
   			}
		}
		
		btnLista.addEventListener('click',function(){
			if (Ti.App.Properties.hasProperty('coneckta') || Ti.App.Properties.hasProperty('paypal')) {
				if (Ti.App.Properties.hasProperty('id_usuario')) {
					if(pagar==1){
            	 		dialogPagoPendiente.show();
					}else{
						versionApp2.open("POST", "http://subtmx.com/webservices/version.php");
						params = {
							version :versionI,
							dispositivo : "ios"
						};
						versionApp2.send(params);
						/*disbloqueado2.open("POST", "http://subtmx.com/webservices/registrodispositivo.php");
						params = {
							user_id : Ti.App.Properties.getInt("id_usuario"),
							device_token:Titanium.Platform.id,
                			device_type:"ios"
						};
						disbloqueado2.send(params);*/
					}
				}else{
					if (Ti.App.Properties.hasProperty('idioma')) {
						Ti.UI.createAlertDialog({title:'Message',message:"Login to continue"}).show();
					}else{
						Ti.UI.createAlertDialog({title:'Mensaje',message:"Inicie sesión para continuar"}).show();
					}
				}
			}else{
				tipopago.show();
			}
		});
		
		versionApp2.onload = function() {
			json = this.responseText;
			response = JSON.parse(json);
			if (response.logged == true) {
				Ti.App.Properties.setInt("radiofirebase",response.radiofirebase);
				disbloqueado2.open("POST", "http://subtmx.com/webservices/registrodispositivo.php");
				params = {
					user_id : Ti.App.Properties.getInt("id_usuario"),
					device_token : Titanium.Platform.id,
					device_type : "ios"
				};
				disbloqueado2.send(params);
			} else {
				version.visible = true;
			}
		}; 

		
		disbloqueado2.onload = function() {
			json = this.responseText;
			response = JSON.parse(json);
			if (response.logged == true) {
				if (Ti.App.Properties.hasProperty('id_usuario')) {
					dialogOpcionServicio.show();
				}else{
					if(Ti.App.Properties.getInt('ingles')==1){
						alert('Only registered users');
					}else{
						alert('Solo usuarios registrados');
					}
	    	}
			}else {
				dispositivoBloqueado.visible=true;
			}
		};
		
		dialogOpcionServicio.addEventListener('click', function(ed){
			if (ed.index === 0){
				idTipoCarro=1;
				nombreTipoCarro.text="Clasic";
				txtAuto.text='Tipo de auto: Clasic';
				dialogTipoViaje.show();
				dialogConfirmarxhora.message = "Estimado usuario le recordamos que este servicio tiene una contratación minima de 2 horas hora.";
			/*}else if (ed.index === 1){
				idTipoCarro=2;
				nombreTipoCarro.text="Light";
				dialogTipoViaje.show();
				dialogConfirmarxhora.message = "Estimado usuario le recordamos que el servicio por hora, tiene un costo de $200 pesos por hora, con 2 horas minimas de contratacion";*/
			}else if (ed.index === 1){
				idTipoCarro=3;
				nombreTipoCarro.text="Premium";
				txtAuto.text='Tipo de auto: Premium';
				dialogTipoViaje.show();
				dialogConfirmarxhora.message = "Estimado usuario le recordamos que este servicio tiene una contratación minima de 2 horas hora.";
			}else if (ed.index === 3){
				latInicio.text = 16.762454;
				lngInicio.text = -99.754535;
				idTipoCarro=4;
				nombreTipoCarro.text="Aeropuerto";
				txtAuto.text='Tipo de auto: Aeropuerto';
				dialogNumPasajero.show();
				Titanium.Geolocation.reverseGeocoder(16.762454,-99.754535,function(evt){
					if (evt.success) {
						var places = evt.places;
						if (places && places.length) {
							txtOrigen.value=places[0].address;
							Ti.App.Properties.setDouble("latIN",latInicio.text);
        					Ti.App.Properties.setDouble("longIN",lngInicio.text);
						}else{
							txtOrigen.value = "Verifique su conexión a internet";
						}
						Ti.API.debug("reverse geolocation result = "+JSON.stringify(evt));
				 	}else{
						Ti.UI.createAlertDialog({
							title:'Conexión inestable',
							message:evt.error
						}).show();
				 	}
			 	});
			}
		});
		
		dialogTipoViaje.addEventListener('click', function(ed){
			if (ed.index === 0){
				idOpcionServicio=1;
				txtDestino.editable = true;
				txtDestino.focus();
				txtOrigen.editable = true;
				Ti.UI.createAlertDialog({title:'Mensaje',message:"Por favor ingrese su destino"}).show();
			}else if (ed.index === 1){
				txtserviciohora.value=2;
            	Ti.App.Properties.setInt("txtserviciohora",txtserviciohora.value);
				idOpcionServicio=2;
				destinoSeleccionado=1; origenSeleccionado=1;
				dialogConfirmarxhora.show();
				txtDestino.editable = false;
				txtOrigen.editable = false;
			}
		});
		
		dialogConfirmarxhora.addEventListener('click', function(ed){
			if (ed.index === 0){
               idOpcionServicio=0;
               txtDestino.value='';
               destinoSeleccionado = 0;
           }else if (ed.index === 1){
           		serviciohora.visible=true;
           }
     	});
		
		dialogNumPasajero.addEventListener('click', function(ed){
			if (ed.index === 0){
				numeroPasajero = 3;
			}else{
				numeroPasajero = 8;
			}
			dialogNumeroVuelo.show();
		});
		
		dialogNumeroVuelo.addEventListener('click', function(e){
			if (e.text == "") {
				dialogNumeroVuelo.show();
			}else{
				idOpcionServicio=3;
				numvuelo = e.text;
				dialogHoraVuelo.show();
			}
		});
		
		dialogHoraVuelo.addEventListener('click', function(e){
			if (e.text == "") {
				dialogHoraVuelo.show();
			}else{
				horavuelo = e.text;
				txtDestino.editable = true;
				Ti.UI.createAlertDialog({title:'Mensaje',message:"Por favor ingrese su destino"}).show();
			}
		});
		
		PagoPen.onload = function(){
			json = this.responseText;
            response = JSON.parse(json);
            if (response.logged == true){
            		dest=response.destino;hor=response.horario_precio;b=response.id_biling;
            		ids=response.id_servicio;desc=response.descripcion_viaje;costo=response.costo_total;
            		kma=response.km_adicional;ckma=response.costo_km_adicional;Ideve=response.id_eventualidad;
            		//costoTo = parseFloat(costo).toFixed(2) - (parseFloat(Ti.App.Properties.getInt("BonoActual"))) -Ti.App.Properties.getDouble("pagoInicial");
            		
            		if (costo <= 0) {
                    	costo = 0;
                	}
            		if(response.status_pago==1){
            			pagar = 1;
            			if (Ti.App.Properties.hasProperty('idioma')) {
            				dialogPagoPendiente.title = "Message";
            				dialogPagoPendiente.message = 'Remember that you have a service pending for pay, amount : $'+response.costo_total;
						}else{
							dialogPagoPendiente.title = "Aviso";
							dialogPagoPendiente.message = 'Le recordamos que tiene un servicio pendiente por pagar, monto: $'+response.costo_total;
						}
            	 		dialogPagoPendiente.show();
            		}else{
            			
            		}
            }
		};
		
		dialogPagoPendiente.addEventListener('click',function(e){
        		if(e.index==0){
        			if (Ti.App.Properties.hasProperty('coneckta')) {
        					pagoReq2.open("POST","http://subtmx.com/webservices/php-conekta-master/conekta_card.php");
            	 			params = {
                    			id_usuario: Ti.App.Properties.getInt('id_usuario'),
                    			id_servicio:ids,
                    			descripcion_viaje: desc,
                    			costo_total:parseFloat(costo).toFixed(2)
             				};
                			pagoReq2.send(params);
        			}else if (Ti.App.Properties.hasProperty('paypal')){
        					pagoReq2.open("POST","http://subtmx.com/webservices/paypal/samples/BillingAgreements/DoReferenceTransaction.php");
            	 			params = {
                				destino:dest,
                    			horario_precio:hor,
                    			id_biling:b,
                    			name: Ti.App.Properties.getString('nombre'),
                    			lastname: Ti.App.Properties.getString('apellido'),
                    			id_usuario: Ti.App.Properties.getInt('id_usuario'),
                    			id_servicio:ids,
                    			descripcion_viaje: desc,
                    			costo_total:parseFloat(costo).toFixed(2)
             				};
                			pagoReq2.send(params);
        			}
        			
            	}
     	});
     	
     	pagoReq2.onerror = function(){
     		pagar = 1;
     		Ti.UI.createAlertDialog({title:'Internet',message:"Sin conexion a internet"}).show();
     	};
     	pagoReq2.onload = function(){
			json = this.responseText;
            response = JSON.parse(json);
            if (response.logged == true){
            	reciboReq.open("POST","http://subtmx.com/webservices/recibo.php");
                    params4 = {
                    	id_servicio: ids,
                        id_usuario: Ti.App.Properties.getInt('id_usuario'),
                        destino:dest
                    };
       			reciboReq.send(params4);
            		endServReq.open("POST","http://subtmx.com/webservices/endServicios.php");
                	params = {
                		id_servicio: ids,
                     km_adicional: kma,
                     costo_km_adicional:ckma,
                     hora_extra: "00:00:00",
                     costo_hora_extra: 0,
                     cargo_hora_pico: 0,
                     tiempo_espera_adicional:"00:00:00",
                     costo_espera_adicional: 0,
                     costo_total: parseFloat(costo).toFixed(2),
                     id_eventualidad:Ideve,
                     costo_pendiente:0,
                     status_promocion:0
                };
           		endServReq.send(params);
           		Ti.App.Properties.setDouble("pagoInicial",0);
              	Ti.App.Properties.setDouble("BonoActual",0);
           		pagar=0;
           		if (Ti.App.Properties.hasProperty('idioma')) {
           			dialCalif.message= 'Payment Successful.The amount of the transaction was: $'+response.costo_total+' MX';
				}else{
					dialCalif.message = 'Pago realizado correctamente, el monto de la transaccion fue: $'+response.costo_total+' MX';
            		}
            		dialCalif.show();
            }else{
            		if (Ti.App.Properties.hasProperty('idioma')) {
            			dialCalif.message= 'Could not make the payment';
            			dialCalif.title='Pending payment';
		       	}else{
            			dialCalif.message="No se pudo realizar el pago";
            			dialCalif.title='Pago pendiente';	
            		}
            		dialCalif.show();
            }
		};
		
		
		btnPedir.addEventListener('click', function() {
			if (idOpcionServicio != 0) {
			}else{
				if (Ti.App.Properties.hasProperty('idioma')) {
					Ti.UI.createAlertDialog({title:'Message',message:"Please select a type of trip"}).show();
				}else{
					Ti.UI.createAlertDialog({title:'Mensaje',message:"Por favor seleccione un tipo de viaje"}).show();
				}
			}
			if (destinoSeleccionado == 0 || origenSeleccionado == 0) {
				
			} else {
				if (Ti.App.Properties.hasProperty('coneckta') || Ti.App.Properties.hasProperty('paypal')) {
					txtviajeOrigen.text = 'Origen: ' + txtOrigen.value;
					txtviajeDestino.text = 'Destino: ' + txtDestino.value;
					rango.open("POST", "http://subtmx.com/webservices/phpgeo-master/tests/geo.php");
					params = {
						latitudo : latInicio.text,
						longitudo : lngInicio.text,
						latitudd : latDestino.text,
						longitudd : lngDestino.text,
					};
					rango.send(params);
				}else{
					tipopago.show();
				}
			}
		}); 

		
		menu.addEventListener('click',function(){
			mainwin.toggleLeftView();
			mainwin.setCenterhiddenInteractivity("TouchEnabled");
		});
		
		Ti.App.iOS.registerUserNotificationSettings({
	    	types: [
            	Ti.App.iOS.USER_NOTIFICATION_TYPE_ALERT,
            	Ti.App.iOS.USER_NOTIFICATION_TYPE_SOUND,
            	Ti.App.iOS.USER_NOTIFICATION_TYPE_BADGE
        	]
    	});
    	
    	Ti.App.iOS.addEventListener('notification', function(e) {
    		if (e.badge > 0) {
        		Ti.App.iOS.scheduleLocalNotification({
            		date: new Date(new Date().getTime()),
            		badge: -1
        		});
    		}
		});
    	
    	info.addEventListener('click',function(){
			var dialog = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'],message: 'Has recibido un descuento de $50 pesos por realizar tu primer viaje con nosotros',title: '!Felicidades¡'});
			if (Ti.App.Properties.hasProperty('idioma')) {
				dialog.buttonNames = ['Accept'];
				dialog.message = 'Received a discount of $ 50 MXN by  make your first trip with us';
				dialog.title = 'Congratulations';
			}
			dialog.show();
		});
		
		autoCheck.addEventListener('change',function(e){
			if (autoCheck.value == true) {
				if (Ti.App.Properties.getString('sexo')=='femenino') {

				}else{
					dialogNombre.show();
				}
				taxiRosa=1;
			}else if(autoCheck.value == false){
				taxiRosa=0;
			}
		});
		
		dialogNombre.addEventListener('click',function(e){
			if (e.text == "") {
				autoCheck.value = false;		
			}else{
				Ti.App.Properties.setString("NMujer",e.text);
			}
		});
		
		btnCotizar2.addEventListener('click', function() {
            vistaCotizacion.visible=false;
    	});
    	
    	btnCotizar.addEventListener('click', function() {
        if (idTipoCarro == 4) {
				if (Ti.App.Properties.hasProperty("paypal")) {
					pagoReq.open("POST", "http://subtmx.com/webservices/paypal/samples/BillingAgreements/DoReferenceAeropuerto.php");
					params = {
						id_biling : Ti.App.Properties.getString("id_biling"),
						name : Ti.App.Properties.getString('nombre'),
						lastname : Ti.App.Properties.getString('apellido'),
						descripcion_viaje : 'Tipo de viaje es: ' + nombreTipoCarro.text + ', Km recorridos: ' + Ti.App.Properties.getDouble('dus'),
						costo_total : tarifaFinal
					};
					pagoReq.send(params);
				} else if (Ti.App.Properties.hasProperty("coneckta")) {
					pagoReq.open("POST", "http://subtmx.com/webservices/php-conekta-master/conekta_card_aeropuerto.php");
					params = {
						id_usuario : Ti.App.Properties.getInt('id_usuario'),
						costo_total : tarifaFinal,
						descripcion_viaje : 'Tipo de viaje es: ' + nombreTipoCarro + ', Km recorridos: ' + Ti.App.Properties.getString('kmt')
					};
					pagoReq.send(params);
				}
        } else {
            numeroPasajero = 0;
            Ti.App.Properties.setInt("vistaActiva",0);
            chofer= require('/interface/chofer');
        	new chofer(latInicio.text,lngInicio.text,latDestino.text,lngDestino.text,txtOrigen.value,idOpcionServicio,taxiRosa,idTipoCarro,nombreTipoCarro.text,numeroPasajero,txtDestino.value).open();
        	window.close();
        }
        vistaCotizacion.visible = false;
    });
    
     var dispositivoBloqueado= Ti.UI.createView({width:'100%', height:'100%', visible:false,zIndex: 20,backgroundColor:'#D9727374'});
     var vistamensajedispositivoBloqueado= Ti.UI.createView({width:'80%', height:Ti.UI.SIZE,zIndex: 20,backgroundColor:'white',borderColor:'black',borderRadius:6,layout:'vertical'});
   	 var TitulomensajedispositivoBloqueado=Titanium.UI.createLabel({text:'NOTIFICACIÓN',top:20,color:'black',font:{fontSize:18,fontFamily: 'Menlo Bold Italic'}});
   	 var txtdispositivoBloqueado=Titanium.UI.createLabel({text:"Tu dispositivo fue bloqueado.\n Consulte a soporte técnico:\n soporte@subtmx.com",top:20,color:'#2980b9',font:{fontSize: 15,fontFamily: 'Menlo Bold Italic'},textAlign:'center',width:'80%'});
   	 var btnmensajedispositivoBloqueado = Titanium.UI.createButton({title:'Aceptar',center:0,backgroundColor:'blue',top:10});
     var vistamensajedispositivoBloqueado2= Ti.UI.createView({width:'100%', height:30,layout:'horizontal',top:0});
     btnmensajedispositivoBloqueado.addEventListener('click',function(){
    	Titanium.App.Properties.removeProperty('coneckta');
		Titanium.App.Properties.removeProperty('paypal');
		Titanium.App.Properties.removeProperty('id_usuario');
		Titanium.App.Properties.removeProperty('usuario');
		Titanium.App.Properties.removeProperty('email');
		Titanium.App.Properties.removeProperty('telefono');
		Titanium.App.Properties.removeProperty('rfc');
		Titanium.App.Properties.removeProperty('razon');
    	inicio = require('/interface/inicio');
		new inicio().open();
		window.close();
    });
		
		window.addEventListener('open', function(e) {
			Titanium.App.Properties.removeProperty('re');
			if (Ti.App.Properties.hasProperty('idioma')) {
				txtOrigen.hintText = 'Enter your Origin';
				txtDestino.hintText = 'Enter your destination';
				btnPedir.title = 'Request a car';
				listText.text = 'Type of trip';
				autoRosa.text = 'Pink type';
				optsServicios.options = ['Classic', 'Light', 'Cancel','Premium','Airport'];
				optsServicios.title = 'Choose a type of service';
				lblMisPedidos.text = 'My Books';
				lblMiPerfil.text = 'Profile';
				lblPromociones.text = 'Promotions';
				lblPagos.text = 'Payment';
				dialogTipoViaje.buttonNames = ['Simple','Per hour'];
				dialogTipoViaje.message = 'Choose a travel option';
				dialogTipoViaje.title = 'Type of trip';
			};
			if (Ti.App.Properties.hasProperty('id_usuario')) {
				versionApp.open("POST", "http://subtmx.com/webservices/version.php");
				params = {
					version :versionI,
					dispositivo : "ios"
				};
				versionApp.send(params);
				/*disbloqueado.open("POST", "http://subtmx.com/webservices/registrodispositivo.php");
				params = {
					user_id : Ti.App.Properties.getInt("id_usuario"),
					device_token:Titanium.Platform.id,
                	device_type:"ios"
				};
				disbloqueado.send(params);*/
			};
		});
		
		versionApp.onload = function() {
			json = this.responseText;
			response = JSON.parse(json);
			if (response.logged == true) {
				Ti.App.Properties.setInt("radiofirebase",response.radiofirebase);
				disbloqueado.open("POST", "http://subtmx.com/webservices/registrodispositivo.php");
				params = {
					user_id : Ti.App.Properties.getInt("id_usuario"),
					device_token:Titanium.Platform.id,
                	device_type:"ios"
				};
				disbloqueado.send(params);
			} else {
				version.visible=true;
			}
		};
		
		
		
		disbloqueado.onload = function() {
			json = this.responseText;
			response = JSON.parse(json);
			if (response.logged == true) {
				if (Ti.App.Properties.hasProperty('coneckta')){
					idconeckta.open("POST", "http://subtmx.com/webservices/conektausuario.php");
					params = {
						id_usuario : Ti.App.Properties.getInt("id_usuario")
					};
					idconeckta.send(params);
				}
				if (Ti.App.Properties.hasProperty('coneckta') || Ti.App.Properties.hasProperty('paypal')) {
					
				}else{
					tipopago.show();
				}
				
				bonoReq.open("POST","http://subtmx.com/webservices/bono.php");
        		params = {
        			id_usuario: Ti.App.Properties.getInt("id_usuario")
        		};
        		bonoReq.send(params);
				PagoPen.open("POST","http://subtmx.com/webservices/statuspago.php");
        		params = {
        			id_usuario:Ti.App.Properties.getInt("id_usuario")
        		};
       			PagoPen.send(params);
			}else{
				dispositivoBloqueado.visible=true;
			}
		}; 
		
		disbloqueado.onerror = function(){
     		Ti.UI.createAlertDialog({title:'Actualización',message:"Favor de actualizar la aplicación"}).show();
     	};
		
		tipopago.addEventListener('click', function(e) {
			if (e.index == 0) {
				Titanium.App.Properties.removeProperty('coneckta');
				if (Ti.App.Properties.hasProperty('paypal')) {
					dialogEditPagos.show();
				}else{
					idPpal.open("POST","http://subtmx.com/webservices/bilingusuario.php");
        			params = {
        				id_usuario: Ti.App.Properties.getInt("id_usuario")
        			};
       				idPpal.send(params);	
				}
			}else{
				Titanium.App.Properties.removeProperty('paypal');
				if (Ti.App.Properties.hasProperty('coneckta')) {
					dialogEditPagos.show();
				}else{
					idconeckta.open("POST","http://subtmx.com/webservices/conektausuario.php");
        			params = {
        				id_usuario: Ti.App.Properties.getInt("id_usuario")
        			};
       				idconeckta.send(params);	
				}
			}
		});
		
		dialogEditPagos.addEventListener('click', function(ed){
			if (ed.index == 0){
				if (Ti.App.Properties.hasProperty('paypal')) {
					paypal= require('/interface/paypal');
        			new paypal().open();
        			window.close();
				}else if (Ti.App.Properties.hasProperty('coneckta')){
					paypal= require('/interface/pago');
        			new paypal().open();
        			window.close();
				}
			}else if (ed.index == 1){
				
			}
		});
		
		idPpal.onload = function(){
            json = this.responseText;
            response = JSON.parse(json);
            if (response.logged == true){
            	Ti.App.Properties.setString("id_biling",response.id_biling);
            	dialogEditPagos.show();
           }else{
           		paypal= require('/interface/paypal');
        		new paypal().open();
        		window.close();
           }
    	};
    	
    	idconeckta.onload = function(){
            json = this.responseText;
            response = JSON.parse(json);
            if (response.logged == true){
            	Ti.App.Properties.setString("coneckta",'coneckta');
           }else{
           		paypal= require('/interface/pago');
        		new paypal().open();
        		window.close();
           }
    	};
    	
    	idPpal.onerror = function(){
     		Ti.UI.createAlertDialog({title:'Internet',message:"Sin conexion a internet"}).show();
     	};
     	
     	idconeckta.onerror = function(){
     		Ti.UI.createAlertDialog({title:'Internet',message:"Sin conexion a internet"}).show();
     	};
		
		bonoReq.onload = function(){
    		json = this.responseText;
    		response = JSON.parse(json);
    		if (response.logged == true){
    			Ti.App.Properties.setInt("bono", 0);
    		}else{
    			window.rightNavButtons = [info];
    			Ti.App.Properties.setInt("bono", 50);
    		}
		};
			
		window.containingNav = navwin;
		window.setLeftNavButton(menu);
		container.add(autocomplete_table);
		window.add(btnPedir);
		window.add(txtOrigen);
		window.add(txtDestino);
		window.add(mapview);
		window.add(toolbar);
		window.add(container);
		vistaBotones.add(btnCotizar);
      	vistaBotones.add(btnCotizar2);
      	vistatxt.add(txtCotizar);
      	vistatxt.add(txtCosto);
      	vistatxt.add(txtPrecio);
      	vistatxt.add(txtAuto);
      	vistatxt.add(txtviajeOrigen);
      	vistatxt.add(txtviajeDestino);
      	vistatxt.add(txtmensaje);
      	vistaCotizar.add(vistatxt);
      	vistaCotizar.add(vistaBotones);
      	vistaCotizar.add(vistaBotones2);
     	vistaCotizacion.add(vistaCotizar);
     	window.add(vistaCotizacion);
     	window.add(version);
     	
     	dispositivoBloqueado.add(vistamensajedispositivoBloqueado);
    	vistamensajedispositivoBloqueado.add(TitulomensajedispositivoBloqueado);
    	vistamensajedispositivoBloqueado.add(txtdispositivoBloqueado);
    	vistamensajedispositivoBloqueado.add(btnmensajedispositivoBloqueado);
    	vistamensajedispositivoBloqueado.add(vistamensajedispositivoBloqueado2);
    	window.add(dispositivoBloqueado);
    	window.add(serviciohora);
     	
		return navwin;
	};
	
	mainwin = NappSlideMenu.createSlideMenuWindow({
		centerWindow: navwin,
		leftWindow:winLeft,
		statusBarStyle: NappSlideMenu.STATUSBAR_WHITE,
		leftLedge:100
	});
	
	winLeft.add(lblVersion);
	winLeft.add(logoLeft);
	winLeft.add(lblHome);
	winLeft.add(lblMisPedidos);
	winLeft.add(lblMiPerfil);
	winLeft.add(lblPromociones);
	winLeft.add(lblPagos);
	winLeft.add(lblConfi);
	winLeft.add(lblNombreCompleto);
	
	return mainwin;
	
};
module.exports = mapa;