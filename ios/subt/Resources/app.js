if(Ti.version < 2.1){
	alert('Lo sentimos esta aplicacion requiere una version 2.1 o superior');
}
(function() {
	if (Ti.App.Properties.hasProperty('id_usuario')) {
		if (Ti.App.Properties.hasProperty('viajeactivo')) {
			inicio = require('interface/chofer');
		}else{
			inicio = require('interface/mapa');	
		}
	}else{
			inicio = require('interface/inicio');
	}
	if (Ti.App.Properties.hasProperty('viajeactivo')) {
		inicio = new inicio(Ti.App.Properties.getString('latInicio'),Ti.App.Properties.getString('lngInicio'),Ti.App.Properties.getString('latDestino'),Ti.App.Properties.getString('lngDestino'),Ti.App.Properties.getString('geocoderlbl'),Ti.App.Properties.getString('idOpcionServicio'),Ti.App.Properties.getString('taxiRosa'),Ti.App.Properties.getString('idTipoCarro'),Ti.App.Properties.getString('nombreTipoCarro'),Ti.App.Properties.getString('numeroPasajero'),Ti.App.Properties.getString('geoDestino'));
	}else{
		inicio = new inicio();
	}
	inicio.open();	
})();